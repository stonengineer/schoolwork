# Lab 3 Problem 2
# Joseph Stone
# Feb 8, 2017

.data 0x10008000
	in_mesg: .asciiz "Please enter a number: "
	fill_mesg: .asciiz " has "
	fin_mesg: .asciiz " 1-bits when converted to binary."

.text
main:
	la $a0, in_mesg       # load effective address of mesg to $a0
	addi $v0, $0, 4       # load constant 4 to register $v0
	syscall               # switch into the kernel to print mesg out

	addi $v0, $0, 5       # reg $v0 = 5
	syscall               # gain keyboard input from user

	add $s0, $v0, $0      # copy the value of $v0 to $s0

	li  $v0, 1            # load print integer into $v0
	add $a0, $s0, $zero   # load number to convert to print
	syscall               # print number to console

	add $s1, $zero, 1

	max_loop:
		bgt $s1, $s0, max_end
		sll $s1, $s1, 1
		j max_loop

max_end:
	add $s2, $zero, $zero
	conv_loop:
		beq $s0, $zero, conv_end
		srl $s1, $s1, 1
		small_if: bgt $s1, $s0, conv_loop
		sub $s0, $s0, $s1
		add $s2, $s2, 1
		j conv_loop

conv_end:
	la $a0, fill_mesg     # load effective address of mesg to $a0
	addi $v0, $0, 4       # load constant 4 to register $v0
	syscall               # switch into the kernel to print mesg out

	li  $v0, 1            # load print integer into $v0
	add $a0, $s2, $zero   # load number to convert to print
	syscall               # print number to console

	la $a0, fin_mesg      # load effective address of mesg to $a0
	addi $v0, $0, 4       # load constant 4 to register $v0
	syscall               # switch into the kernel to print mesg out

	j final

final:
	addi $v0, $0, 10      # 'exit MIPS program' syscall
	syscall