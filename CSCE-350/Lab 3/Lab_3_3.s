# Lab 3 Problem 3
# Joseph Stone
# Feb 10, 2017

.data 0x10008000
B:	.word 722		# B[0]
	.word 21 		# B[1]
	.word 4 		# B[2]
	.word 89		# B[3]
	.word 16384		# B[4]
	.word 350		# B[5]
	.word 6046		# B[6]
	.word 897		# B[7]
	.word 1201		# B[8]
	.word 0			# B[9]
	.word 904		# B[10]
	.word 897		# B[11]
	.word 4805		# B[12]
	.word 679		# B[13]
	.word 7			# B[14]
SZ:	.word 15

.text
main:
	la $t7, SZ                           # load location of size variable to $t7
	la $t6, B                            # load location of B array to $t6

	lw $t0, 0($t7)                       # set $t0 to hold list size
	add $t1, $zero, $zero                # initialize the outer loop counting variable

	outer_loop:
		bgt $t1, $t0, exit
		add $t2, $t1, 1                  # initialize the inner loop counting variable
		inner_loop:
			bgt $t2, $t0, inner_iter

			sll $t3, $t1, 2
			sub $t4, $t2, $t1
			sll $t5, $t4, 2

			add $t6, $t6, $t3
			lw $s0, 0($t6)
			add $t6, $t6, $t5
			lw $s1, 0($t6)

			bgt $s0, $s1, flip

			add $t2, $t2, 1
			sub $t6, $t6, $t5
			sub $t6, $t6, $t3
			j inner_loop

	inner_iter:
		add $t1, $t1, 1
		j outer_loop

	flip:
		sw $s0, 0($t6)
		sub $t6, $t6, $t5
		sw $s1, 0($t6)
		sub $t6, $t6, $t3
		add $t2, $t2, 1
		j inner_loop

	exit:
		addi $v0, $0, 10      # 'exit MIPS program' syscall
		syscall