# Lab 3 Problem 1
# Joseph Stone
# Feb 8, 2017

.data 0x10008000          # data degment start (assembler directive)
	not_two: .asciiz " is not a power of two."
	yes_two: .asciiz " is a power of two."
	i: .word 256

.text                     # code segment start (assembler directive)
main:
	la $t7, i             # load the address of B[0] (&(B[0]) == B) into register $t1
	lw $t1, 0($t7)        # load value of B[0] into register $t0

	li  $v0, 1            # load print integer into $v0
	add $a0, $t1, $zero   # load number to convert to print
	syscall               # print number to console

	add $t0, $zero, 2     # load 2 to $t0 to begin power function

	loop:                 # begin loop
		ble $t1, $t0, end # if $t1 <= t0 then exit
		sll $t0, $t0, 1   # shift $t0 left 1 bit (multiply by 2)
		j loop            # back to top of loop till we're done
	end:
		bne $t0, $t1, no  # if $t0 != $t1, i was not a power of two
		beq $t1, $t1, yes # if $t0 != $t1, i was a power of two

no:
	la $a0, not_two       # load effective address of not_two to $a0
	addi $v0, $0, 4       # load constant 4 to register $v0
	syscall               # switch into the kernel to print mesg out
	j final               # jump to the exit function

yes:
	la $a0, yes_two       # load effective address of mesg to $a0
	addi $v0, $0, 4       # load constant 4 to register $v0
	syscall               # switch into the kernel to print mesg out
	j final               # jump to the exit function

final:
	addi $v0, $0, 10      # 'exit MIPS program' syscall
	syscall