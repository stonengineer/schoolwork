//RegisterFile module

module RegisterFile (Reg1, Reg2, Data, Instr1, Instr2, Memaddr, RegWrite, CLK, Reset_L);

	input [31:0] Data;
	input [4:0] Instr1, Instr2, Memaddr;
	input RegWrite, CLK, Reset_L;
	output [31:0] Reg1, Reg2;

	reg [31:0] registers [0:31];
	reg [6:0] i, p = 0;

	initial begin
		while(i < 32) begin
			registers[i] = 0;
			i = i + 1;
		end
		i = 0;
	end

	always @(Reset_L) begin
		while(i < 32) begin
			registers[i] = 0;
			i = i + 1;
		end
	end

	assign Reg1 = registers[Instr1];
	assign Reg2 = registers[Instr2];

	always @(negedge CLK) begin
		if(RegWrite) begin
			registers[Memaddr] = Data;
		end
	end

endmodule