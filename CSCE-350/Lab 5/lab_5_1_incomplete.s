#counts length of a string
.data
.data
string: .asciiz "CSCE350"

printedMessage: .asciiz "The length of the string: "

.text
main:
  	la $a0, string             # Load address of string.
        jal strlen              # Call strlen procedure.
        jal print
        addi $a1, $a0, 0        # Move address of string to $a1
        addi $v1, $v0, 0        # Move length of string to $v1
        addi $v0, $0, 11        # System call code for message.
        la $a0, printedMessage            # Address of message.
        syscall
        addi $v0, $0, 10        # System call code for exit.
        syscall

/**** wirte your code here ****/

print:
	li $v0, 4
  	la $a0, printedMessage
  	syscall

  	li $v0, 1
  	move $a0, $t0
  	syscall

jr $ra
