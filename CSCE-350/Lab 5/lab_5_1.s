# Lab 5 Problem 1
# Joseph Stone
# Feb 24, 2017

#counts length of a string
.data
.data
string: .asciiz "CSCE350"

printedMessage: .asciiz "The length of the string: "

.text
main:
	la $a0, string             # Load address of string.
	jal strlen              # Call strlen procedure.
	jal print
	addi $a1, $a0, 0        # Move address of string to $a1
	addi $v1, $v0, 0        # Move length of string to $v1
	addi $v0, $0, 11        # System call code for message.
	la $a0, printedMessage            # Address of message.
	syscall
	addi $v0, $0, 10        # System call code for exit.
	syscall

strlen:
	lbu $s0, 0($a0)
	add $t0, $zero, $zero
	len_loop:
		beq $s0, $zero, len_exit
		add $t0, $t0, 1
		add $a0, $a0, 1
		lbu $s0, 0($a0)
		j len_loop
	len_exit:
		jr $ra

print:
	li $v0, 4
	la $a0, printedMessage
	syscall

	li $v0, 1
	move $a0, $t0
	syscall

jr $ra
