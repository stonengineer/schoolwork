# Lab 5 Problem 2
# Joseph Stone
# Feb 24, 2017

.data
	buffer: .space 10
	query_user: .asciiz "Please enter an integer (max 10-digits): "
	out_user: .asciiz "\nString integer converted to integer type: "

.text
main:
	li $v0, 4
	la $a0, query_user
	syscall

	li $v0, 8
	la $a0, buffer
	li $a1, 11
	syscall

	jal atoi

	li $v0, 4
	la $a0, out_user
	syscall

	li $v0, 1
	move $a0, $t0
	syscall

	li $v0, 10
	syscall

atoi:
	lbu $s0, 0($a0)
	add $t0, $zero, $zero
	conv_loop:
		beq $s0, $zero, exit_loop
		blt $s0, 48, exit_loop
		bgt $s0, 57, exit_loop
		addi $s0, $s0, -48
		mul $t0, $t0, 10
		add $t0, $t0, $s0
		add $a0, $a0, 1
		lbu $s0, 0($a0)
		j conv_loop
	exit_loop:
		jr $ra