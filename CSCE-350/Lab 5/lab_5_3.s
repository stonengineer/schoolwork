# Lab 5 Problem 3
# Joseph Stone
# Feb 24, 2017

.data
	input_buffer: .space 100
	reversed_buffer: .space 100
	query_user: .asciiz "Please enter a string: "
	out_user: .asciiz "\nReversed string: "

.text
main:
	li $v0, 4
	la $a0, query_user
	syscall

	li $v0, 8
	la $a0, input_buffer
	la $a1, 100
	syscall

	add $t0, $zero, -1
	move $s1, $a0
	la $s0, reversed_buffer
	jal reverse

	li $v0, 4
	la $a0, out_user
	syscall

	li $v0, 4
	la $a0, reversed_buffer
	syscall

	li $v0, 10
	syscall

reverse:
	beq $t0, -1, go_to_end
	reverse_copy_loop:
		beq $t0, $zero, exit_loop
		lbu $s2, 0($s1)
		sb $s2, 0($s0)
		add $s0, $s0, 1
		add $s1, $s1, -1
		add $t0, $t0, -1
		j reverse_copy_loop
	exit_loop:
		jr $ra

go_to_end:
	lbu $s2, 0($s1)
	beq $s2, $zero, yay_at_end
	add $t0, $t0, 1
	add $s1, $s1, 1
	j go_to_end
yay_at_end:
	add $t0, $t0, 1
	add $s1, $s1, -2
	j reverse