module DFF_PC(Q, Qbar, C, D, PREbar, CLRbar, Wen);
	input D, C, PREbar, CLRbar, Wen;
	output Q, Qbar;
	
	/* Completed this module */
	/* Use D-latch module designed in Part 2 and extend it */
	wire not_pre, not_clr, c_bar;
	wire n1, n2, n3, n4, n5, n6, n7, n8;
	
	not not_1(not_pre, PREbar);
	not not_2(not_clr, CLRbar);
	not not_3(c_bar, C);
	
	nand nand_1(n1, Qbar, D, C);
	nand nand_2(n2, Wen, Q, C);
	nand nand_3(n3, not_pre, n1, n4);
	nand nand_4(n4, n3, n2, not_clr);
	nand nand_5(n5, n3, c_bar);
	nand nand_6(n6, c_bar, n4);
	nand nand_7(Q, n5, Qbar);
	nand nand_8(Qbar, n6, Q);
	
endmodule

module m555(clock);
    parameter InitDelay = 10, Ton = 50, Toff = 50;
    output clock;
    reg clock;

    initial begin
        #InitDelay clock = 1;
    end

    always begin
        #Ton clock = ~clock;
        #Toff clock = ~clock;
    end
endmodule

module testD(q, qbar, clock, data, PREbar, CLRbar, Wen);
    input  q, qbar, clock;
    output data, PREbar, CLRbar, Wen;
    reg    data, PREbar, CLRbar, Wen;

    initial begin
        $monitor ($time, " q = %d, qbar = %d, clock = %d, data = %d Wen = %d PREbar = %d CLRbar = %d", q, qbar,  clock, data, Wen, PREbar, CLRbar);
        data = 0; Wen = 1; PREbar = 1; CLRbar = 1;
        #25  
        data = 1;
        #100 
        
        data = 0;
        #50 
        data = 1;
        #50 
        data = 0;
        #100 
        data = 1;
        #50 
        data = 0;
        #50 
        data = 1;
        #100
        
        Wen = 0;
        
        data = 0;
        #50 
        data = 1;
        #50 
        data = 0;
        #100 
        data = 1;
        #50 
        data = 0;
        #50 
        data = 1;
        #100
        
        Wen = 1;
        
        data = 0;
        #50 
        data = 1;
        #50 
        data = 0;
        #100 
        data = 1;
        #50 
        data = 0;
        #50 
        data = 1;
        #100
        
        CLRbar = 0;
        
        data = 0;
        #50 
        data = 1;
        #50 
        data = 0;
        #100 
        data = 1;
        #50 
        data = 0;
        #50 
        data = 1;
        #100
        
        CLRbar = 1;
        
        PREbar = 0;
        
        data = 0;
        #50 
        data = 1;
        #50 
        data = 0;
        #100 
        data = 1;
        #50 
        data = 0;
        #50 
        data = 1;
        #100
        
        PREbar = 1;
        
        data = 0;
        #50 
        data = 1;
        #50 
        data = 0;
        #100 
        data = 1;
        #50 
        data = 0;
        #50 
        data = 1;
        #100
	
        $finish;
    end
endmodule

module testBenchD;
    wire clock, q, qbar, data, PREbar, CLRbar, Wen;
    m555 clk(clock);
    DFF_PC dff(q, qbar, clock, data, PREbar, CLRbar, Wen);
    testD td(q, qbar, clock, data, PREbar, CLRbar, Wen);
endmodule