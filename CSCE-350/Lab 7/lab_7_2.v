module m555(clock);
    parameter InitDelay = 10, Ton = 50, Toff = 50;
    output clock;
    reg clock;
 
    initial begin
        #InitDelay clock = 1;
    end
 
    always begin
        #Ton clock = ~clock;
        #Toff clock = ~clock;
    end
endmodule

module d_latch(q, q_bar, clock, data);
    input clock, data;
    output q, q_bar;

    wire n1, n2;
    wire d_bar;

    not d_not(d_bar, data);

    nand nand_1(n1, data, clock);
    nand nand_2(n2, d_bar, clock);
    nand nand_3(q_bar, q, n2);
    nand #1 nand_4(q, q_bar, n1);
endmodule

module testD(q, qbar, clock, data);
    input  q, qbar, clock;
    output data;
    reg    data;
 
    initial begin
        $monitor ($time, " q = %d, qbar = %d, clock = %d, data = %d", q, qbar, clock, data);
        data = 0;
        #25  
        data = 1;
        #100 
        data = 0;
        #50 
        data = 1;
        #50 
        data = 0;
        #100 
        data = 1;
        #50 
        data = 0;
        #50 
        data = 1;
        #100
        $finish; /* $finish simulation after 100 time simulation units */
    end
endmodule
 
module testBenchD;
    wire clock, q, qbar, data;
    m555 clk(clock);
    d_latch dl(q, qbar, clock, data);
    testD td(q, qbar, clock, data);
endmodule