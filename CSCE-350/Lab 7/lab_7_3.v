`include "lab_7_2.v"

module DFF(q, q_bar, clock, data);
	input clock, data;
	output q, q_bar;
	wire c_bar, m, m_bar;

	not not_1(c_bar, clock);

    d_latch master(m, m_bar, clock, data);
	d_latch slave(q, q_bar, c_bar, m);
endmodule

module testD3(q, qbar, clock, data);
    input  q, qbar, clock;
    output data;
    reg    data;
 
    initial begin
        $monitor ($time, " q = %d, qbar = %d, clock = %d, data = %d", q, qbar, clock, data);
        data = 0;
        #25  
        data = 1;
        #100 
        data = 0;
        #50 
        data = 1;
        #50 
        data = 0;
        #100 
        data = 1;
        #50 
        data = 0;
        #50 
        data = 1;
        #100
        $finish; /* $finish simulation after 100 time simulation units */
    end
endmodule
 
module testBenchD3;
    wire clock, q, qbar, data;
    m555 clk(clock);
    DFF dff(q, qbar, clock, data);
    testD3 td(q, qbar, clock, data);
endmodule