
module DFF32_PC(Q, Qbar, C, D, PREbar, CLRbar, Wen);
	input  [31:0] D;
	input  C, PREbar, CLRbar, Wen;
	output [31:0] Q, Qbar;
	
	/* Complete this module with 1-bit DFF_PC developed in Part 4. */
endmodule

module m555(clock);
    parameter InitDelay = 10, Ton = 50, Toff = 50;
    output clock;
    reg clock;

    initial begin
        #InitDelay clock = 1;
    end

    always begin
        #Ton clock = ~clock;
        #Toff clock = ~clock;
    end
endmodule

module testD32(q, qbar, clock, data, PREbar, CLRbar, Wen);
    input  clock;
    input  [31:0] q, qbar;
    output PREbar, CLRbar, Wen;
    output [31:0] data;
    reg    PREbar, CLRbar, Wen;
    reg    [31:0] data;

    initial begin
        $monitor ($time, " q = %x, qbar = %x, clock = %d, data = %x Wen = %d PREbar = %d CLRbar = %d", q, qbar,  clock, data, Wen, PREbar, CLRbar);
        data = 0; Wen = 1; PREbar = 1; CLRbar = 1;
        #25  
        data = -1;
        #100 
        
        data = 1815681630;
        #50 
        data = 4168106685;
        #50 
        data = 1815681630;
        #100 
        data = 4168106685;
        #50 
        data = 1815681630;
        #50 
        data = 4168106685;
        #100
        
        Wen = 0;
        
        data = 1815681630;
        #50 
        data = 4168106685;
        #50 
        data = 1815681630;
        #100 
        data = 4168106685;
        #50 
        data = 1815681630;
        #50 
        data = 4168106685;
        #100
        
        Wen = 1;
        
        data = 4089469290;
        #50 
        data = 2336693550;
        #50 
        data = 4089469290;
        #100 
        data = 2336693550;
        #50 
        data = 4089469290;
        #50 
        data = 2336693550;
        #100
        
        CLRbar = 0;
        
        data = 1815681630;
        #50 
        data = 4168106685;
        #50 
        data = 1815681630;
        #100 
        data = 4168106685;
        #50 
        data = 1815681630;
        #50 
        data = 4168106685;
        #100
        
        CLRbar = 1;
        
        PREbar = 0;
        
        data = 1815681630;
        #50 
        data = 4168106685;
        #50 
        data = 1815681630;
        #100 
        data = 4168106685;
        #50 
        data = 1815681630;
        #50 
        data = 4168106685;
        #100
        
        PREbar = 1;
        
        data = 1815681630;
        #50 
        data = 4168106685;
        #50 
        data = 1815681630;
        #100 
        data = 4168106685;
        #50 
        data = 1815681630;
        #50 
        data = 4168106685;
        #100
        
        $finish; 
    end
endmodule

module testBenchD;
    wire clock, PREbar, CLRbar, Wen;
    wire [31:0] data, q, qbar;
    
    m555 clk(clock);
    DFF32_PC dff(q, qbar, clock, data, PREbar, CLRbar, Wen);
    testD32 td(q, qbar, clock, data, PREbar, CLRbar, Wen);
endmodule