`include "lab_7_4.v"

module DFF32_PC(Q, Qbar, C, D, PREbar, CLRbar, Wen);
	input  [31:0] D;
	input  C, PREbar, CLRbar, Wen;
	output [31:0] Q, Qbar;
	
	/* Complete this module with 1-bit DFF_PC developed in Part 4. */
	DFF_PC bit_0 (Q[0], Qbar[0], C, D[0], PREbar, CLRbar, Wen);
	DFF_PC bit_1 (Q[1], Qbar[1], C, D[1], PREbar, CLRbar, Wen);
	DFF_PC bit_2 (Q[2], Qbar[2], C, D[2], PREbar, CLRbar, Wen);
	DFF_PC bit_3 (Q[3], Qbar[3], C, D[3], PREbar, CLRbar, Wen);
	DFF_PC bit_4 (Q[4], Qbar[4], C, D[4], PREbar, CLRbar, Wen);
	DFF_PC bit_5 (Q[5], Qbar[5], C, D[5], PREbar, CLRbar, Wen);
	DFF_PC bit_6 (Q[6], Qbar[6], C, D[6], PREbar, CLRbar, Wen);
	DFF_PC bit_7 (Q[7], Qbar[7], C, D[7], PREbar, CLRbar, Wen);
	DFF_PC bit_8 (Q[8], Qbar[8], C, D[8], PREbar, CLRbar, Wen);
	DFF_PC bit_9 (Q[9], Qbar[9], C, D[9], PREbar, CLRbar, Wen);
	DFF_PC bit_10 (Q[10], Qbar[10], C, D[10], PREbar, CLRbar, Wen);
	DFF_PC bit_11 (Q[11], Qbar[11], C, D[11], PREbar, CLRbar, Wen);
	DFF_PC bit_12 (Q[12], Qbar[12], C, D[12], PREbar, CLRbar, Wen);
	DFF_PC bit_13 (Q[13], Qbar[13], C, D[13], PREbar, CLRbar, Wen);
	DFF_PC bit_14 (Q[14], Qbar[14], C, D[14], PREbar, CLRbar, Wen);
	DFF_PC bit_15 (Q[15], Qbar[15], C, D[15], PREbar, CLRbar, Wen);
	DFF_PC bit_16 (Q[16], Qbar[16], C, D[16], PREbar, CLRbar, Wen);
	DFF_PC bit_17 (Q[17], Qbar[17], C, D[17], PREbar, CLRbar, Wen);
	DFF_PC bit_18 (Q[18], Qbar[18], C, D[18], PREbar, CLRbar, Wen);
	DFF_PC bit_19 (Q[19], Qbar[19], C, D[19], PREbar, CLRbar, Wen);
	DFF_PC bit_20 (Q[20], Qbar[20], C, D[20], PREbar, CLRbar, Wen);
	DFF_PC bit_21 (Q[21], Qbar[21], C, D[21], PREbar, CLRbar, Wen);
	DFF_PC bit_22 (Q[22], Qbar[22], C, D[22], PREbar, CLRbar, Wen);
	DFF_PC bit_23 (Q[23], Qbar[23], C, D[23], PREbar, CLRbar, Wen);
	DFF_PC bit_24 (Q[24], Qbar[24], C, D[24], PREbar, CLRbar, Wen);
	DFF_PC bit_25 (Q[25], Qbar[25], C, D[25], PREbar, CLRbar, Wen);
	DFF_PC bit_26 (Q[26], Qbar[26], C, D[26], PREbar, CLRbar, Wen);
	DFF_PC bit_27 (Q[27], Qbar[27], C, D[27], PREbar, CLRbar, Wen);
	DFF_PC bit_28 (Q[28], Qbar[28], C, D[28], PREbar, CLRbar, Wen);
	DFF_PC bit_29 (Q[29], Qbar[29], C, D[29], PREbar, CLRbar, Wen);
	DFF_PC bit_30 (Q[30], Qbar[30], C, D[30], PREbar, CLRbar, Wen);
	DFF_PC bit_31 (Q[31], Qbar[31], C, D[31], PREbar, CLRbar, Wen);

endmodule

module testD32(q, qbar, clock, data, PREbar, CLRbar, Wen);
    input  clock;
    input  [31:0] q, qbar;
    output PREbar, CLRbar, Wen;
    output [31:0] data;
    reg    PREbar, CLRbar, Wen;
    reg    [31:0] data;

    initial begin
        $monitor ($time, " q = %x, qbar = %x, clock = %d, data = %x Wen = %d PREbar = %d CLRbar = %d", q, qbar,  clock, data, Wen, PREbar, CLRbar);
        data = 0; Wen = 1; PREbar = 1; CLRbar = 1;
        #25  
        data = -1;
        #100 
        
        data = 1815681630;
        #50 
        data = 4168106685;
        #50 
        data = 1815681630;
        #100 
        data = 4168106685;
        #50 
        data = 1815681630;
        #50 
        data = 4168106685;
        #100
        
        Wen = 0;
        
        data = 1815681630;
        #50 
        data = 4168106685;
        #50 
        data = 1815681630;
        #100 
        data = 4168106685;
        #50 
        data = 1815681630;
        #50 
        data = 4168106685;
        #100
        
        Wen = 1;
        
        data = 4089469290;
        #50 
        data = 2336693550;
        #50 
        data = 4089469290;
        #100 
        data = 2336693550;
        #50 
        data = 4089469290;
        #50 
        data = 2336693550;
        #100
        
        CLRbar = 0;
        
        data = 1815681630;
        #50 
        data = 4168106685;
        #50 
        data = 1815681630;
        #100 
        data = 4168106685;
        #50 
        data = 1815681630;
        #50 
        data = 4168106685;
        #100
        
        CLRbar = 1;
        
        PREbar = 0;
        
        data = 1815681630;
        #50 
        data = 4168106685;
        #50 
        data = 1815681630;
        #100 
        data = 4168106685;
        #50 
        data = 1815681630;
        #50 
        data = 4168106685;
        #100
        
        PREbar = 1;
        
        data = 1815681630;
        #50 
        data = 4168106685;
        #50 
        data = 1815681630;
        #100 
        data = 4168106685;
        #50 
        data = 1815681630;
        #50 
        data = 4168106685;
        #100
        
        $finish; 
    end
endmodule

module testBenchD5;
    wire clock, PREbar, CLRbar, Wen;
    wire [31:0] data, q, qbar;
    
    m555 clk(clock);
    DFF32_PC dff(q, qbar, clock, data, PREbar, CLRbar, Wen);
    testD32 td(q, qbar, clock, data, PREbar, CLRbar, Wen);
endmodule