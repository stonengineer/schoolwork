//ControlUnit module

// instruction opcode
//R-Type (Opcode 000000)
`define R_TYPE         6'b000000     //supported
//I-Type (All opcodes except 000000, 00001x, and 0100xx)
`define OPCODE_ADDI    6'b001000     //supported
`define OPCODE_ADDIU   6'b001001     //supported
`define OPCODE_ANDI    6'b001100     //supported
`define OPCODE_BEQ     6'b000100     //supported
`define OPCODE_BNE     6'b000101     //supported
`define OPCODE_ORI     6'b001101     //supported
`define OPCODE_XORI    6'b001110     //supported
`define OPCODE_NOP     6'b110110     //supported
`define OPCODE_SLTI    6'b001010     //supported
`define OPCODE_SLTIU   6'b001011     //supported
`define OPCODE_LW      6'b100011     //supported
`define OPCODE_SW      6'b101011     //supported
// J-Type (Opcode 00001x)
`define OPCODE_J       6'b000010     //supported

module ControlUnit ( RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, Jump, SignExtend, ALUOp, Instr );

	input [5:0] Instr;
	output reg RegDst, ALUSrc, MemtoReg, RegWrite, MemRead, MemWrite, Branch, Jump, SignExtend;
	output reg [3:0] ALUOp;

	reg [5:0] Instr_handles_X;

	always @(Instr) begin
		//$display("Instruction - %d", Instr);
		Instr_handles_X = Instr;
		case (Instr_handles_X)
			`R_TYPE: begin
				RegDst = 1;
				ALUSrc = 0;
				MemtoReg = 0;
				RegWrite = 1;
				MemRead = 0;
				MemWrite = 0;
				Branch = 0;
				Jump = 0;
				SignExtend = 1'bX;
				ALUOp = 4'b1111;
			end
			`OPCODE_ADDI: begin
				RegDst = 0;
				ALUSrc = 1;
				MemtoReg = 0;
				RegWrite = 1;
				MemRead = 0;
				MemWrite = 0;
				Branch = 0;
				Jump = 0;
				SignExtend = 1;
				ALUOp = 4'b0111;
			end
			`OPCODE_ADDIU: begin
				RegDst = 0;
				ALUSrc = 1;
				MemtoReg = 0;
				RegWrite = 1;
				MemRead = 0;
				MemWrite = 0;
				Branch = 0;
				Jump = 0;
				SignExtend = 1;
				ALUOp = 4'b0001;
			end
			`OPCODE_LW: begin
				RegDst = 0;
				ALUSrc = 1;
				MemtoReg = 1;
				RegWrite = 1;
				MemRead = 1;
				MemWrite = 0;
				Branch = 0;
				Jump = 0;
				SignExtend = 1;
				ALUOp = 4'b0111;
			end
			`OPCODE_SW: begin
				RegDst = 1'bX;
				ALUSrc = 1;
				MemtoReg = 1'bX;
				RegWrite = 0;
				MemRead = 0;
				MemWrite = 1;
				Branch = 0;
				Jump = 0;
				SignExtend = 1;
				ALUOp = 4'b0111;
			end
			`OPCODE_BEQ: begin
				RegDst = 1'bX;
				ALUSrc = 0;
				MemtoReg = 1'bX;
				RegWrite = 0;
				MemRead = 0;
				MemWrite = 0;
				Branch = 1;
				Jump = 0;
				SignExtend = 1'bX;
				ALUOp = 4'b0010;
			end
			`OPCODE_BNE: begin
				RegDst = 1'bX;
				ALUSrc = 0;
				MemtoReg = 1'bX;
				RegWrite = 0;
				MemRead = 0;
				MemWrite = 0;
				Branch = 1;
				Jump = 0;
				SignExtend = 1'bX;
				ALUOp = 4'b1001;
			end
			`OPCODE_J: begin
				RegDst = 1'bX;
				ALUSrc = 0;
				MemtoReg = 1'bX;
				RegWrite = 0;
				MemRead = 0;
				MemWrite = 0;
				Branch = 0;
				Jump = 1;
				SignExtend = 1'bX;
				ALUOp = 4'bX;
			end
			`OPCODE_ORI: begin
				RegDst = 0;
				ALUSrc = 1;
				MemtoReg = 0;
				RegWrite = 1;
				MemRead = 0;
				MemWrite = 0;
				Branch = 0;
				Jump = 0;
				SignExtend = 0;
				ALUOp = 4'b0101;
			end
			`OPCODE_ANDI: begin
				RegDst = 0;
				ALUSrc = 1;
				MemtoReg = 0;
				RegWrite = 1;
				MemRead = 0;
				MemWrite = 0;
				Branch = 0;
				Jump = 0;
				SignExtend = 0;
				ALUOp = 4'b0100;
			end
			`OPCODE_SLTI: begin
				RegDst = 0;
				ALUSrc = 1;
				MemtoReg = 0;
				RegWrite = 1;
				MemRead = 0;
				MemWrite = 0;
				Branch = 0;
				Jump = 0;
				SignExtend = 1;
				ALUOp = 4'b1010;
			end
			`OPCODE_SLTIU: begin
				RegDst = 0;
				ALUSrc = 1;
				MemtoReg = 0;
				RegWrite = 1;
				MemRead = 0;
				MemWrite = 0;
				Branch = 0;
				Jump = 0;
				SignExtend = 1;
				ALUOp = 4'b1011;
			end
			`OPCODE_XORI: begin
				RegDst = 0;
				ALUSrc = 1;
				MemtoReg = 0;
				RegWrite = 1;
				MemRead = 0;
				MemWrite = 0;
				Branch = 0;
				Jump = 0;
				SignExtend = 0;
				ALUOp = 4'b0110;
			end
			default: $display("Error in Control Unit - Cant process %d instruction", Instr);
		endcase
	end

endmodule