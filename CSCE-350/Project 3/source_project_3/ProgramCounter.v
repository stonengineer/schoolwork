//ProgramCounter module

module ProgramCounter(PC, PC_IN, startPC, Reset_L, CLK, Jump, Branch, Instr);
	
	input [31:0] PC_IN, startPC;
	input Reset_L, CLK, Jump, Branch;
	input [15:0] Instr;
	output reg [31:0] PC;

	always @(negedge Reset_L) begin
		PC = startPC;
		//$display("Reset L - %d", PC); //log resets
	end

	always @(posedge CLK) begin
		/*if(Jump) begin
			PC = Instr;
		end
		else if(Branch) begin
			PC = PC_IN + 4 + Instr;
		end*/
		if(Reset_L) begin
			PC = PC_IN + 4;
			//$display("Increment PC - %d", PC); //log pc
		end
	end

endmodule 