//RegisterFile module

module RegisterFile (Reg1, Reg2, Data, Instr1, Instr2, Memaddr, RegWrite, CLK, Reset_L);

	input [31:0] Data;
	input [4:0] Instr1, Instr2, Memaddr;
	input RegWrite, CLK, Reset_L;
	output [31:0] Reg1, Reg2;

	reg [31:0] registers [0:31];
	reg [6:0] i, p, n = 0;

	initial begin
		while(i < 32) begin
			registers[i] = 0;
			i = i + 1;
		end
		i = 0;
	end

	always @(Reset_L) begin
		while(i < 32) begin
			registers[i] = 0;
			i = i + 1;
		end
	end

	assign Reg1 = registers[Instr1];
	assign Reg2 = registers[Instr2];

	always @(negedge CLK) begin
		if(RegWrite) begin
			registers[Memaddr] = Data;
		end
	end

	always@(Data) begin
		$display("Registers at instruction %d", n);
		while(p < 16) begin
			$display("Register %d: %d | Register %d: %d | Register %d: %d | Register %d: %d", p, registers[p], (p+1), registers[p+1], (p+2), registers[p+2], (p+3), registers[p+3]);
			p = p + 4;
		end
		$display(""); //newline
		p = 0;
		n = n + 1;
	end

endmodule