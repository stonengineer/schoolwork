# Lab 4 Problem 1
# Joseph Stone
# Feb 17, 2016

	.data
msg1:   .asciiz  "The sum is "
NL:     .asciiz  "\n"
A:      .word    3
B:      .word    10
C:      .word    20

	.text
	.globl  main            # main is a global symbol

main:
	## Do pre-function call preparations (if any).

	## COMPLETE HERE
	## Set first arg as A.
	## Set second arg as B.
	## Set third arg as C.
	la $a1, A
	lw $s0, 0($a1)
	la $a1, B
	lw $s1, 0($a1)
	la $a1, C
	lw $s2, 0($a1)

	## COMPLETE HERE
	## Call Sum function.
	jal Sum

	## Do post-function call processing (if any).


	li       $v0, 4
	la       $a0, msg1
	syscall                 # print "The sum is "

	li       $v0, 1
	## FIX move instruction
	move     $a0, $a1       # move sum to $a0
	syscall                 # print the sum

	li       $v0, 4
	la       $a0, NL
	syscall                 # print newline

	li       $v0, 10
	syscall                 # exit

Sum:
	## COMPLETE HERE
	## compute A+B+C.
	add $a1, $s0, $s1
	add $a1, $a1, $s2
	jr $ra