# Lab 4 Problem 3
# Joseph Stone
# Feb 17, 2016

.data
	msg_input: .asciiz "Please enter a number: "
	msg_output: .asciiz "\nCorresponding Fibonacci: "

.text
main:
	la $a0, msg_input
	addi $v0, $0, 4
	syscall # prints original message

	addi $v0, $0, 5
	syscall

	add $s0, $v0, $zero
	add $s7, $zero, $zero

	jal Fibonacci
	move $s7, $t0

	li $v0, 4
	la $a0, msg_output
	syscall

	li $v0, 1
	add $a0, $s7, $zero
	syscall

	addi $v0, $0, 10
	syscall

Fibonacci:
	bge $s0, 2, fib_recursion
	move $t0, $s0
	jr $ra

	fib_recursion:
		addi $sp, $sp, -12
		sw $ra, 0($sp)
		sw $s0, 4($sp)

		addi $s0, $s0, -1
		jal Fibonacci
		sw $t0, 8($sp)

		lw $s0, 4($sp)
		addi $s0, $s0, -2
		jal Fibonacci

		lw $t1, 8($sp)
		add $t0, $t0, $t1
		lw $ra, 0($sp)
		addi $sp, $sp, 12
		jr $ra