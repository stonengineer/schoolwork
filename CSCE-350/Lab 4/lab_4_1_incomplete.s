        .data
msg1:   .asciiz  "The sum is "
NL:     .asciiz  "\n"
A:      .word    3
B:      .word    10
C:      .word    20

        .text
        .globl  main            # main is a global symbol

main:
        ## Do pre-function call preparations (if any).

        ## COMPLETE HERE
        ## Set first arg as A.
        ## Set second arg as B.
        ## Set third arg as C.

        ## COMPLETE HERE
        ## Call Sum function.

        ## Do post-function call processing (if any).


        li       $v0, 4
        la       $a0, msg1
        syscall                 # print "The sum is "

        li       $v0, 1
        ## FIX move instruction
        move     $a0, $????     # move sum to $a0
        syscall                 # print the sum

        li       $v0, 4
        la       $a0, NL
        syscall                 # print newline

        li       $v0, 10
        syscall                 # exit

Sum:
        ## COMPLETE HERE
        ## compute A+B+C.