# Lab 4 Problem 2
# Joseph Stone
# Feb 17, 2016

.data
	.align   2                              # Request alignment on word (4 byte) boundary

	msg_1: .asciiz "Original Matrix:\n"
	msg_2: .asciiz "Transposed Matrix:\n"
	msg_nl: .asciiz "\n"
	msg_s: .asciiz " "

	## 4x4 Matrix Declaration ##
	A0:      .word 41, 42, 43, 44           # Declare and initialize 1st Row of A
	A1:      .word 55, 56, 57, 58           # Declare and initialize 2nd Row of A
	A2:      .word 19, 10, 11, 12           # Declare and initialize 3rd Row of A
	A3:      .word 23, 24, 25, 26           # Declare and initialize 4th Row of A
	A:       .word A0, A1, A2, A3           # Declare and initialize the pointer to the rows

	NUMROWS_A:       .word   4              # the number of rows
	NUMCOLS_A:       .word   4              # the number of columns

	## 4x4 Matrix Declaration ##
	T0:      .word  0,  0,  0,  0           # Declare and initialize 1st Row of T
	T1:      .word  0,  0,  0,  0           # Declare and initialize 2nd Row of T
	T2:      .word  0,  0,  0,  0           # Declare and initialize 3rd Row of T
	T3:      .word  0,  0,  0,  0           # Declare and initialize 4th Row of T
	T:       .word T0, T1, T2, T3           # Declare and initialize the pointer to the rows

	NUMROWS_T:       .word   4              # the number of rows
	NUMCOLS_T:       .word   4              # the number of columns

.text
main:
	li $v0, 4
	la $a0, msg_1
	syscall # prints original message

	add $t7, $zero, $zero
	add $t6, $zero, $zero

	la $a0, NUMROWS_A
	lw $a1, ($a0)
	la $a0, NUMROWS_A
	lw $a2, ($a0)

	print_a_outer:
		bge $t7, $a1, done_print_a
		print_a_inner:
			bge $t6, $a2, done_print_a_inner
			la $t1, A
			sll $t2, $t7, 2       # Shift left twice (same as i * 4)
			add $t2, $t2, $t1     # Address of pointer A[i]
			lw $t3, ($t2)         # Get address of an array A[i] and put it into register $t3
			sll $t4, $t6, 2       # Shift left twice (same as j * 4)
			add $t4, $t3, $t4     # Address of A[i][j]
			lw $t0, ($t4)         # Load value of A[i][j]

			li $v0, 1
			move $a0, $t0
			syscall                 # print

			li $v0, 4
			la $a0, msg_s
			syscall

			add $t6, $t6, 1
			j print_a_inner
		done_print_a_inner:
			add $t7, $t7, 1
			add $t6, $zero, $zero

			li $v0, 4
			la $a0, msg_nl
			syscall

			j print_a_outer
	done_print_a:
		li $v0, 4
		la $a0, msg_nl
		syscall

		li $v0, 4
		la $a0, msg_2
		syscall # prints original message

		jal Transpose
		jal Print_transpose
		jal Exit

Transpose:
	add $t7, $zero, $zero
	add $t6, $zero, $zero

	la $a0, NUMROWS_T
	lw $a1, ($a0)
	la $a0, NUMROWS_T
	lw $a2, ($a0)

	transpose_a_t_outer:
		bge $t7, $a1, done_transpose
		transpose_a_t_inner:
			bge $t6, $a2, done_transpose_inner
			la $t1, A
			sll $t2, $t7, 2       # Shift left twice (same as i * 4)
			add $t2, $t2, $t1     # Address of pointer A[i]
			lw $t3, ($t2)         # Get address of an array A[i] and put it into register $t3
			sll $t4, $t6, 2       # Shift left twice (same as j * 4)
			add $t4, $t3, $t4     # Address of A[i][j]
			lw $t0, ($t4)         # Load value of A[i][j]

			la $t1, T
			sll $t2, $t6, 2       # Shift left twice (same as j * 4)
			add $t2, $t2, $t1     # Address of pointer A[j]
			lw $t3, ($t2)         # Get address of an array A[j] and put it into register $t3
			sll $t4, $t7, 2       # Shift left twice (same as i * 4)
			add $t4, $t3, $t4     # Address of T[j][i]
			sw $t0, ($t4)         # store value of A[i][j] in T[j][i]

			add $t6, $t6, 1
			j transpose_a_t_inner
		done_transpose_inner:
			add $t7, $t7, 1
			add $t6, $zero, $zero
			j transpose_a_t_outer
	done_transpose:
		jr $ra

Print_transpose:
	add $t7, $zero, $zero
	add $t6, $zero, $zero

	la $a0, NUMROWS_T
	lw $a1, ($a0)
	la $a0, NUMROWS_T
	lw $a2, ($a0)

	print_t_outer:
		bge $t7, $a1, done_print_t
		print_t_inner:
			bge $t6, $a2, done_print_t_inner
			la $t1, T
			sll $t2, $t7, 2       # Shift left twice (same as i * 4)
			add $t2, $t2, $t1     # Address of pointer T[i]
			lw $t3, ($t2)         # Get address of an array T[i] and put it into register $t3
			sll $t4, $t6, 2       # Shift left twice (same as j * 4)
			add $t4, $t3, $t4     # Address of T[i][j]
			lw $t0, ($t4)         # Load value of T[i][j]

			li $v0, 1
			move $a0, $t0
			syscall                 # print

			li $v0, 4
			la $a0, msg_s
			syscall

			add $t6, $t6, 1
			j print_t_inner
		done_print_t_inner:
			add $t7, $t7, 1
			add $t6, $zero, $zero

			li $v0, 4
			la $a0, msg_nl
			syscall

			j print_t_outer
	done_print_t:
		jr $ra

Exit:
	li $v0, 10
	syscall # exit