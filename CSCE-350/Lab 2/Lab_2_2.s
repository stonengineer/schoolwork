# Lab 2 Problem 2
# Joseph Stone
# Feb 2, 2017

.data 0x10008000          # data degment start (assembler directive)
B:
	.word 300             # B[0]
	.word 200             # B[1]
	.word 100             # B[2]
	.word 0               # B[3]
i:
	.word 3               # i = 3

.text                     # code segment start (assembler directive)
main:
	la $t1, B             # load the address of B[0] (&(B[0]) == B) into register $t1
	lw $t0, 0($t1)        # load value of B[0] into register $t0
	add $t2, $t0, 0       # add B[0] value to summation register $t2

	la $t3, i             # load the address of i into register  $t3
	lw $t4, 0($t3)        # load the value of i into register $t4
	add $t4, $t4, 1       # increment $t4 to correct size

	add $t1, $t1, $t4     # move address of $t1 to B[1]
	lw $t0, ($t1)         # load value of B[1]
	add $t2, $t2, $t0     # add B[1] to accumulation register $t2

	add $t1, $t1, $t4     # move address of $t1 to B[2]
	lw $t0, ($t1)         # load value of B[2]
	add $t2, $t2, $t0     # add B[2] to accumulation register $t2

	add $t1, $t1, $t4     # move address of $t1 to B[3]
	lw $t0, ($t1)         # load value of B[3]
	add $t2, $t2, $t0     # add B[3] to accumulation register $t2

	sw $t2, 0($t1)        # store $t2 into B[3]

	addi $v0, $0, 10      # 'exit MIPS program' syscall
	syscall