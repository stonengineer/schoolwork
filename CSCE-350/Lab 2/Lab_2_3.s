# Lab 2 Problem 3
# Joseph Stone
# Feb 2, 2017

.data 0x10008000          # data degment start (assembler directive)
	mesg: .asciiz "Enter the value for register s0: "

.text                     # code segment start (assembler directive)
main:
	la $a0, mesg          # load effective address of mesg to $a0
	addi $v0, $0, 4       # load constant 4 to register $v0
	syscall               # switch into the kernel to print mesg out

	addi $v0, $0, 5       # reg $v0 = 5
	syscall               # gain keyboard input from user

	add $s0, $v0, $0      # copy the value of $v0 to $s0

	addi $v0, $0, 10      # 'exit MIPS program' syscall
	syscall