# Project 1 Problem 1
# Joseph Stone
# Mar 21, 2017

.data
	M: .space 160       # 20 * 8 (sizeof (block_t))
	Free_List: .word M  # Points to beginning of space
	MAX_CHAR: .word 50  # Maximum number of characters to read

	disp_mesg: .asciiz "LINKED LIST PROGRAM\nEnter an empty string to stop inserting values."
	input_mesg: .asciiz "Enter a value: "
	newline: .asciiz "\n"
	output_mesg: .asciiz "\n\nThe List: "
	arrow: .asciiz " -> "
	null_char: .asciiz "END"

.text
main:
	la $a0, disp_mesg      # load initial message
	jal print_var          # run method to print
	la $a0, newline        # load new line
	jal print_var          # run method to print

	jal input_loop         # run until empty string is input

input_loop:
	la $a0, input_mesg     # load query message
	jal print_var          # run method to print
	
	jal read               # read in string
	
	move $s1, $v0          # save inputted string
	
	move $a0, $s1          # load inputted string to chop newline
	jal rm_newline         # chop newline
	
	move $a0, $s1          # load inputted string to find length
	jal strlen             # find length of string
	
	ble $v0, $zero, exit   # if string length == 0, it's an empty string and exit
	
	jal insert             # runs insert function to put string in list
	
	j input_loop           # loop

exit:
	move $a0, $s0          # load initial for printing
	jal print_list         # iterate and print
	
	la $a0, newline        # new line
	jal print_var          # run method to print
	
	li $v0, 10             # load MIPS exit command
	syscall                # that's all, folks!

read:
	addi $sp, $sp, -8      # reserve 8 bytes from stack pointer
	sw $ra, 0($sp)         # save the return address
	sw $s0, 4($sp)         # save the head of the list
	
	lw $a1, MAX_CHAR       # specificy maximum number of characters to be read
	
	move $a0, $a1          # send number to malloc to reserve space
	jal malloc             # call malloc method
	
	move $a0, $v0          # save return memory address from malloc
	
	lw $a1, MAX_CHAR       # load maximum number of chars to read
	li $v0, 8              # tell kernel to read string
	syscall                # call kernel
	
	move $v0, $a0          # restore string address
	
	lw $s0, 4($sp)         # restore head of list
	lw $ra, 0($sp)         # restore return address
	addi $sp, $sp, 8       # restore stack pointer
	jr $ra                 # return to code

rm_newline:
	li $t0, 0x0A           # ASCII null character
	j remove_loop          # let's remove!

remove_loop:               # changes last newline from entered string to a null char
	lb $t1, 0($a0)
	beq $t1, $t0, replace_char
	beq $t1, $zero, newline_removed
	addi $a0, $a0, 1
	j remove_loop

replace_char:              # actually does the replace
	sb $zero, 0($a0)

newline_removed:           # let's go back
	jr $ra

strlen:                    # counts length of string
	move $v0, $a0          # set up temporary variable with the string
	j strlen_loop          # let's go!

strlen_loop:               # loops through until last char is reached, counting as it goes
	lb $t1, 0($a0)
	addi $a0, $a0, 1
	bne $t1, $zero strlen_loop
	
	subu $v0, $a0, $v0
	addi $v0, $v0, -1
	jr $ra

insert:
	addi $sp, $sp, -4      # reserve one byte from the stack pointer
	sw $ra, 0($sp)         # store the return address
	
	li $a0, 8              # load size of node
	jal malloc             # reserve space for node
	move $s2, $v0          # remember node address
	
	sw $zero, 0($s2)       # temp->next = NULL
	sw $s1, 4($s2)         # temp->data = string
	
	li $s3, 0              # prev = NULL
	move $s4, $s0          # s4 = s0 (head)
	j null_return          # if head is null we create a head

null_return:               # if head != NULL, loop
	bne $s4, $zero, find_back
	j push_back

find_back:
	lw $a0, 0($s4)             # a0 = curr->next
	beq $a0, $zero, push_back  # if curr->next == 0, add to back
	
	move $s3, $s4              # prev = curr
	
	lw $s4, 0($s4)             # curr = curr->next
	
	j find_back                # loop

push_back:
	beq $s4, $zero, make_head  # if head is null we make curr the head
	sw $s2, 0($s4)             # curr->next = temp
	j insert_done              # let's exit

make_head:
	move $s0, $s2              # new head node
	
insert_done:
	lw $ra, 0($sp)             # load return address
	addi $sp, $sp, 4           # restore stack pointer
	jr $ra                     # go back

print_list:
	addi $sp, $sp, -8          # reserve 2 bytes
	sw $ra, 0($sp)             # store return address
	sw $s0, 4($sp)             # store head
	
	la $a0, output_mesg        # load output message
	jal print_var              # print output message
	
	beq $s0, $zero, print_return
	
	print_loop:
		lw $a0, 4($s0)         # load data of curr
		jal print_var          # print data of curr
		la $a0, arrow          # load arrow
		jal print_var          # print arrow
		lw $s0, 0($s0)         # curr = curr->next
		beq $s0, $zero, print_return
		j print_loop           # loop

print_return:
	la $a0, null_char          # load exit character
	jal print_var              # print exit character

	lw $s0, 4($sp)             # restore head
	lw $ra, 0($sp)             # restore return address
	addi $sp, $sp, 8           # restore stack pointer
	jr $ra                     # return to code

malloc:
	li $v0, 9                  # prep MIPS to reserve memory
	syscall                    # tell kernel to reserve memory
	jr $ra                     # return to code

print_var:                     #displays string in ($a0)
	li $v0, 4                  # prep MIPS to print string
	syscall                    # tell kernel to print string
	jr $ra                     # return to code