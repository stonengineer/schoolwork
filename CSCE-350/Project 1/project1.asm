#Larry Harris
#Joe Stone
#CSCE 350
#Project 1


#Stack is First In Last Out



.data
	prompt:						.asciiz "Input a postfix expression to be evaluated, no parentheses and 1 digit numbers i.e. 0-9: \n"
	stack_err_mesg: 			.asciiz "Error: stack is empty cannot perform pop, please check if expression is feasible\n"
	postfix_err_mesg: 			.asciiz "Error: Incorrect format, make sure numbers are one digit and no parentheses."
	postfix_buffer: 			.space  64
	NL:							.asciiz "\n"
	
	addition: 					.asciiz "+"
	subtract: 					.asciiz "-"
	multiply: 					.asciiz "*"
	divide: 					.asciiz "/"
	
	stack_buffer:				.word 0 #buffer stores data going to and from stack
	TOS: 						.word 0 #top of stack
	Free_List: 					.word M 			# will be stores in $s0
	M: 							.space 1024
.text
.globl main

main:
	la $a0, prompt
	li $v0, 4
	syscall
	
	jal 	mem_init	#jump to the memory initializer
	
	la $a0, postfix_buffer
	li $a1, 64
	li $v0, 8
	syscall
	
	la 		$a0, postfix_buffer
	jal 	Eval_Postfix		#jump to postfix expression evaluator

	move 	$a0 ,$v0
	li 		$v0, 1
	syscall
	
	li		$v0, 10
	syscall


#################
#################	
	
#memory management

#void mem_init()
#{
#    int i;
#    /* It's a good idea to clear the data area initially. */
#    (void) memset( (void *) M, 0, N * sizeof(block_t) ); 
#    for ( i = 1; i < N; i++ )
#    {
#        M[i - 1].next = &M[i];
#    } 
#    /* Note: M[N - 1].next = NULL */
#}

mem_init:
	la 		$t0, Free_List
	lw		$t0, 0($t0)
	li 		$t1, 0
	li 		$t2, 1020

mem_loop:
	bge 	$t1, $t2, init_exit
	addi 	$t3, $t0, 8
	sw		$t3, 4($t0)
	addi 	$t0, $t0, 8
	addi	$t1, $t1, 8
	j 		mem_loop
	
init_exit:
	jr		$ra

######################	
# block_t* mem_alloc()
#{
#    block_t *p = FreeList;  /* Let p point where F points to */
#    if ( FreeList )         /* if FreeList is not null */
#    {
#        FreeList = FreeList->next;
#    }
#    else
#    {
#        /* Exception */
#    } 
#    return( p );
#}

mem_alloc:
	la 		$t0, Free_List #get Free_List pointer
	bne		$t0, $0, alloc_next
	addi 	$v0, $0, -1				# exception returns -1
	j 		alloc_exit
alloc_next:
	lw		$v0, ($t0) #load first block address
	lw 		$t1, 4($v0) #load next pointer t1
	sw 		$t1, ($t0) #store t1 next pointer
	sw		$0, 4($v0) #overwrite old pointer
alloc_exit:
	jr		$ra

	
#############################	
# void mem_dealloc(block_t *p)
#{
#    if ( p ) /* if p is not null */
#    { p->next = FreeList;
#        FreeList = p; /* Update FreeList to point to the latest freed element */}
#    else
#    {/* Exception */}
#}

mem_dealloc:
	move 	$t0, $a0
	bne 	$t0, $0, dealloc_next
	addi 	$t0, $0, -1
	j 		dealloc_exit
dealloc_next:
	la 		$t1, Free_List
	lw		$t2,($t1) #update next pointer to Free_List pointer
	sw 		$t2, 4($t0)
	sw		$0,	 ($t0) #clear mem on old data
	sw 		$t0, 0($t1) #update Free_List pointer to a0
dealloc_exit:
	jr		$ra
	

#################
#################	
# Stack	
#a0 =data_t* pushes data onto stack


stack_push:
	addi 	$sp, -8
	sw 		$ra, ($sp) #save variables
	sw 		$a0, 4($sp)
	
	jal 	mem_alloc #allocate 8 bytes for 2 numbers to be used from the stack, 4 bytes each
	move 	$t0, $v0	#move return pointer to t0
	lw 		$ra, ($sp) #reload variables from stack
	lw 		$a0, 4($sp)
	
	#store new data
	lw 		$t1, ($a0) #get number from pointer
	sw 		$t1, ($t0) #store number in new block_t
	la 		$t1, TOS   #Top Of Stack address

	#update stack
	lw 		$t2, ($t1) #t2 is block_t pointer
	sw 		$t2, 4($t0) # store pointer
	sw 		$t0, ($t1) #point top of stack to new block

	addi 	$sp, 8 #return stack pointer to the beginning
	jr 		$ra
		

stack_pop:
	addi 	$sp, -4 #save return address
	sw 		$ra, ($sp)
	
	la 		$t0, TOS 
	lw 		$t1, ($t0) 
	beq 	$t1, $0, stack_exception #check for null
	
	lw		$t3, ($t1) #get number from pointer
	la		$v0, stack_buffer
	sw 		$t3, ($v0) #save data value
	
	lw 		$t2, 4($t1) #load next pointer
	sw 		$t2, ($t0) #store next pointer number TOS
	
	add 	$a0, $t1, $0 #move address of old block to deallocate
	jal 	mem_dealloc
	
	la		$v0, stack_buffer	#v0 is returned data
	lw 		$ra, ($sp)
	addi 	$sp, 4
	jr 		$ra

stack_error:
	la $t0, TOS 		#get address of the stack 
	lw $t1, ($t0)		# load number from address of stack
	slt $v0, $0, $t1 		#if t1 is not 0 v0 will be 1, if t1 is 0 v0 will be 0
	jr 		$ra				#exit loop

stack_exception:
	la 		$a0, stack_err_mesg
	li 		$v0, 4
	syscall #print error mesg

	
	#restore return address and exit
	lw 		$ra, ($sp)
	addi 	$sp, 4
	li 		$v0, 10
	syscall
	jr 		$ra

###############################	
# Postfix expression evaluator
# int Eval_Postfix(string* s)


Eval_Postfix:
	addi 	$sp, -4 			#save return address
	sw 		$ra, ($sp)

	add 	$t7, $a0, $0
	lb		$t0, 0($t7)
postfix_loop:
	lb		$t0, 0($t7)
	la 		$t1, NL
	lb 		$t1, 0($t1) #get value ascii value of new line
	beq 	$t0, $t1, exit 		# if at end of string, exit
	
# check if string is operand

	la 		$t1, addition
	lb 		$t1, 0($t1) 	#get ascii value
	beq 	$t0, $t1, op_addition 

	la 		$t1, subtract
	lb 		$t1, 0($t1) 	#get ascii value
	beq 	$t0, $t1, op_subtract

	la 		$t1, divide
	lb 		$t1, 0($t1) 	#get ascii value
	beq 	$t0, $t1, op_divide

	la 		$t1, multiply
	lb 		$t1, 0($t1) 	#get ascii value
	beq 	$t0, $t1, op_multiply
	
#check  if string is number and check for errors

	li 		$t1, 48
	blt 	$t0, $t1, postfix_exception

	li 		$t1, 57
	bgt		$t0, $t1, postfix_exception 

	addi 	$t0, $t0, -48
	
	la		$a0, stack_buffer #use buffer because stack takes in pointer to data_t, not data_t 
	sw		$t0, ($a0)
	jal 	stack_push

	addi 	$t7, $t7, 1 	# increment the string pointer
	j		postfix_loop
	
####################
#operand performance

op_addition:
	jal 	stack_pop	#get first number
	addi    $sp, -4
	lw 		$t4 ,($v0)	#save return value
	sw		$t4, ($sp)
	
	jal 	stack_pop	#get second number
	lw		$t4, ($sp)
	addi	$sp, 4
	lw 		$t3 ,($v0)	
	
	add 	$t3, $t3, $t4   #perform add operation
	la 		$a0, stack_buffer
	sw		$t3, ($a0)		#store new value on the stack
	jal 	stack_push
	addi 	$t7, $t7, 1
	j 		postfix_loop

op_subtract:
	jal 	stack_pop		#get last number in stack
	lw 		$t4 ,($v0)
	addi    $sp, -4
	sw		$t4, ($sp)
	
	jal 	stack_pop		#get second to last number in stack
	lw		$t4, ($sp)
	addi	$sp, 4
	lw 		$t3 ,($v0)

	sub 	$t3, $t3, $t4 	#subtract the last number from the second to last number
	la		$a0, stack_buffer
	sw		$t3, ($a0)   	#put number on stack
	jal		stack_push	
	addi 	$t7, $t7, 1
	
	
	j 		postfix_loop

op_divide:
	jal 	stack_pop	
	lw 		$t4 ,($v0)
	addi    $sp, -4
	sw		$t4, ($sp)
	
	jal 	stack_pop
	lw		$t4, ($sp)
	addi	$sp, 4
	lw 		$t3 ,($v0)
	
	div 	$t3, $t3, $t4 			#Divide second to last number but last number
	la 		$a0, stack_buffer
	sw		$t3, ($a0)
	jal		stack_push
	addi 	$t7, $t7, 1
	
	
	j 		postfix_loop
	
	
#mult works same as div, add, sub
op_multiply:
	jal 	stack_pop		
	lw 		$t4 ,($v0)
	addi    $sp, -4
	sw		$t4, ($sp)
	
	jal 	stack_pop
	lw		$t4, ($sp)
	addi	$sp, 4
	lw 		$t3 ,($v0)
	
	mul 	$t3, $t3, $t4 
	la		$a0, stack_buffer
	sw		$t3, ($a0)
	jal		stack_push
	addi 	$t7, $t7, 1
	
	
	j 		postfix_loop

###############
#Error Handling	

postfix_exception:  

	#print error mesg
	la 		$a0, postfix_err_mesg
	li 		$v0, 4
	syscall 		

	
	#restore return address and exit
	lw 		$ra, ($sp)
	addi 	$sp, 4
	li 		$v0, 10
	syscall

	
#End Program
exit:
	jal 	stack_pop
	lw 		$v0, ($v0) #load final answer from buffer
	addi 	$sp, -4
	sw 		$v0, 0($sp)
	jal		stack_error		#check for postfix errors
	li		$t1, 1
	beq		$t1, $v0, postfix_exception #

	lw		$v0, ($sp)
	lw 		$ra, 4($sp)
	addi 	$sp, 8
	jr 		$ra