module mux_4_1(S, a, b, c, d, ao1, ao0);
	input wire a, b, c, d, ao1, ao0;
	output wire S;
	wire no1, no0, a1, a2, a3, a4;
	
	not n1(no1, ao1);
	not n0(no0, ao0);
	
	and and1(a1, a, no0, no1);
	and and2(a2, b, no0, ao1);
	and and3(a3, c, ao0, no1);
	and and4(a4, d, ao0, ao1);
	
	or or1(S, a1, a2, a3, a4);
endmodule

module full_adder(S, Co, a, b, ci);
	input wire a, b, ci;
	output wire S, Co;
	
	mux_4_1 out1(S, a, ~a, ~a, a, ci, b);
	mux_4_1 out2(Co, 1'b0, a, a, 1'b1, ci, b);
endmodule

module test_full_adder();              /* test bench module for full_adder() */
    reg    a, b, c;
    wire   S,Co;

    full_adder fm(S,Co,a,b,c);

    initial begin
        $monitor ($time,"\ta=%b\tb=%b\tc=%b\tS=%b\tC=%b",a,b,c,S,Co);
        a = 0; b = 0; c = 0;
        #1 
		a = 0; b = 0; c = 1;
        #1 
		a = 0; b = 1; c = 0;
        #1 
        a = 0; b = 1; c = 1;
        #1 
		a = 1; b = 0; c = 0;
        #1 
		a = 1; b = 0; c = 1;
        #1 
		a = 1; b = 1; c = 0;
        #1 
		a = 1; b = 1; c = 1;
        #1 
        $finish;
    end
endmodule