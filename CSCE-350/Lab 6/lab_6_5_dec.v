module decoder_3_8(S, a, b, c);
	input wire a, b, c;
	output wire [7:0] S;
	
	and a0(S[0], !a, !b, !c);
	and a1(S[1], !a, !b, c);
	and a2(S[2], !a, b, !c);
	and a3(S[3], !a, b, c);
	and a4(S[4], a, !b, !c);
	and a5(S[5], a, !b, c);
	and a6(S[6], a, b, !c);
	and a7(S[7], a, b, c);
endmodule

module test_3_8_decoder();              /* test bench module for decoder_3_8() */
    reg    a, b, c;
    wire [7:0] S;

    decoder_3_8 fm(S,a,b,c);

    initial begin
        $monitor ($time,"\ta=%b\tb=%b\tc=%b\tS=%b",a,b,c,S);
        a = 0; b = 0; c = 0;
        #1 
		a = 0; b = 0; c = 1;
        #1 
		a = 0; b = 1; c = 0;
        #1 
        a = 0; b = 1; c = 1;
        #1 
		a = 1; b = 0; c = 0;
        #1 
		a = 1; b = 0; c = 1;
        #1 
		a = 1; b = 1; c = 0;
        #1 
		a = 1; b = 1; c = 1;
        #1 
        $finish;
    end
endmodule