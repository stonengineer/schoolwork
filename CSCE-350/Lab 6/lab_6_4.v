//`include "lab_6_3.v"

module adder_4_bit(S, Co, A, B, Cin);
	input wire Cin;
	input wire [3:0] A, B;
	output wire Co;
	output wire [3:0] S;

	wire c_int;
	
	full_adder_nand a0(S[0], c_int, A[0], B[0], Cin);
	full_adder_nand a1(S[1], c_int, A[1], B[1], c_int);
	full_adder_nand a2(S[2], c_int, A[2], B[2], c_int);
	full_adder_nand a3(S[3], Co, A[3], B[3], c_int);
endmodule

module full_adder_nand(S, Co, a, b, ci);
	input a, b, ci;
	output S, Co;
	wire no1, no2, no3, no4, no5, no6, no7;
	
	nand n1(no1, a, b);
	nand n2(no2, a, no1);
	nand n3(no3, no1, b);
	nand n4(no4, no2, no3);
	nand n5(no5, no4, ci);
	nand n6(no6, no4, no5);
	nand n7(no7, no5, ci);
	nand n8(S, no6, no7);
	nand n9(Co, no5, no1);
endmodule

module test_4_bit_adder();              /* test bench module for adder_4_bit() */
    reg [3:0] A, B;
    wire [3:0] S;
    wire Cout;
    reg C0;

    adder_4_bit fm(S,Cout,A,B,C0);

    initial begin
		$monitor($time, "\tA=%d\tB=%d\tCin=%b\tS=%d\tCout=%b", A, B, C0, S, Cout);
		
		A = 0;	B = 0;	C0 = 0;
		#1
		A = 0;	B = 0;	C0 = 1;
		#1
		A = 0;	B = 1;	C0 = 0;
		#1
		A = 1;	B = 0;	C0 = 0;
		#1
		A = 1;	B = 1;	C0 = 0;
		#1
		A = 1;	B = 1;	C0 = 1;
		#1
		A = 7;	B = 7;	C0 = 0;
		#1
		A = 7;	B = 7;	C0 = 1;
		#1
		A = 15;	B = 0;	C0 = 0;
		#1
		A = 15;	B = 0;	C0 = 1;
		#1
		A = 15;	B = 1;	C0 = 0;
		#1
		A = 15;	B = 1;	C0 = 1;
		#1
		A = 0;	B = 15;	C0 = 0;
		#1
		A = 0;	B = 15;	C0 = 1;
		#1
		A = 1;	B = 15;	C0 = 0;
		#1
		A = 1;	B = 15;	C0 = 1;
		#1
		A = 15;	B = 15;	C0 = 0;
		#1
		A = 15;	B = 15;	C0 = 1;
		#1
		$finish;
	end
endmodule