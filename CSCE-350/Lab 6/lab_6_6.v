//`include "lab_6_4.v"

module adder_32_bit(S, Co, A, B, Ci);
	input wire [31:0] A, B;
	input wire Ci;
	output wire [31:0] S;
	output wire Co;
	
	wire c_int;
	
	adder_4_bit a0(S[3:0], c_int, A[3:0], B[3:0], Ci);
	adder_4_bit a1(S[7:4], c_int, A[7:4], B[7:4], c_int);
	adder_4_bit a2(S[11:8], c_int, A[11:8], B[11:8], c_int);
	adder_4_bit a3(S[15:12], c_int, A[15:12], B[15:12], c_int);
	adder_4_bit a4(S[19:16], c_int, A[19:16], B[19:16], c_int);
	adder_4_bit a5(S[23:20], c_int, A[23:20], B[23:20], c_int);
	adder_4_bit a6(S[27:24], c_int, A[27:24], B[27:24], c_int);
	adder_4_bit a7(S[31:28], Co, A[31:28], B[31:28], c_int);
endmodule

module adder_4_bit(S, Co, A, B, Cin);
	input wire Cin;
	input wire [3:0] A, B;
	output wire Co;
	output wire [3:0] S;

	wire c_int;
	
	full_adder_nand a0(S[0], c_int, A[0], B[0], Cin);
	full_adder_nand a1(S[1], c_int, A[1], B[1], c_int);
	full_adder_nand a2(S[2], c_int, A[2], B[2], c_int);
	full_adder_nand a3(S[3], Co, A[3], B[3], c_int);
endmodule

module full_adder_nand(S, Co, a, b, ci);
	input a, b, ci;
	output S, Co;
	wire no1, no2, no3, no4, no5, no6, no7;
	
	nand n1(no1, a, b);
	nand n2(no2, a, no1);
	nand n3(no3, no1, b);
	nand n4(no4, no2, no3);
	nand n5(no5, no4, ci);
	nand n6(no6, no4, no5);
	nand n7(no7, no5, ci);
	nand n8(S, no6, no7);
	nand n9(Co, no5, no1);
endmodule

module test_32_bit_adder();              /* test bench module for adder_32_bit() */
    reg [31:0] A, B;
	reg Ci;
	wire [31:0] S;
	wire Co;

    adder_32_bit fm(S,Co,A, B, Ci);

    initial begin
		$monitor($time, "\tA=%d\tB=%d\tCin=%b\tS=%d\tCout=%b", A, B, Ci, S, Co);
		
		A = 0;		B = 0;		Ci = 0;
		#1
		A = 0;		B = 0;		Ci = 1;
		#1
		A = 0;		B = 1;		Ci = 0;
		#1
		A = 1;		B = 0;		Ci = 0;
		#1
		A = 1;		B = 1;		Ci = 0;
		#1
		A = 1;		B = 1;		Ci = 1;
		#1
		A = 16777215;	B = 16777215;	Ci = 0;
		#1
		A = 16777215;	B = 16777215;	Ci = 1;
		#1
		A = 4294967295;	B = 0;		Ci = 0;
		#1
		A = 4294967295;	B = 0;		Ci = 1;
		#1
		A = 4294967295;	B = 1;		Ci = 0;
		#1
		A = 4294967295;	B = 1;		Ci = 1;
		#1
		A = 0;		B = 4294967295;	Ci = 0;
		#1
		A = 0;		B = 4294967295;	Ci = 1;
		#1
		A = 1;		B = 4294967295;	Ci = 0;
		#1
		A = 1;		B = 4294967295;	Ci = 1;
		#1
		A = 4294967295;	B = 4294967295;	Ci = 0;
		#1
		A = 4294967295;	B = 4294967295;	Ci = 1;
		#1
		$finish;
	end
endmodule