module full_adder_nand(S, Co, a, b, ci);
	input a, b, ci;
	output S, Co;
	wire no1, no2, no3, no4, no5, no6, no7;
	
	nand n1(no1, a, b);
	nand n2(no2, a, no1);
	nand n3(no3, no1, b);
	nand n4(no4, no2, no3);
	nand n5(no5, no4, ci);
	nand n6(no6, no4, no5);
	nand n7(no7, no5, ci);
	nand n8(S, no6, no7);
	nand n9(Co, no5, no1);
endmodule

module test_full_nand_adder();              /* test bench module for full_adder_nand() */
    reg    a, b, c;
    wire   S,Co;

    full_adder_nand fm(S,Co,a,b,c);

    initial begin
        $monitor ($time,"\ta=%b\tb=%b\tc=%b\tS=%b\tC=%b",a,b,c,S,Co);
        a = 0; b = 0; c = 0;
        #1 
		a = 0; b = 0; c = 1;
        #1 
		a = 0; b = 1; c = 0;
        #1 
        a = 0; b = 1; c = 1;
        #1 
		a = 1; b = 0; c = 0;
        #1 
		a = 1; b = 0; c = 1;
        #1 
		a = 1; b = 1; c = 0;
        #1 
		a = 1; b = 1; c = 1;
        #1 
        $finish;
    end
endmodule