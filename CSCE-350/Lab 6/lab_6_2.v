module half_adder(S, C, a, b);
	input a, b;
	output S, C;
	
	xor x1(S, a, b);
	and a1(C, a, b);
endmodule

module test_half_adder();              /* test bench module for half_adder() */
    reg    a, b;
    wire   S, C;

    half_adder fm(S,C,a,b);

    initial begin
        $monitor ($time,"\ta=%b\tb=%b\tS=%b\tC=%b",a,b,S,C);
        a = 0; b = 0;
        #1 
        a = 0; b = 1;
        #1
        a = 1; b = 0;
        #1
        a = 1; b = 1;
        #1 
        $finish;
    end
endmodule