/*
 File: vm_pool.C
 
 Author:
 Date  :
 
 */

/*--------------------------------------------------------------------------*/
/* DEFINES */
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* INCLUDES */
/*--------------------------------------------------------------------------*/

#include "vm_pool.H"
#include "console.H"
#include "utils.H"
#include "assert.H"
#include "simple_keyboard.H"
#include "page_table.H"

/*--------------------------------------------------------------------------*/
/* DATA STRUCTURES */
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* CONSTANTS */
/*--------------------------------------------------------------------------*/

#define PDE 0x3FF
#define PTE 0x3FF
#define OFF 0xFFF

/*--------------------------------------------------------------------------*/
/* FORWARDS */
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* METHODS FOR CLASS   V M P o o l */
/*--------------------------------------------------------------------------*/

/* Initializes the data structures needed for the management of this
* virtual-memory pool.
* _base_address is the logical start address of the pool.
* _size is the size of the pool in bytes.
* _frame_pool points to the frame pool that provides the virtual
*     memory pool with physical memory frames.
* _page_table points to the page table that maps the logical memory
*     references to physical addresses. 
*/
VMPool::VMPool(unsigned long  _base_address,
               unsigned long  _size,
               ContFramePool *_frame_pool,
               PageTable     *_page_table) {

    base_address = _base_address;
    size = _size;
    frame_pool = _frame_pool;
    page_table = _page_table;

    next_vm_pool = NULL;

    page_table->register_pool(this);
    
    last_region_index = 0;
    size_in_pages = size / PageTable::PAGE_SIZE;
    long * ba = (long *) base_address;
    ba[0] = size_in_pages - 1;

    Console::puts("Constructed VMPool object.\n");
}

/* Allocates a region of _size bytes of memory from the virtual
* memory pool. If successful, returns the virtual address of the
* start of the allocated region of memory. If fails, returns 0. */
unsigned long VMPool::allocate(unsigned long _size) {
    unsigned long retval = base_address + PageTable::PAGE_SIZE;
    long num_pages = _size / PageTable::PAGE_SIZE + ((_size % PageTable::PAGE_SIZE == 0) ? 0 : 1);
    long * ba = (long *) base_address;
    //Console::puts("retval,num_pages,base_address,last_region_index:"); Console::putui(retval); Console::putui(num_pages); Console::putui(base_address); Console::puts("\n");
    for(int ba_index = 0; ba_index <= last_region_index; ba_index++) {
        if(ba[ba_index] >= num_pages) {
            if(ba[ba_index] > num_pages) {
                if(last_region_index >= ((PageTable::PAGE_SIZE / 4) - 1)) {
                    Console::puts("out of space for region index descriptor\n");
                    assert(false);
                }
                for(int lr_index = last_region_index; lr_index > ba_index; lr_index--) {
                    ba[lr_index+1] = ba[lr_index];
                }
                last_region_index++;
                ba[ba_index+1] = ba[ba_index] - num_pages;
            }
            ba[ba_index] = num_pages * -1;
            return retval;
        }
        else {
            retval += ((ba[ba_index] < 0) ? (-ba[ba_index]) : (ba[ba_index])) * PageTable::PAGE_SIZE;
        }
    }
    return 0;
}

/* Releases a region of previously allocated memory. The region
* is identified by its start address, which was returned when the
* region was allocated. */
void VMPool::release(unsigned long _start_address) {
    long * ba = (long *) base_address;
    unsigned long region_val = base_address + PageTable::PAGE_SIZE;

    int ba_index = 0;
    for(; ba_index <= last_region_index; ba_index++) {
        if(region_val == _start_address) {
            break;
        }
        else {
            region_val += ((ba[ba_index] < 0) ? (-ba[ba_index]) : (ba[ba_index])) * PageTable::PAGE_SIZE;
        }
    }

    if(ba_index > last_region_index) {
        Console::puts("invalid start address for release\n");
        assert(false);
    }

    long num_pages = ba[ba_index];
    if(num_pages > 0) {
        Console::puts("attempted to free released memory\n");
        assert(false);
    }
    num_pages *= -1;

    for(int ipage = 0; ipage < num_pages; ipage++) {
        page_table->free_page(region_val + (ipage * PageTable::PAGE_SIZE));
    }

    ba[ba_index] = num_pages;
    if(ba_index+1 <= last_region_index) {
        if(ba[ba_index+1] > 0) {
            ba[ba_index] += ba[ba_index+1];
            for(int comp_index = ba_index+1; comp_index < last_region_index; comp_index++) {
                ba[comp_index] = ba[comp_index+1];
            }
            last_region_index--;
        }
    }
    if(ba_index > 0) {
        if(ba[ba_index-1] > 0) {
            ba[ba_index-1] += ba[ba_index];
            for(int comp_index = ba_index; comp_index < last_region_index; comp_index++) {
                ba[comp_index] = ba[comp_index+1];
            }
            last_region_index--;
        }
    }

    Console::puts("Released region of memory.\n");
}

/* Returns false if the address is not valid. An address is not valid
* if it is not part of a region that is currently allocated. */
bool VMPool::is_legitimate(unsigned long _address) {
    long * ba = (long *) base_address;
    unsigned long region_val = base_address + PageTable::PAGE_SIZE;

    if((_address >= base_address) && (_address < region_val)) {
        return true;
    }

    for(int ba_index = 0; ba_index <= last_region_index; ba_index++) {
        unsigned long next_region_val = region_val + ((ba[ba_index] < 0) ? (-ba[ba_index]) : (ba[ba_index])) * PageTable::PAGE_SIZE;
        if((_address >= region_val) && (_address < next_region_val)) {
            return ba[ba_index] < 0;
        }
        region_val = next_region_val;
    }
    return false;
}

/* Returns true if the passed address is controlled by this VMPool */
bool VMPool::contains_address(unsigned long _address) {
    unsigned long max_address = base_address + (size_in_pages * PageTable::PAGE_SIZE);
    return ((_address >= base_address) && (_address < max_address));
}
