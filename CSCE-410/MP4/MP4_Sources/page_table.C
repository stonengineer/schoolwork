#include "assert.H"
#include "exceptions.H"
#include "console.H"
#include "paging_low.H"
#include "page_table.H"
#include "machine.H"

PageTable * PageTable::current_page_table = NULL;
unsigned int PageTable::paging_enabled = 0;
ContFramePool * PageTable::kernel_mem_pool = NULL;
ContFramePool * PageTable::process_mem_pool = NULL;
unsigned long PageTable::shared_size = 0;
VMPool * PageTable::vmpool_head = NULL;

#define PDE 0x3FF
#define PTE 0x3FF
#define OFF 0xFFF

#define MB * (0x1 << 20)

void PageTable::init_paging(ContFramePool * _kernel_mem_pool, ContFramePool * _process_mem_pool, const unsigned long _shared_size)
{
   kernel_mem_pool = _kernel_mem_pool;
   process_mem_pool = _process_mem_pool;
   shared_size = _shared_size;
   
   Console::puts("Initialized Paging System\n");
}

void PageTable::create_page_directory() {
   unsigned long page_directory_frame_index = kernel_mem_pool->get_frames(1); //changed from kernel_mem_pool
   current_page_table->page_directory = (unsigned long *) (Machine::PAGE_SIZE * page_directory_frame_index);

   unsigned long * page_table = create_page_table();

   //don't know why this is necessary...
   unsigned long address = 0;
   for(int i = 0; i < 1024; i++) {
      page_table[i] = address | 3;
      address = address + 4096;
   }

   current_page_table->page_directory[0] = (unsigned long) page_table; //first directory index is our page table
   current_page_table->page_directory[0] |= 3; //mark the page_table as R/W and present

   //mark all remaining page directories as R/W but not present
   for(int i = 1; i < 1023; i++) {
      current_page_table->page_directory[i] = 0 | 2;
   }
   current_page_table->page_directory[1023] = (unsigned long) current_page_table->page_directory | 3;
}

unsigned long * PageTable::create_page_table() {
   unsigned long page_table_frame_index = kernel_mem_pool->get_frames(1); //changed from kernel_mem_pool
   unsigned long * page_table = (unsigned long *) (Machine::PAGE_SIZE * page_table_frame_index);

   //builds page table with no frames, need to get_frames()
   for(int i = 0; i < 1024; i++) {
      page_table[i] = 0 | 2;
   }
   
   return page_table;
}

PageTable::PageTable()
{
   current_page_table = this;

   create_page_directory(); //create array of page directories

   Console::puts("Constructed Page Table object\n");
}


void PageTable::load()
{
   current_page_table = this;

   Console::puts("Loaded page table\n");
}

void PageTable::enable_paging()
{
   paging_enabled = 1;

   write_cr3((unsigned long)(current_page_table->page_directory));
   unsigned long cr0 = read_cr0();
   cr0 |= 0x80000000;
   write_cr0(cr0);

   Console::puts("Enabled paging\n");
}

void PageTable::handle_fault(REGS * _r)
{
   unsigned long err_addr = read_cr2();

   bool rejected_by_pool = false;
   VMPool * temp = vmpool_head;
   while(temp != NULL) {
      if(temp->contains_address(err_addr)) {
         if(!temp->is_legitimate(err_addr)) {
            rejected_by_pool = true;
            break;
         }
      }
      temp = temp->next_vm_pool;
   }
   if(rejected_by_pool) {
      Console::puts("illegitimate registered pool!\n");
      assert(false);
   }

   unsigned long pde_index = (err_addr >> 22) & PDE;
   unsigned long pte_index = (err_addr >> 12) & PTE;

   unsigned long curr_pde = current_page_table->page_directory[pde_index];
   unsigned long * curr_page_table;

   //test pde
   if(curr_pde & 1) {
      //present
      curr_page_table = (unsigned long *) (curr_pde & 0xFFFFFC00);
   }
   else {
      //not present
      unsigned long page_table_frame_index = process_mem_pool->get_frames(1); //changed from kernel_mem_pool
      unsigned long * page_table = (unsigned long *) (Machine::PAGE_SIZE * page_table_frame_index);
      current_page_table->page_directory[pde_index] = ((unsigned long) page_table) | 3;

      unsigned long * fake_address = (unsigned long *) (0xFFC00000 | (pde_index << 12));

      //builds page table with no frames, need to get_frames()
      for(int i = 0; i < 1024; i++) {
         fake_address[i] = 0 | 2;
      }

      curr_page_table = page_table;
   }

   unsigned long * fake_address = (unsigned long *) (0xFFC00000 | (pde_index << 12));
   unsigned long curr_pte = fake_address[pte_index];
   
   if(curr_pte & 1) {
      //why are we here
      return;
   }
   else {
      unsigned long pte_frame_index = process_mem_pool->get_frames(1);
      unsigned long address = (Machine::PAGE_SIZE * pte_frame_index);
      fake_address[pte_index] = address | 3;
   }

   Console::puts("handled page fault\n");
}

void PageTable::register_pool(VMPool * _vm_pool) {
   _vm_pool->next_vm_pool = vmpool_head;
   vmpool_head = _vm_pool;

   Console::puts("registered vm pool\n");
}

void PageTable::free_page(unsigned long _page_no) {
   unsigned long pde_index = (_page_no >> 22) & PDE;
   unsigned long pte_index = (_page_no >> 12) & PTE;

   unsigned long curr_pde = current_page_table->page_directory[pde_index];
   unsigned long * curr_page_table;

   //test pde
   if(curr_pde & 1) {
      //present
      curr_page_table = (unsigned long *) (curr_pde & 0xFFFFFC00);
   }
   else {
      //not present
      return;
   }

   unsigned long curr_pte = curr_page_table[pte_index];
   if(curr_pte & 1) {
      unsigned long frame_no = curr_pte / PAGE_SIZE;
      process_mem_pool->release_frames(frame_no);
      curr_page_table[pte_index] &= 0xFFFFFFFE; //clear present bit
   }
   else {
      return;
   }

   write_cr3(read_cr3()); //clear TLB
   Console::puts("Freed page\n");
}
/* If page is valid, release frame and mark page invalid. */
