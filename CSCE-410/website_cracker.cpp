#include <stdlib.h>
#include <iostream>
#include <string>
#include <stdio.h>

int main() {
  std::cout << "Base url, minus the ---: ";
  std::string base_url;
  std::getline(std::cin, base_url);

  char possible_letters[62] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

  int count = 0;
  for(int a = 0; a < 62; a++) {
    for(int b = 0; b < 62; b++) {
      for(int c = 0; c < 62; c++) {
        std::cout << "\r" << count << ": "; std::cout.flush();
        std::string test_url = (base_url + possible_letters[a] + possible_letters[b] + possible_letters[c]);
        std::string test_command = "curl -s -o /dev/null -I -w \"%{http_code}\" " + test_url;
        FILE *fp = popen(test_command.c_str(), "r");
        char buf[1024];
        while(fgets(buf, 1024, fp)) {
          if(buf[0] != '4' || buf[1] != '0' || buf[2] != '4') {
            printf("%c%c%c %c%c%c\n", possible_letters[a], possible_letters[b], possible_letters[c], buf[0], buf[1], buf[2]);
          }
        }
        fclose(fp);
        ++count;
      }
    }
  }
}