# When done, submit this entire file to the autograder.

# Part 1

def sum(arr)
  # YOUR CODE HERE
  temp = 0 # variable to hold calculated sum
  arr.each { |a| (temp = temp + a) } # iterates through array, adding to temp
  return temp # return the sum of the array's contents
end

def max_2_sum(arr)
  # YOUR CODE HERE
  if arr.empty? then # emtpy arrays always return 0
    return 0
  end
  if arr.length == 1 then # size-1 arrays return the value it contains
    return arr.shift
  end
  max1 = arr.shift # init max1 to the first element of array, removing it
  max2 = arr.shift # init max2 to the second element of array, removing it
  while !(arr.empty?) do
    temp = arr.shift # pop off front value of array
    if temp > max1 then # if it's larger than max1, set max1 to it
      max1 = temp
    elsif temp > max2 then # otherwise, test max2
      max2 = temp
    end
  end
  return max1 + max2 # after loop we have largest 2 values, return sum
end

def sum_to_n?(arr, n)
  # YOUR CODE HERE
  if arr.length < 2 then # always false if array is smaller than size 2
    return false
  end

  # test all permutations of sums of array elements by looping through the array, and for each element looping from that element through the array. For example, [1,2,3,4,5] would compute by calculating: 1+2, 1+3, 1+4, 1+5, 2+3, 2+4, 2+5, ..., 4+5, all the while testing each computation against n
  outer_index = 0 # keep track of starting iterator
  while outer_index < arr.length do
    inner_index = outer_index + 1 # inner loop starts one after outer
    while inner_index < arr.length do
      sum = arr.at(outer_index) + arr.at(inner_index) # test value
      if sum == n then # test value against sum
        return true
      end
      inner_index = inner_index + 1 # increment inner index
    end
    outer_index = outer_index + 1 # increment outer index
  end
  return false # if no combinations work, then it's false
end

# Part 2

def hello(name)
  # YOUR CODE HERE
  return "Hello, " + name # Hello, world!
end

def starts_with_consonant?(s)
  # YOUR CODE HERE
  # used seeded test array in order to utilize the .include? function
  valid_letters = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z", "B", "C", "D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z"]
  return valid_letters.include?(s[0])
end

def binary_multiple_of_4?(s)
  # YOUR CODE HERE
  if s.size == 0 then # 0 is not a multiple of 4
    return false
  end
  # if there's anything other than 0s or 1s, then the input string is not a binary number
  if s.count("01") != s.size then 
    return false
  end

  decimal_rep = s.to_i(base=2) # such a cool function, ruby is nice

  return (decimal_rep % 4) == 0 # modulous 4 == 0 determines multiple
end

# Part 3

class BookInStock
  # YOUR CODE HERE

  def initialize(isbn, price)
    if isbn.size == 0 then # can't have empty string isbn
      raise ArgumentError.new("ISBN is empty string")
    end
    if price <= 0 then # price must be positive and books ain't cheap
      raise ArgumentError.new("Price cannot be zero or less")
    end

    @isbn = isbn # not entirely sure how this works, but it seems like
    @price = price # the @ symbol sets a global class variable

    @initialized = true # saw this on an online tutorial
  end

  def initialized?
    return @initialized || false # that's pretty neat! 
  end

  def isbn # getter for isbn
    return @isbn
  end

  def price # getter for price
    return @price
  end

  def isbn=(isbn) # setter for isbn
    @isbn = isbn
  end

  def price=(price) # setter for price
    @price = price
  end

  def price_as_string 
    string_price = @price.to_s # convert price to string
    output_str = "$" # begin building our output
    iter_index = 0 # keeps track of our iteration through string_price
    original_decs = 0 # counts the number of decimals originally in string_price
    hit_decimal = false # determines if we've seen a decimal
    while iter_index < string_price.size do
      output_str = output_str + string_price[iter_index] # always pass to output_str
      if string_price[iter_index] == "." then # we saw a decimal!
        hit_decimal = true
      elsif hit_decimal then # if we've seen a decimal, keep track of the number of numbers afterwards so we can possibly append 0s
        original_decs = original_decs + 1
      end
      iter_index = iter_index + 1
    end

    if !hit_decimal then # add a decimal if there wasn't one originally
      output_str = output_str + "."
    end

    while original_decs < 2 do # add 0s until we hit 2 past the decimal
      output_str = output_str + "0"
      original_decs = original_decs + 1
    end

    return output_str # should be all good! 
  end
end
