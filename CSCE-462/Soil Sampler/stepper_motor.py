import RPi.GPIO as IO
IO.setwarnings(False) 
import time
import thread
import ultrasonic_sensor
import moisture_sensor
from enum import Enum

class MotorAction(Enum):
    down = 1
    up = 2
    reset = 3
    none = 0
    measure = 4
    measuring = 5


step_pin = int(16)
dir_pin = int(12)
top_switch = int(21)
bottom_switch = int(20)
position = float(0)
revolution_distance = 0.8 # 8 mm
steps_per_revolution = float(200) #1.8 degree per step
max_distance = float(30) #300 mm
step_distance = revolution_distance / steps_per_revolution
max_steps = int(steps_per_revolution * max_distance / revolution_distance) 
offset_distance = float(0)
motor_action = MotorAction.none
moving_threshold = float(3) # 3 cm

cmd_distance = float(0)

measured_moisture = float(0)

IO.setmode (IO.BCM) 
IO.setup(step_pin, IO.OUT)
IO.setup(dir_pin, IO.OUT)
IO.setup(top_switch, IO.IN, pull_up_down = IO.PUD_UP)
IO.setup(bottom_switch, IO.IN, pull_up_down = IO.PUD_UP)

def get_measured_moisture():
    global measured_moisture
    return measured_moisture

def can_step_up():
    return IO.input(top_switch) == 1

def read_top_switch():
    return IO.input(top_switch)

def can_step_down():
    return IO.input(bottom_switch) == 1

def read_bottom_switch():
    return IO.input(bottom_switch)

def stepDWN():
    IO.output(dir_pin,0)

def stepUP():
    IO.output(dir_pin, 1)

def motorStep():
    IO.output(step_pin,1)
    time.sleep(0.0015)
    IO.output(step_pin,0)
    time.sleep(.002)


def get_position():
    global position
    return position

def motor_reset():
    for x in range (0, max_steps):
        if can_step_up():
            motor_up()
        else:
            break
    global position
    position = 0

def motor_up():
    stepUP()
    motorStep()
    global position
    position = position - step_distance
    
def motor_down():
    stepDWN()
    motorStep()
    global position
    position = position + step_distance

def cmd_none():
    global motor_action
    motor_action = MotorAction.none

def cmd_down(distance):
    global motor_action
    motor_action = MotorAction.down
    global cmd_distance
    cmd_distance = distance
    
def cmd_up(distance):
    global motor_action
    motor_action = MotorAction.up
    global cmd_distance
    cmd_distance = distance

def cmd_measure():
    global motor_action
    motor_action = MotorAction.measure
   
def cmd_reset():
    global motor_action
    motor_action = MotorAction.reset

def get_cmd_action():
    global motor_action
    return motor_action


def motor_thread():
    reset_us_distance = float(0)
    while(True):
        global motor_action
        global cmd_distance
        global measured_moisture
        if (motor_action == MotorAction.reset):
            motor_reset()
            motor_action = MotorAction.none
            
        elif (motor_action == MotorAction.up):
           # print "up"
            if can_step_up():
                motor_up()
                cmd_distance = cmd_distance - step_distance
                if cmd_distance <= 0:
                    motor_action = MotorAction.none
            else:
                motor_action = MotorAction.none
                
        elif (motor_action == MotorAction.down):
            if can_step_down():
                motor_down()
                cmd_distance = cmd_distance - step_distance
                if cmd_distance <= 0:
                    motor_action = MotorAction.none
            else:
                motor_action = MotorAction.none
                
        elif (motor_action == MotorAction.measure):
            measured_moisture = 0
            motor_reset()
            reset_us_distance = ultrasonic_sensor.get_distance()
            motor_action = MotorAction.measuring
            
        elif (motor_action == MotorAction.measuring):
            current_us_distance = ultrasonic_sensor.get_distance()
            if reset_us_distance - current_us_distance > moving_threshold:
                measure_now = 0
                #ultrasonic_sensor.cmd_print()
                is_moving = ultrasonic_sensor.get_is_moving_down()
                if is_moving:
                    if can_step_down():
                        motor_down()
                    else:
                        measure_now = 1
                else:
                    measure_now = 1
                if measure_now == 1:
                    time.sleep(1)
                    measured_moisture = moisture_sensor.get_moisture()
                    motor_action = MotorAction.none
            else:
                if can_step_down():
                    motor_down()
                else:
                    motor_action = MotorAction.none


        
    

thread.start_new_thread(motor_thread, ())

