import RPi.GPIO as IO
import time
import array
import thread

IO.setmode(IO.BCM)

Trig = 23
Echo = 24

IO.setup(Trig, IO.OUT)
IO.setup(Echo, IO.IN)

IO.output(Trig, 0)

distance = float(0)
is_moving_down = 0

moving_threshold = float(7.5)
measurements = array.array('f')
num_meas = 0
measurement_loop = 3
max_time = 0.006
can_print = 0

time.sleep(0.1)

def cmd_print():
    global can_print
    can_print = 1

def print_measurements():
    print "Measurements: "
    for x in range (0, num_meas):
        print str(x) + " " + str(measurements[x])

def get_is_moving_down():
    global is_moving_down
    return is_moving_down

def calc_is_moving_down():
    value = 0
    if num_meas > 1:
        for x in range(0,num_meas):
            if measurements[x] > moving_threshold:
                value = 1
                break
    print "v: " + str(value)
    print_measurements()
    return value

def get_distance():
    global distance
    return distance


print "Starting Measurement.."
def readDistance():
    start = time.time()
    start0 = start
    stop = time.time()
    
    IO.output(Trig, 1)
    time.sleep(0.00001)
    IO.output(Trig, 0)
    
    while IO.input(Echo) == 0 and start - start0 < max_time:
        start = time.time()

    while IO.input(Echo) == 1 and stop - start < max_time:
        stop = time.time()
        
    pulse_duration = stop - start
    distance = pulse_duration * 17150
    #distance = round(distance, 2)
    return distance


def calc_average(data):
    measurements.append(data)
    global num_meas
    num_meas = num_meas + 1
    if (num_meas > measurement_loop):
        measurements.pop(0)
        num_meas = num_meas - 1

    measurement_sum = float(0)    
    for x in range(0, num_meas):
        measurement_sum = measurement_sum + measurements[x]
    average = float(measurement_sum / num_meas)  
    return average


def distance_thread():
    while(True):
        global distance
        global is_moving_down
        global can_print
        data = readDistance()
        if (data < 30 and data > 0):
            previous_distance = distance
            distance = calc_average(data)
            is_moving_down = calc_is_moving_down()
            if is_moving_down == 0:
                if can_print == 1:
                    print_measurements()
            #is_moving_down = measurements[0] - measurements[num_meas - 1] > moving_threshold
            #is_moving_down = previous_distance - distance > moving_threshold
        time.sleep(0.5)
    IO.cleanup()

print "before thread D"
thread.start_new_thread(distance_thread, ())
print "after thread"




    

