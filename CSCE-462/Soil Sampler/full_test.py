import RPi.GPIO as IO
IO.setwarnings(False) 
import time
import ultrasonic_sensor
import moisture_sensor
import stepper_motor


def check_motor_cmd_done():
    while(True):
        current_motor_action = stepper_motor.get_cmd_action()
        distance = float(ultrasonic_sensor.get_distance())
        moisture = float(moisture_sensor.get_moisture())
        motor_position = float(stepper_motor.get_position())
        is_moving = ultrasonic_sensor.get_is_moving_down()
        #measured moisture will be 0.0 until it reaches dirt
        #you can send this to the app 
        measured_moisture = stepper_motor.get_measured_moisture()
        print "Moisture: " + str(moisture) + " " + str(measured_moisture) + " US Distance: " + str(distance) + "Is Moving: "  + str(is_moving) + " Motor Position: " + str(motor_position)

        if (current_motor_action == stepper_motor.MotorAction.none):
            break    
        time.sleep(1) 

def test_motor():
    # lowers the moisture spike
    print "starting down"
    stepper_motor.cmd_down(10)
    check_motor_cmd_done()
    print "starting reset"
    stepper_motor.cmd_reset()
    check_motor_cmd_done()
    print "starting down"
    stepper_motor.cmd_down(10)
    check_motor_cmd_done()
    print "starting up"
    stepper_motor.cmd_up(20)
    check_motor_cmd_done()
    print "starting down"
    stepper_motor.cmd_down(50)
    check_motor_cmd_done()
    print "starting up"
    stepper_motor.cmd_up(5)
    check_motor_cmd_done()



def mainloop():
    print "measure"
    stepper_motor.cmd_measure()
    check_motor_cmd_done()
    
    
    # raises moisture spike up
        
#main
#test_motor()
mainloop()

