package edu.tamu.jwstone.soilsamplerapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class BluetoothSerialService {

    private static final UUID SerialPortServiceClass_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private final BluetoothAdapter mAdapter;
    private final Handler mHandler;
    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;
    private int mState;

    public static final int STATE_NONE = 0;
    public static final int STATE_LISTEN = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_CONNECTED = 3;


    private Context mContext;

    public BluetoothSerialService(Context context, Handler handler) {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mState = STATE_NONE;
        mHandler = handler;
        mContext = context;
    }

    private synchronized void setState(int state) {
        mState = state;

        mHandler.obtainMessage(1, state, -1).sendToTarget();
    }

    public synchronized int getState() { return mState; }

    public synchronized void start() {
        if(mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if(mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_NONE);
    }

    public synchronized void connect(BluetoothDevice device) {
        if(mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread.cancel();
                mConnectThread = null;
            }
        }

        if(mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
        setState(STATE_CONNECTING);
    }

    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
        if(mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if(mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();

        Message msg = mHandler.obtainMessage(4);
        Bundle bundle = new Bundle();
        bundle.putString("Device", device.getName());
        msg.setData(bundle);
        mHandler.sendMessage(msg);

        setState(STATE_CONNECTED);
    }

    public synchronized void stop() {
        if(mConnectThread != null) {
            mConnectThread.cancel();
            mConnectThread = null;
        }

        if(mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_NONE);
    }

    public void write(byte[] out) {
        ConnectedThread tmp;
        synchronized(this) {
            if(mState != STATE_CONNECTED)
                return;
            tmp = mConnectedThread;
        }
        tmp.write(out);
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            mmDevice = device;
            BluetoothSocket tmp = null;

            try {
                tmp = device.createRfcommSocketToServiceRecord(SerialPortServiceClass_UUID);
            }
            catch(Exception e) {
                String message = "Error creating socket";
                mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();
                //e.printStackTrace();
            }
            String message = "Created socket";
            mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();

            mmSocket = tmp;
        }

        public void run() {
            setName("ConnectThread");

            mAdapter.cancelDiscovery();

            try {
                mmSocket.connect();
            }
            catch(IOException e) {
                String message = "Connection failed.";
                mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();
                e.printStackTrace();
                try {
                    mmSocket.close();
                }
                catch(IOException e2) {
                    message = "Unable to close socket.";
                    mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();
                    e2.printStackTrace();
                }
                return;
            }
            String message = "Connected to device";
            mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();
            synchronized (BluetoothSerialService.this) {
                mConnectThread = null;
            }

            connected(mmSocket, mmDevice);
        }

        public void cancel() {
            try {
                mmSocket.close();
            }
            catch(IOException e) {
                String message = "Unable to close socket.";
                mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();
                e.printStackTrace();
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            }
            catch(IOException e) {
                String message = "Error getting I/O stream";
                mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();
                e.printStackTrace();
            }

            String message = "Acquired I/O Streams";
            mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];
            int bytes;

            while(true) {
                try {
                    bytes = mmInStream.read(buffer);

                    //send the buffer and bytes back to Homepage
                    mHandler.obtainMessage(2, bytes, -1, buffer).sendToTarget();
                }
                catch(IOException e) {
                    String message = "Connection lost";
                    mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();
                    e.printStackTrace();
                    break;
                }
            }
        }

        public void write(byte[] buffer) {
            try {
                mmOutStream.write(buffer);

                //maybe send ACK back to Homepage
                mHandler.obtainMessage(3, buffer.length, -1, buffer).sendToTarget();
            }
            catch(IOException e) {
                String message = "Error during write";
                mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();
                e.printStackTrace();
            }
        }

        public void cancel() {
            try {
                mmSocket.close();
            }
            catch(IOException e) {
                String message = "Error closing socket";
                mHandler.obtainMessage(5,message.length(), -1, message).sendToTarget();
                e.printStackTrace();
            }
        }
    }
}
