package edu.tamu.jwstone.soilsamplerapp;

/**
 * Created by Joe on 11/28/2017.
 */

public class MessageBuff {

    private int depth;
    private int count;
    private String[] messages;

    public MessageBuff(int depth) {
        this.depth = depth;
        messages = new String[depth];
        count = 0;
    }

    public void addMessage(String message) {
        String messageToAdd = message;
        if(message.length() > 49) {
            messageToAdd = message.substring(0,49);
        }

        if(count == 0) {
            messages[0] = messageToAdd;
            ++count;
        }
        else if(count < depth-1) {
            for(int i = count+1; i > 0; i--) {
                messages[i] = messages[i-1];
            }
            messages[0] = messageToAdd;
            ++count;
        }
        else {
            for(int i = depth-1; i > 0; i--) {
                messages[i] = messages[i-1];
            }
            messages[0] = messageToAdd;
        }
    }

    public String[] getMessageBuff() { return messages; }

    public int getCount() { return count; }
}
