package edu.tamu.jwstone.soilsamplerapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.os.ParcelUuid;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class Homepage extends AppCompatActivity {

    private BluetoothSerialService mSerialService;
    private BluetoothAdapter mBluetoothAdapter;

    int count = 0;
    Integer[] results = new Integer[4];
    byte[] readBuf;

    MessageBuff homepageLog = new MessageBuff(16);
    MessageBuff prevResultsIndex = new MessageBuff(22);

    ArrayList<String> previousResultsList = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        System.out.println("Initializing app.");

        /* Commented out bluetooth crap until I take another look at it */
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter == null) {
            System.out.println("Bluetooth disabled");
            return;
        }
        BluetoothDevice device0 = (BluetoothDevice) mBluetoothAdapter.getBondedDevices().toArray()[0];
        System.out.println(device0.getAddress());
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(device0.getAddress());

        mSerialService = new BluetoothSerialService(this, mHandlerBT);
        mSerialService.start();
        mSerialService.connect(device);
    }

    private final Handler mHandlerBT = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case 2:
                    readBuf = (byte[]) msg.obj;

                    homepageLog.addMessage("Received data from rpi");
                    updateLog();

                    String rec = new String(readBuf);

                    String formatted = "";
                    for(char c : rec.toCharArray()) {
                        if(c > 47 && c < 58) {
                            formatted += c;
                        }
                        else if(c > 64 && c < 91) {
                            formatted += c;
                        }
                        else if(c > 96 && c < 123) {
                            formatted += c;
                        }
                    }
                    rec = formatted;

                    if(count < 4) {
                        results[count] = Integer.parseInt(rec);
                    }
                    else {
                        homepageLog.addMessage("Finished soil analysis");
                        updateLog();
                    }
                    ++count;
                    //readBuf now holds whatever rpi sent
                    break;
                case 3:
                    //receive ACK that we sent bluetooth something
                    break;
                case 5:
                    String updLog = (String) msg.obj;
                    homepageLog.addMessage(updLog);
                    updateLog();
                    break;
            }
        }
    };

    private void updateLog() {
        String log = "";
        int count = 0;
        for(String temp : homepageLog.getMessageBuff()) {
            log += temp + "\n";
            if(++count == homepageLog.getCount()) {
                break;
            }
        }
        TextView update = (TextView) findViewById(R.id.logText);
        update.setText(log);
    }

    public void startCommunication(View view) {

        mSerialService.write("Start".getBytes());
    }

    public void graphDisplay(View view) {
        setContentView(R.layout.graph_positions);

        TextView updateP1 = (TextView) findViewById(R.id.p1Text);
        TextView updateP2 = (TextView) findViewById(R.id.p2Text);
        TextView updateP3 = (TextView) findViewById(R.id.p3Text);
        TextView updateP4 = (TextView) findViewById(R.id.p4Text);
        updateP1.setText("P1 - " + results[0] + "%");
        updateP2.setText("P2 - " + results[1] + "%");
        updateP3.setText("P3 - " + results[2] + "%");
        updateP4.setText("P4 - " + results[3] + "%");

        ProgressBar p1 = (ProgressBar) findViewById(R.id.barP1);
        ProgressBar p2 = (ProgressBar) findViewById(R.id.barP2);
        ProgressBar p3 = (ProgressBar) findViewById(R.id.barP3);
        ProgressBar p4 = (ProgressBar) findViewById(R.id.barP4);
        p1.setProgress(results[0]);
        p2.setProgress(results[1]);
        p3.setProgress(results[2]);
        p4.setProgress(results[3]);
    }

    public void returnBack(View view) {
        setContentView(R.layout.activity_homepage);
        updateLog();
    }
}
