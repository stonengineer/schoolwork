import spidev
import smtplib
from time import sleep
import numpy as np
import array
import thread



#Open spi bus
spi = spidev.SpiDev()
spi.open(0,0) #1st parm is channel
    
measurements = array.array('f')
num_meas = 0

moisture_loop = 100
moisture = float(0)





def get_moisture():
    global moisture
    return moisture


def readChannel(chan):
    adc = spi.xfer2([1,(8+chan)<<4,0]) #sends 3 bytes:1
    
    data = ((adc[1]&3)  << 8) + adc[2]
    data = data - 1023
    if (data != 0):
        data = data * -1
    percent = data/10.24  
    return percent
    #print("ADC Output: {0:4d}   Percentage: {1:3}%".format (data, percent))
    #return percent
    #sleep(0.1)

def calc_average():
    data = readChannel(0)
    measurements.append(data)
    global num_meas
    num_meas = num_meas + 1
    if (num_meas > moisture_loop):
        measurements.pop(0)
        num_meas = num_meas - 1

    moisture_sum = float(0)    
    for x in range(0, num_meas):
        moisture_sum = moisture_sum + measurements[x]
    average_moisture = float(moisture_sum / num_meas)  
    return average_moisture

def moisture_thread():
    while(True):
        global moisture
        moisture = calc_average()
        sleep(0.01)

print "before thread"
thread.start_new_thread(moisture_thread, ())
print "after thread"

    

