from bluetooth import *
import threading
import time

class ServerThread(threading.Thread):
    def run(self):
        global sendValue
        sendValue = '-1'
        server_sock=BluetoothSocket( RFCOMM )
        server_sock.bind(("",PORT_ANY))
        server_sock.listen(1)

        port = server_sock.getsockname()[1]

        uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

        advertise_service( server_sock, "SampleServer",
                           service_id = uuid,
                           service_classes = [ uuid, SERIAL_PORT_CLASS ],
                           profiles = [ SERIAL_PORT_PROFILE ], 
        #                   protocols = [ OBEX_UUID ] 
                            )
                           
        print "Waiting for connection on RFCOMM channel %d" % port

        client_sock, client_info = server_sock.accept()
        print "Accepted connection from ", client_info

        try:
            while True:
                data = client_sock.recv(1024)
                if len(data) == 0: break
                if data == 'Start':
                    DataThread().start()
                    count = 0
                    while count < 5:
                        if sendValue != '-1':
                            client_sock.send(str(sendValue))
                            sendValue = '-1'
                            count = count + 1
        except IOError:
            pass

        print "disconnected"

        client_sock.close()
        server_sock.close()
        print "all done"

class DataThread(threading.Thread):
    def run(self):
        global sendValue
        sendValue = '10'
        time.sleep(1)
        sendValue = '25'
        time.sleep(1)
        sendValue = '65'
        time.sleep(1)
        sendValue = '50'
        time.sleep(1)
        sendValue = 'Done'

ServerThread().start()
