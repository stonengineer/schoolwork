#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

int red2 = 16;
int blue2 = 22;
int green2 = 18;
int red1 = 32;
int blue1 = 38;
int green1 = 36;
int pin_b = 40;
int seg_A = 37;
int seg_B = 33;
int seg_C = 31;
int seg_D = 29;
int seg_E = 15;
int seg_F = 13; 
int seg_G1 = 24;
int seg_G2 = 26;
int gnd_16 = 21;
int gnd_11 = 19;

// set up lights
pinMode(red2, OUTPUT);
pinMode(blue2, OUTPUT);
pinMode(green2, OUTPUT);
pinMode(red1,OUTPUT);
pinMode(blue1,OUTPUT);
pinMode(green1,OUTPUT);
pinMode(pin_b,INPUT);
pinMode(seg_A,OUTPUT);
pinMode(seg_B,OUTPUT);
pinMode(seg_C,OUTPUT);
pinMode(seg_D,OUTPUT);
pinMode(seg_E,OUTPUT);
pinMode(seg_F,OUTPUT);
pinMode(gnd_16,OUTPUT);
pinMode(gnd_11,OUTPUT);

void zero()
{
digitalWrite(seg_A,1);
digitalWrite(seg_B,1);
digitalWrite(seg_C,1);
digitalWrite(seg_D,1);
digitalWrite(seg_E,1);
digitalWrite(seg_F,1);
digitalWrite(gnd_16,0);
digitalWrite(gnd_11,0);
}

void ten()
{
     for (int i = 0; i < 100; i=i+1)
     {
          digitalWrite(seg_A,0);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,1);
	  digitalWrite(seg_D,0);
	  digitalWrite(seg_E,0);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,0);
	  digitalWrite(seg_G2,0);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50);
	  zero();
          delay(50);
     }
}

void eleven()
{
     for (int i = 0; i < 100; i=i+1)
     {
          digitalWrite(seg_A,0);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,1);
	  digitalWrite(seg_D,0);
	  digitalWrite(seg_E,0);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,0);
	  digitalWrite(seg_G2,0);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50);
	  one();
          delay(50);
     }
}

void twelve()
{
     for (int i = 0; i < 100; i=i+1)
     {
          digitalWrite(seg_A,0);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,1);
	  digitalWrite(seg_D,0);
	  digitalWrite(seg_E,0);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,0);
	  digitalWrite(seg_G2,0);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50);
	  two();
          delay(50);
     }
}

void thirteen()
{
     for (int i = 0; i < 100; i=i+1)
     {
          digitalWrite(seg_A,0);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,1);
	  digitalWrite(seg_D,0);
	  digitalWrite(seg_E,0);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,0);
	  digitalWrite(seg_G2,0);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50);
	  three();
          delay(50);
     }
}

void fourteen()
{
     for (int i = 0; i < 100; i=i+1)
     {
          digitalWrite(seg_A,0);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,1);
	  digitalWrite(seg_D,0);
	  digitalWrite(seg_E,0);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,0);
	  digitalWrite(seg_G2,0);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50);
	  four();
          delay(50);
     }
}

void fifteen()
{
     for (int i = 0; i < 100; i=i+1)
     {
          digitalWrite(seg_A,0);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,1);
	  digitalWrite(seg_D,0);
	  digitalWrite(seg_E,0);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,0);
	  digitalWrite(seg_G2,0);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50);
	  five();
          delay(50);
     }
}

void sixteen()
{
     for (int i = 0; i < 100; i=i+1)
     {
          digitalWrite(seg_A,0);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,1);
	  digitalWrite(seg_D,0);
	  digitalWrite(seg_E,0);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,0);
	  digitalWrite(seg_G2,0);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50);
	  six();
          delay(50);
     }
}

void seventeen()
{
     for (int i = 0; i < 100; i=i+1)
     {
          digitalWrite(seg_A,0);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,1);
	  digitalWrite(seg_D,0);
	  digitalWrite(seg_E,0);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,0);
	  digitalWrite(seg_G2,0);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50);
	  seven();
          delay(50);
     }
}

void eighteen()
{
     for (int i = 0; i < 100; i=i+1)
     {
          digitalWrite(seg_A,0);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,1);
	  digitalWrite(seg_D,0);
	  digitalWrite(seg_E,0);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,0);
	  digitalWrite(seg_G2,0);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50);
	  eight();
          delay(50);
     }
}

void nineteen()
{
     for (int i = 0; i < 100; i=i+1)
     {
	  digitalWrite(seg_A,0);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,1);
	  digitalWrite(seg_D,0);
	  digitalWrite(seg_E,0);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,0);
	  digitalWrite(seg_G2,0);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50);
	  nine();
          delay(50);
     }
}


void twenty()
{
     for (int i = 0; i < 100; i=i+1)
     {
	  digitalWrite(seg_A,1);
	  digitalWrite(seg_B,1);
	  digitalWrite(seg_C,0);
	  digitalWrite(seg_D,1);
	  digitalWrite(seg_E,1);
	  digitalWrite(seg_F,0);
	  digitalWrite(seg_G1,1);
	  digitalWrite(seg_G2,1);
	  digitalWrite(gnd_16,0);
	  digitalWrite(gnd_11,1);
	  delay(50)
	  zero();
          delay(50);	
     }

}

void step1(int green, int red)
{
digitalWrite(green,1);
digitalWrite(red,1);
}

void step2(int green2, int blue2)
{
  digitalWrite(green2,0);
  int i;
  for(i=0; i<10; i=i+1)
  {
      
    digitalWrite(blue2,1);
    delay(1000);
    digitalWrite(blue2,0);
    delay(1000);
  }
  
}

void step3(int red2, int red1, int green1, int blue1)
{
  digitalWrite(red2,1);
  digitalWrite(red1,0);
  digitalWrite(green1,1);
  delay(10000); // wait 10 sec
  digitalWrite(green1,0);
  int i;
  for(i=0; i<10; i=i+1)
  {
    digitalWrite(blue1,1);
    delay(500);
    digitalWrite(blue1,0);
    delay(500);
  }
}

void step4(int red2, int red1, int green)
{
  digitalWrite(red1,1);
  digitalWrite(red2,0);
  digitalWrite(green,1);
  delay(30000); // wait 30 sec
} 
  

int main(void){

if(wiringPiSetupPhys() == -1){
	exit (1);
}

while(1){

  if(digitalRead(pin_b) == 0)
  {
     step1(green2, red1);
    
     if(digitalRead(pin_b) == 1)
     {
	step2(green2, blue2);
	step3(red2, red1, green1, blue1);
	step4(red2, red1, green2);
	
        digitalWrite(red2,0);
        digitalWrite(blue2,0);
        digitalWrite(green2,0);
        digitalWrite(red1,0);
        digitalWrite(blue1,0);
        digitalWrite(green1,0);
        delay(20000);
     }
	
  }
}
return 0;
}
