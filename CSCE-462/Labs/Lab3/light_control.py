import time
import RPi.GPIO as IO

IO.setwarnings(False)

#7segment pinout
top = 4
top_left = 5
bot_left = 6
bot = 7
bot_right = 8
top_right = 9
mid = 10

#+10LED
LED_10 = 19

#LED pinout
l1_green = 11
l1_yello = 12
l1_red = 13
l2_green = 14
l2_yello = 16
l2_red = 17

#Button pinout
button = 18

#7segment number definitions
display=[
    [1,1,1,1,1,1,0],  #0
    [0,0,0,0,1,1,0],  #1
    [1,0,1,1,0,1,1],  #2
    [1,0,0,1,1,1,1],  #3
    [0,1,0,0,1,1,1],  #4
    [1,1,0,1,1,0,1],  #5
    [1,1,1,1,1,0,1],  #6
    [1,0,0,0,1,1,0],  #7
    [1,1,1,1,1,1,1],  #8
    [1,1,0,1,1,1,1]]  #9

#setup pins for I/O
IO.setmode(IO.BCM)
IO.setup(top, IO.OUT, initial=IO.LOW)
IO.setup(top_left, IO.OUT, initial=IO.LOW)
IO.setup(bot_left, IO.OUT, initial=IO.LOW)
IO.setup(bot, IO.OUT, initial=IO.LOW)
IO.setup(bot_right, IO.OUT, initial=IO.LOW)
IO.setup(top_right, IO.OUT, initial=IO.LOW)
IO.setup(mid, IO.OUT, initial=IO.LOW)
IO.setup(l1_green, IO.OUT, initial=IO.LOW)
IO.setup(l1_yello, IO.OUT, initial=IO.LOW)
IO.setup(l1_red, IO.OUT, initial=IO.HIGH)
IO.setup(l2_green, IO.OUT, initial=IO.HIGH)
IO.setup(l2_yello, IO.OUT, initial=IO.LOW)
IO.setup(l2_red, IO.OUT, initial=IO.LOW)
IO.setup(button, IO.IN, pull_up_down=IO.PUD_UP)
IO.setup(LED_10, IO.OUT, initial=IO.LOW)

while True:
    if IO.input(button) == 0:
        IO.output(l2_green, 0)
        i = 0
        while i < 10:
            if i%2 == 0:
                IO.output(l2_yello, 1)
            else:
                IO.output(l2_yello, 0)
            i += 1
            time.sleep(0.5)
        IO.output(l2_yello, 0)
        IO.output(l2_red, 1)
        IO.output(l1_red, 0)
        IO.output(l1_green, 1)
        n = 1
        while n >= 0:
            i = 9
            while i >= 0:
                IO.output(top, display[i][0])
                IO.output(top_left, display[i][1])
                IO.output(bot_left, display[i][2])
                IO.output(bot, display[i][3])
                IO.output(bot_right, display[i][4])
                IO.output(top_right, display[i][5])
                IO.output(mid, display[i][6])
                if n == 1:
                    IO.output(LED_10,1)
                    time.sleep(1)
                else:
                    IO.output(LED_10,0)
                    IO.output(l1_green,0)
                    IO.output(l1_yello, 1)
                    time.sleep(0.5)
                    IO.output(l1_yello, 0)
                    time.sleep(0.5)
                i -= 1
            n -= 1
        IO.output(l1_yello, 0)
        IO.output(l1_red,1)
        IO.output(l2_red,0)
        IO.output(l2_green, 1)
        IO.output(top, 0)
        IO.output(top_left, 0)
        IO.output(bot_left, 0)
        IO.output(bot, 0)
        IO.output(bot_right, 0)
        IO.output(top_right, 0)
        IO.output(mid, 0)
