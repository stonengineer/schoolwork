//includes
#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

#define BUZZER 7

void square_wave(double MHz) {
  double period = 1.0 / MHz;
  double us = period;// / 2.0;
  while(1) {
    digitalWrite(BUZZER, 1);
    delayMicroseconds(us);
    digitalWrite(BUZZER, 0);
    delayMicroseconds(us);
  }
}

void triangle_wave(double MHz) {
  double period = 1.0 / MHz;
  double us = period;// / 2.0;
  while(1) {
    int i;
    for(i = 1; i <= 20; i++) {
      digitalWrite(BUZZER, (i/20.0));
      delayMicroseconds(us/20.0);
    }
    for(i = 19; i >= 0 ; i--) {
      digitalWrite(BUZZER, (i/20.0));
      delayMicroseconds(us/20.0);
    }
  }
}

int main(void) {
  if(wiringPiSetupPhys() == -1) { return 1; }

  pinMode(BUZZER, OUTPUT);

  triangle_wave(0.001);

  return 0;
}
  