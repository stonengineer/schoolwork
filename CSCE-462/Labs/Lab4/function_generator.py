import time
import RPi.GPIO as IO
import math
import Adafruit_MCP4725
IO.setwarnings(False)

dac = Adafruit_MCP4725.MCP4725()

#BUTTON pin
button = 8

IO.setmode(IO.BOARD)
IO.setup(button, IO.IN, pull_up_down=IO.PUD_UP)

def sine_wave(freq, max_vol):
    t = 0.0
    tstep = 0.05
    while True:
        newfreq = freq / (2.0*3.1415)
        voltage = ((1241*max_vol)/2)*(1.5+.5*math.sin(newfreq*t))
        dac.set_voltage(int(voltage))
        t += tstep
        time.sleep(0.0005)
        if(IO.input(button) == 0):
            return
        

def square_wave(freq, max_vol):
  period = 1 / freq
  half_cycle = float(period)/2
  while True:
    dac.set_voltage(int(max_vol * 1241))
    time.sleep(half_cycle)
    dac.set_voltage(int(0))
    time.sleep(half_cycle)
    if(IO.input(button) == 0):
            return
      
def triangle_wave(freq,max_vol):
    voltage = 0
    while True:
      while voltage < 1241*max_vol:
          voltage += freq*11.5
          dac.set_voltage(int(voltage))
          time.sleep(0.0005)
      while voltage != 0:
          voltage -= freq*11.5
          dac.set_voltage(int(voltage))
          time.sleep(0.0005)
      if(IO.input(button) == 0):
          return

while True:
    if IO.input(button) == 0:
        shape = raw_input('Shape of waveform: ')
        freq = raw_input('Frequency: ')
        vmax = raw_input('Max Output Voltage: ')
        if shape == "sine":
            sine_wave(float(freq), float(vmax))
        elif shape == "square":
            square_wave(float(freq), float(vmax))
        elif shape == "triangle":
            triangle_wave(float(freq), float(vmax))
        else:
            print('Shapes are: sine square triangle')
