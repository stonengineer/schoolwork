#working
import time
import Adafruit_MCP4725
import math
import RPi.GPIO as IO


#create a DAC instance.


def sin_wave():
  t = 0.0
  tstep = 0.05
  while True:
    voltage = 2048*(1+0.5*math.sin(6.2832*t))
    dac.set_voltage(int(voltage))
    t += tstep
    time.sleep(0.0005)
    
def s_wave(freq, max_vol):
    t = 0.0
    tstep = 0.05
    while True:
        #x = max_vol/2048 - 2048
        newfreq = freq / (2.0*3.1415)
        #freq1 = freq*37.5
        voltage = ((1241*max_vol)/2)*(1.5+.5*math.sin(newfreq*t))
        #voltage = 2048*(1+0.5*math.sin(newfreq*t))
        dac.set_voltage(int(voltage))
        t += tstep
        time.sleep(0.0005)
        

def square_wave(freq, max_vol):
  period = 1 / freq
  half_cycle = float(period)/2
  while True:
    dac.set_voltage(int(max_vol * 1241))
    time.sleep(half_cycle)
    dac.set_voltage(int(0))
    time.sleep(half_cycle)
    #time.sleep(0.0005)
      
def tri2_wave(freq,max_vol):
    voltage = 0
    while True:
      while voltage < 1241*max_vol:
        dac.set_voltage(int(voltage))
        voltage = voltage + freq*11.5
        time.sleep(0.0005)
      while voltage != 0:
        dac.set_voltage(int(voltage))
        voltage = voltage - freq*11.5
        time.sleep(0.0005)
        
        
                
dac = Adafruit_MCP4725.MCP4725()
IO.setmode(IO.BCM)
IO.setup(12, IO.IN)
while True:
    
    #if IO.input(12) == 1:
        while True:
            print('Press Ctrl-C to quit...')
            wave_type = input('Choose a number! 1. sine wave 2. square wave 3. triangular wave: ')
            frequency = input('type frequency: ')
            max_voltage = input('type max output voltage: ')

            if wave_type == 1:
               s_wave(float(frequency), float(max_voltage))
            elif wave_type == 2:
               square_wave(float(frequency), float(max_voltage))
            elif wave_type == 3:
               #tri2_wave(float(frequency), float(max_voltage))
               tri2_wave(frequency, max_voltage)
            




    

 
