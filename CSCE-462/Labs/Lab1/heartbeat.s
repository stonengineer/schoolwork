.data
	.balign 4
	running_msg: .asciz "Running heartbeat proc\n" @print to confirm we're doing something
	pin: .int 7 @pin that turns LED on/off
	delay_time: .int 200 @time between turning LED on/off

.text
.global main
.extern printf
.extern wiringPiSetup
.extern delay
.extern digitalWrite
.extern pinMode

main: 
	push {ip, lr} @push return address + dummy register
	ldr r0, =running_msg
	bl printf @print running message

	bl wiringPiSetup @load wiring pi setup function
	b init @jump to initialize function

init:
	ldr r0, =pin @set register 0 to handle pin 7
	ldr r0, [r0]
	mov r1, #1 @turn pin 7 'on'
	bl pinMode @load pin module function

turnOnLoop:
	@turn pin 7 to 'on'
	ldr r0, =pin
	ldr r0, [r0]
	mov r1, #1
	bl digitalWrite

	@delay 'delay_time' ms (200 by default)
	ldr r0, =delay_time
	ldr r0, [r0]
	bl delay

	@jump to off loop
	b turnOffLoop

turnOffLoop:
	@turn pin 7 to 'off'
	ldr r0, =pin
	ldr r0, [r0]
	mov r1, #0
	bl digitalWrite

	@delay 'delay_time' ms (200 by default)
	ldr r0, =delay_time
	ldr r0, [r0]
	bl delay

	@jump to on loop
	@I made it an infinite loop because I liked it better that way
	b turnOnLoop

done:
	pop {ip, pc} @finish

