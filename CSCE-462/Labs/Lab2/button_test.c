#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	wiringPiSetup();
	pinMode(1, INPUT);
	pullUpDnControl(1, PUD_UP);
	while(1) {
		printf("\rButton=%d", digitalRead(1));
		fflush(stdout);
		delay(1);
	}
	return 0;
}