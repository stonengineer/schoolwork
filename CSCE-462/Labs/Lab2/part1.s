	.arch armv6
	.eabi_attribute 28, 1
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 6
	.eabi_attribute 34, 1
	.eabi_attribute 18, 4
	.text
	.align	2
	.global	main
	.syntax unified
	.arm
	.fpu vfp
	.type	main, %function
main:
	.fnstart
.LFB6:
	push	{fp, lr}
	.save {fp, lr}
	.setfp fp, sp, #4
	add	fp, sp, #4
	.pad #56
	sub	sp, sp, #56
	mov	r3, #1
	str	r3, [fp, #-40]
	mov	r3, #3
	str	r3, [fp, #-44]
	mov	r3, #7
	str	r3, [fp, #-48]
	mov	r3, #12
	str	r3, [fp, #-52]
	bl	wiringPiSetup
	mov	r3, r0
	cmn	r3, #1
	moveq	r3, #1
	movne	r3, #0
	uxtb	r3, r3
	cmp	r3, #0
	beq	.L2
	mov	r0, #1
	bl	exit
.L2:
	mov	r1, #1
	ldr	r0, [fp, #-44]
	bl	pinMode
	mov	r1, #1
	ldr	r0, [fp, #-48]
	bl	pinMode
	mov	r1, #1
	ldr	r0, [fp, #-52]
	bl	pinMode
	mov	r1, #0
	ldr	r0, [fp, #-40]
	bl	pinMode
	mov	r1, #2
	ldr	r0, [fp, #-40]
	bl	pullUpDnControl
	mov	r3, #1
	str	r3, [fp, #-8]
	mov	r3, #0
	str	r3, [fp, #-12]
	mov	r3, #0
	str	r3, [fp, #-16]
	mov	r3, #1
	str	r3, [fp, #-20]
	mov	r3, #0
	str	r3, [fp, #-24]
	mov	r3, #0
	str	r3, [fp, #-28]
	mov	r3, #0
	str	r3, [fp, #-32]
	mov	r3, #1
	str	r3, [fp, #-36]
	mov	r2, #0
	ldr	r3, .L8+8
	strd	r2, [fp, #-60]
.L7:
	ldr	r1, [fp, #-8]
	ldr	r0, [fp, #-44]
	bl	digitalWrite
	ldr	r1, [fp, #-12]
	ldr	r0, [fp, #-48]
	bl	digitalWrite
	ldr	r1, [fp, #-16]
	ldr	r0, [fp, #-52]
	bl	digitalWrite
	ldr	r0, [fp, #-40]
	bl	digitalRead
	vmov	s15, r0	@ int
	vcvt.f64.s32	d7, s15
	vstr.64	d7, [fp, #-60]
	vldr.64	d7, [fp, #-60]
	vldr.64	d6, .L8
	vcmp.f64	d7, d6
	vmrs	APSR_nzcv, FPSCR
	beq	.L3
	ldr	r2, [fp, #-20]
	ldr	r3, [fp, #-8]
	eor	r3, r3, r2
	str	r3, [fp, #-20]
	ldr	r2, [fp, #-24]
	ldr	r3, [fp, #-12]
	eor	r3, r3, r2
	str	r3, [fp, #-24]
	ldr	r2, [fp, #-28]
	ldr	r3, [fp, #-16]
	eor	r3, r3, r2
	str	r3, [fp, #-28]
.L3:
	mov	r0, #1
	bl	delay
	ldr	r3, [fp, #-32]
	add	r3, r3, #1
	str	r3, [fp, #-32]
	ldr	r3, [fp, #-32]
	cmp	r3, #1000
	blt	.L7
	ldr	r3, [fp, #-36]
	cmp	r3, #0
	beq	.L5
	mov	r3, #0
	str	r3, [fp, #-16]
	ldr	r3, [fp, #-16]
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-12]
	str	r3, [fp, #-8]
	mov	r3, #0
	str	r3, [fp, #-36]
	b	.L6
.L5:
	ldr	r3, [fp, #-20]
	str	r3, [fp, #-8]
	ldr	r3, [fp, #-24]
	str	r3, [fp, #-12]
	ldr	r3, [fp, #-28]
	str	r3, [fp, #-16]
	mov	r3, #1
	str	r3, [fp, #-36]
.L6:
	mov	r3, #0
	str	r3, [fp, #-32]
	b	.L7
.L9:
	.align	3
.L8:
	.word	0
	.word	1072693248
	.word	-1074790400
	.fnend
	.size	main, .-main
	.ident	"GCC: (Raspbian 6.3.0-18+rpi1) 6.3.0 20170516"
	.section	.note.GNU-stack,"",%progbits
