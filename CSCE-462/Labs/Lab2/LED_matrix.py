import time
import RPi.GPIO as GPIO

GPIO.setwarnings(False)

#dip switch pin defines
DIP_1 = 17
DIP_2 = 18
DIP_3 = 27
DIP_4 = 22
DIP_5 = 23
DIP_6 = 24
DIP_7 = 25
DIP_8 = 4

#LED matrix pin defines
LED_1 = 10
LED_2 = 9
LED_3 = 11
LED_4 = 8
LED_5 = 7
LED_6 = 3
LED_7 = 14
LED_8 = 5
LED_9 = 6
LED_10 = 12
LED_11 = 13
LED_12 = 19
LED_13 = 16
LED_14 = 26
LED_15 = 20
LED_16 = 21

#LED matrix number definitions
digits=[[0b000,0b111,0b101,0b101,0b101,0b111,0b000,0b000], #0
[0b000,0b010,0b110,0b010,0b010,0b111,0b000,0b000], #1
[0b000,0b111,0b001,0b111,0b100,0b111,0b000,0b000], #2
[0b000,0b111,0b001,0b111,0b001,0b111,0b000,0b000], #3
[0b000,0b101,0b101,0b111,0b001,0b001,0b000,0b000], #4
[0b000,0b111,0b100,0b111,0b001,0b111,0b000,0b000], #5
[0b000,0b111,0b100,0b111,0b101,0b111,0b000,0b000], #6
[0b000,0b111,0b001,0b001,0b001,0b001,0b000,0b000], #7
[0b000,0b111,0b101,0b111,0b101,0b111,0b000,0b000], #8
[0b000,0b111,0b101,0b111,0b001,0b001,0b000,0b000], #9
[0b000,0b111,0b101,0b111,0b101,0b101,0b000,0b000], #A
[0b000,0b100,0b100,0b111,0b101,0b111,0b000,0b000], #B
[0b000,0b111,0b100,0b100,0b100,0b111,0b000,0b000], #C
[0b000,0b001,0b001,0b111,0b101,0b111,0b000,0b000], #D
[0b000,0b111,0b100,0b110,0b100,0b111,0b000,0b000], #E
[0b000,0b111,0b100,0b111,0b100,0b100,0b000,0b000]] #F

#setup pins for I/O
GPIO.setmode(GPIO.BCM)
GPIO.setup(DIP_1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(DIP_2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(DIP_3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(DIP_4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(DIP_5, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(DIP_6, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(DIP_7, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(DIP_8, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(LED_1, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_2, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_3, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_4, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_5, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_6, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_7, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_8, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_9, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_10, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_11, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_12, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_13, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_14, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_15, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(LED_16, GPIO.OUT, initial=GPIO.LOW)

print "1 2 3 4 5 6 7 8"

while True:
    #print GPIO.input(DIP_1),GPIO.input(DIP_2),GPIO.input(DIP_3),GPIO.input(DIP_4),GPIO.input(DIP_5),GPIO.input(DIP_6),GPIO.input(DIP_7),GPIO.input(DIP_8),"\r",
    curr_num = 0
    if GPIO.input(DIP_1) == 0:
        curr_num += 1
    if GPIO.input(DIP_2) == 0:
        curr_num += 2
    if GPIO.input(DIP_3) == 0:
        curr_num += 4
    if GPIO.input(DIP_4) == 0:
        curr_num += 8
    if GPIO.input(DIP_5) == 0:
        curr_num += 16
    if GPIO.input(DIP_6) == 0:
        curr_num += 32
    if GPIO.input(DIP_7) == 0:
        curr_num += 64
    if GPIO.input(DIP_8) == 0:
        curr_num += 128
    
    print hex(curr_num).split('x')[-1],"\r",
        
    left_digit = curr_num / 16
    right_digit = curr_num % 16
    
    #print first row
    GPIO.output(LED_9,1)
    GPIO.output(LED_13,digits[left_digit][0]&0b100)
    GPIO.output(LED_3,digits[left_digit][0]&0b010)
    GPIO.output(LED_4,digits[left_digit][0]&0b001)
    GPIO.output(LED_10,0)
    GPIO.output(LED_6,0)
    GPIO.output(LED_11,digits[right_digit][0]&0b100)
    GPIO.output(LED_15,digits[right_digit][0]&0b010)
    GPIO.output(LED_16,digits[right_digit][0]&0b001)
    GPIO.output(LED_9,0)
    
    #print second row
    GPIO.output(LED_14,1)
    GPIO.output(LED_13,digits[left_digit][1]&0b100)
    GPIO.output(LED_3,digits[left_digit][1]&0b010)
    GPIO.output(LED_4,digits[left_digit][1]&0b001)
    GPIO.output(LED_10,0)
    GPIO.output(LED_6,0)
    GPIO.output(LED_11,digits[right_digit][1]&0b100)
    GPIO.output(LED_15,digits[right_digit][1]&0b010)
    GPIO.output(LED_16,digits[right_digit][1]&0b001)
    GPIO.output(LED_14,0)
    
    #print third row
    GPIO.output(LED_8,1)
    GPIO.output(LED_13,digits[left_digit][2]&0b100)
    GPIO.output(LED_3,digits[left_digit][2]&0b010)
    GPIO.output(LED_4,digits[left_digit][2]&0b001)
    GPIO.output(LED_10,0)
    GPIO.output(LED_6,0)
    GPIO.output(LED_11,digits[right_digit][2]&0b100)
    GPIO.output(LED_15,digits[right_digit][2]&0b010)
    GPIO.output(LED_16,digits[right_digit][2]&0b001)
    GPIO.output(LED_8,0)
    
    #print fourth row
    GPIO.output(LED_12,1)
    GPIO.output(LED_13,digits[left_digit][3]&0b100)
    GPIO.output(LED_3,digits[left_digit][3]&0b010)
    GPIO.output(LED_4,digits[left_digit][3]&0b001)
    GPIO.output(LED_10,0)
    GPIO.output(LED_6,0)
    GPIO.output(LED_11,digits[right_digit][3]&0b100)
    GPIO.output(LED_15,digits[right_digit][3]&0b010)
    GPIO.output(LED_16,digits[right_digit][3]&0b001)
    GPIO.output(LED_12,0)
    
    #print fifth row
    GPIO.output(LED_1,1)
    GPIO.output(LED_13,digits[left_digit][4]&0b100)
    GPIO.output(LED_3,digits[left_digit][4]&0b010)
    GPIO.output(LED_4,digits[left_digit][4]&0b001)
    GPIO.output(LED_10,0)
    GPIO.output(LED_6,0)
    GPIO.output(LED_11,digits[right_digit][4]&0b100)
    GPIO.output(LED_15,digits[right_digit][4]&0b010)
    GPIO.output(LED_16,digits[right_digit][4]&0b001)
    GPIO.output(LED_1,0)
    
    #print sixth row
    GPIO.output(LED_7,1)
    GPIO.output(LED_13,digits[left_digit][5]&0b100)
    GPIO.output(LED_3,digits[left_digit][5]&0b010)
    GPIO.output(LED_4,digits[left_digit][5]&0b001)
    GPIO.output(LED_10,0)
    GPIO.output(LED_6,0)
    GPIO.output(LED_11,digits[right_digit][5]&0b100)
    GPIO.output(LED_15,digits[right_digit][5]&0b010)
    GPIO.output(LED_16,digits[right_digit][5]&0b001)
    GPIO.output(LED_7,0)
    
    #print seventh row
    GPIO.output(LED_2,1)
    GPIO.output(LED_13,digits[left_digit][6]&0b100)
    GPIO.output(LED_3,digits[left_digit][6]&0b010)
    GPIO.output(LED_4,digits[left_digit][6]&0b001)
    GPIO.output(LED_10,0)
    GPIO.output(LED_6,0)
    GPIO.output(LED_11,digits[right_digit][6]&0b100)
    GPIO.output(LED_15,digits[right_digit][6]&0b010)
    GPIO.output(LED_16,digits[right_digit][6]&0b001)
    GPIO.output(LED_2,0)
    
    #print eigth row
    GPIO.output(LED_5,1)
    GPIO.output(LED_13,digits[left_digit][7]&0b100)
    GPIO.output(LED_3,digits[left_digit][7]&0b010)
    GPIO.output(LED_4,digits[left_digit][7]&0b001)
    GPIO.output(LED_10,0)
    GPIO.output(LED_6,0)
    GPIO.output(LED_11,digits[right_digit][7]&0b100)
    GPIO.output(LED_15,digits[right_digit][7]&0b010)
    GPIO.output(LED_16,digits[right_digit][7]&0b001)
    GPIO.output(LED_5,0)
    
    time.sleep(0.0001)
