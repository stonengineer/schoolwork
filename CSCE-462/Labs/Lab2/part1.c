#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

int main(void) {
	int BUTT = 1;
	int LED1 = 3;
	int LED2 = 7;
	int LED3 = 12;

	if(wiringPiSetup() == -1) {
		exit(1);
	}

	pinMode(LED1, OUTPUT);
	pinMode(LED2, OUTPUT);
	pinMode(LED3, OUTPUT);
	pinMode(BUTT, INPUT);

	pullUpDnControl(BUTT, PUD_UP);

	int LED1_val = 1;
	int LED2_val = 0;
	int LED3_val = 0;

	int LED1_sav = 1;
	int LED2_sav = 0;
	int LED3_sav = 0;

	int timer = 0;
	int is_on = 1;
	double BUTT_val = -1;

	while(1) {
		digitalWrite(LED1, LED1_val);
		digitalWrite(LED2, LED2_val);
		digitalWrite(LED3, LED3_val);
		BUTT_val = digitalRead(BUTT);
		if(BUTT_val != HIGH) {
			LED1_sav ^= LED1_val;
			LED2_sav ^= LED2_val;
			LED3_sav ^= LED3_val;
		}
		delay(1);
		++timer;
		if(timer >= 1000) {
			if(is_on) {
				LED1_val = LED2_val = LED3_val = 0;
				is_on = 0;
			}
			else {
				LED1_val = LED1_sav;
				LED2_val = LED2_sav;
				LED3_val = LED3_sav;
				is_on = 1;
			}
			timer = 0;
		}
	}

	return 0;
}