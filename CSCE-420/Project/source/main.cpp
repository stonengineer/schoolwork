/*
 * File: main.cpp
 * 
 * Author: Joseph Stone
 *            422008041
 * 
 * Course: CSCE-420
 * 
 * Due: 23 April, 2018
 *
 * Premise: This program provides a terminal
 *          level GUI for the user to
 *          interface with. 
 *
*/

#include "network.h"
#include "alphabet.h"
#include <string>

AlphabetGenerator ag;

void main_menu(Network net);

/* waits for user to finish reading results
 * before quitting or returning to main menu */
void done(Network net) {
  cout << "Finished. Return to main menu? (y/n) ";
  std::string temp;
  std::getline(std::cin, temp);
  if(temp[0] == 'y' || temp[0] == 'Y') {
    cout << "\033[2J\033[1;1H";
    main_menu(net);
  }
  else {
    return;
  }
}

/* generates the letter guessed by an 
 * output of results based on the largest
 * index of the output vector             */
int guess(vector<double> output) {
  double largest = output[0];
  int index = 0;
  for(int i = 1; i < output.size(); i++) {
    if(output[i] > largest) {
      largest = output[i];
      index = i;
    }
  }
  return index;
}

/* passes each letter alphabetically
 * into neural network and prints the
 * network's guess for each letter    */
void system_test(Network net) {
  cout << "Running system test." << endl;

  int passed = 0;
  cout << "INP OUT CRT" << endl;
  for(int i = 0; i < 26; i++) {
    vector<double> input(35);
    for(int j = 0; j < 35; j++) {
      input[j] = ag.get_alphabet()[i][j];
    }

    vector<double> output = net.run(input);

    cout << " " << ((char) ('A' + i)) << "   ";
    int guessed = guess(output);
    int percent = (100.0*output[guessed]);
    cout << ((char) ('A' + guessed)) << "  " << percent << "%" << endl;

    passed += ((int)(guessed == i));
  }

  cout << "The network guessed " << passed << "/26 correctly, or " << (100.0* ((double)passed) / 26.0) << "%" << endl;

  done(net);
}

/* allows the user to type in a series of
 * hex numbers corresponding to a 7x5 letter
 * then has the network guess the letter     */
void input_test(Network net) {
  cout << "Running input test." << endl;

  unsigned char *letter = new unsigned char[5];
  for(int i = 0; i < 5; i++) {
    cout << "Enter a hexadecimal column: ";
    unsigned int temp;
    std::cin >> std::hex >> temp;
    letter[i] = ((char) temp);
    std::string clr_buff;
    std::getline(std::cin, clr_buff);
  }

  vector<float> input = ag.generate_letter(letter);
  vector<double> converted_input(35);
  for(int i = 0; i < 35; i++) {
    converted_input[i] = input[i];
  }
  vector<double> output = net.run(converted_input);

  cout << "Network guess: " << ((char) ('A' + guess(output))) << " with " << (output[guess(output)]*100.0) << "% accuracy" << endl;

  done(net);
}

/* true if the v contains e */
bool contains(vector<int> v, int e) {
  for(int i = 0; i < v.size(); i++) {
    if(v[i] == e) {
      return true;
    }
  }
  return false;
}

/* for each letter of the alphabet, runs
 * the neural network flipping one bit at
 * a time until the network guesses wrong,
 * then reports the number of bits flipped. */
void flip_test(Network net) {
  cout << "Running flip test." << endl;

  srand(time(NULL));

  vector<int> bits_flipped;
  for(int i = 0; i < 26; i++) {
    int count = 0;
    int letter = i;
    vector<double> input(35);
    while(letter == i) {
      input.clear();
      input.resize(35);
      for(int j = 0; j < 35; j++) {
        input[j] = ag.get_alphabet()[i][j];
      }

      vector<int> used_indexes;
      for(int j = 0; j < count; j++) {
        if(used_indexes.size() == 35) {
          cout << "Error in flip function." << endl;
          break;
        }
        int index = rand() % 35;
        while(contains(used_indexes, index)) {
          index = rand() % 35;
        }
        used_indexes.push_back(index);
        double new_input = (input[index] == 0.0 ? 1.0 : 0.0);
        input[index] = new_input;
      }

      vector<double> output = net.run(input);

      letter = guess(output);

      ++count;
    }

    cout << "It took " << count << " bits being flipped before" << endl;
    cout << "we stopped guessing " << ((char)('A'+i)) << "." << endl;
    cout << "Final pattern: ";
    for(int j = 0; j < 35; j++) {
      if((j%7) == 0) {
        cout << endl;
      }
      cout << input[j];
    }
    cout << endl << endl;

    bits_flipped.push_back(count);
  }

  cout << "Final results: " << endl;

  cout << "[";
  for(int i = 0; i < 26; i++) {
    cout << "  " << ((char)('A'+i));
  }
  cout << "]" << endl;

  cout << "[";
  for(int i = 0; i < bits_flipped.size(); i++) {
    printf(" %2d", bits_flipped[i]);
    //cout << " " << bits_flipped[i];
  }
  cout << "]" << endl;

  done(net);
}

/* allows the user to specify one of four actions */
void main_menu(Network net) {
  cout << "*****************************" << endl;
  cout << "*****************************" << endl;
  cout << "**        Main Menu        **" << endl;
  cout << "*****************************" << endl;
  cout << "** 1 - Run All Letters     **" << endl;
  cout << "** 2 - Input Char Sequence **" << endl;
  cout << "** 3 - Run Flip Test       **" << endl;
  cout << "** X - Exit                **" << endl;
  cout << "*****************************" << endl;
  cout << "*****************************" << endl;
  cout << "Enter your choice: ";

  std::string temp;
  std::getline(std::cin, temp);
  if(temp[0] == '1') {
    system_test(net);
  }
  else if(temp[0] == '2') {
    input_test(net);
  }
  else if(temp[0] == '3') {
    flip_test(net);
  }
  else {
    return;
  }
}

/* initializes the neural network, then calls main menu */
int main() {
  cout << "Welcome to the Neural Network Project!" << endl;
  cout.flush();

  double learning_rate = 0.19;

  Network net(learning_rate);

  cout << endl;

  main_menu(net);
}
