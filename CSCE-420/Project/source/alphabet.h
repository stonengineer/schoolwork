/*
 * File: alphabet.h
 * 
 * Author: Joseph Stone
 *            422008041
 * 
 * Course: CSCE-420
 * 
 * Due: 23 April, 2018
 *
 * Premise: This program reads and formats the 
 *          different inputs specified in font5x7.h
 *          and outputs them as a vector of floats
 *          to be inputted into the neural network.
 *          It also contains a function to generate
 *          letters based on a 5-length array of
 *          unsigned chars.
 *
*/

#ifndef _ALPHABET_H
#define _ALPHABET_H

#include "font5x7.h"
#include <vector>
#include <iostream>

class AlphabetGenerator {
private:
  const int start = 165; // the index of 'A' in font5x7.h
  const int end = 295; // the index after 'Z' in font5x7.h
  vector<vector<float>> alphabet; // holds the dot-alphabet

public:
  /* Constructor, initializes class */
  AlphabetGenerator() {
    // set alphabet to only hold 26 letters
    alphabet.resize(26);

    // read the unsigned chars from font5x7.h
    int index = start;
    for(int i = 0; i < 26; i++) {
      unsigned char *letter = new unsigned char[5];
      for(int j = 0; j < 5; j++) {
        unsigned char mask = 0x40;
        unsigned char column = Font5x7[index];
        letter[j] = column;
        ++index;
      }

      // custom function to create dot-matrix from uchar input
      alphabet[i] = generate_letter(letter);
    }
    //print_alphabet(); // used for testing
  }

  /* Custom function to create dot-matrixes
      I: length 5 array of unsigned chars
      O: dot-matrix representation of the letter */
  vector<float> generate_letter(unsigned char *letter) {
    vector<float> result;
    int index = 0;
    for(int j = 0; j < 5; j++) {
      unsigned char mask = 0x40;
      unsigned char column = letter[index];
      for(int k = 0; k < 7; k++) {
        result.push_back(((bool) (column & mask)));
        mask = mask >> 1; // bit masking makes the solution simple
      }
      ++index;
    }
    return result;
  }

  /* GETTER method for alphabet */
  vector<vector<float>> get_alphabet() { return alphabet; }

  /* Prints alphabet as 7x5 dot matrix */
  void print_alphabet() {
    for(int i = 0; i < alphabet.size(); i++) {
      cout << ((char)('A'+i)) << alphabet[i].size();
      for(int j = 0; j < alphabet[i].size(); j++) {
        if((j%7) == 0) {
          cout << endl;
        }
        cout << alphabet[i][j];
      }
      cout << endl << endl;
    }
  }

};

#endif
