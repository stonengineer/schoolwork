/*
 * File: network.cpp
 * 
 * Author: Joseph Stone
 *            422008041
 * 
 * Course: CSCE-420
 * 
 * Due: 23 April, 2018
 *
 * Premise: This program implements the
 *          functions defined in network.h
 *
*/

#include "network.h"
#include "alphabet.h"
#include <fstream>

extern AlphabetGenerator ag;

void Neuron::init() {
  input_loaded = false;
  ai.resize(35);
  wi.resize(35);
  aj = 0.0;
}

double Neuron::input_function() {
  if(!input_loaded) {
    cerr << "No input to run function on" << endl;
    exit(1);
  }

  double sum = 0.0;
  for(int i = 0; i < ai.size(); i++) {
    sum += (ai[i] * wi[i]);
  }

  return sum;
}

double calc_abs(double a) {
  return (a > 0 ? a : (-a));
}

double Neuron::exp_activation() {
  double in = input_function();

  // use approximation to speed up function
  double calc_sig = (((in/(1.0+calc_abs(in)))+1.0)/2.0);

  aj = calc_sig;

  return aj;
}

double Neuron::exp_derivative() {
  double gx = exp_activation();

  return (gx * (1.0 - gx));
}

double Neuron::tanh_activation() {
  double in = input_function();

  // use approximation to speed up function
  double calc_tan = (in/(1.0+calc_abs(in)));

  aj = calc_tan;

  return aj;
}

double Neuron::tanh_derivative() {
  double gx = tanh_activation();

  return (1.0 - (gx*gx));
}

void Neuron::load_input(vector<double> input) {
  if(ai.size() > input.size()) {
    cerr << "Input too small for layer" << endl;
    exit(1);
  }

  for(int i = 0; i < ai.size(); i++) {
    ai[i] = input[i];
  }

  input_loaded = true;
}

Network::Network(double learning_rate) {
  network_learning_rate = learning_rate;

  hidden_layer.resize(HIDDEN_LAYER_SIZE);
  output_layer.resize(OUTPUT_LAYER_SIZE);

  for(int i = 0; i < HIDDEN_LAYER_SIZE; i++) {
    hidden_layer[i].init();
  }
  for(int i = 0; i < OUTPUT_LAYER_SIZE; i++) {
    output_layer[i].init();
  }

  cout << "Training Neural Network..." << endl;
  cout << "Expect up to 5000 iterations for 90% accuracy." << endl;
  cout.flush();

// uncommenting the following line runs an efficiency test over 10 attempts
//#define EFF_TEST

#ifdef EFF_TEST
  int passed = 0;
  int failed = 0;
  vector<int> iterations;
  for(int i = 0; i < 10; i++) {
    cout.flush();
    int iters = back_prop_learning(true);
    if(iters != MAX_ITERATIONS) {
      ++passed;
    }
    else {
      ++failed;
    }
    iterations.push_back(iters);
  }
  cout << endl << "Convergence over 10 runs: " << ((double)100.0*(passed / 10.0)) << "%" << endl;
  double conv = 0.0;
  for(int j = 0; j < iterations.size(); j++) {
    conv += iterations[j];
  }
  conv /= ((double)iterations.size());
  cout << "Average convergence iteration: " << conv << endl;
  exit(0);
#endif

#ifndef EFF_TEST
  int failed = 0;
  while(back_prop_learning(true) == MAX_ITERATIONS) {
    ++failed;
    if(failed == 5) {
      cout << "Network training failed. Please restart the program." << endl;
      exit(0);
    }
  }
#endif

}

double calc_random() {
  return (((double) rand() / (double) RAND_MAX) * 0.1) - 0.1;
}

int Network::back_prop_learning(bool verbose) {
  // error holders
  vector<double> output_error(HIDDEN_LAYER_SIZE);
  vector<double> hidden_error(INPUT_SIZE);

  srand(time(NULL));

  // set initial weights
  for(int i = 0; i < HIDDEN_LAYER_SIZE; i++) {
    for(int j = 0; j < 35; j++) {
      double weight = 0.0;
      while(weight == 0.0) {
        weight = calc_random();
      }
      hidden_layer[i].set_weight(j, weight);
    }
  }
  for(int i = 0; i < OUTPUT_LAYER_SIZE; i++) {
    for(int j = 0; j < 35; j++) {
      double weight = 0.0;
      while(weight == 0.0) {
        weight = calc_random();
      }
      output_layer[i].set_weight(j, weight);
    }
  }

  // backwards prop algorithm
  double loss = 100;
  int iterations = 0;
  double temp_learning_rate = network_learning_rate;
  std::ofstream plot_data;
  plot_data.open("../data.csv");
  while(loss > 1.0) {
    // decrease learning rate
    temp_learning_rate -= (0.0018 * temp_learning_rate *iterations / MAX_ITERATIONS);
    loss = 0.0;

    // bitwise generation of 'y[i]'
    unsigned int expected = 0x2000000;

    // loop over all 26 letters
    for(int i = 0; i < 26; i++) {
      // create input layer
      vector<double> input_layer(INPUT_SIZE);
      for(int j = 0; j < INPUT_SIZE; j++) {
        input_layer[j] = ag.get_alphabet()[i][j];
      }
      
      // call activation functions and pass values through forward prop
      for(int j = 0; j < HIDDEN_LAYER_SIZE; j++) {
        hidden_layer[j].load_input(input_layer);
        hidden_layer[j].tanh_activation();
      }
      for(int j = 0; j < OUTPUT_LAYER_SIZE; j++) {
        output_layer[j].load_input(hidden_output());
        output_layer[j].exp_activation();
      }

      // calculate errors through backwards prop
      unsigned int mask = 0x2000000;
      for(int j = 0; j < HIDDEN_LAYER_SIZE; j++) {
        if(j < output_layer.size()) {
          output_error[j] = output_layer[j].exp_derivative() * (((double)((bool)(expected & mask))) - output_layer[j].get_output());
          loss += pow((((double)((bool)(expected & mask))) - output_layer[j].get_output()), 2);
          mask = mask >> 1;
        }
        else {
          output_error[j] = 0.0;
        }
      }
      for(int j = 0; j < INPUT_SIZE; j++) {
        double weight_error_sum = 0.0;
        for(int k = 0; k < HIDDEN_LAYER_SIZE; k++) {
          weight_error_sum += (hidden_layer[j].get_weights()[k] * output_error[k]);
        }
        hidden_error[j] = hidden_layer[j].tanh_derivative() * weight_error_sum;
      }

      // update weights based on error generation
      for(int j = 0; j < OUTPUT_LAYER_SIZE; j++) {
        for(int k = 0; k < output_layer[j].get_weights().size(); k++) {
          double curr_weight = output_layer[j].get_weights()[k];
          double new_weight = curr_weight + (temp_learning_rate * output_layer[j].get_inputs()[k] * output_error[j]);
          output_layer[j].set_weight(k, new_weight);
        }
      }
      for(int j = 0; j < HIDDEN_LAYER_SIZE; j++) {
        for(int k = 0; k < hidden_layer[j].get_weights().size(); k++) {
          double curr_weight = hidden_layer[j].get_weights()[k];
          double new_weight = curr_weight + (temp_learning_rate * hidden_layer[j].get_inputs()[k] * hidden_error[j]);
          hidden_layer[j].set_weight(k, new_weight);
        }
      }

      // update expected output
      expected = expected >> 1;
    }

    // keep user updated on status
    if(verbose) {
      double acc_calc = (10.0 - loss)*10;
      double print_acc = (acc_calc > 0 ? acc_calc : 0.0);
      cout << "\rIteration " << iterations << " : accuracy=" << print_acc << "% learning=" << temp_learning_rate << "     ";
      cout.flush();

      plot_data << iterations << "," << print_acc << endl;
    }

    // 10000 iterations fails
    if(iterations == MAX_ITERATIONS) {
      if(verbose) {
        cout << "Failed." << endl;
      }
      plot_data.close();
      return iterations;
    }

    ++iterations;
  }
  if(verbose) {
    cout << "Passed." << endl;
  }
  plot_data.close();
  return iterations;
}

vector<double> Network::hidden_output() {
  vector<double> result(HIDDEN_LAYER_SIZE);
  for(int i = 0; i < HIDDEN_LAYER_SIZE; i++) {
    result[i] = hidden_layer[i].get_output();
  }
  return result;
}

vector<double> Network::final_output() {
  vector<double> result(OUTPUT_LAYER_SIZE);
  for(int i = 0; i < OUTPUT_LAYER_SIZE; i++) {
    result[i] = output_layer[i].get_output();
  }
  return result;
}

vector<double> Network::run(vector<double> input) {
  for(int i = 0; i < HIDDEN_LAYER_SIZE; i++) {
    hidden_layer[i].load_input(input);
    hidden_layer[i].tanh_activation();
  }

  for(int i = 0; i < OUTPUT_LAYER_SIZE; i++) {
    output_layer[i].load_input(hidden_output());
    output_layer[i].exp_activation();
  }

  return final_output();
}
