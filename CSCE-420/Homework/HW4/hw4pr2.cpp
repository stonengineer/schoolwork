// Joseph Stone
// 422008041
// CSCE 420
// Due: May 1, 2018
// hw4pr2.cpp

/* Write a program named hw4pr2.cpp, etc., which reads two words per line from
 * the keyboard until end of file (Control-D) and outputs a consistent
 * linearization.
*/

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <stack>
#include <algorithm>

#define cout std::cout
#define endl std::endl
#define endl2 endl << endl
#define string std::string
#define vector std::vector

struct DNode {
  string data;
  DNode * parent;
  vector<DNode*> children;
  int dependents;
};

bool sort_funct(DNode* a, DNode* b) {
  return (a->dependents < b->dependents);
}

class DependencyTree {
private:
  DNode * head;
  DNode* DFS(string _data) {
    DNode * ret = nullptr;
    std::stack<DNode*> s;
    s.push(head);
    while(!s.empty()) {
      DNode* curr = s.top();
      s.pop();
      if(curr->data == _data) {
        ret = curr;
        break;
      }
      else {
        for(int i = 0; i < curr->children.size(); i++) {
          s.push(curr->children[i]);
        }
      }
    }
    return ret;
  }
  void update_dependencies(DNode* _curr, bool added) {
    DNode * curr = _curr;
    while(curr != nullptr) {
      curr->dependents += (added ? 1 : -1);
      curr = curr->parent;
    }
  }
  void print_recursive(DNode* node) {
    cout << node->data << endl;

    std::sort(node->children.begin(), node->children.end(), sort_funct);

    for(DNode* n : node->children) {
      print_recursive(n);
    }
  }

public:
  DependencyTree(string _data) {
    head = new DNode;
    head->data = _data;
    head->parent = nullptr;
    head->dependents = 0;
  }
  bool add(string _parent, string _child) {
    DNode * parent = DFS(_parent);

    if(parent == nullptr) {
      return false;
    }
    if(DFS(_child) != nullptr) {
      return true;
    }

    DNode * child = new DNode;
    child->data = _child;
    child->parent = parent;
    child->dependents = 0;
    parent->children.push_back(child);

    update_dependencies(parent, true);

    return true;
  }
  void print_all() {
    print_recursive(head);
  }

};

int main() {
  DependencyTree tree("main");
  int empty_count = 0;
  while(true) {
    cout << "Enter the next dependency pair: ";
    string line;
    if(!getline(std::cin, line)) {
      break;
    }
    if(!line.empty()) {
      std::istringstream iss(line);
      vector<string> words;
      for(string s; iss >> s; ) {
        words.push_back(s);
      }

      tree.add(words[0], words[1]);
    }
    else {
      ++empty_count;
      if(empty_count == 3) {
        cout << "Press CTRL+D to exit." << endl;
        empty_count = 0;
      }
    }
  }

  tree.print_all();

  cout << endl2;
}
