// Joseph Stone
// 422008041
// CSCE 420
// Due: May 1, 2018
// hw4pr1.cpp

/* Write a program named hw4pr1.cpp, etc., to print the truth table
 * for the FuzzyImplies function corresponding to FuzzyAnd(X,Y) = min(X,Y), for
 * X and Y equal to 0, 0.25, .5, .75, and 1.  Hint: There are 25 lines in this
 * truth table.
 */

#include <iostream>

double min(double x, double y) {
  return (x < y ? x : y);
}

double FuzzyAnd(double x, double y) {
  return min(x, y);
}

double FuzzyNot(double x) {
  return (1.0 - x);
}

double FuzzyImplies(double x, double y) {
  return FuzzyNot(FuzzyAnd(x, FuzzyNot(y)));
}

int main() {
  int iter = 0;
  double vals[5] = { 0.0, 0.25, 0.5, 0.75, 1.0 };
  for(int i = 0; i < 5; i++) {
    for(int j = 0; j < 5; j++) {
      ++iter;
      double x = vals[i];
      double y = vals[j];
      double r = FuzzyImplies(x, y);
      printf("%2d: FuzzyImplies(%0.2f,%0.2f) = %0.2f=>%0.2f = !(%0.2f & !%0.2f) = %0.2f\n", iter, x, y, x, y, x, y, r);
    }
  }
}
