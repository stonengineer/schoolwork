// Joseph Stone
// 422008041
// CSCE 420
// Due: May 1, 2018
// hw4pr3.cpp

/* N-grams are sequences of N objects such as characters or words
 * (see chapter 22).  For example, by counting how many times in a dictionary that
 * 3-grams of letters occur at the beginning of words, we can predict the most 
 * likely third letter of a word from the first two letters. 
 * 
 * Write a program named hw4pr3.cpp, etc., to do this, using the dictionary on 
 * compute.cse.tamu.edu in /usr/share/dict/words.  (It contains one word per line,
 * but ignore words that are not entirely composed of letters, ignore words with
 * fewer than three characters, and ignore case.)  Stop on end of file from the
 * keyboard (Control-D).
*/

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#define vector std::vector
#define string std::string
#define cout std::cout
#define endl std::endl
#define endl2 endl << endl

vector<string> dictionary;

string to_low(string raw) {
  string s = "";
  for(int i = 0; i < raw.length(); i++) {
    if(raw[i] >= 65 && raw[i] <= 90) {
      s += ((char)(raw[i] + 32));
    }
    else {
      s += raw[i];
    }
  }
  return s;
}

bool pass(string test) {
  bool has_nums = false;
  for(int i = 0; i < test.length(); i++) {
    if(test[i] < 'a' || test[i] > 'z') {
      has_nums = true;
    }
  }

  return (!has_nums && (test.length() >= 3));
}

void read_dictionary() {
  int count = 0;
  std::ifstream dict("words");
  while(!(dict.eof())) {
    cout << "\r[";
    double percent = (((double)count) / (479828));
    int draw_eqs = (percent * 50.0);
    for(int i = 0; i < draw_eqs; i++) {
      cout << "=";
    }
    for(int i = 0; i < (50-draw_eqs); i++) {
      cout << " ";
    }
    printf("] %.0f", ((double)(percent*100.0)));
    cout << "%";
    string test;
    dict >> test;
    string edited_test = to_low(test);
    if(pass(edited_test)) {
      dictionary.push_back(edited_test);
    }
    ++count;
  }
  cout << endl;
}

int next_guess(string beg) {
  int alphabet[26] = { 0 };
  for(int i = 0; i < dictionary.size(); i++) {
    if(dictionary[i][0] == beg[0] && dictionary[i][1] == beg[1]) {
      int index = ((int)(dictionary[i][2]-97));
      ++alphabet[index];
    }
  }
  int largest = 0;
  int index = -1;
  for(int i = 0; i < 26; i++) {
    if(alphabet[i] > largest) {
      largest = alphabet[i];
      index = i;
    }
  }
  return index;
}

int main() {
  cout << "Reading in dictionary..." << endl;
  read_dictionary();
  cout << "done" << endl2;

  while(true) {
    cout << "Enter first two letters of a word: ";
    string first_two_letters;
    if(!(std::cin >> first_two_letters)) {
      break;
    }
    if(first_two_letters.length() == 2) {
      int guess = next_guess(first_two_letters);
      if(guess >= 0) {
        cout << "The most likely next letter is " << ((char)(guess + 97)) << ".";
      }
      else {
        cout << "There is no word with that beginning.";
      }
    }
    cout << endl;
  }
  cout << endl;
}
