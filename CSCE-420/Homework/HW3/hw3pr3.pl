% Joseph Stone
% 422008041
% CSCE-420
% Due: 26 March, 2018
% hw3pr3.pl

%You should write enough predicates to deduce
%    ?- grandchild(X, elizabeth).
%    ?- brother_in_law(X, diana).
%    ?- great_grandparent(X, zara).
%    ?- ancestor(X, eugenie).
%    ?- first_cousin(mia,X).
%?- mother(X, charles). returns X = elizabeth.

% define men
male(king_george).
male(spencer).
male(philip).
male(charles).
male(mark).
male(andrew).
male(edward).
male(william).
male(harry).
male(peter).
male(mike).
male(james).
male(prince_george).

% define women
female(mum).
female(kydd).
female(elizabeth).
female(margaret).
female(diana).
female(anne).
female(sarah).
female(sophie).
female(kate).
female(autumn).
female(zara).
female(beatrice).
female(eugenie).
female(louise).
female(charlotte).
female(savannah).
female(isla).
female(mia).

% define marriages
spouce(king_george,mum).
spouce(elizabeth,philip).
spouce(spencer,kydd).
spouce(diana,charles).
spouce(anne,mark).
spouce(andrew,sarah).
spouce(edward,sophie).
spouce(william,kate).
spouce(peter,autumn).
spouce(mike,zara).

% define children
child(elizabeth,king_george).
child(margaret,king_george).
child(diana,spencer).
child(charles,philip).
child(anne,philip).
child(andrew,philip).
child(edward,philip).
child(william,charles).
child(harry,charles).
child(peter,mark).
child(zara,mark).
child(beatrice,andrew).
child(eugenie,andrew).
child(louise,edward).
child(james,edward).
child(prince_george,william).
child(charlotte,william).
child(savannah,peter).
child(isla,peter).
child(mia,mike).

% define relationships

% define marriage convolution
married(X,Y):-spouce(X,Y);spouce(Y,X).

% a son has to be male, and a child of spouces
son(X,Y):-male(X),(child(X,Y);(married(Y,S),child(X,S))).

% a daughter has to be female, and a child of spouces
daughter(X,Y):-female(X),(child(X,Y);(married(Y,S),child(X,S))).

% a brother must be male, and a child of the same spouces
brother(X,Y):-male(X),(child(X,S),child(Y,S)).

% a sister must be female, and a child of the same spouces
sister(X,Y):-female(X),(child(X,S),child(Y,S)).

% a BIL must be male, and married to a brother or sister
brother_in_law(X,Y):-male(X),((married(Y,S),brother(X,S));(married(X,T),married(Y,Q),sister(T,Q))).

% a SIL must be female, and married to a brother or sister
sister_in_law(X,Y):-female(X),((married(Y,S),sister(X,S));(married(X,T),married(Y,Q),brother(T,Q))).

% an uncle must be male, and a brother to one of the parents
uncle(X,Y):-male(X),child(Y,S),((brother(X,S));(married(S,T),brother(X,T));(brother_in_law(X,S))).

% an aunt must be female, and a sister to one of the parents
aunt(X,Y):-female(X),child(Y,S),((sister(X,S));(married(S,T),sister(X,T));(sister_in_law(X,S))).

% a first cousin is a child of an uncle or aunt
first_cousin(X,Y):-(uncle(S,X),child(Y,S));(aunt(S,X),married(S,T),child(Y,T)).

% a grandchild is the child of your child
grandchild(X,Y):-((child(S,Y),child(X,S));(married(Y,T),child(R,T),child(X,R));(married(Y,Z),child(Q,Z),married(Q,P),child(X,P))).

% a great grandparent is the parent of one of your grandparents
great_grandparent(X,Y):-grandchild(Y,S),(child(S,X);(child(S,T),married(T,X))).

% an ancestor is the highest-order relative that can be found
ancestor(X,Y):-(great_grandparent(X,Y);grandchild(Y,X);son(Y,X);daughter(Y,X)).

% a mother is your female parent
mother(X,Y):-child(Y,S),spouce(X,S).

% a father is your male parent
father(X,Y):-child(Y,X).
