% Joseph Stone
% 422008041
% CSCE-420
% Due: 26 March, 2018
% hw3pr4.pl

% define recursive is_sorted algorithm
% true iff the inputted list is sorted ascendingly
my_sorted([]).
my_sorted([_]).
my_sorted([H,B|T]) :- H =< B, my_sorted([B|T]).

% define recursive permutation algorithm
% returns true iff the inputted lists are permutations
my_perm(L,[H|P]) :- my_delete(H,L,R), my_perm(R,P).
my_perm([],[]).

% define recursive delete algorithm
% assists permutation algorithm
my_delete(X,[X|T],T).
my_delete(X,[H|T],[H|N]) :- my_delete(X,T,N).

% recursive sort algorithm
% sorts List into Sorted
my_sort(List,Sorted) :- my_perm(List,Sorted), my_sorted(Sorted).
