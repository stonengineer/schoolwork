% Joseph Stone
% 422008041
% CSCE-420
% Due: 26 March, 2018
% hw3pr2.pl

% define the adjacencies
adjacent(a,s).
adjacent(a,z).
adjacent(a,t).
adjacent(z,o).
adjacent(s,f).
adjacent(s,r).
adjacent(s,o).
adjacent(t,l).
adjacent(f,b).
adjacent(r,p).
adjacent(r,c).
adjacent(l,m).
adjacent(b,p).
adjacent(b,g).
adjacent(b,u).
adjacent(p,c).
adjacent(c,d).
adjacent(m,d).
adjacent(u,v).
adjacent(u,h).
adjacent(v,i).
adjacent(h,e).
adjacent(i,n).

% path(P,Start,Goal) means P is a path from Start to Goal

% define convolution
path(X,Y):-adjacent(X,Y);adjacent(Y,X).

% begins recursion and prints first move
path(Start,Goal):-
    (adjacent(Start,Goal) -> (write(Start),write(' to '),write(Goal),nl) ; rpath(Start,Goal)),
    !.

% recursive path function that writes as it goes
rpath(Start,Goal):-
    adjacent(Start,S),
    write(Start),
    write(' to '),
    write(S),
    nl,
    (adjacent(S,Goal) -> (write(S),write(' to '),write(Goal),nl) ; path(S,Goal)),
    !.