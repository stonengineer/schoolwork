% Joseph Stone
% 422008041
% CSCE-420
% Due: 26 March, 2018
% hw3pr1.pl

% only thing we know
unicorn(u).

% make it dynamic
:- dynamic(mythical/1).

% rules
immortal(X) :- mythical(X).
mortal(X) :- \+mythical(X).
mammal(X) :- \+mythical(X).
horned(X) :- \+mortal(X);mammal(X).
magical(X) :- horned(X).
