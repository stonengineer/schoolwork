// Joseph Stone
// 422008041
// CSCE-420
// Due: 5 March, 2018
// hw2pr4.cpp

//includes
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

//defines to make code more readable
#define cout std::cout
#define endl std::endl
#define vector std::vector
#define string std::string

//function declarations
vector<char> parse_input(vector<string> input);
int pow(int a, int b);
bool algorithm(vector<string> sentences, vector<char> unique_vars, vector<bool> values);
void output(vector<char> unique_vars, vector<char> truth_table, vector<unsigned int> masks);

int main() {
    //read in user's input
    string line;
    vector<string> sentences;
    cout << "Enter the clauses in your CNF formula, one per line:" << endl;
    while(!getline(std::cin, line).eof()) {
        sentences.push_back(line);
    }

    //figure out the unique variables
    vector<char> unique_vars = parse_input(sentences);
    
    //create masks for bitwise manipulation
    vector<unsigned int> masks;
    for(int i = 0; i < unique_vars.size(); i++) {
        masks.push_back((unsigned int) pow(2, i));
    }

    //create truth table by running algorithm on each set of input combos
    vector<char> truth_table;
    for(unsigned int line_iter = 0; line_iter < pow(2, unique_vars.size()); line_iter++) {
        vector<bool> values;
        for(int input_value_iter = unique_vars.size()-1; input_value_iter >= 0; input_value_iter--) {
            values.push_back(line_iter&masks[input_value_iter]);
        }
        truth_table.push_back((algorithm(sentences, unique_vars, values) ? 'T' : 'F'));
    }

    //send result to output function
    output(unique_vars, truth_table, masks);

    return 0;
}

vector<char> parse_input(vector<string> input) {
    //takes vector of strings and figures out unique char variables in strings
    vector<char> vars; //return object 
    for(int input_iter = 0; input_iter < input.size(); input_iter++) {
        string temp = input[input_iter];
        for(int temp_iter = 0; temp_iter < temp.length(); temp_iter++) {
            char curr_char = temp[temp_iter];
            if((curr_char < 65) || (curr_char > 90)) { //ignore non-alphabet
                continue;
            }
            if((find(vars.begin(), vars.end(), curr_char)) == vars.end()) {
                //only add values we don't already have in the vector
                vars.push_back(curr_char);
            }
        }
    }
    return vars;
}

bool algorithm(vector<string> sentences, vector<char> unique_vars, vector<bool> values) {
    bool eval = true; //final result of sentences logic
    for(string sentence : sentences) {
        if(sentence.length() < 3) {
            //we know these sentences must be single-var
            bool negate_var = (sentence.length() == 2) ? true : false; //determine whether we negate
            int var_index = find(unique_vars.begin(), unique_vars.end(), sentence[negate_var]) - unique_vars.begin(); //figure out where var is in input vector
            bool var_value = values[var_index]; //pull out var's corresponding value
            if(negate_var) { var_value ^= true; } //negate if necessary
            eval = (eval && var_value); //'and' var's value with eval
        }
        else {
            //these sentences are multi-var
            bool negate = false;
            int index = 0;
            if(sentence[index] == '-') { //determine whether we need to negate
                negate = true;
                ++index; //increment index if so
            }
            char first_var = sentence[index]; //pull off first var
            int first_var_index = find(unique_vars.begin(), unique_vars.end(), first_var) - unique_vars.begin(); //figure out where var is in input vector
            bool first_var_value = values[first_var_index]; //pull out var's corresponding value
            if(negate) { first_var_value ^= true; } //negate if necessary
            ++index; //increment index
            
            if(sentence[index] != '+') {
                //multi-vars must be ORs
                cout << "Error, invalid input string" << endl;
                return false;
            }
            ++index;
            
            //do the same thing we did to get first var and get second var
            negate = false;
            if(sentence[index] == '-') {
                negate = true;
                ++index;
            }
            char second_var = sentence[index];
            int second_var_index = find(unique_vars.begin(), unique_vars.end(), second_var) - unique_vars.begin();
            bool second_var_value = values[second_var_index];
            if(negate) { second_var_value ^= true; }
            
            //'and' eval with the OR of the two values we received
            eval = (eval && (first_var_value || second_var_value));
        }
    }
    return eval;
}

//recursive power function
int pow(int a, int b) {
    if(b == 0) {
        return 1;
    }
    return a*pow(a, b-1);
}

//prints the truth table prettily using bitwise operations
void output(vector<char> unique_vars, vector<char> truth_table, vector<unsigned int> masks) {
    for(int unique_vars_iter = 0; unique_vars_iter < unique_vars.size(); unique_vars_iter++) {
        cout << unique_vars[unique_vars_iter] << "  ";
    }
    cout << "Formula" << endl;
    for(int num_dashes = 0; num_dashes < ((unique_vars.size()*3) + 7); num_dashes++) {
        cout << "-";
    }
    cout << endl;

    int num_lines = pow(2, unique_vars.size());

    for(unsigned int line_iter = 0; line_iter < num_lines; line_iter++) {
        for(int input_value_iter = unique_vars.size()-1; input_value_iter >= 0 ; input_value_iter--) {
            char input_value = (line_iter&masks[input_value_iter]) ? 'T' : 'F';
            cout << input_value << "  ";
        }
        cout << "   " << truth_table[line_iter] << endl;
    }
}
