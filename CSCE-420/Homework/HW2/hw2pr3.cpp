// Joseph Stone
// 422008041
// CSCE-420
// Due: 5 March, 2018
// hw2pr3.cpp

//includes
#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <fstream>
#include <queue>
#include <algorithm>

//defines to make code more readable
#define cout std::cout
#define endl std::endl
#define endl2 endl << endl
#define string std::string
#define vector std::vector
#define queue std::queue

//change imply symbol from :- to ~
#define implies '~'

//togglable 'verbose' tag
#define TESTING_MODE 0

struct map { //keeps track of each unique var's situation
    char name;
    bool cond;
    bool set;
};

class loop_counter { //counts from 0->max_size then loops, starting at start
private:
    int index;
    int max_size;
public:
    loop_counter(int start, int size) {
        index = start;
        max_size = size;
    }
    void incr() {
        ++index;
        if(index >= max_size) {
            index = 0;
        }
    }
    int count() { return index; }
};

//main algorithm to solve sentences
bool algorithm(vector<map*> clauses, vector<char> conditions);

//returns true if token is in the list
bool search_mapped(vector<map*> list, char token) {
    for(map* m : list) {
        if(m->name == token) {
            return true;
        }
    }
    return false;
}

//returns the bool value of the token, and false if it's not found
bool access_mapped(vector<map*> list, char token) {
    for(map* m : list) {
        if(m->name == token) {
            return m->cond;
        }
    }
    return false;
}

//returns the bool 'set' value of the token, which signifies whether we
//know the value of the token in list
bool is_mapped_set(vector<map*> list, char token) {
    for(map* m : list) {
        if(m->name == token) {
            return m->set;
        }
    }
    return false;
}

//returns true if every token in the list has been set to a value
bool all_defined(vector<map*> list) {
    for(map* m : list) {
        if(!m->set) {
            return false;
        }
    }
    return true;
}

int main() {
    //input file from user
    string line;
    vector<string> sentences;
    std::ifstream file_input;
    if(TESTING_MODE) {
        cout << "Reading from \'hw2pr3_input.txt\'" << endl2;
        file_input.open("hw2pr3_input.txt");
    }
    else {
        cout << "Enter the name of the test file to read: ";
        getline(std::cin, line);
        file_input.open(line);
    }
    while(!getline(file_input, line).eof()) {
        sentences.push_back(line);
    }
    
    if(TESTING_MODE) { //report what was read and next step
        cout << "Inputted Set:" << endl;
        for(string sentence : sentences) {
            cout << sentence << endl;
        }
        cout << "-------------" << endl2;
        
        cout << "Sending input to parser" << endl2;
    }
    
    //parse the input
    vector<map*> clauses; //individual variables
    vector<char> conditions; //sentences that correlate the variables
    for(string sentence : sentences) {
        if((sentence.length() < 4) && (sentence[0] != '?')) {
            //we know these are assignment statements
            //negate is true if the sentence starts with '-'
            bool negate = (sentence.length() == 3) ? true : false;
            //find the name of the var, will be at pos 1 if negate is true
            char curr_name = sentence[negate];
            
            //add this var to the clauses if it's not in there already
            if(!search_mapped(clauses, curr_name)) {
                map* temp = new map;
                temp->name = curr_name;
                temp->cond = false;
                temp->set = false;
                clauses.push_back(temp);
            }
            
            //update the information known about this clause
            for(int i = 0; i < clauses.size(); i++) {
                if(clauses[i]->name == curr_name) {
                    clauses[i]->cond = !negate;
                    clauses[i]->set = true;
                }
            }
        }
        else {
            //we know these are either queries or correlation sentences
            for(int s_iter = 0; s_iter < sentence.length(); s_iter++) {
                char curr_char = sentence[s_iter];
                if((curr_char < 65) || (curr_char > 90)) {
                    //not an uppercase letter
                    if(curr_char == ':') { //let's convert the implies sequence
                        //to a single char
                        if(sentence[s_iter+1] != '-') { //no single colons allowed!
                            cout << "Error, invalid sentence" << endl;
                            return 1;
                        }
                        ++s_iter; //ignore next char
                        curr_char = implies; //replace :- with ~
                    }
                }
                else {
                    //an uppercase letter; must be a variable!
                    if(!search_mapped(clauses, curr_char)) {
                        map* temp = new map; //create new variable
                        temp->name = curr_char; //know it's name
                        temp->cond = false; //don't know it's value yet
                        temp->set = false; //know it's not been written to yet
                        clauses.push_back(temp); //add to list
                    }
                }
                conditions.push_back(curr_char); //add parsed input
            }
        }
    }
    
    if(TESTING_MODE) { 
        //report parsing success, what the known clauses are
        //and state next step in process
        cout << "Input successfully parsed!" << endl;
        cout << "Parsed code:" << endl;
        for(int i = 0; i < conditions.size(); i++) {
            if(conditions[i] == '.') {
                cout << endl;
            }
            else {
                cout << conditions[i];
            }
        }
        cout << "-------------" << endl2;
        
        cout << "Clauses:" << endl;
        for(map* m : clauses) {
            if(m->set) {
                cout << m->name << "=" << m->cond << endl;
            }
            else {
                cout << m->name << "=?" << endl;
            }
        }
        cout << "-------------" << endl2;
        
        cout << "Running algorithm on sentences" << endl2;
    }
    
    //print TRUE if we found a solution, FALSE otherwise
    cout << (algorithm(clauses, conditions) ? "TRUE" : "FALSE") << endl2;
}

bool algorithm(vector<map*> clauses, vector<char> conditions) {
    //keep track of how many times the loop has run
    int run_index = 0;
    int MAX_RUNS = 1000; //break after 1000 attempts
    char queried = '\0';
    
    //initialize loop_counter var
    loop_counter loop(0, conditions.size());
    
    while((!all_defined(clauses)) || (run_index < conditions.size())) {
        //run until we've defined everything (hopefully)
        char left_var = '*'; //keeps track of the left-side variable
        queue<char> right_vars; //keeps track of the right-side variables
        queue<char> ops; //keeps track of the operations specified by sentence
        while(conditions[loop.count()] != '.') { //read until we hit a period
            char curr_char = conditions[loop.count()];
            switch(curr_char) {
                case '?': //this signifies a query
                    loop.incr(); //increment loop
                    for(map* m : clauses) { //test to see if we've solved the query
                        if(m->name == conditions[loop.count()]) {
                            queried = m->name;
                            if(m->set) { //we solved it!
                                return m->cond; //return the value
                            }
                            break;
                        }
                    }
                    break;
                case '~': //the next three are var operations
                case ',':
                case '-':
                    ops.push(curr_char);
                    break;
                default: //these must be variables
                    if(left_var == '*') {
                        left_var = curr_char; //first var encountered is left-side var
                    }
                    else {
                        right_vars.push(curr_char); //all others are right-side vars
                    }
                    break;
            }
            loop.incr();
        }
        loop.incr();
        
        while(!ops.empty()) { //run through this sentence's operations
            char op = ops.front();
            ops.pop();
            switch(op) { //just in case there's a rule I'm missing, easier to add this way
                case '~': //should be every single time
                    bool eval; //solution to RHS
                    bool vars_set = true; //never set LHS if any vars on RHS aren't set
                    while(!right_vars.empty()) { //run through the RHS variables
                        //necessary holding variables
                        bool first_val;
                        bool first_set = false;
                        bool negate_first = false;
                        bool second_val;
                        bool second_set = false;
                        bool negate_second = false;
                        
                        char first_name = right_vars.front(); //grab first variable
                        right_vars.pop();
                        if(ops.front() == '-') { //if we negate, negate it
                            negate_first = true;
                            ops.pop();
                        }
                        if(!is_mapped_set(clauses, first_name)) {
                            vars_set = false; //if it's not set, remember
                        }
                        first_val = access_mapped(clauses, first_name); //grab value assigned
                        if(negate_first) { first_val ^= true; } //negate if necessary
                        first_set = true; //first var has been set!
                        if(right_vars.empty()) { //if it's the only RHS variable
                            eval = first_val; //we've evaluated the RHS!
                            continue;
                        }
                        
                        if(ops.front() == ',') { //otherwise, figure out second variable
                            ops.pop();
                            char second_name = right_vars.front(); //grab second variable
                            right_vars.pop();
                            if(ops.front() == '-') { //if we negate, negate it
                                negate_second = true;
                                ops.pop();
                            }
                            if(!is_mapped_set(clauses, second_name)) {
                                vars_set = false; //if it's not set, remember
                            }
                            second_val = access_mapped(clauses, second_name); //grab value assigned
                            if(negate_second) { second_val ^= true; } //negate if necessary
                            second_set = true; //second var has been set!
                            eval = (eval && (first_val && second_val)); //evaluate the RHS!
                        }
                    }
                    for(int i = 0; i < clauses.size(); i++) {
                        if(clauses[i]->name == left_var) { //update LHS value
                            clauses[i]->cond = (!eval || clauses[i]->cond);
                            clauses[i]->set = vars_set;
                            break;
                        }
                    }
                    break;
            }
        }
        
        if(TESTING_MODE) { //report clauses at this state
            cout << "Clauses: ";
            for(map* m : clauses) {
                if(m->set) {
                    cout << m->name << "=" << m->cond << " ";
                }
                else {
                    cout << m->name << "=?" << " ";
                }
            }
            cout << endl;
        }
        
        //return false if we exceed MAX_RUNS
        ++run_index;
        if(run_index >= MAX_RUNS) {
            break;
        }
    }
    return false;
}
