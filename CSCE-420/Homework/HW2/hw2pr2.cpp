// Joseph Stone
// 422008041
// CSCE-420
// Due: 5 March, 2018
// hw2pr2.cpp

#include <string>
#include <stdlib.h>
#include "MinimaxTree.h"

#define vector std::vector
#define cout std::cout
#define endl std::endl
#define string std::string

MinimaxTree parse_input(string s); //turns the input into a tree
int min(int a, int b); //returns the min of the two ints
int max(int a, int b); //returns the max of the two ints
int minimax_algorithm(MinimaxTree tree); //base algorithm that finds minimum of tree
int max_value(Node* curr); //max utility value
int min_value(Node* curr); //min utility value

int main() {
    string tree_input; //stores input
    cout << "Enter the input tree: ";
    std::getline(std::cin, tree_input);
    cout << "Read in successfully!" << endl; //yay, getline works!
    cout << endl;

    cout << "Inputted tree:" << endl; //prove we actually built the tree right
    MinimaxTree tree = parse_input(tree_input);
    tree.print_tree();
    cout << endl;
    
    cout << "Minimum value: " << minimax_algorithm(tree) << endl; //print final value
    
    return 0;
}

MinimaxTree parse_input(string s) {
    MinimaxTree tree; //invoke constructor, builds head
    string num = ""; //allows multi-digit or negative numbers to work
    for(int i = 1; i < s.length(); i++) { //head is already created, so start at index 1
        char c = s[i]; //grab off next char
        if(c == '(') { //open parens means create new subtree
            int value = (tree.get_curr()->value == INT_MIN) ? INT_MAX : INT_MIN; //set it as max or min based on parent's value
            tree.add_child(value); //create subtree child
            tree.move_curr(c); //move current directory to child
        }
        else if(c == ')') { //closed parens means back up tree
            if(num != "") { //if there's a number before, remember to store it
                int value = stoi(num); //what is it
                tree.add_child(value); //create a child node for the value
                num = ""; //clear buffer string
            }
            tree.move_curr(c); //move up tree
        }
        else if(c == ',') { //new node (probably)
            if(num != "") { //if we have a saved value
                int value = stoi(num); //convert
                tree.add_child(value); //create a child node for the value
                num = ""; //clear buffer string
            } //otherwise just ignore the comma
        }
        else if(((c > 47) && (c < 58)) || (c == 45))  {
            //only grab digits or the negative sign
            num += c; //push char onto string buffer
        } //ignore everything else
    }
    return tree; //return final tree
}

int min(int a, int b) {
    return (a < b) ? a : b; //returns smaller
}

int max(int a, int b) {
    return (a > b) ? a : b; //returns larger
}

int minimax_algorithm(MinimaxTree tree) {
    return min_value(tree.get_head()); //min tree, so top node is min algo
}

int max_value(Node* curr) {
    if(curr->children.empty()) { //leaf
        return curr->value; //return the value
    }
    int value = INT_MIN; //as close to -inf as we can get
    for(int i = 0; i < curr->children.size(); i++) { //go through all children
        value = max(value, min_value(curr->children[i])); //based on figure 5.3
    }
    return value; //return value
}

int min_value(Node* curr) {
    if(curr->children.empty()) { //leaf
        return curr->value; //return the value
    }
    int value = INT_MAX; //as close to inf as we can get
    for(int i = 0; i < curr->children.size(); i++) { //go through all children
        value = min(value, max_value(curr->children[i])); //based on figure 5.3
    }
    return value; //return value
}
