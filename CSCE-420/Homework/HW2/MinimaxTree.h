#include <climits>
#include <vector>
#include <iostream>
#include <queue>

#define vector std::vector
#define cout std::cout
#define endl std::endl

struct Node { //holds relevant information for nodes
    Node* parent; //this node's parent
    vector<Node*> children; //this node's children
    int value; //value of this node
    int level; //level of this node (for printing purposes)
};

class MinimaxTree { //defines tree
private:
    Node* head; //head value of tree
    Node* curr; //current node

public:
    MinimaxTree() { //constructor, builds head node
        Node* temp = new Node;
        temp->parent = NULL; //no parent for head
        temp->value = INT_MAX; //since we're starting as min node
        temp->level = 0; //only level 0 node
        curr = temp; //current node
        head = curr; //also head node
    }

    void add_child(int value) { //creates child for current node
        Node* temp = new Node;
        temp->parent = curr; //parent is curr node
        temp->value = value; //value is passed in
        temp->level = temp->parent->level + 1; //increase level by 1
        curr->children.push_back(temp); //add to current node's children vector
    }

    void move_curr(char c) { //update curr based on input
        if(c == '(') { //curr becomes the last child inputted for it's children
            curr = curr->children.back();
        }
        else if(c == ')') { //curr becomes its' parent node
            curr = curr->parent;
        }
        else { //that's all we can do
            cout << "Invalid input char" << endl;
        }
    }

    Node* get_head() { return head; } //head getter
    Node* get_curr() { return curr; } //curr getter

    void print_tree() { //prints the tree prettily
        if(head == nullptr) {
            cout << "Empty tree" << endl;
            return;
        }
        std::queue<Node*> bfs;
        bfs.push(head);
        int level = head->level;
        while(!bfs.empty()) {
            Node* print = bfs.front();
            bfs.pop();
            if(print->level > level) {
                level = print->level;
                cout << endl;
            }
            for(int i = 0; i < print->children.size(); i++) {
                bfs.push(print->children[i]);
            }
            if(print->value != INT_MAX && print->value != INT_MIN) {
                cout << print->value << " ";
            }
            else {
                cout << "X ";
            }
        }
        cout << endl;
    }
};
