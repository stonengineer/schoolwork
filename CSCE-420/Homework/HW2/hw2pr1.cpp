// Joseph Stone
// 422008041
// CSCE-420
// Due: 5 March, 2018
// hw2pr1.cpp

#include <iostream> //for terminal I/O
#include <string> //for ease of getline
#include <vector> //much nicer than regular arrays
#include <algorithm> //allows for vector's delete and random_shuffle

#define cout std::cout //I didn't
#define endl std::endl //want to
#define string std::string //be using
#define vector std::vector //namespace std

//declare these functions so main can call them
string build_uniques(string, string, string);
int* solve(string first, string second, string result, string unique_letters, int n);

int main() {
    string first, second, result, uniques; //inputs and unique tester string
    do { //cycle inputs until user gives a good set
        cout << "Enter the first word: ";
        std::getline(std::cin, first);
        cout << "Enter the second word: ";
        std::getline(std::cin, second);
        cout << "Enter the result: ";
        std::getline(std::cin, result);
        uniques = build_uniques(first, second, result); //build unique string
    }while(uniques.length() > 10); //make sure unique string is less than 10
    
    int n = uniques.length(); //gotta send the length to solve function
    int* corr_numbers = solve(first, second, result, uniques, n); //pull out corresponding numbers

    //catch the segfault of dereferening a null pointer
    if(corr_numbers == nullptr) {
        return 1;
    }
    
    cout << "LET NUM" << endl; //table headers
    for(int i = 0; i < n; i++) { //print out each unique char and its corresponding number
        cout << " " << uniques[i] << "   " << corr_numbers[i] << " " << endl;
    }
    
    return 0; //that's all folks!
}

//rips through input strings to pull out unique characters
string build_uniques(string first, string second, string third) {
    string uniques = "";
    int index = 0;
    
    for(int i = 0; i < 3; i++) {
        //work smarter not harder
        string test = (i == 0) ? first:
                      (i == 1) ? second:
                                 third;
        
        //iterate through each string and test each char against our current uniques
        for(int n = 0; n < test.length(); n++) {
            if(uniques.find(string(1, test[n])) == string::npos) {
                uniques += string(1, test[n]);
                ++index;
            }
            if(index == 11) { //let them know why the input is looping back
                cout << "Error, more than 10 unique characters in input." << endl;
            }
        }
    }
    
    return uniques;
}

//function to take the words, the unique letters, and corresponding
//numbers, and figure out how bad our guess is
int distance(string first, string second, string result, string letters, int* weights) {
    //convert each string of characters into a string of corresponding numbers
    //i.e. turns "TWO" into "012", or whatever gets passed in to weights
    string wfirst = "";
    for(int i = 0; i < first.length(); i++) {
        wfirst += std::to_string(weights[letters.find(string(1, first[i]))]);
    }
    string wsecond = "";
    for(int i = 0; i < second.length(); i++) {
        wsecond += std::to_string(weights[letters.find(string(1, second[i]))]);
    }
    string wresult = "";
    for(int i = 0; i < result.length(); i++) {
        wresult += std::to_string(weights[letters.find(string(1, result[i]))]);
    }
    
    //turn these strings of numbers into real ints
    int a = stoi(wfirst);
    int b = stoi(wsecond);
    int c = stoi(wresult);
    
    //calculate distance and return abs(distance)
    int dis = c - (a + b);
    if(dis < 0) {
        return (-1 * dis);
    }
    return dis;
}

//didn't want to google how to use C++'s default
//min function so I wrote a quick one of my own
int min(int a, int b) {
    return (a < b) ? a : b;
}

//didn't want to google how to use C++'s default
//min function so I wrote a quick one of my own
int max(int a, int b) {
    return (a > b) ? a : b;
}

//real meat of the homework
int* solve(string first, string second, string result, string unique_letters, int n) {
    vector<int> nums_pool; //saves unused numbers
    int* sure_nums = new int[n]; //positions of numbers we're sure about
    int num_certainties = 0; //keep track of number of guesses we've made
    char operation; //saves whether these numbers will be less than or equal to ten
    int findex, sindex; //indeces of the two numbers that are less than or equal to ten

    //populate the initial number sets
    for(int i = 0; i < 10; i++) {
        if(i < n) {
            sure_nums[i] = -1; //we don't know anything yet
        }
        nums_pool.push_back(i); //all numbers are fair game
    }

    //this condition is based on the inputs being the same length, but
    //it will go through and make educated guesses regarding numbers
    if(first.length() == second.length()) {
        if(result.length() < first.length()) { //mathematically impossible
            cout << "ERROR - result is too short" << endl;
            return NULL;
        }
        if(result.length() > first.length()) { //mathematically impossible
            cout << "ERROR - result is too long" << endl;
            return NULL;
        }
        if(result.length()-1 == first.length()) { //if result is one character longer than the inputs
            ++num_certainties; //we're certain that...
            sure_nums[unique_letters.find(string(1, result[0]))] = 1; //the most significant char of results corresponds to a '1'!
            nums_pool.erase(std::remove(nums_pool.begin(), nums_pool.end(), 1), nums_pool.end()); //remove the 1 from the nums_pool
            
            ++num_certainties; //also...
            findex = unique_letters.find(string(1, first[0])); //the most significant chars of the inputs must add up to at least ten!
            sindex = unique_letters.find(string(1, second[0]));
            sure_nums[findex] = -2; //mark them as -2 since we know something
            sure_nums[sindex] = -2; //is up, but don't know exact numbers
            operation = '>'; //used greater than symbol for geq
        }
        else { //result is the same length as the inputs
            ++num_certainties; //we can be certain that the most significant chars
            findex = unique_letters.find(string(1, first[0])); //of the inputs must
            sindex = unique_letters.find(string(1, second[0])); //add up to less than 10
            sure_nums[findex] = -2; //see above, exact same syntax
            sure_nums[sindex] = -2;
            operation = '<';
        }
    }
    else {
        int longer_input_length = max(first.length(), second.length()); //gets the longer input
        //if the inputs aren't the same, the result should be the same length as the longer
        if(longer_input_length > result.length()) {
            cout << "ERROR - result is too short" << endl;
            return NULL;
        }
        if(longer_input_length < result.length()) {
            cout << "ERROR - result is too long" << endl;
            return NULL;
        }

        string longer_input = (longer_input_length == first.length()) ? first : second; //figure out which string is longer
        string shorter_input = (longer_input_length != first.length()) ? first : second; //figure out which string is shorter
        //check that the result's first few chars are equal to the first few chars of the longer input string
        for(int i = 0; i < (longer_input.length() - shorter_input.length())-1; i++) {
            if(result[i] != longer_input[i]) { //breaks the system
                cout << "ERROR - mathematically impossible input" << endl;
                return NULL;
            }
        }
    }

    int* this_nums; //temporary 'try' values
    int removed = -1; //allows us to insert a removed value back into nums_pool
    do {
        this_nums = new int[n]; //init new array
        std::random_shuffle(nums_pool.begin(), nums_pool.end()); //shuffle number pool
        int index = 0; //index for number pool
        for(int i = 0; i < n; i++) { //iterate through the number of necessary numbers
            if(sure_nums[i] == -1) { //we have no idea, populate this_nums with the first index of the shuffled number pool
                this_nums[i] = nums_pool[index];
                ++index;
            }
            else if(sure_nums[i] == -2) { //we don't know what the number is, but we know something about it
                if(i == min(findex, sindex)) { //if we're at the first index, just populate this_nums with the first index of the shuffled number pool
                    this_nums[i] = nums_pool[index];
                    ++index;
                }
                else { //otherwise, we need to find a good number to put into the corresponding slot
                    int first = this_nums[min(findex, sindex)]; //figure out what we put in the first slot
                    switch(operation) { //decide whether we're geq or ltn 10
                        case '>': //geq 10!
                            for(int v = index; v < nums_pool.size(); v++) { //go through remaining numbers
                                if((nums_pool[v]+first) >= 10) { //if we find one that sums to greater than or equal to 10...
                                    removed = nums_pool[v]; //keep track of the one we removed
                                    this_nums[i] = removed; //add it to this_nums
                                    nums_pool.erase(std::remove(nums_pool.begin(), nums_pool.end(), removed), nums_pool.end()); //erase it from the nums_pool so it doesn't become duplicated in this_nums
                                }
                            }
                            break;
                        case '<': //ltn 10!
                            for(int v = index; v < nums_pool.size(); v++) { //go through remaining numbers
                                if((nums_pool[v]+first) < 10) { //if we find one that sums to less than 10...
                                    removed = nums_pool[v]; //keep track of the one we removed
                                    this_nums[i] = removed; //add it to this_nums
                                    nums_pool.erase(std::remove(nums_pool.begin(), nums_pool.end(), removed), nums_pool.end()); //erase it from the nums_pool so it doesn't become duplicated in this_nums
                                }
                            }
                            break;
                    }
                    if(removed == -1) { //if there wasn't a number in this_nums that made the operation work
                        this_nums[i] = nums_pool[index]; //just send the next value in; this run is a bust, but better to take a knowing loss than risk a segfault by not adding the num
                    }
                }
            }
            else { //otherwise we know the number, add it to this_nums
                this_nums[i] = sure_nums[i];
            }
        }
        if(removed != -1) { //if we removed something...
            nums_pool.push_back(removed); //add it back to the nums_pool
            removed = -1; //reset removed flag
        }
    }while(distance(first, second, result, unique_letters, this_nums) != 0); //eventually we'll hit distance == 0

    return this_nums; //we've found the golden ticket!!!
}
