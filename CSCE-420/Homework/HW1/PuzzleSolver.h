#define ROWS 3
#define COLS 3
#define UP   0
#define RGHT 1
#define DOWN 2
#define LEFT 3
#define INIT -1

class PuzzleSolver {

private:
    int xpos;
    int ypos;
    int last_move;
    int count;
    int** curr_state;
    int** goal_state;
    
    int state_difference(int** state) {
        //finds the absolute value of the difference between input state and the goal state
        int diff = 0;
        for(int y = 0; y < ROWS; y++) {
            for(int x = 0; x < COLS; x++) {
                diff += ((state[y][x] - goal_state[y][x]) * (state[y][x] > goal_state[y][x] ? 1 : -1));
            }
        }
        return diff;
    }
    
    int** make_child(int move) {
        //create a 'what if' 2d int array
        int** child = new int*[ROWS];
        for(int y = 0; y < ROWS; y++) {
            child[y] = new int[COLS];
            for(int x = 0; x < COLS; x++) {
                child[y][x] = curr_state[y][x];
            }
        }
        
        //run the move on it
        int temp = -1;
        switch(move) {
            case UP:
                temp = child[ypos-1][xpos];
                child[ypos-1][xpos] = 0;
                child[ypos][xpos] = temp;
                break;
            case RGHT:
                temp = child[ypos][xpos+1];
                child[ypos][xpos+1] = 0;
                child[ypos][xpos] = temp;
                break;
            case DOWN:
                temp = child[ypos+1][xpos];
                child[ypos+1][xpos] = 0;
                child[ypos][xpos] = temp;
                break;
            case LEFT:
                temp = child[ypos][xpos-1];
                child[ypos][xpos-1] = 0;
                child[ypos][xpos] = temp;
                break;
        }
        return child; //return the 'what if'
    }

public:
    PuzzleSolver(const char* start, const char* goal) {
        //initialize privates, set xpos and ypos to the location of 0 inside of start
        int index = 0;
        curr_state = new int*[ROWS];
        goal_state = new int*[ROWS];
        
        for(int y = 0; y < ROWS; y++) {
            curr_state[y] = new int[COLS];
            goal_state[y] = new int[COLS];
            for(int x = 0; x < COLS; x++) {
                curr_state[y][x] = start[index] - '0';
                if(curr_state[y][x] == 0) {
                    xpos = x;
                    ypos = y;
                }
                goal_state[y][x] = goal[index] - '0';
                index += 2;
            }
        }
        
        last_move = INIT;
        count = 0;
        
        std::cout << "Initialized. (x,y)=(" << xpos << "," << ypos << ")" << std::endl;
    }
    
    int generate_next() {
        //test possible moves, the best will result in the smallest difference
        //between the next state and the goal
        int best_move = INIT;
        int difference = 100; //max is <81 anyways
        if((last_move != DOWN) && (ypos-1 >= 0)) {
            int temp = state_difference(make_child(UP));
            if(temp < difference) {
                difference = temp;
                best_move = UP;
            }
        }
        if((last_move != LEFT) && (xpos+1 < COLS)) {
            int temp = state_difference(make_child(RGHT));
            if(temp < difference) {
                difference = temp;
                best_move = RGHT;
            }
        }
        if((last_move != UP) && (ypos+1 < ROWS)) {
            int temp = state_difference(make_child(DOWN));
            if(temp < difference) {
                difference = temp;
                best_move = DOWN;
            }
        }
        if((last_move != RGHT) && (xpos-1 >= 0)) {
            int temp = state_difference(make_child(LEFT));
            if(temp < difference) {
                difference = temp;
                best_move = LEFT;
            }
        }
        
        if(best_move == INIT) {
            return INIT; //failure
        }
        
        curr_state = make_child(best_move); //make the current state the best move
        ++count; //add another state
        switch(best_move) { //increment xpos or ypos
            case UP:
                --ypos;
                break;
            case RGHT:
                ++xpos;
                break;
            case DOWN:
                ++ypos;
                break;
            case LEFT:
                --xpos;
                break;
        }
        last_move = best_move; //update last move
        return best_move; //return best move
    }
    
    bool matches() { //true if curr state is equal to goal state
        for(int y = 0; y < ROWS; y++) {
            for(int x = 0; x < COLS; x++) {
                if(curr_state[y][x] != goal_state[y][x]) {
                    return false;
                }
            }
        }
        return true;
    }
    
    //getters
    
    int** get_curr_state() {
        return curr_state;
    }
    
    int get_count() {
        return count;
    }
};