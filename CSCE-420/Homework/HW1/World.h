class World { //abstracts the 2d int array
private:
	int num_rows;
	int num_columns;
	int** this_world;

public:
	World(int columns, int rows, int** world) { //constructor initializes private fields
		num_rows = rows;
		num_columns = columns;
		this_world = new int*[num_rows];
		for(int y = 0; y < num_rows; y++) {
			this_world[y] = new int[num_columns];
			for(int x = 0; x < num_columns; x++) {
				this_world[y][x] = world[y][x];
			}
		}
	}

	World(World &w) { //copy constructor
		num_rows = w.get_num_rows();
		num_columns = w.get_num_columns();
		this_world = w.get_world();
	}

	int read_square(int row, int column) { //return the data stored at this square
		return this_world[row][column];
	}

	bool suck(int row, int column) {  //clean the square if there's not an obstacle
		if(this_world[row][column] != -1) {
			this_world[row][column] = false;
			return true;
		}
		return false;
	}

	int sum() { //return the sum of the 2d world array
		int total = 0;
		for(int y = 0; y < num_rows; y++) {
			for(int x = 0; x < num_columns; x++) {
				total += this_world[y][x];
			}
		}
		return total;
	}

	//getters
	int get_num_rows() { return num_rows; }
	int get_num_columns() { return num_columns; }
	int** get_world() { return this_world; }
};