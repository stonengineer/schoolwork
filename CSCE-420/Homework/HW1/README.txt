It looks like the g++ used by compute.cse.tamu.edu isn't 7.2.0, so I just used g++ -std=c++11 hw1prX.cpp
to compile, then just ./a.out to run. To my understanding, std=c++17 should be backwards-compatible, so
it shouldn't generate any issues. Thanks!
