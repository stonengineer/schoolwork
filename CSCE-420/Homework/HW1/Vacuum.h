#include "World.h"
#include <stack>

class Vacuum {
private:
    int curr_x;
    int curr_y;
    int cleaned;
    World* this_world;
    std::stack<int> moves;
    int** known_world;
    
public:
    Vacuum(World world) { //initialize privates, and set all values of known_world to 2
        curr_x = 0;
        curr_y = 0;
        cleaned = 0;
        this_world = new World(world);

        known_world = new int*[this_world->get_num_rows()];
        for(int y = 0; y < this_world->get_num_rows(); y++) {
            known_world[y] = new int[this_world->get_num_columns()];
            for(int x = 0; x < this_world->get_num_columns(); x++) {
                known_world[y][x] = 2;
            }
        }
    }
    
    const int SUCK = 0; //constants
    const int DLFT = 1;
    const int DRGT = 2;
    const int ULFT = 3;
    const int URGT = 4;
    const int OBST = 5;
    
    bool finished() { //stack is empty
        return moves.empty();
    }
    
    void push_move(int where) { //push a move that's inside the defined constants
        if(where >= SUCK && where <= OBST) {
            moves.push(where);
        }
    }
    
    int pop_move() { //pop the move off the top and return it
        int top = moves.top();
        moves.pop();
        return top;
    }
    
    int get_num_cleaned() { //return number of cleaned squares
        return cleaned;
    }
    
    bool move(int where) { //logic behind movement
        if(where == SUCK) {
            ++cleaned;
            this_world->suck(curr_x, curr_y);
            return true;
        }
        else if(where == DLFT) {
            ++curr_y;
            return true;
        }
        else if(where == DRGT) {
            ++curr_x;
            return true;
        }
        else if(where == ULFT) {
            --curr_x;
            return true;
        }
        else if(where == URGT) {
            --curr_y;
            return true;
        }
        else {
            --cleaned;
            return false;
        }
    }
    
    void update_stack() { //logic behind figuring out what to do next
        int square = this_world->read_square(curr_x, curr_y); //read this square
        known_world[curr_y][curr_x] = square; //update our known world

        if(square == -1) {
            //backtrace
            return;
        }
        if(((curr_y+1) == this_world->get_num_rows()) && (((curr_x+1) == this_world->get_num_columns()))) {
            //we're at the opposite square from the start
            if(square == 1) {
                //clean it if necessary
                moves.push(SUCK);
            }
            return;
        }
        if(((curr_x+1) != this_world->get_num_columns()) && (known_world[curr_y][curr_x+1] > 0)) {
            //we push right as the lowest priority
            moves.push(DRGT);
        }
        if(((curr_y+1) != this_world->get_num_rows()) && (known_world[curr_y+1][curr_x] > 0)) {
            //we push down as the higher priority
            moves.push(DLFT);
        }
        if(this_world->read_square(curr_x, curr_y) == 1) {
            //we push suck as the highest priority
            moves.push(SUCK);
        }
    }
};