// Joseph Stone
// 422008041
// CSCE-420
// Due: February 12, 2018
// hw1pr3.cpp

#include <iostream>
#include <string>
#include "PuzzleSolver.h"

#define tab "    " //used for output readability

//print this puzzle out!
void print_puzzle(int** puzzle) {
    for(int y = 0; y < ROWS; y++) {
        for(int x = 0; x < COLS; x++) {
            std::cout << puzzle[y][x] << " ";
        }
        std::cout << std::endl << "          ";
    }
    std::cout << std::endl;
}

int main() {
    std::cout << "Welcome to 8-puzzle Solver!" << std::endl;
    
    std::string start, goal; //grab the inputs
    std::cout << "Enter 8-puzzle starting state by rows (0 for blank): ";
    std::getline(std::cin, start);
    std::cout << "Enter ending state by rows (0 for blank): ";
    std::getline(std::cin, goal);
    
    PuzzleSolver solve(start.c_str(), goal.c_str()); //initialize the PuzzleSolver
    
    std::cout << "Solution:" << std::endl; //begin output as specified in Homework.txt
    std::cout << tab << "Start ";
    print_puzzle(solve.get_curr_state());
    std::cout << "Swap the blank" << std::endl;
    
    int MAX = 0; //only allow 10 tries
    while(!solve.matches()) { //load up the stack
        switch(solve.generate_next()) { //print what the solver did
            case UP:
                std::cout << tab << "UP    "; std::cout.flush();
                print_puzzle(solve.get_curr_state());
                break;
            case RGHT:
                std::cout << tab << "RIGHT "; std::cout.flush();
                print_puzzle(solve.get_curr_state());
                break;
            case DOWN:
                std::cout << tab << "DOWN  "; std::cout.flush();
                print_puzzle(solve.get_curr_state());
                break;
            case LEFT:
                std::cout << tab << "LEFT  "; std::cout.flush();
                print_puzzle(solve.get_curr_state());
                break;
            case INIT:
                std::cout << tab << "ERROR" << std::endl; std::cout.flush();
                break;
        }
        ++MAX;
        if(MAX == 10) { //oops
            std::cout << "Error - solution has more than 10 moves." << std::endl;
            return 1;
        }
    }
    
    std::cout << "Done! Generated " << solve.get_count() << " states." << std::endl; //end output as specified in Homework.txt
    return 0;
}