// Joseph Stone
// 422008041
// CSCE-420
// Due: February 12, 2018
// hw1pr1.cpp

//strictly necessary
#include <iostream>
#include <string>
#include "Vacuum.h"

//included in order to procrastinate writing my ethics paper
#include <thread>
#include <chrono>

int main() {
    std::string input_rows, input_columns; //query the user for rows and columns
    std::cout << "Enter the number of rows: ";
    std::getline(std::cin, input_rows);
    std::cout << "Enter the number of columns: ";
    std::getline(std::cin, input_columns);

    int rows, columns; //convert string inputs into integers
    rows = stoi(input_rows);
    columns = stoi(input_columns);

    std::string line; //build and fill 2d world
    int** input_world = new int*[rows];
    for(int y = 0; y < rows; y++) {
        input_world[y] = new int[columns];
        std::cout << "Enter the data for this row, separated by commas: ";
        std::getline(std::cin, line);
        int index = 0;
        for(int x = 0; x < columns; x++) {
            int dirty = line[index] - 48;
            input_world[y][x] = dirty;
            index += 2;
        }
    }
    
    for(int y = 0; y < columns; y++) { //print out inputted initial world
    	for(int x = 0; x < rows; x++) {
            if(input_world[y][x] < 0) {
                std::cout << input_world[y][x] << " ";
            }
            else {
                std::cout  << " " << input_world[y][x] << " ";
            }
    	}
    	std::cout << std::endl;
    }

    World world(columns, rows, input_world); //create world

    std::cout << "Press enter to run the vacuum..."; //I like having an 'on' button
    std::getline(std::cin, line);
    std::cout << std::endl;
    
    Vacuum cleaner(world); //create cleaner, passing it world (it only uses it to suck, read current square, and see the world boundaries)
    cleaner.update_stack(); //figure out where to move initially
    
    while(!cleaner.finished()) { //move until the stack is empty
        int where = cleaner.pop_move(); //pull off first move
        //announce what we're doing
        if(where == cleaner.SUCK) { //clean the square
            std::cout << "SUCK" << std::endl;
            cleaner.move(where);
        }
        else if(where == cleaner.DLFT) { //move down, push an up to stack
            std::cout << "DOWN" << std::endl;
            cleaner.push_move(cleaner.URGT);
            cleaner.move(where);
            cleaner.update_stack();
        }
        else if(where == cleaner.DRGT) { //move right, push a left to stack
            std::cout << "RIGHT" << std::endl;
            cleaner.push_move(cleaner.ULFT);
            cleaner.move(where);
            cleaner.update_stack();
        }
        else if(where == cleaner.URGT) { //move up
            std::cout << "UP" << std::endl;
            cleaner.move(where);
        }
        else if(where == cleaner.ULFT) { //move left
            std::cout << "LEFT" << std::endl;
            cleaner.move(where);
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(250)); //makes it look more real-worldly
    }
    
    std::cout << "DONE" << std::endl; //we're done!

    input_world = world.get_world(); //print out the final world (should be all 0s)
    for(int y = 0; y < columns; y++) {
        for(int x = 0; x < rows; x++) {
            if(input_world[y][x] < 0) {
                std::cout << input_world[y][x] << " ";
            }
            else {
                std::cout  << " " << input_world[y][x] << " ";
            }
        }
        std::cout << std::endl;
    }

    return 0;
}