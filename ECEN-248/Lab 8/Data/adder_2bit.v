`timescale 1ns / 1ps
`default_nettype none

module adder_2bit(Carry, Sum, A, B);
	
	//declare all ports
	output wire Carry; 
	output wire [1:0] Sum;
	input wire [1:0] A, B;
	
	//intermediate wire
	wire intCarry;
	
	//call the wires
	full_adder add0(Sum[0], intCarry, A[0], B[0], 1'b0);
	full_adder add1(Sum[1], Carry, A[1], B[1], intCarry);

endmodule