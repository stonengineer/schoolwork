`timescale 1ns / 1ps

module d_flip_flop(Q, notQ, Clk, D);

	//declare all ports
	output wire Q, notQ; //outputs of slave
	input wire Clk, D;
	
	//intermediate nets
	wire notClk;
	wire Qm;
	wire notQm; //notQm is used in instantiation
					//but left unconnected
	not #2 not0(notClk, Clk);
	
	//instantiate the D-Latches
	d_latch d0(Qm, notQm, notClk, D);
	d_latch d1(Q, notQ, Clk, Qm);
	
endmodule