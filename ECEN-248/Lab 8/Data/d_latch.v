`timescale 1ns / 1ps

module d_latch(Q, notQ, En, D);

	//declare all ports
	output wire Q, notQ;
	input wire En, D;
	
	//Intermediate wires
	wire invD, nandDEN, nandIDEN;
	
	//First round of gates
	not #2 not0(invD, D);
	nand #2 nand0(nandDEN, D, En);
	nand #2 nand1(nandIDEN, invD, En);
	
	//Output gates
	nand #2 nand2(Q, nandDEN, notQ);
	nand #2 nand3(notQ, nandIDEN, Q);
	
endmodule