`timescale 1ns / 1ps

module d_flip_flop_behavioral(
	output reg Q, //described in behavorial statement
	output wire notQ, //described in dataflow statement
	input wire D, Clk //input and clock signal
	);
	
	//describe behavior of D flip-flop
	//(posedge means positive edge)
	always@(posedge Clk) //trigger when rising edge of Clk
		Q <= D; //non-block assignment statement
	
	assign notQ = ~Q;
	
endmodule