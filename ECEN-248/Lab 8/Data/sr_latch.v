`timescale 1ns / 1ps

module sr_latch(Q, notQ, En, S, R);

	//declare all ports
	output wire Q, notQ;
	input wire En, S, R;
	
	//intermediate nets
	wire nandSEN, nandREN;
	nand #4 nands(nandSEN, S, En);
	nand #4 nandr(nandREN, R, En);
	
	//delay of nand0 is 2ns
	nand #4 nand0(Q, nandSEN, notQ);
	nand #4 nand1(notQ, nandREN, Q);
	
endmodule