`timescale 1ns / 1ps
`default_nettype none
/* This Verilog module describes a 4-bit ALU unit *
 * with addition/subtraction units and 4:1 MUXs,  *
 * which serves the purpose of performing additi- *
 * on, subtraction, and an AND function, based on *
 * the user's input.                              */

module four_bit_alu (
	output wire [3:0] Result, //4-bit output
	output wire Overflow, //1-bit signal for overflow
	input wire [3:0] opA, opB, //4-bit operands
		/* ctrl | oper *
		 *  00  |  AND *
		 *  01  |  ADD *
		 *  10  |  AND *
		 *  11  |  SUB */
	input wire [1:0] ctrl //2-bit operation select
	);
	
	//generate AND output
	wire[3:0] andAB;
	assign andAB = opA & opB;
	
	//generate add/sub output
	wire[3:0] sumAB;
	wire tempOverflow;
	add_sub add1(sumAB, tempOverflow, opA, opB, ctrl[1]);
	
	//4:1 MUX into Result
	four_bit_mux mux1(Result, andAB, sumAB, ctrl[0]);
	
	//Overflow output
	assign Overflow = add1.Overflow & ctrl[0];
	
endmodule