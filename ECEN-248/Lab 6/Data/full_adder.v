`timescale 1ns / 1ps
`default_nettype none
/* This module describes the gate level *
 * model of a full adder in Verilog     */
 
module full_adder(S, Cout, A, B, Cin);

	//declare output and input ports
	input wire A, B, Cin; //1 bit input wires
	output wire S, Cout; //1 bit output wires
	
	//declare internal nets
	wire andBCin, andACin, andAB; //1 bit wires
	
	//use dataflow to describe the gate level activity
	assign S = A ^ B ^ Cin; //the carat (^) is for XOR
	assign andAB = A & B; //the ampersand (&) is for AND
	assign andACin = A & Cin;
	assign andBCin = B & Cin;
	assign Cout = andAB | andACin | andBCin; //the pipe (j) is for OR
	
endmodule