`timescale 1ns / 1ps
`default_nettype none

/* This module describes a FSM created *
 * to manage the traffic light at a    *
 * highway to farm road intersection   */

module t1c_fsm(
    output reg [2:0] state, //output for debugging
    output reg RstCount, //use an always block
    output reg [1:0] highwaySignal, farmSignal,
    input wire [30:0] Count, //times lights
    input wire Clk, Rst //clock and reset
);

    parameter S0 = 3'b001
              S1 = 3'b010
              S2 = 3'b011
              S3 = 3'b100
              S4 = 3'b101
              S5 = 3'b110

    //intermediate nets
    //reg [2:0] state; //6 states requre 3 bits
    reg [2:0] nextState; //driven by always block
    
    always@(state)
        case(state)
            S0: begin
                highwaySignal = 2'b10;
                farmSignal = 2'b10;
            end
            S1: begin
                highwaySignal = 2'b00;
                farmSignal = 2'b10;
            end
            S2: begin
				highwaySignal = 2'b01;
				farmSignal = 2'b10;
			end
			S3: begin
				highwaySignal = 2'b10;
				farmSignal = 2'b10;
			end
			S4: begin
				highwaySignal = 2'b10;
				farmSignal = 2'b00;
			end
			S5: begin
				highwaySignal = 2'b10;
				farmSignal = 2'b01;
			end
		endcase

	always@(posedge Clk)
		case(state)
			S0: begin
				if(Count == 50000000) begin
					nextState = S1;
					RstCount = 1;
				end
				else begin
					nextState = S0;
					RstCount = 0;
				end
			end
			S1: begin
				if(Count == 1500000000) begin
					nextState = S2;
					RstCount = 1;
				end
				else begin
					nextState = S1;
					RstCount = 0;
				end
			end
			S2: begin
				if(Count == 150000000) begin
					nextState = S3;
					RstCount = 1;
				end
				else begin
					nextState = S2;
					RstCount = 0;
				end
			end
			S3: begin
				if(Count == 50000000) begin
					nextState = S4;
					RstCount = 1;
				end
				else begin
					nextState = S3;
					RstCount = 0;
				end
			end
			S4: begin
				if(Count == 750000000) begin
					nextState = S5;
					RstCount = 1;
				end
				else begin
					nextState = S4;
					RstCount = 0;
				end
			end
			S5: begin
				if(Count == 150000000) begin
					nextState = S0;
					RstCount = 1;
				end
				else begin
					nextState = S5;
					RstCount = 0;
				end
			end
		endcase
	
	always@(posedge Clk)
		if(Rst) //reset state
			state <= S0;
		else
			state <= nextState;

endmodule 