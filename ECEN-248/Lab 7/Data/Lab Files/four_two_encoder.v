`timescale 1ns / 1ps
`default_nettype none

/* This module describes a 2:4 encoder using *
 * behavioral constructs in Verilog HDL.     */
 
module four_two_encoder(
	input wire [3:0] W,
	output wire zero,
	output reg [1:0] Y
	);
	
	//we can mix levels of abstraction
	assign zero = (W == 4'b0000); //tests for zero
	
	//behavioral portion
	always@(W) //run when W changes
		case(W) //selection based on W
			4'b0001: Y = 2'b00; //when 0001 is input, 00 returns
			4'b0010: Y = 2'b01; //when 0010 is input, 01 returns
			4'b0100: Y = 2'b10; //when 0100 is input, 10 returns
			4'b1000: Y = 2'b11; //when 1000 is input, 11 returns
			default: Y = 2'bXX; //default results in XX return
		endcase //end of case W

endmodule