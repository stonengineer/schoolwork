`timescale 1ns / 1ps
`default_nettype none

/* This module describes a 4-bit 2:1 MUX using *
 * behavioral methods in Verilog HDL.          */
 
module four_bit_mux(Y, A, B, S);
	
	//declare IO ports
	output reg [3:0] Y; //output is a 4-bit wide reg
	input wire [3:0] A, B; //A and B are 4-bit wide wires
	input wire S; //select is still 1-bit wide
	
	always@(A or B or S) //enacted whenever A, B, or S changes
		begin
			if(S == 1'b0) //if select = 0,
				Y = A; //set output to A.
			else //otherwise, select is 1, so
				Y = B; //set output to B.
		end
		
endmodule