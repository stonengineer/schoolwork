`timescale 1ns / 1ps
`default_nettype none

/* This module describes a 4-bit, 4:1 MUX *
 * using behavioral constructs in Verilog */
 
module mux_4bit_4to1(Y, A, B, C, D, S);

	//declare IO Ports
	output reg [3:0] Y; //output is a 4-bit wide reg
	input wire [3:0] A, B, C, D; //input are 4-bit wide wires
	input wire [1:0] S; //select is a 2-bit wire
	
	always@(*) //trigger when anything changes
		//we do not use begin/end because case is one clause
		case(S) //select based on S
			2'b00: Y = A; //when select is 00
			2'b01: Y = B; //when select is 01
			2'b10: Y = C; //when select is 10
			2'b11: Y = D; //when select is 11
		endcase //designates end of case statement
		
endmodule