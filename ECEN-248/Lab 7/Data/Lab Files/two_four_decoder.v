`timescale 1ns / 1ps
`default_nettype none

/* This module describes a 2:4 decoder using *
 * behavioral constructs in Verilog HDL.     */
 
module two_four_decoder(
	input wire [1:0] W,
	input wire En,
	output reg [3:0] Y
	);
	
	always@(En or W) //runs when En or W changes
		if(En == 1'b1) //can nest case in if
			case(W) //selection based on W
				2'b00: Y = 4'b0001; //input of 00 yields 0001
				2'b01: Y = 4'b0010; //input of 01 yields 0010
				2'b10: Y = 4'b0100; //input of 10 yields 0100
				2'b11: Y = 4'b1000; //input of 11 yields 1000
			endcase //designates end of case
		else //if En is false
			Y = 4'b0000; //when En is 0, Y = 0000

endmodule