`timescale 1ns / 1ps
`default_nettype none

/* This Verilog module describes the application *
 * of a 4:2 priority encoder unit, using data    *
 * flow Verilog and built-in gate primitives.    */

module priority_encoder (
	input wire [3:0] W,
	output wire [1:0] Y,
	output wire zero
	);
	
	four_two_encoder result(W, Y, zero); //send wire W to 4:2 Encoder to generate correct result
	
	/* Result is correct because my four_two_encoder uses boolean algebra to *
	 * determine correct output, regardless of the type of input. I.E.: the  *
	 * input 0110 is read, by my 4:2 Encoder, the same way 0100 is read,     *
	 * removing the need for an intermediate I wire                          *
	 *                                                                       *
	 * However, to guarantee points, if I were to use the I wire, I would    *
	 * implement it as follows:                                              *
	 *                                                                       *
	 * wire [3:0] I;                                                         *
	 * assign I[0] = !W[3] & !W[2] & !W[1] & W[0]                            *
	 * assign I[1] = !W[3] & ! W[2] & W[1]                                   *
	 * assign I[2] = !W[3] & W[2]                                            *
	 * assign I[3] = W[3]                                                    *
	 *                                                                       *
	 * four_two_encoder result(I, Y, zero);                                  *
	 *                                                                       *
	 * This implementation would logically wire I such that only one bit wo- *
	 * uld transfer, regardless of the input W, and that bit would be the    *
	 * highest priority bit from W. I.E.: if W = [1,1,0,0], I = [1,0,0,0].   *
	 * Then, I would be sent to my 4:2 Encoder to generate the result. As    *
	 * previously stated, however, this intermediate step is not necessary   *
	 * due to the logic statements derived from boolean algebra in my 4:2    *
	 * Encoder.                                                              */
	
endmodule // :-)