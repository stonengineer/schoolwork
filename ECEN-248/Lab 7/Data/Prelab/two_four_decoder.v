`timescale 1ns / 1ps
`default_nettype none

/* This Verilog module describes the application *
 * of a 2:4 decoder unit, using structural Veri- *
 * log and built-in gate primitives.             */

module two_four_decoder (
	input wire [1:0] W,
	input wire En,
	output wire [3:0] Y
	);
	
	//generate output
	and a0 (Y[0], En, !W[1], !W[0]); //output based on structural concepts
	and a0 (Y[1], En, !W[1], W[0]); //output based on structural concepts
	and a0 (Y[2], En, W[1], !W[0]); //output based on structural concepts
	and a0 (Y[3], En, W[1], W[0]); //output based on structural concepts
	
endmodule