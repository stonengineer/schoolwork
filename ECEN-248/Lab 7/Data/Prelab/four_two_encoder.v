`timescale 1ns / 1ps
`default_nettype none

/* This Verilog module describes the application *
 * of a 4:2 encoder unit, using data flow Veril- *
 * og and built-in gate primitives.              */

module four_two_encoder (
	input wire [3:0] W,
	output wire [1:0] Y,
	output wire zero
	);
	
	//generate output
	assign zero = !(W[3] | W[2] | W[1] | W[0]); //zero is 1 only when all inputs are 0
	assign Y[1] = W[3] | W[2]; //priority bit is 1 when W3 or W2 is 1
	assign Y[0] = W[3] | (!W[2] & W[1]); //low-priority bit is 1 when W3 is 1, or when 
	                                     //W2 is 0 and W1 is 1. This creates the a logic
										 //flow in accordance to the 5-bit truth table given,
										 //but with respect to the priority of the input bits.
										 //This respect allows for the input of any boolean value
										 //into the encoder, with a priority result output instead 
										 //of an error.
		
endmodule