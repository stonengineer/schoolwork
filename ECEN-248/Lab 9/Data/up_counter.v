`timescale 1ns / 1ps
`default_nettype none

/* This module describes a simple *
 * 4-bit up-counter using half-   *
 * adder modules                  */

module up_counter(Count, Carry3, En, Clk, Rst);
	
	//Define IO utilities
	output reg [3:0] Count;
	output wire Carry3;
	input wire En, Clk, Rst;
	
	//intermediate nets
	wire [3:0] Carry, Sum;
	
	half_adder h0(Sum[0], Carry[0], En, Count[0]);
	half_adder h1(Sum[1], Carry[1], Carry[0], Count[1]);
	half_adder h2(Sum[2], Carry[2], Carry[1], Count[2]);
	half_adder h3(Sum[3], Carry[3], Carry[2], Count[3]);
	
	assign Carry3 = Carry[3];
	
	//Positive edge triggered flip-flops
	//for count with asynchronous reset
	always@(posedge Clk or posedge Rst)
		if(Rst)
			Count <= 0; //reset count
		else
			Count <= Sum;

endmodule