`timescale 1ns / 1ps
`default_nettype none

/* This simple module will demonstrate the *
 * concept of clock frequency division us- *
 * ing a simple counter. We will use beha- *
 * vioral Verilog for this module          */

module clock_divider(ClkOut, ClkIn);
	
	//ouput needs to be reg because we will
	//drive it with a behavioral statement
	output wire [3:0] ClkOut;
	input wire ClkIn; //wires can drive regs
	
	//can be changed at compile time
	parameter n = 26; //make count 5-bits for now
	
	reg [n-1:0] Count; //count bit width based on n
	
	always@(posedge ClkIn)
		Count <= Count + 1; //increment count
	
	//wire from least to most significant bit
	assign ClkOut[3:0] = Count[n-1:n-4];
	
endmodule