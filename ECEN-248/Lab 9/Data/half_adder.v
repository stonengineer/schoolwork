`timescale 1ns / 1ps
`default_nettype none

/* This is a half-adder using *
 * dataflow Verilog           */

module half_adder(S, Cout, A, B);
	
	//initilize IO wires
	output wire S, Cout;
	input wire A, B;
	
	//output to wires
	assign S = A ^ B; //A XOR B
	assign Cout = A & B; //A AND B
	
endmodule