`timescale 1ns / 1ps
`default_nettype none

/* This is the top-level module that *
 * wires all of our synchronous com- *
 * ponents together.                 */

module top_level(LEDs, SWs, North, South, FastClk);
	
	//all ports will be wires because
	//we will use structural Verilog
	//define IO lines
	output wire[4:0] LEDs;
	input wire[1:0] SWs;
	input wire North, South, FastClk;
	
	//intermediate nets
	wire [3:0] Clocks;
	reg SlowClk; //will use always block for MUX
	
	//behavioral description of a mux that
	//selects between four available clock signals
	always@(*)
		case(SWs)
			2'b00: SlowClk = Clocks[0];
			2'b01: SlowClk = Clocks[1];
			2'b10: SlowClk = Clocks[2];
			2'b11: SlowClk = Clocks[3];
		endcase
	
	//instantiate up counter
	up_counter up(LEDs[3:0], LEDs[4], North, SlowClk, South);
	
	//instantiate clock divider
	clock_divider clk_div0(
		.ClkOut(Clocks),
		.ClkIn(FastClk)
	);

endmodule