`timescale 1ns / 1ps
`default_nettype none

/* This behavioral Verilog description *
 * models an up/down counter that      *
 * counts between 0 and 19             */

module up_down_counter(
	output reg [4:0] Count, //need 5 bits to get to 19
	input wire Up, Down, //tells us to count up or down
	input wire Clk, South //clock and if we need to reset
);

	//synchronous logic with synchronous reset
	always@(posedge Clk)
		if(South)
			Count <= 0;
		else if(Up)
			begin
				if(Count == 19) //if at max num
					Count <= 0; //roll over
				else //count up
					Count <= Count + 1;
			end
		else if(Down)
			begin
				if(Count == 0) //if at min num
					Count <= 19; //roll over
				else //count down
					Count <= Count - 1;
			end

endmodule