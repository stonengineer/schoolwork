`timescale 1ns / 1ps
`default_nettype none

/* This is a behavioral verilog description *
 * of a 2-bit saturating counter.           */

module saturating_counter(Count, Up, Clk, Rst);
	
	//IO Wires
	output wire [1:0] Count; //2-bit output
	input wire Up; //input bit asserted for up
	input wire Clk, Rst; //synchronous circuit input

	parameter S0 = 2'b00,
			  S1 = 2'b01,
              S2 = 2'b10,
              S3 = 2'b11;

    //intermediate nets
    reg [1:0] state; //4 states
    reg [1:0] nextState; //holds next state

    always@(*)
        case(state)
            S0: begin
                if(Up) //count up
                    nextState = S1;
                else //saturate
                    nextState = S0;
            end
            S1: begin
                if(Up) //count up
                    nextState = S2;
                else //count down
                    nextState = S0;
            end
            S2: begin
                if(Up) //count up
                    nextState = S3;
                else //count down
                    nextState = S1;
            end
            S3: begin
                if(Up) //saturate
                    nextState = S3;
                else //count down
                    nextState = S2;
            end
        endcase //no default because all cases have been accounted for

    always@(posedge Clk) //describe state logic
        if(Rst) //reset state
            state <= S0;
        else
            state <= nextState;

    //describe output logic
    assign Count = state;

endmodule