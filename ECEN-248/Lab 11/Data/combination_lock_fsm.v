`timescale 1ns / 1ps
`default_nettype none

/* This module describes the combination- *
 * lock FSM discussed in the prelab using *
 * behavioral Verilog HDL.                */

module combination_lock_fsm (
	output reg [2:0] state, //output state
	output wire Locked, //asserted when locked
	input wire Right, Left, //indicate direction
	input wire [4:0] Count, //indicate position
	input wire Center, //unlock button
	input wire Clk, South //clock and reset
);

    //define states
	parameter S0 = 3'b000,
	          S1 = 3'b001,
              S2 = 3'b010,
              S3 = 3'b011,
              S4 = 3'b100;

    //intermediate nets
    reg[2:0] nextState; //next state
    wire lockTest = 1;

    //next state logic
    always@(*)
        case(state)
            S0: begin
                if(Right)
                    nextState = S1;
                else
                    nextState = S0;
            end
            S1: begin
                if(Left && Count == 5'b01101)
                    nextState = S2;
                else begin
                    if(Left)
                        nextState = S0;
                    else 
                        nextState = S1;
                end
            end
            S2: begin
                if(Right && Count == 5'b00111)
                    nextState = S3;
                else begin
                    if(Right)
                        nextState = S0;
                    else 
                        nextState = S2;
                end
            end
            S3: begin
                if(Center && Count == 5'b10001)
                begin
                    nextState = S4;
                    lockTest = 0;
                end
                else begin
                    if(Center)
                        nextState = S0;
                    else 
                        nextState = S3;
                end
            end
            S4: begin
                nextState = S4;
                lockTest = 0;
            end
        endcase

    always@(posedge Clk) //describe state logic
        if(South) //reset state
            state <= S0;
        else begin
            state <= nextState;
            assign Locked = lockTest;
        end

endmodule 