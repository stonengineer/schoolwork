`timescale 1ns/1ps
`default_nettype none

module half_adder(S,Cout,A,B);
	// Inputs A and B
	input wire A,B;
	// S = Sum, Cout = overflow
	output wire Cout,S;
	
	// S = A xor B
	assign S = A^B;
	// Cout = A and B
	assign Cout = A & B;
endmodule