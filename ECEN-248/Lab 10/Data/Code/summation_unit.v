
/*
	Sums all values from the carry_lookahead_unit and the generate_propagation_unit

*/
module summation_unit(S,P,C);
	//Declare inputs and outputs 
	input wire [3:0] G,C;
	output wire [3:0] S;
	// Si = Pi XOR Ci
	assign S[0] = P[0] ^ C[0];
	assign S[1] = P[1] ^ C[1];
	assign S[2] = P[2] ^ C[2];
	assign S[3] = P[3] ^ C[3];
endmodule
