
module carry_lookahead_unit(C,G,P,C0);
	// We use wires becuase we will be using assigns
	output wire [4:1] C; //C4, C3, C2, C1
	input wire [3:0] G,P; // Input binary values for addition
	input wire C0; // Input Carry

	//Compute individual Carries according to the Carry Lookahead
	// .... this is going to be some crazy assigns... 

	//C1
	assign C[1] = G[0] | P[0]&C0;
	assign C[2] = G[1] | P[1]&G[0] | P[1]&P[0]&C0;
	assign C[3] = G[2] | P[2]&G[1] | P[2]&P[1]&G[0] | P[2]&P[1]&P[0]&C0;
	//There has to be a better way to do this, but we can't use the carries because wouldnt that defeat the point... 
	assign C[4] = G[3] | P[3]&G[2] | P[3]&P[2]&G[1] | P[3]&P[2]&P[1]&G[0] | P[3]&P[2]&P[1]&P[0]&C0;

endmodule