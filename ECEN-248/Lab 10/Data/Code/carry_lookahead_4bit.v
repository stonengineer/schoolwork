module carry_lookahead_4bit (Cout , S, X, Y, Cin ); 
/* ports are wires as we will use structural */ 
output wire Cout ; // C4 from the carry lookahead unit
// C 4 for a 4−bit adder 
output wire [3:0] S;  // Sum from the summation unit
// final 4−bit sum vector 
input wire [3:0] X, Y; // inputs for the propagation generator
// the 4−bit addends 
input wire Cin ; // C0, input Carry
// Internal nets for propogation
wire [3:0] G,P;
wire [4:1] C; 
// Get our G and Ps from the propagation unit
generate_propagation_unit unit1(G,P,X,Y);
//pass them with the C0 into the Carry lookahead unit
carry_lookahead_unit lookahead_unit1(C,G,P,C0);
//Set the overflow bit C4
assign Cout  = C[4];
//Sum the carrys and propagated values
summation_unit sum1(S,P,C);
//Thus we have a carry lookahead adder

endmodule