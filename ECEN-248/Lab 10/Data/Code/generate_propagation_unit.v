

module generate_propagation_unit(G,P,X,Y);
	// 4-bit X, adding x0, y0
	input wire [3:0] X,Y;
	// P = Carry, G = Sum
	output wire [3:0] G,P;
	/*
		Implement propagation throughout the circuit
		G[i] = Xi AND Yi
		P[i] = Xi XOR Yi
		Look y' thurrr!
	*/
	assign G[0] = X[0] & Y[0];
	assign P[0] = X[0] ^ Y[0];
	assign G[1] = X[1] & Y[1];
	assign P[1] = X[1] ^ Y[1];
	assign G[2] = X[2] & Y[2];
	assign P[2] = X[2] ^ Y[2];
	assign G[3] = X[3] & Y[3];
	assign P[3] = X[3] ^ Y[3];
endmodule