/* This module describes the block carry *
 * lookahead unit for a 2 level carry-   *
 * lookahead adder of 16-bits            */

module block_carry_lookahead_unit(G_star, P_star, C, G, P, C0);

	//Define IO wires
	output wire G_star, P_star; //block generate and propogate
	output wire [3:1] C; //C3, C2, C1
	input wire [3:0] G, P; //generates and propogates
	input wire C0; //input carry

	//Assign C outputs
	assign #4 C[1] = G[0] | (P[0] & C0);
	assign #4 C[2] = G[1] | (P[1] & C[1]);
	assign #4 C[3] = G[2] | (P[2] & C[2]);
	
	//Assign star outputs
	assign #4 G_star = G[3] | (P[3] & C[3]);
	assign #2 P_star = P[0] & P[1] & P[2] & P[3];
	
endmodule 