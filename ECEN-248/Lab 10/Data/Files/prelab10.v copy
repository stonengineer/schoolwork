`timescale 1ns / 1ps
`default_nettype none

/* This file describes the different modules *
 * required by the prelab for Lab 10. I will *
 * be using dataflow Verilog to describe     *
 * the following modules.                    */

/* ----------------------------------------- */

/* This module describes the Carry Generate/ *
 * Propagate Unit for a 4-bit Carry-lookahe- *
 * ad addition.                              */

module generate_propagate_unit(G, P, X, Y);
    
    //Define IO wires
    output wire [3:0] G, P;
    input wire [3:0] X, Y;

    //Provide logical output
    assign G = X & Y;
    assign P = X ^ Y;

endmodule

/* ----------------------------------------- */

/* This module describes the 4-bit Carry-    *
 * lookahead unit for a Carry-Lookahead Addr */

module carry_lookahead_unit(C, G, P, C0);
    
    //Define IO wires
    output wire [4:1] C; //C4-C1
    input wire [3:0] G, P; //Generate/Prop
    input wire C0; //input carry

    //Define intermediate nets
    wire C11, C21, C22, C31, C32, C33;
    wire C41, C42, C43, C44;

    //Assign intermediate nets
    assign C11 = P[0] & C0;
    assign C21 = P[1] & G[0];
    assign C22 = P[1] & P[0] & C0;
    assign C31 = P[2] & G[1];
    assign C32 = P[2] & P[1] & G[0];
    assign C33 = P[2] & P[1] & P[0] & C0;
    assign C41 = P[3] & G[2];
    assign C42 = P[3] & P[2] & G[1];
    assign C43 = P[3] & P[2] & P[1] & G[0];
    assign C44 = P[3] & P[2] & P[1] & P[0] & C0;

    //Write outputs
    assign C[4] = G[3] | C41 | C42 | C43 | C44;
    assign C[3] = G[2] | C31 | C32 | C33;
    assign C[2] = G[1] | C21 | C22;
    assign C[1] = G[0] | C11;

endmodule

/* ----------------------------------------- */

/* This module describes the 4-bit Summation *
 * Unit for a Carry-Lookahead Adder          */

module summation_unit(S, P, C);
    
    //Define IO wires
    output wire [3:0] S; //Resulting sum
    input wire [3:0] P, C; //Prop and carry

    //Write output
    S = P ^ C;

endmodule

/* ----------------------------------------- */

/* This is the top-level module for a 4-bit  *
 * carry-lookahead adder                     */

module carry_lookahead_4bit(Cout, S, X, Y, Cin);
    
    //Define IO wires
    output wire Cout; //C4 for 4-bit adder
    output wire [3:0] S; //Resulting sum
    input wire [3:0] X, Y; //Input numbers
    input wire Cin; //Input carry

    //Define intermediate nets
    wire [3:0] Cint, G, P;

    //Give values to nets
    generate_propagate_unit g0(G, P, X, Y);
    carry_lookahead_unit c0(Cint, G, P, Cin);

    //Generate output
    summation_unit s0(S, P, Cint);
    assign Cout = Cint[3];

endmodule

/* ----------------------------------------- */

/* Problem 3: 
 * The gate-count of my carry-lookahead adder 
 * is 8 + 14 + 4 = 26 gates.
*/

/* ----------------------------------------- */

/* Problem 4:
 * - 4 Unit propagation delay
 * - 32 + (14 * 4) + 16 = 104 Gates
*/

/* ----------------------------------------- */