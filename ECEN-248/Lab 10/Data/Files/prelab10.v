`default_nettype none

/* This file describes the different modules *
 * required by the prelab for Lab 10. I will *
 * be using dataflow Verilog to describe     *
 * the following modules.                    */

/* ----------------------------------------- */

/* This module describes the Carry Generate/ *
 * Propagate Unit for a 4-bit Carry-lookahe- *
 * ad addition.                              */

module generate_propagate_unit(G, P, X, Y);
    
    //Define IO wires
    output wire [15:0] G, P;
    input wire [15:0] X, Y;

    //Provide logical output
    assign #2 G = X & Y;
    assign #2 P = X ^ Y;

endmodule

/* ----------------------------------------- */

/* This module describes the 4-bit Carry-    *
 * lookahead unit for a Carry-Lookahead Addr */

module carry_lookahead_unit(C, G, P, C0);
    
    //Define IO wires
    output wire [4:1] C; //C4-C1
    input wire [3:0] G, P; //Generate/Prop
    input wire C0; //input carry

    //Write outputs
	 assign #4 C[1] = G[0] | (P[0] & C0);
	 assign #4 C[2] = G[1] | (P[1] & C[1]);
	 assign #4 C[3] = G[2] | (P[2] & C[2]);
	 assign #4 C[4] = G[3] | (P[3] & C[3]);

endmodule

/* ----------------------------------------- */

/* This module describes the 4-bit Summation *
 * Unit for a Carry-Lookahead Adder          */

module summation_unit(S, P, C);
    
    //Define IO wires
    output wire [15:0] S; //Resulting sum
    input wire [15:0] P, C; //Prop and carry

    //Write output
    assign #2 S = P ^ C;

endmodule

/* ----------------------------------------- */

/* This is the top-level module for a 4-bit  *
 * carry-lookahead adder                     */

module carry_lookahead_4bit(Cout, S, X, Y, Cin);
    
    //Define IO wires
    output wire Cout; //C4 for 4-bit adder
    output wire [3:0] S; //Resulting sum
    input wire [3:0] X, Y; //Input numbers
    input wire Cin; //Input carry

    //Define intermediate nets
    wire [4:1] Cint;
	 wire [3:0] G, P, C;

    //Give values to nets
    generate_propagate_unit g0(G, P, X, Y);
    carry_lookahead_unit c0(Cint, G, P, Cin);
	 assign C[0] = Cin;
	 assign C[1] = Cint[1];
	 assign C[2] = Cint[2];
	 assign C[3] = Cint[3];
	 
    //Generate output
    summation_unit s0(S, P, C);
    assign Cout = Cint[4];

endmodule

/* ----------------------------------------- */

/* Problem 3: 
 * The gate-count of my carry-lookahead adder 
 * is 8 + 14 + 4 = 26 gates.
*/

/* ----------------------------------------- */

/* Problem 4:
 * - 4 Unit propagation delay
 * - 32 + (14 * 4) + 16 = 104 Gates
*/

/* ----------------------------------------- */