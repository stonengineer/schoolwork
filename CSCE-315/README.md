# CSCE-315
Programming Studio
This course is intended to be an intensive programming experience that integrates core concepts in Computer Science and familiarizes students with a variety of programming/development tools and techniques. Students will primarily work in small teams on month-long projects emphasizing different specializations within computer science. The course focuses on honing good programming techniques to ease code integration, reuse, and clarity. The primary goal for this class is to have students emerge with strong programming skills, able to address both individual and team programming challenges competently. The class is meant to allow students to improve their programming skills through significant practice.

Among the topics to be covered in lecture periods are:

Style considerations in writing code.
Design of software systems and APIs.
Coding beyond the single component.
Design for portability, performance, testability.
Specification and documentation.
Basic software tools and their use.
Subject-specific topics related to the team projects (DB, AI, HCI)
Though many topics will overlap, this course is not intended to be as in-depth or comprehensive as a standard software engineering course, which focuses more on project management - students may take the software engineering class after taking this class.

http://faculty.cs.tamu.edu/tanzir/fall2017/315/