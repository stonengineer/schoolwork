#include "book.h"

Book::Book()
{
	//This does nothing
}

Book::Book(std::string BOOK_ISBN, std::string BOOK_TITLE)
{
	//push data to Book and update DEFINED_ASPECTS accordingly
	ISBN = BOOK_ISBN;
	DEFINED_ASPECTS[0] = 1;
	TITLE = BOOK_TITLE;
	DEFINED_ASPECTS[1] = 1;
}

bool Book::set_author(std::string BOOK_AUTHOR)
{
	//set author and update DEFINED_ASPECTS accordingly
	AUTHOR = BOOK_AUTHOR;
	DEFINED_ASPECTS[2] = 1;
	return DEFINED_ASPECTS[2];
}

bool Book::set_edition(std::string BOOK_EDITION)
{
	//set edition and update DEFINED_ASPECTS accordingly
	EDITION = BOOK_EDITION;
	DEFINED_ASPECTS[3] = 1;
	return DEFINED_ASPECTS[3];
}

bool Book::set_pub_date(std::string BOOK_PUB_DATE)
{
	//set publication date and update DEFINED_ASPECTS accordingly
	PUB_DATE = BOOK_PUB_DATE;
	DEFINED_ASPECTS[4] = 1;
	return DEFINED_ASPECTS[4];
}

bool Book::set_new_cost(double BOOK_NEW_COST)
{
	//set new cost and update DEFINED_ASPECTS accordingly
	NEW_COST = BOOK_NEW_COST;
	DEFINED_ASPECTS[5] = 1;
	return DEFINED_ASPECTS[5];
}

bool Book::set_used_cost(double BOOK_USED_COST)
{
	//set used cost and update DEFINED_ASPECTS accordingly
	USED_COST = BOOK_USED_COST;
	DEFINED_ASPECTS[6] = 1;
	return DEFINED_ASPECTS[6];
}

bool Book::set_rent_cost(double BOOK_RENT_COST)
{
	//set rental cost and update DEFINED_ASPECTS accordingly
	RENT_COST = BOOK_RENT_COST;
	DEFINED_ASPECTS[7] = 1;
	return DEFINED_ASPECTS[7];
}

bool Book::set_ebook_cost(double BOOK_EBOOK_COST)
{
	//set ebook cost and update DEFINED_ASPECTS accordingly
	EBOOK_COST = BOOK_EBOOK_COST;
	DEFINED_ASPECTS[8] = 1;
	return DEFINED_ASPECTS[8];
}

void Book::print_book()
{
	//custom print book function
	
	//write XXXXX if we have no cost information
	std::string new_cost_display, used_cost_display, rent_cost_display, ebook_cost_display;
	if(NEW_COST == 0) { new_cost_display = "XXXXX"; }
	else { new_cost_display = std::to_string(NEW_COST); }
	if(USED_COST == 0) { used_cost_display = "XXXXX"; }
	else { used_cost_display = std::to_string(USED_COST); }
	if(RENT_COST == 0) { rent_cost_display = "XXXXX"; }
	else { rent_cost_display = std::to_string(RENT_COST); }
	if(EBOOK_COST == 0) { ebook_cost_display = "XXXXX"; }
	else { ebook_cost_display = std::to_string(EBOOK_COST); }

	printf(" %s | %16.16s | %8.8s | %7.1s | %7.7s | %5.5s | %5.5s | %5.5s | %5.5s\n", ISBN.c_str(), TITLE.c_str(), AUTHOR.c_str(), EDITION.c_str(), PUB_DATE.c_str(), new_cost_display.c_str(), used_cost_display.c_str(), rent_cost_display.c_str(), ebook_cost_display.c_str());
}

std::string Book::get_ISBN() { return ISBN; }

std::string Book::get_title() { return TITLE; }

std::string Book::get_author() { return AUTHOR; }

std::string Book::get_edition() { return EDITION; }

std::string Book::get_pub_date() { return PUB_DATE; }

double Book::get_new_cost() { return NEW_COST; }

double Book::get_used_cost() { return USED_COST; }

double Book::get_rent_cost() { return RENT_COST; }

double Book::get_ebook_cost() { return EBOOK_COST; }

bool* Book::get_defined_aspects() { return DEFINED_ASPECTS; }
