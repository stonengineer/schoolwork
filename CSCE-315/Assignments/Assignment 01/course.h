#ifndef _COURSE_H
#define _COURSE_H

#include <vector>
#include "book.h"

//struct to handle sections and book requirements
struct AssignedBook {
	Book book;
	int section_number;
	char requirement;
};

//class to handle Courses
class Course
{
private: //data specific to courses
	std::string DEPARTMENT_CODE;
	std::string NUMBER;
	std::string NAME;
	std::vector<AssignedBook> COURSE_BOOK_LIST;

public:
	Course(std::string COURSE_DEPARTMENT_CODE, std::string COURSE_NUMBER, std::string COURSE_NAME); //constructor
	bool assign_book(Book COURSE_BOOK, int COURSE_SECTION_NUMBER, char COURSE_BOOK_REQUIREMENT); //assigns book to class
	std::string get_department_code(); //returns DEPARTMENT_CODE
	std::string get_course_number(); //returns NUMBER
	std::string get_course_name(); //returns NAME
	std::vector<AssignedBook> get_book_list(); //returns COURSE_BOOK_LIST
	void print_course(); //custom print function
};

#endif
