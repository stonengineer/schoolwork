#ifndef _BOOK_H
#define _BOOK_H

#include <string>

//class to handle Courses
class Book 
{
private: //data specific to books
	std::string ISBN;
	std::string TITLE;
	std::string AUTHOR;
	std::string EDITION;
	std::string PUB_DATE;
	double NEW_COST;
	double USED_COST;
	double RENT_COST;
	double EBOOK_COST;
	bool DEFINED_ASPECTS[9] = {0,0,0,0,0,0,0,0,0}; //array of bools to handle what data has been given
	
public:
	Book(); //empty constructor
	Book(std::string BOOK_ISBN, std::string BOOK_TITLE); //data constructor
	
	//setter methods
	bool set_author(std::string BOOK_AUTHOR);
	bool set_edition(std::string BOOK_EDITION);
	bool set_pub_date(std::string BOOK_PUB_DATE);
	bool set_new_cost(double BOOK_NEW_COST);
	bool set_used_cost(double BOOK_USED_COST);
	bool set_rent_cost(double BOOK_RENT_COST);
	bool set_ebook_cost(double BOOK_EBOOK_COST);

	//custom print method
	void print_book();

	//getter methods
	std::string get_ISBN();
	std::string get_title();
	std::string get_author();
	std::string get_edition();
	std::string get_pub_date();
	double get_new_cost();
	double get_used_cost();
	double get_rent_cost();
	double get_ebook_cost();
	bool* get_defined_aspects();
};

#endif
