/* Individual Assignment
 * CSCE-315, s. 502
 * Joseph Stone, 422008041
 * 9/17/2017
*/

#include <iostream>   //handles standard io
#include <sstream>    //stringstreams allow for easy building of strings
#include <algorithm>  //for std::find method
#include "course.h"   //custom class, pulls course and book

std::vector<Book> defined_books;      //vector of the books defined this run
std::vector<Course> defined_courses;  //vector of the courses defined this run

/*main logic function
 *----------------------
 * INPUT: current line from stdin
 * OUTPUT: none
 * FUNCTION: defines the logic behind
 *   handling commands. These commands are
 *   defined in IndividualAssignment.pdf
*/
void handle_input(std::string line)
{
	//splits the input string into tokens delimited by spaces
	std::string buffer;
	std::vector<std::string> delimited_input;
	std::stringstream ss(line);
	while(ss >> buffer) { delimited_input.push_back(buffer); }

	//we perform functions based on the first token
	switch(delimited_input.at(0).at(0)) { //switch on the first letter of the token
		int isbn_index, course_index; //shared between several cases
		case 'B': { //adds book to library
			std::cout << "Adding book to library...";
			//add spaces back to the book's name
			std::ostringstream book_title;
			for(int i = 2; i < delimited_input.size(); i++) {
				book_title << delimited_input[i] << " ";
			}
			Book new_book(delimited_input[1], book_title.str()); //use Book class constructor to build new book
			defined_books.push_back(new_book); //add new book to defined books list
			std::cout << "done" << std::endl; //success!
			break;
		}
		case 'D': { //defines aspect of a defined book
			std::cout << "Defining ";
			//search defined_books for the book by ISBN
			isbn_index = -1;
			for(int i = 0; i < defined_books.size(); i++) {
				if(defined_books[i].get_ISBN() == delimited_input[1])
					isbn_index = i;
			}
			if(isbn_index == -1) { break; } //-1 means the book wasn't found
			if(delimited_input[2] == "A") { //define author
				std::cout << "author...";
				//add spaces back to author's name
				std::ostringstream book_author;
				for(int i = 3; i < delimited_input.size(); i++) {
					book_author << delimited_input[i] << " ";
				}
				defined_books[isbn_index].set_author(book_author.str()); //set the author's name
			}
			else if(delimited_input[2] == "E") { //define edition
				std::cout << "edition...";
				defined_books[isbn_index].set_edition(delimited_input[3]); //set the edition number
			}
			else if(delimited_input[2] == "D") { //define date
				std::cout << "date...";
				defined_books[isbn_index].set_pub_date(delimited_input[3]); //set the book's publishing date
			}
			std::cout << "done" << std::endl; //success!
			break;
		}
		case 'M': { //define a book's cost
			std::cout << "Defining ";
			//search defined_books for the book by ISBN
			isbn_index = -1;
			for(int i = 0; i < defined_books.size(); i++) {
				if(defined_books[i].get_ISBN() == delimited_input[1])
					isbn_index = i;
			}
			if(isbn_index == -1) { break; } //-1 means the book wasn't found
			double value = std::stod(delimited_input[2]); //convert string token to double
			if(delimited_input[3] == "N") { //define book's new cost
				std::cout << "new cost...";
				defined_books[isbn_index].set_new_cost(value);
			}
			else if(delimited_input[3] == "U") { //define book's used cost
				std::cout << "used cost...";
				defined_books[isbn_index].set_used_cost(value);
			}
			else if(delimited_input[3] == "R") { //define book's rental cost
				std::cout << "rent cost...";
				defined_books[isbn_index].set_rent_cost(value);
			}
			else if(delimited_input[3] == "E") { //define book's ebook cost
				std::cout << "ebook cost...";
				defined_books[isbn_index].set_ebook_cost(value);
			}
			std::cout << "done" << std::endl; //success!
			break;
		}
		case 'C': { //define new course
			std::cout << "Defining course...";
			std::string department_code = delimited_input[1];
			std::string course_number = delimited_input[2];
			//add spaces back to course's name
			std::ostringstream course_name;
			for(int i = 3; i < delimited_input.size(); i++) {
				course_name << delimited_input[i] << " ";
			}
			Course new_course(department_code, course_number, course_name.str()); //create new course
			defined_courses.push_back(new_course); //add course to defined_courses
			std::cout << "done" << std::endl; //success!
			break;
		}
		case 'A': { //assign book to course
			std::cout << "Assigning book to course...";
			//search defined_books for the book by ISBN
			isbn_index = -1;
			for(int i = 0; i < defined_books.size(); i++) {
				if(defined_books[i].get_ISBN() == delimited_input[1])
					isbn_index = i;
			}
			if(isbn_index == -1) { break; } //-1 means the book wasn't found
			//search defined_courses for the course by department code and course number
			course_index = -1;
			for(int i = 0; i < defined_courses.size(); i++) {
				if(defined_courses[i].get_department_code() == delimited_input[2]) {
					if(defined_courses[i].get_course_number() == delimited_input[3])
						course_index = i;
				}
			}
			if(course_index == -1) { break; } //-1 means the course wasn't found
			defined_courses[course_index].assign_book(defined_books[isbn_index], std::stoi(delimited_input[4]), delimited_input[5].at(0)); //assign book to course
			std::cout << "done" << std::endl; //success!
			break;
		}
		case 'G': { //a command beginning with a G could be any of 3 printing functions
			if(delimited_input[0][1] == 'C') { //C means to print all books in every section of a course
				printf("Printing all books for every section of %s %s\n", delimited_input[1].c_str(), delimited_input[2].c_str());
				printf(" SN | R/D |     ISBN      |       Title      |  Author  | Edition |   Date  |  New  | Used  | Rent  | Elec\n");
				for(int i = 0; i < defined_courses.size(); i++) { //search through all courses
					//if we find a course with a matching department code and course number...
					if(delimited_input[1].compare(defined_courses[i].get_department_code()) == 0 && delimited_input[2].compare(defined_courses[i].get_course_number()) == 0) {
						for(int b = 0; b < defined_courses[i].get_book_list().size(); b++) {
							//...we print all of the book's assigned to the course
							printf("%d |  %c  |", defined_courses[i].get_book_list()[b].section_number, defined_courses[i].get_book_list()[b].requirement);
							defined_courses[i].get_book_list()[b].book.print_book();
						}
					}
				}
			}
			if(delimited_input[0][1] == 'S') { //S means to print all books in a given section of a course
				printf("Printing all books for section %s of %s %s\n", delimited_input[3].c_str(), delimited_input[1].c_str(), delimited_input[2].c_str());
				printf(" R/D |     ISBN      |       Title      |  Author  | Edition |   Date  |  New  | Used  | Rent  | Elec\n");
				for(int i = 0; i < defined_courses.size(); i++) { //search through all courses
					//if we find a course with a matching department code and course number...
					if(delimited_input[1].compare(defined_courses[i].get_department_code()) == 0 && delimited_input[2].compare(defined_courses[i].get_course_number()) == 0) {
						for(int b = 0; b < defined_courses[i].get_book_list().size(); b++) {
							//...and if the course's assigned book is for the correct section...
							if(defined_courses[i].get_book_list()[b].section_number == std::stoi(delimited_input[3])) {
								//...we print the data for that book
								printf("  %c  |", defined_courses[i].get_book_list()[b].requirement);
								defined_courses[i].get_book_list()[b].book.print_book();
							}
						}
					}
				}
			}
			if(delimited_input[0][1] == 'B') { //B means to print all known information about a specific book
				printf("Printing all known information for ISBN: %s\n", delimited_input[1].c_str());
				printf("     ISBN      |       Title      |  Author  | Edition |   Date  |  New  | Used  | Rent  | Elec\n");
				for(int i = 0; i < defined_books.size(); i++) { //search through all books
					//if the ISBN's match...
					if(defined_books[i].get_ISBN().compare(delimited_input[1]) == 0) {
						//...print the book
						defined_books[i].print_book();
					}
				}
			}
			break;
		}
		case 'P': { //a command beginning with a P could be any of 5 printing functions
			if(delimited_input[0][1] == 'B') { //B means to print all defined books
				std::cout << "Printing all defined books." << std::endl;
				printf("     ISBN      |       Title      |  Author  | Edition |   Date  |  New  | Used  | Rent  | Elec\n");
				for(int i = 0; i < defined_books.size(); i++) { //iterate through defined_books...
					defined_books[i].print_book(); //...and print each book
				}
			}
			if(delimited_input[0][1] == 'C') { //C means to print all defined courses
				std::cout << "Printing all defined courses." << std::endl;
				printf(" Code | Num | Name\n");
				for(int i = 0; i < defined_courses.size(); i++) { //iterate through defined_courses...
					defined_courses[i].print_course(); //...and print each course
				}
			}
			if(delimited_input[0][1] == 'Y') { //Y means to print all books published on the specified date
				printf("Printing all books published %s.\n", delimited_input[1].c_str());
				printf("     ISBN      |       Title      |  Author  | Edition |   Date  |  New  | Used  | Rent  | Elec\n");
				for(int i = 0; i < defined_books.size(); i++) { //iterate through defined_books...
					//...if we find a matching publication date...
					if(delimited_input[1].compare(defined_books[i].get_pub_date()) == 0) {
						defined_books[1].print_book(); //...print the book
					}
				}
			}
			if(delimited_input[0][1] == 'D') { //D means to print all books in a given department
				printf("Printing all books in department %s.\n", delimited_input[1].c_str());
				printf("     ISBN      |       Title      |  Author  | Edition |   Date  |  New  | Used  | Rent  | Elec\n");
				std::vector<std::string> printed_books; //used to remove duplicate prints
				for(int i = 0; i < defined_courses.size(); i++) { //iterate through defined_courses
					//if we find a matching department code...
					if(delimited_input[1].compare(defined_courses[i].get_department_code()) == 0) {
						for(int b = 0; b < defined_courses[i].get_book_list().size(); b++) { //iterate through the assigned books for said course
							//if we have not printed this book already...
							if(std::find(printed_books.begin(), printed_books.end(), defined_courses[i].get_book_list()[b].book.get_ISBN()) == printed_books.end()) {
								defined_courses[i].get_book_list()[b].book.print_book(); //...print the book...
								printed_books.push_back(defined_courses[i].get_book_list()[b].book.get_ISBN()); //...and add the book's ISBN to the vector
							}
						}
					}
				}
			}
			if(delimited_input[0][1] == 'M') { //M means to print the maximum and minimum book cost average for a given department
				printf("Printing average min/max cost for all books in %s department.\n", delimited_input[1].c_str());
				double max_sum = 0; int max_count = 0; //handles max_average
				double min_sum = 0; int min_count = 0; //handles min_average
				for(int i = 0; i < defined_courses.size(); i++) { //iterate through all courses
					//if we find a matching department code...
					if(defined_courses[i].get_department_code().compare(delimited_input[1]) == 0) {
						for(int b = 0; b < defined_courses[i].get_book_list().size(); b++) { //iterate through the assigned books for said course
							double curr_max = -1; //hopefully smaller than 0
							double curr_min = 10000000000; //I doubt a book costs this much
							if(defined_courses[i].get_book_list()[b].book.get_defined_aspects()[5]) { //new book cost is defined
								if(defined_courses[i].get_book_list()[b].book.get_new_cost() > curr_max) { //test if cost is more than we've seen
									curr_max = defined_courses[i].get_book_list()[b].book.get_new_cost(); //update curr_max if so
								}
								if(defined_courses[i].get_book_list()[b].book.get_new_cost() < curr_min) { //test if cost is less than we've seen
									curr_min = defined_courses[i].get_book_list()[b].book.get_new_cost(); //update curr_min if so
								}
							}
							if(defined_courses[i].get_book_list()[b].book.get_defined_aspects()[6]) { //used book cost is defined
								if(defined_courses[i].get_book_list()[b].book.get_used_cost() > curr_max) { //test if cost is more than we've seen
									curr_max = defined_courses[i].get_book_list()[b].book.get_used_cost(); //update curr_max if so
								}
								if(defined_courses[i].get_book_list()[b].book.get_used_cost() < curr_min) { //test if cost is less than we've seen
									curr_min = defined_courses[i].get_book_list()[b].book.get_used_cost(); //update curr_min if so
								}
							}
							if(defined_courses[i].get_book_list()[b].book.get_defined_aspects()[7]) { //rental book cost is defined
								if(defined_courses[i].get_book_list()[b].book.get_rent_cost() > curr_max) { //test if cost is more than we've seen
									curr_max = defined_courses[i].get_book_list()[b].book.get_rent_cost(); //update curr_max if so
								}
								if(defined_courses[i].get_book_list()[b].book.get_rent_cost() < curr_min) { //test if cost is less than we've seen
									curr_min = defined_courses[i].get_book_list()[b].book.get_rent_cost(); //update curr_min if so
								}
							}
							if(defined_courses[i].get_book_list()[b].book.get_defined_aspects()[8]) { //ebook cost is defined
								if(defined_courses[i].get_book_list()[b].book.get_ebook_cost() > curr_max) { //test if cost is more than we've seen
									curr_max = defined_courses[i].get_book_list()[b].book.get_ebook_cost(); //update curr_max if so
								}
								if(defined_courses[i].get_book_list()[b].book.get_ebook_cost() < curr_min) { //test if cost is less than we've seen
									curr_min = defined_courses[i].get_book_list()[b].book.get_ebook_cost(); //update curr_min if so
								}
							}
							
							//if we found any cost definitions, factor them in and increment count
							if(curr_max != -1) { max_sum += curr_max; ++max_count; }
							if(curr_min != 10000000000) { min_sum += curr_min; ++min_count; }
						}
					}
				}
				printf("Max = $%5.2f | Min = $%5.2f\n", (max_sum/max_count), (min_sum/min_count)); //print everything
			}
			break;
		}
		default: { //whoops?
			std::cout << "Sorry, that code is not supported." << std::endl;
			break;
		}
	}
}

bool send_input_to_handler(std::string line) {
	//I ran into a weird problem where the code wouldn't allocate to
	//memory correctly, so this handles those cases (weird bug, couldn't
	//fix; but this handles everything and doesn't change the UI so...)
	try {
		handle_input(line);
		return true;
	}
	catch(...) {
		return false;
	}
}

int main() //main method
{
	//very straightforward
	std::cout
		<< "*******************************************\n"
		<< "*******************************************\n"
		<< "*** Course Textbook Information Program ***\n"
		<< "*******************************************\n"
		<< "*******************************************\n"
		<< "                                           \n"
		<< "The following commands are defined:        \n"
		<< "        B | D | M | C | A | GC | GS        \n"
		<< "        GB | PB | PC | PY | PD | PM        \n"
		<< "If you know the syntax for the command you \n"
		<< "wish to perform, enter it below.           \n"
		<< "To quit, press CTRL+D                      \n"
	<< std::endl;
	
	std::cout << "$ ";
	
	std::string line;
	while(std::getline(std::cin, line)) {
		send_input_to_handler(line);
		std::cout << "$ ";
	}
}
