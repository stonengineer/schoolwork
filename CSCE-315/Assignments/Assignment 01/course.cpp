#include "course.h"

//constructor
Course::Course(std::string COURSE_DEPARTMENT_CODE, std::string COURSE_NUMBER, std::string COURSE_NAME)
{
	//pull in necessary data
	DEPARTMENT_CODE = COURSE_DEPARTMENT_CODE;
	NUMBER = COURSE_NUMBER;
	NAME = COURSE_NAME;
}

//assigns book to course
bool Course::assign_book(Book COURSE_BOOK, int COURSE_SECTION_NUMBER, char COURSE_BOOK_REQUIREMENT)
{
	//create instance of AssignedBook struct, populate, and push to vector
	AssignedBook *new_book = new AssignedBook;
	new_book->book = COURSE_BOOK;
	new_book->section_number = COURSE_SECTION_NUMBER;
	new_book->requirement = COURSE_BOOK_REQUIREMENT;

	COURSE_BOOK_LIST.push_back(*new_book);

	return true; //success!
}

std::string Course::get_department_code() { return DEPARTMENT_CODE; }

std::string Course::get_course_number() { return NUMBER; }

std::string Course::get_course_name() { return NAME; }

std::vector<AssignedBook> Course::get_book_list() { return COURSE_BOOK_LIST; }

void Course::print_course() 
{
	printf(" %4.4s | %3.3s | %s\n", DEPARTMENT_CODE.c_str(), NUMBER.c_str(), NAME.c_str());
}
