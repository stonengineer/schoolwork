import java.util.Scanner;

public class Lab08D
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter an integer: ");
		int num = sc.nextInt();
		System.out.println();

		System.out.println("Multiplication table for " + num + "'s:");
		int multiplier = 0;
		while(multiplier < 10)
		{
			System.out.println(++multiplier + "*" + num + "=" + (multiplier * num));
		}
	}
}