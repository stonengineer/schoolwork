import java.util.Scanner;
import java.lang.Math;

public class Lab08C
{
	private int a, b;

	public static void main(String[] args)
	{
		Lab08C lab = new Lab08C();
		lab.input();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the first number: ");
		a = sc.nextInt();
		System.out.print("Enter the second number: ");
		b = sc.nextInt();
		System.out.println();
	}

	public void output()
	{
		int min = Math.min(a, b);
		int max = Math.max(a, b);

		System.out.println("All the odd numbers between " + min + " and " + max + ":");
		while(min < max)
		{
			if(min % 2 != 0)
				System.out.print(min + " ");
			++min;
		}
		System.out.println();
	}
}