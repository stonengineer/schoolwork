import java.util.Scanner;

public class Lab08E
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your name: ");
		String name = sc.nextLine();
		System.out.print("Number of times to display name: ");
		int disp = sc.nextInt();

		int i = 0;
		while(i < disp)
		{
			System.out.println(name);
			name = " " + name;
			++i;
		}
	}
}