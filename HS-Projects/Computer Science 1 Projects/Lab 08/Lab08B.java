import java.util.Scanner;

public class Lab08B
{
	private int stars;

	public static void main(String[] args)
	{
		Lab08B lab = new Lab08B();
		lab.input();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of stars to display: ");
		stars = sc.nextInt();
		System.out.println();
	}

	public void output()
	{
		int count = 0;
		while(count < stars)
		{
			System.out.print("*");
			++count;
		}
		System.out.println();
	}
}