import java.util.Scanner;
import java.text.DecimalFormat;

public class Lab02B
{
	private double side;
	private double volume;

	public static void main(String[] args)
	{
		Lab02B lab = new Lab02B();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the length of one side of a cube: ");
		side = sc.nextDouble();
	}

	public void process()
	{
		volume = (side * side * side);
	}

	public void output()
	{
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println("\nThe volume of a cube with a side length of " + side + " inches is " + df.format(volume) + " cubic inches.");
	}
}