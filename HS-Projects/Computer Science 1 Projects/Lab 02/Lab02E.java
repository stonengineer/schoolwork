import java.util.Scanner;
import java.text.DecimalFormat;

public class Lab02E
{
	private final double tax = 0.0825;
	private double initial_price;
	private double tax_total;
	private double total;

	public static void main(String[] args)
	{
		Lab02E lab = new Lab02E();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the price for the computer system: ");
		initial_price = sc.nextDouble();
	}

	public void process()
	{
		tax_total = initial_price * tax;
		total = initial_price + tax_total;
	}

	public void output()
	{
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println("\nSYSTEM COST");
		System.out.println("===========");
		System.out.println("Purchase    = $" + df.format(initial_price));
		System.out.println("Tax         = $" + df.format(tax_total));
		System.out.println("-----------");
		System.out.println("Total       = $" + df.format(total));
	}
}