import java.util.Scanner;
import java.text.DecimalFormat;

public class Lab02C
{
	private final double PI = 3.1416;
	private double diameter;
	private double area;

	public static void main(String[] args)
	{
		Lab02C lab = new Lab02C();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the diameter of the circle: ");
		diameter = sc.nextDouble();
	}

	public void process()
	{
		double radius = (diameter / 2.0);
		area = PI * (radius * radius);
	}

	public void output()
	{
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println("\nThe area of a circle with a diameter of " + diameter + " is " + df.format(area) + ".");
	}
}