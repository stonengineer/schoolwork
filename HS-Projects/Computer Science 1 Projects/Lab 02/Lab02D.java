import java.util.Scanner;
import java.text.DecimalFormat;

public class Lab02D
{
	private final double PI = 3.1416;
	private double radius;
	private double surface_area;
	private double volume;

	public static void main(String[] args)
	{
		Lab02D lab = new Lab02D();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the radius of the sphere: ");
		radius = sc.nextDouble();
	}

	public void process()
	{
		surface_area = 4.0 * PI * (radius * radius);
		volume = (4.0 / 3.0) * PI * (radius * radius * radius);
	}

	public void output()
	{
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println("\nThe surface area of a sphere with a radius of " + radius + " is " + df.format(surface_area) + ".");
		System.out.println("The volume of a sphere with radius of " + radius + " is " + df.format(volume) + ".");
	}
}