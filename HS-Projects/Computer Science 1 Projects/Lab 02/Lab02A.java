import java.util.Scanner;
import java.text.DecimalFormat;

public class Lab02A
{
	private int a;
	private int b;
	private double area;

	public static void main(String[] args)
	{
		Lab02A lab = new Lab02A();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the length of side a: ");
		a = sc.nextInt();
		System.out.print("Enter the length of side b: ");
		b = sc.nextInt();
	}

	public void process()
	{
		area = a * b / 2.0;
	}

	public void output()
	{
		DecimalFormat df = new DecimalFormat("#.0");
		System.out.println("\nThe area of a right triangle with sides of length " + a + " inches and " + b + " inches is " + df.format(area) + " square inches.");
	}
}