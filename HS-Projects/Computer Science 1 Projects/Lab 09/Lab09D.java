import java.util.Scanner;
import java.lang.Math;

public class Lab09D
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the first character: ");
		char a = sc.next().charAt(0);
		System.out.print("Enter the second character: ");
		char b = sc.next().charAt(0);
		System.out.println();

		System.out.print("The alphabet from " + a + " to " + b + ": ");
		if(a < b)
			for(char c = a; c <= b; c++)
				System.out.print(c + " ");
		else
			for(char c = a; c >= b; c--)
				System.out.print(c + " ");
		System.out.println("\nThere are " + (Math.abs((b-a))+1) + " characters from " + a + " to " + b + ".");
	}
}