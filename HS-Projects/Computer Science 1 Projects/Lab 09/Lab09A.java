import java.util.Scanner;

public class Lab09A
{
	private int goblins;

	public static void main(String[] args)
	{
		Lab09A lab = new Lab09A();
		lab.input();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of Goblins to display: ");
		goblins = sc.nextInt();
		System.out.println();
	}

	public void output()
	{
		System.out.print("There be goblins here: ");
		for(int i = 0; i < goblins; i++)
			System.out.print("*");
		System.out.println();
	}
}