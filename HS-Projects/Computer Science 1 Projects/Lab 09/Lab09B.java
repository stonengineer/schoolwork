import java.util.Scanner;
import java.text.DecimalFormat;

public class Lab09B
{
	private int a, b, sum, count;
	private double average;

	public static void main(String[] args)
	{
		Lab09B lab = new Lab09B();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the first number: ");
		a = sc.nextInt();
		System.out.print("Enter the last number: ");
		b = sc.nextInt();
		System.out.println();
	}

	public void process()
	{
		count = 0;
		sum = 0;

		for(int i = Math.min(a, b); i < Math.max(a, b); i++)
		{
			if((i % 2 == 0) && (i % 5 != 0))
			{
				sum += i;
				count++;
			}
		}

		average = sum * 1.0 / count;
	}

	public void output()
	{
		DecimalFormat df = new DecimalFormat("#.#");
		System.out.println("The sum of the even numbers between " + Math.min(a,b) + " and " + Math.max(a,b) + " (excluding multiples of 5) is " + sum);
		System.out.println("The average of the even numbers (excluding multiples of 5) is " + df.format(average));
	}
}