import java.util.Scanner;

public class Lab09E
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of verses to display: ");
		int bottles = sc.nextInt();
		System.out.println();

		for(int i = 99; i > (99-bottles); i--)
		{
			System.out.println(i + " bottles of beer on the wall");
			System.out.println(i + " bottles of beer");
			System.out.println("If one of those bottles should happen to fall");
			System.out.println((i-1) + " bottles of beer on the wall\n");
		}
	}
}