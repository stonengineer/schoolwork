import java.util.Scanner;

public class Lab09C
{
	private String sentence;

	public static void main(String[] args)
	{
		Lab09C lab = new Lab09C();
		lab.input();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a sentence: ");
		sentence = sc.nextLine();
		System.out.println();
	}

	public void output()
	{
		for(int i = sentence.length()-1; i >= 0; i--)
		{
			System.out.print(sentence.charAt(i));
		}
		System.out.println();
	}
}