import java.util.Scanner;

public class Lab07A
{
	private int age;

	public static void main(String[] args)
	{
		Lab07A lab = new Lab07A();
		lab.input();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your age: ");
		age = sc.nextInt();
		System.out.println();
	}

	public void output()
	{
		System.out.print("At age " + age);
		if(age > 16)
			System.out.println(", a person is old enough to drive");
		else
			System.out.println(", a person is not old enough to drive.");

		System.out.print("At age " + age);
		if(age > 17)
			System.out.println(", a person is old enough to vote");
		else
			System.out.println(", a person is not old enough to vote.");

		System.out.print("At age " + age);
		if(age > 20)
			System.out.println(", a person is old enough to drink alcohol legally");
		else
			System.out.println(", a person is not old enough to drink alcohol legally.");
	}
}