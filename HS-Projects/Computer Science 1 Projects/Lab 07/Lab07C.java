import java.util.Scanner;
import java.text.DecimalFormat;

public class Lab07C
{
	private int x1, y1, x2, y2;

	public static void main(String[] args)
	{
		Lab07C lab = new Lab07C();
		lab.input();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the coordinates for the first point: ");
		x1 = sc.nextInt();
		y1 = sc.nextInt();
		System.out.print("Enter the coordinates for the second point: ");
		x2 = sc.nextInt();
		y2 = sc.nextInt();
		System.out.println();
	}

	public void output()
	{
		double delta_x = x2 - x1;
		double delta_y = y2 - y1;

		DecimalFormat df = new DecimalFormat("#.###");
		System.out.print("The slope of a line containing (" + x1 + "," + y1 + ") and (" + x2 + "," + y2 + ") is ");
		if(delta_x == 0)
			System.out.println("zero.");
		else if(delta_y == 0)
			System.out.println("undefined.");
		else
			System.out.println(df.format(delta_x / delta_y));
	}
}