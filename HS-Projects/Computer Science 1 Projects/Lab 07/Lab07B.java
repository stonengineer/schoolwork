import java.util.Scanner;

public class Lab07B
{
	private String name;
	private int grade;

	public static void main(String[] args)
	{
		Lab07B lab = new Lab07B();
		lab.input();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a name: ");
		name = sc.nextLine();
		System.out.print("Enter a grade: ");
		grade = sc.nextInt();
		System.out.println();
	}

	public void output()
	{
		System.out.print(name + ", you made a grade of " + grade + ".");
		if(grade > 89)
			System.out.print(" Congratulations!\n");
		else if(grade > 69)
			System.out.println();
		else
			System.out.print(" Poor kid, study harder!\n");
	}
}