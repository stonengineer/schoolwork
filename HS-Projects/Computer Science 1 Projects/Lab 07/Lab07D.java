import java.util.Scanner;

public class Lab07D
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your weight: ");
		int weight = sc.nextInt();
		System.out.print("1] Mercury\n2] Venus\n3] Mars\n4] Jupiter\n5] Saturn\n6] Uranus\n7] Neptune\n8] Pluto\nSelect a planet: ");
		int planet = sc.nextInt();
		System.out.println();

		String choice = "N/A";
		double multiplier = 0;
		if(planet == 1)
		{
			choice = "Mercury";
			multiplier = 0.37;
		}
		else if(planet == 2)
		{
			choice = "Venus";
			multiplier = 0.88;
		}
		else if(planet == 3)
		{
			choice = "Mars";
			multiplier = 0.38;
		}
		else if(planet == 4)
		{
			choice = "Jupiter";
			multiplier = 2.64;
		}
		else if(planet == 5)
		{
			choice = "Saturn";
			multiplier = 1.15;
		}
		else if(planet == 6)
		{
			choice = "Uranus";
			multiplier = 1.15;
		}
		else if(planet == 7)
		{
			choice = "Neptune";
			multiplier = 1.12;
		}
		else if(planet == 8)
		{
			choice = "Pluto";
			multiplier = 0.04;
		}
		else
		{
			System.out.println("Please enter a valid planet number.");
			System.exit(0);
		}

		System.out.println("On " + choice + ", you would weigh " + (double)(weight * multiplier) + " pounds.");
	}
}