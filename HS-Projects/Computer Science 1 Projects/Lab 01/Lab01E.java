public class Lab01E
{
	public static void main(String[] args)
	{
		String l1 = "   /\\        \n";
		String l2 = "  /  \\       \n";
		String l3 = " /    \\      \n";
		String l4 = "/------\\     \n";
		String l5 = "|   _  |     \n";
		String l6 = "|  | | |     \n";
		String l7 = "|  | | |     \n";
		String l8 = " ------      \n";
		String l9 = "    **       \n";
		String l0 = "    *********\n";

		System.out.println(l1 + l2 + l3 + l4 + l5 + l6 + l7 + l8 + l9 + l0);
	}
}