public class Lab01F
{
	public static void main(String[] args)
	{
		String l0 = "          e        \n";
		String l1 = "           n       \n";
		String l2 = "    e     o        \n";
		String l3 = "     o     t       \n";
		String l4 = "    j     s        \n";
		String l5 = "****************** \n";
		String l6 = "*             *   *\n";
		String l7 = "*    Java     *   *\n";
		String l8 = "*             **** \n";
		String l9 = "*   is fun    *    \n";
		String la = "*             *    \n";
		String lb = "*             *    \n";
		String lc = "***************    \n";

		System.out.println(l0 + l1 + l2 + l3 + l4 + l5 + l6 + l7 + l8 + l9 + la + lb + lc);
	}
}