import java.util.Scanner;
import java.io.File;

public class Lab13D
{
	String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
	int[] days = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };


	public static void main(String[] args) {
		Lab13D lab = new Lab13D();
		String month = lab.input();
		lab.output(month);
	}

	public String input() {
		Scanner kybd = new Scanner(System.in);
		System.out.print("Enter the name of a month: ");
		return kybd.next();
	}

	public int indexOf(String month) {
		for(int i = 0; i < 12; i++) {
			if(months[i].equalsIgnoreCase(month)) {
				return i;
			}
		}
		return -1;
	}

	public void output(String month) {
		int index = indexOf(month);
		if(index >= 0) {
			System.out.println(month + " has " + days[index] + " days");
		}
		else {
			System.out.println(month + " is not a valid name of a month");
		}
	}
}