import java.util.Scanner;
import java.io.File;

public class Lab13C
{
	public static void main(String[] args) {
		Lab13C lab = new Lab13C();
		char[] list = lab.fileInput();
		char[] tuple = lab.kybdInput();
		char searchItem = tuple[0];
		char replaceValue = tuple[1];
		lab.output(list, searchItem, replaceValue);
	}

	public char[] fileInput() {
		char[] list = new char[0];
		try {
			Scanner file_in = new Scanner(new File("lab13c.dat"));
			String input = file_in.nextLine();
			list = input.toCharArray();
		}
		catch(Exception e) {
			System.out.println("Error reading file.");
			System.exit(1);
		}
		return list;
	}

	public char[] kybdInput() {
		char[] tuple = new char[2];
		Scanner kybd_in = new Scanner(System.in);
		System.out.print("Enter a search value: ");
		tuple[0] = kybd_in.next().charAt(0);
		System.out.print("Enter a replacement value: ");
		tuple[1] = kybd_in.next().charAt(0);
		return tuple;
	}

	public int replaceAll(char[] list, char searchItem, char replaceValue) {
		int count = 0;
		for(int i = 0; i < list.length; i++) {
			if(list[i] == searchItem) {
				list[i] = replaceValue;
				++count;
			}
		}
		return count;
	}

	public void output(char[] list, char searchItem, char replaceValue) {
		System.out.println("Original Data:");
		for(int i = 0; i < list.length; i++) {
			System.out.print(list[i]);
		}
		System.out.println();
		System.out.println("Search For: " + searchItem);
		System.out.println("Replace With: " + replaceValue);
		System.out.println("Report: " + searchItem + " was replaced " + replaceAll(list, searchItem, replaceValue) + " times with " + replaceValue);
		System.out.println("Altered Data:");
		for(int i = 0; i < list.length; i++) {
			System.out.print(list[i]);
		}
		System.out.println();
	}
}