import java.util.Scanner;
import java.io.File;

public class Lab13B
{
	public static void main(String[] args) {
		Lab13B lab = new Lab13B();
		int[] list = lab.fileInput();
		int[] tuple = lab.kybdInput();
		int searchItem = tuple[0];
		int replacement = tuple[1];
		lab.output(list, searchItem, replacement);
	}

	public int[] fileInput() {
		int[] list = new int[14];
		int index = 0;
		try {
			Scanner file_in = new Scanner(new File("lab13b.dat"));
			while(file_in.hasNext()) {
				list[index++] = file_in.nextInt();
			}
		}
		catch(Exception e) {
			System.out.println("Error reading flie.");
		}
		return list;
	}

	public int[] kybdInput() {
		int[] tuple = new int[2];
		Scanner kybd_in = new Scanner(System.in);
		System.out.print("Enter a number to replace: ");
		tuple[0] = kybd_in.nextInt();
		System.out.print("Enter a replacement value: ");
		tuple[1] = kybd_in.nextInt();
		return tuple;
	}

	public static int replace(int[] list, int size, int searchItem) {
		for(int i = 0; i < size; i++) {
			if(list[i] == searchItem) {
				return i;
			}
		}
		return -1;
	}

	public void output(int[] list, int searchItem, int replacement) {
		System.out.print("Original data: ");
		for(int i = 0; i < 14; i++) {
			System.out.print(list[i] + " ");
		}
		System.out.println();
		int index = replace(list, 14, searchItem);
		if(index >= 0) {
			System.out.println("The " + searchItem + " found at position " + index + " was replaced with " + replacement);
			System.out.print("The modified list: ");
			list[index] = replacement;
			for(int i = 0; i < 14; i++) {
				System.out.print(list[i] + " ");
			}
		}
		else {
			System.out.println("Could not find " + searchItem + " in list.");
		}
		System.out.println();
	}
}