import java.util.Scanner;
import java.io.File;

public class Lab13A 
{
	public static void main(String[] args) {
		Lab13A lab = new Lab13A();
		int[] list = lab.fileInput();
		int searchItem = lab.kybdInput();
		int count = lab.find(list, searchItem);
		lab.output(list, searchItem, count);
	}

	public int[] fileInput() {
		int index = 0;
		int[] list = new int[14];
		try {
			Scanner file_in = new Scanner(new File("lab13a.dat"));
			for(int i = 0; i < 14; i++) {
				list[i] = file_in.nextInt();
			}
		}
		catch(Exception e) {
			System.out.println("Error reading file");
		}
		return list;
	}

	public int kybdInput() {
		Scanner kybd_in = new Scanner(System.in);
		System.out.print("Enter a number: ");
		int searchItem = kybd_in.nextInt();
		System.out.println();
		return searchItem;
	}

	public int find(int[] array, int searchItem) {
		int count = 0;
		for(int i = 0; i < 14; i++) {
			if(array[i] == searchItem) {
				++count;
			}
		}
		return count;
	}

	public void output(int[] list, int searchItem, int count) {
		System.out.println("Original Data:");
		for(int i = 0; i < 14; i++) {
			System.out.print(list[i] + " ");
		}
		System.out.println();
		System.out.println("Search Item: " + searchItem);
		System.out.println("Number of occurences: " + count);
	}
}
