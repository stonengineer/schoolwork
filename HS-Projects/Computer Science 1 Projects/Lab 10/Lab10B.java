import java.util.Scanner;
import java.io.*;

public class Lab10B
{
	private int[] list;
	private int count, sum;
	private double average;

	public static void main(String[] args)
	{
		Lab10B lab = new Lab10B();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		try
		{
			Scanner sc = new Scanner(new File("Data/lab10b.dat"));
			list = new int[8];
			count = sum = 0;
			while(sc.hasNext())
			{
				list[count++] = sc.nextInt();
				sum += list[count-1];
			}
		}
		catch(IOException e)
		{
			System.out.println("File not found.");
			System.exit(0);
		}
	}

	public void process()
	{
		average = sum * 1.0 / count;
		for(int i = 0; i < count; i++)
		{
			int temp = list[i];
			for(int y = 0; y < count; y++)
			{
				if(temp < list[y])
				{
					int hold = list[y];
					list[i] = hold;
					list[y] = temp;
					temp = hold;
				}
			}
		}
	}

	public void output()
	{
		for(int i = 0; i < count; i++)
		{
			System.out.print(list[i] + " ");
		}
		System.out.println("\nThe sum of the numbers is " + sum);
		System.out.println("The average of the numbers is " + average);
	}
}