import java.util.Scanner;
import java.io.*;

public class Lab10D
{
	private String[] names;
	private int[] age;
	private int count;

	public static void main(String[] args)
	{
		Lab10D lab = new Lab10D();
		lab.input();
		lab.output();
	}

	public void input()
	{
		try
		{
			Scanner sc = new Scanner(new File("Data/lab10d.dat"));
			sc.useDelimiter("/|\n");
			names = new String[1];
			age = new int[1];
			count = 0;
			while(sc.hasNext())
			{
				names[count] = sc.next();
				age[count++] = sc.nextInt();
				String[] name_holder = new String[count+1];
				int[] age_holder = new int[count+1];
				for(int i = 0; i < count; i++)
				{
					name_holder[i] = names[i];
					age_holder[i] = age[i];
				}
				names = name_holder;
				age = age_holder;
			}
		}
		catch(IOException e)
		{
			System.out.println("File not found.");
			System.exit(0);
		}
	}

	public void output()
	{
		for(int i = 0; i < count; i++)
		{
			System.out.println(names[i] + " is " + age[i] + " years old.");
		}
	}
}