import java.util.Scanner;
import java.io.*;

public class Lab10A
{
	private int[] list;
	private int count;

	public static void main(String[] args)
	{
		Lab10A lab = new Lab10A();
		lab.input();
		lab.output();
	}

	public void input()
	{
		try
		{
			list = new int[400];
			count = 0;
			Scanner sc = new Scanner(new File("Data/lab10a.dat"));
			while(sc.hasNext())
			{
				list[count++] = sc.nextInt();
			}
		}
		catch(IOException e)
		{
			System.out.println("File not found.");
			System.exit(0);
		}
	}

	public void output()
	{
		for(int i = 0; i < count; i++)
		{
			System.out.print(list[i] + " ");
		}
		System.out.println("\nThis array contains " + count + " numbers.");
		System.out.println("There are " + (400 - count) + " unused elements.");
	}
}