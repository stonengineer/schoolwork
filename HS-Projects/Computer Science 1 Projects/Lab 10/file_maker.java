import java.io.*;
import java.util.*;
import java.lang.Math;
import java.text.DecimalFormat;

public class file_maker
{
	public static void main(String[] args)
	{
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		try
		{
			PrintWriter writer = new PrintWriter("Data/lab10e.dat", "UTF-8");
			for(int i = 0; i < 1000; i++)
			{
				DecimalFormat df = new DecimalFormat("#.##");
				double write = (Math.random() * 1000);
				writer.print(df.format(write) + " ");	
			}
			writer.close();
		}
		catch(IOException e)
		{
			System.out.println("Error.");
			System.exit(0);
		}
	}
}