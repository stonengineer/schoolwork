import java.util.*;
import java.io.*;

public class Lab10E
{
	private double[] list;
	private int count;
	private double search_item, replace_value;

	public static void main(String[] args)
	{
		Lab10E lab = new Lab10E();
		lab.file_input();
		lab.keyboard_input();
		lab.output();
	}

	public void file_input()
	{
		try
		{
			Scanner sc = new Scanner(new File("Data/lab10e.dat"));
			list = new double[1];
			count = 0;
			while(sc.hasNext())
			{
				list[count++] = sc.nextDouble();
				double[] temp = new double[count+1];
				for(int i = 0; i < count; i++)
					temp[i] = list[i];
				list = temp;
			}
		}
		catch(IOException e)
		{
			System.out.println("File not found");
			System.exit(0);
		}
	}

	public void keyboard_input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a value to search for: ");
		search_item = sc.nextDouble();
		System.out.print("Enter a replacement value: ");
		replace_value = sc.nextDouble();
	}

	public void output()
	{
		Arrays.sort(list);
		int index = Arrays.binarySearch(list, 0, count, search_item);
		if(index < 0)
		{
			System.out.println("Key was not found.");
			String[] none = new String[1];
			main(none);
		}
		else
		{
			list[index] = replace_value;
			System.out.println("The Modified List:");
			for(int i = 0; i < count; i++)
			{
				System.out.print(list[i] + " ");
			}
			System.out.println();
		}
	}
}