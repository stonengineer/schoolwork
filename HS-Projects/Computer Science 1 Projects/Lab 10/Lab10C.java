import java.util.Scanner;
import java.io.*;

public class Lab10C
{
	private double[] list;
	private int count;

	public static void main(String[] args)
	{
		Lab10C lab = new Lab10C();
		lab.input();
		lab.output();
	}

	public void input()
	{
		try
		{
			Scanner sc = new Scanner(new File("Data/lab10c.dat"));
			count = 0;
			list = new double[1];
			while(sc.hasNext())
			{
				list[count++] = sc.nextDouble();
				double[] temp = new double[count+1];
				for(int i = 0; i < count; i++)
				{
					temp[i] = list[i];
				}
				list = temp;
			}
		}
		catch(IOException e)
		{
			System.out.println("File not found.");
			System.exit(0);
		}
	}

	public void output()
	{
		for(int i = 0; i < count; i++)
		{
			double temp = list[i];
			for(int y = 0; y < count; y++)
			{
				if(temp < list[y])
				{
					double hold = list[y];
					list[i] = hold;
					list[y] = temp;
					temp = hold;
				}
			}
		}

		System.out.print("The sorted list: ( ");
		for(int i = 0; i < count; i++)
		{
			System.out.print(list[i] + " ");
		}
		System.out.println(").");
		System.out.println("The smallest value is " + list[0]);
		System.out.println("The largest value is " + list[count-1]);
	}
}