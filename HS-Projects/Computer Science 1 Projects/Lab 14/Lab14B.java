import java.util.Scanner;
import java.io.File;

public class Lab14B
{
	public static void main(String[] args) {
		Lab14B lab = new Lab14B();
		String[] words = lab.input();
		String[] corrected = lab.correctSpelling(words);
		lab.output(words, corrected);
	}

	public String[] input() {
		try {
			String accumulate = "";
			Scanner file_in = new Scanner(new File("lab14b.dat"));
			while(file_in.hasNext()) {
				accumulate += file_in.next() + ",";
			}
			return accumulate.split(",");
		}
		catch(Exception e) {
			System.out.println("Error reading lab14b.dat");
			return "-".split("-");
		}
	}

	public String[] correctSpelling(String[] words) {
		String accumulate = "";
		for(String word : words) {
			while(word.indexOf("ei") != -1) {
				int index = word.indexOf("ei");
				String temp = word.substring(0, index);
				temp += "ie";
				index += 2;
				temp += word.substring(index, word.length());
				word = temp;
			}
			while(word.indexOf("cie") != -1) {
				int index = word.indexOf("cie");
				String temp = word.substring(0, index);
				temp += "cei";
				index += 3;
				temp += word.substring(index, word.length());
				word = temp;
			}
			accumulate += word + ",";
		}
		return accumulate.split(",");
	}

	public void output(String[] words, String[] corrected) {
		if(words.length != corrected.length) {
			System.out.println("Something went wrong!");
			return;
		}
		for(int i = 0; i < words.length; i++) {
			if(words[i].equals(corrected[i])) {
				System.out.println("The word \"" + words[i] + "\" is spelled correctly.");
			}
			else {
				System.out.println("The word \"" + words[i] + "\" is spelled incorrectly.");
				System.out.println("It should be spelled \"" + corrected[i] + "\"");
			}
			System.out.println();
		}
	}
}