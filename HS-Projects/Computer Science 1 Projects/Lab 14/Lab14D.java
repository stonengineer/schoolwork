import java.util.Scanner;
import java.util.Arrays;

public class Lab14D
{
	static String[] threeLetterBlends = new String[]{"sch", "scr", "spl", "spr", "squ", "str", "thr"};
	static String[] twoLetterBlends = new String[]{"bl", "br", "ch", "cl", "cr", "dr", "dw", "fl", "fr", "gl", "gr", "kl", "ph", "pl", "pr", "qu", "sc", "sh", "sk", "sl", "sn", "sm", "sp", "sq", "st", "sw", "th", "tr", "tw", "wh", "wr"};

	public static void main(String[] args)
	{
		System.out.print("Enter a sentence: ");
		Scanner sc = new Scanner(System.in);
		String inputSentence = sc.nextLine();
		String[] words = inputSentence.toLowerCase().split("\\s+");
		String convertedSentence = "";
		for(int i = 0; i < words.length; i++) {
			convertedSentence += translate(words[i]) + " ";
		}
		System.out.println("The original sentence was:\n"+inputSentence+"\n");
		System.out.println("Translated into Pig Latin:\n"+convertedSentence);
	}

	private static String translate(String word)
	{
		if(startsWithVowel(word)) {
			return (word+"yay");
		}

		int start = -1;
		if(startsWithThreeLetterBlend(word)) {
			start = 3;
		}
		else if(startsWithTwoLetterBlend(word)) {
			start = 2;
		}
		else {
			start = 1;
		}

		String result = "";
		for(int i = start; i < word.length(); i++) {
			result += word.charAt(i);
		}
		for(int i = 0; i < start; i++) {
			result += word.charAt(i);
		}
		result += "ay";
		
		return result;
	}

	private static boolean startsWithVowel(String word) {
		char first = word.charAt(0);
		return ((first == 'a') || (first == 'e') || (first == 'i') || (first == 'o') || (first == 'u') || (first == 'y'));
	}

	private static boolean startsWithThreeLetterBlend(String word) {
		if(word.length() < 3) {
			return false;
		}
		String firstThree = "" + word.charAt(0) + word.charAt(1) + word.charAt(2);
		return Arrays.asList(threeLetterBlends).contains(firstThree);
	}

	private static boolean startsWithTwoLetterBlend(String word) {
		if(word.length() < 2) {
			return false;
		}
		String firstTwo = "" + word.charAt(0) + word.charAt(1);
		return Arrays.asList(twoLetterBlends).contains(firstTwo);
	}
}