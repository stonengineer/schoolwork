import java.util.Scanner;
import java.io.File;

public class Lab14A
{
	public static void main(String[] args) {
		Lab14A lab = new Lab14A();
		String[] list = lab.fileInput();
		String city = lab.kybdInput();
		lab.output(list, city);
	}

	public String[] fileInput() {
		String[] list = new String[0];
		try {
			String combine = "";
			Scanner file_in = new Scanner(new File("lab14a.dat"));
			while(file_in.hasNext()) {
				combine += (file_in.nextLine() + "\n");
			}
			list = combine.split("\n");
		}
		catch(Exception e) {
			System.out.println("Error reading input file.");
		}
		return list;
	}

	public String kybdInput() {
		Scanner kybd_in = new Scanner(System.in);
		System.out.print("Enter the name of a city: ");
		return kybd_in.nextLine();
	}

	public void output(String[] list, String city) {
		for(int i = 0; i < list.length; i++) {
			if(list[i].equalsIgnoreCase(city)) {
				System.out.println(city + " is equal to " + list[i]);
			}
			else {
				System.out.println(city + " is Not equal to " + list[i]);
			}
		}
	}
}