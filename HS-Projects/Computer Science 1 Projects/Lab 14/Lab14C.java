import java.util.Scanner;

public class Lab14C 
{
	public static void main(String[] args) {
		Lab14C lab = new Lab14C();
		for(int i = 0; i < 3; i++) {
			String numbers = lab.input();
			String adjusted = lab.process(numbers);
			lab.output(adjusted);
		}
	}

	private static boolean isEven(char c) {
		return ((Integer.parseInt(String.valueOf(c)) % 2) == 0);
	}

	public String input() {
		Scanner kybd_in = new Scanner(System.in);
		System.out.print("Enter a sequence of numbers: ");
		return kybd_in.nextLine();
	}

	public String process(String numbers) {
		String populate = numbers;
		char past = '1';
		for(int i = 0; i < numbers.length()-1; i++) {
			if(isEven(numbers.charAt(i)) && isEven(past)) {
				populate = populate.substring(0, i);
				if(past > numbers.charAt(i)) {
					populate += 'X';
				}
				else {
					populate += 'Z';
				}

				String end = populate.substring(i-1, populate.length()-1);
				populate += end;
			}
			past = numbers.charAt(i);
		}
		return populate + numbers.charAt(numbers.length()-1);
	}

	public void output(String numbers) {
		System.out.println(numbers);
	}
}