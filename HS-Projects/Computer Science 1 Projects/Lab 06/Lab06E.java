import java.util.Scanner;
import java.lang.Math;
import java.text.DecimalFormat;

public class Lab06E
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the x and y coordinate for the first point: ");
		int x1 = sc.nextInt();
		int y1 = sc.nextInt();
		System.out.print("Enter the x and y coordinate for the second point: ");
		int x2 = sc.nextInt();
		int y2 = sc.nextInt();
		System.out.println();

		double distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));

		DecimalFormat df = new DecimalFormat("#.#");
		System.out.println("The distance between the points (" + x1 + "," + y1 + ") and (" + x2 + "," + y2 + ") is " + df.format(distance));
	}
}