import java.util.Scanner;
import java.lang.Math;
import java.text.DecimalFormat;

public class Lab06G
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the lengths of two sides and their included angle: ");
		double side_one = sc.nextDouble();
		double side_two = sc.nextDouble();
		double angle = sc.nextDouble();
		System.out.println();

		double angle_radians = angle * Math.PI / 180.0;
		double side_three = Math.sqrt(Math.pow(side_one, 2) + Math.pow(side_two, 2) - (2 * side_one * side_two * Math.cos(angle_radians)));

		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println("A triangle with sides of length " + side_one + " and " + side_two + " and an included angle measuring " + angle + " degrees has a third side of length " + df.format(side_three));
	}
}