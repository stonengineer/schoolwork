import java.util.Scanner;
import java.lang.Math;

public class Lab06A
{
	private int a, b, distance;

	public static void main(String[] args)
	{
		Lab06A lab = new Lab06A();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter two integers: ");
		a = sc.nextInt();
		b = sc.nextInt();
	}

	public void process()
	{
		distance = Math.abs(a - b);
	}

	public void output()
	{
		System.out.println("\nThe distance from " + a + " to " + b + " is " + distance + ".");
	}
}