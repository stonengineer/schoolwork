import java.lang.Math;

public class Lab06F
{
	public static void main(String[] args)
	{
		int roll_one = (int)(Math.random() * 6 + 1);
		int roll_two = (int)(Math.random() * 6 + 1);

		System.out.println("You rolled " + roll_one + " and " + roll_two);
		System.out.println("Total this roll: " + (roll_one + roll_two));
	}
}