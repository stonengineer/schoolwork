import java.util.Scanner;
import java.io.*;
import java.lang.Math;
import java.text.DecimalFormat;

public class Lab06B
{
	private int s, a;
	private double r, area, volume, side;

	public static void main(String[] args)
	{
		Lab06B lab = new Lab06B();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		try
		{
			Scanner sc = new Scanner(new File("Data/lab06b.dat"));
			s = sc.nextInt();
			r = sc.nextDouble();
			a = sc.nextInt();
		}
		catch(IOException e)
		{
			System.out.println("File not found.");
			System.exit(0);
		}
	}

	public void process()
	{
		area = Math.pow(s, 2);
		volume = (4.0 / 3.0) * Math.PI * Math.pow(r, 3);
		side = Math.sqrt(a);
	}

	public void output()
	{
		DecimalFormat df = new DecimalFormat("#,###.##");
		System.out.println("\nThe area of a square whose side length is " + s + " is " + df.format(area));
		System.out.println("The volume of a sphere whose radius is " + r + " is " + df.format(volume));
		System.out.println("The side length of a square whose area is " + a + " is " + df.format(side));
	}
}