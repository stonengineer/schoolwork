import java.util.Scanner;
import java.lang.Math;

public class Lab06C
{
	private final int board_size = 32;
	int area, boards;

	public static void main(String[] args)
	{
		Lab06C lab = new Lab06C();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the number of square feet: ");
		area = sc.nextInt();
		System.out.println();
	}

	public void process()
	{
		boards = area / board_size;
		if((boards * board_size) < area)
			boards = boards + 1;
	}

	public void output()
	{
		System.out.println("A storage shed requiring " + area + " square feet of plywood will need " + boards + " sheets of plywood to finish the job.");
	}
}