import java.util.Scanner;
import java.io.*;
import java.lang.Math;
import java.text.DecimalFormat;

public class Lab12E
{
	private final double a = 32.2;

	public static void main(String[] args) {
		Lab12E lab = new Lab12E();
		int distance;
		double time, speed;

		distance = lab.input();
		time = lab.time(distance);
		speed = lab.speed(distance);
		lab.output(distance, time, speed);
	}

	public int input() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the distance <in feet> to the ground: ");
		return sc.nextInt();
	}

	public double time(int distance) {
		return Math.sqrt(distance * 2 / a);
	}

	public double speed(int distance) {
		return a * Math.pow(time(distance), 2);
	}

	public void output(int distance, double time, double speed) {
		DecimalFormat df = new DecimalFormat("#.#");

		System.out.println();
		System.out.println("It would take an object " + df.format(time) + " seconds to fall " + distance + " feet.");
		System.out.println("The object would be moving " + df.format(speed) + " feet per second when it hit the ground.");
	}
}