import java.util.Scanner;
import java.io.*;

public class Lab12B
{
	public static void main(String[] args) {
		Lab12B lab = new Lab12B();
		double radius, area, circumference;
		radius = lab.input();
		area = lab.area(radius);
		circumference = lab.circumference(radius);
		lab.output(radius, area, circumference);
	}

	public double input() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the radius of a circle: ");
		return sc.nextDouble();
	}

	public static double area(double radius) {
		return (3.14 * radius * radius);
	}

	public static double circumference(double radius) {
		return (2 * 3.14 * radius);
	}

	public void output(double radius, double area, double circumference) {
		System.out.println();
		System.out.println("The area of a circle with a radius of " + radius + " is " + area);
		System.out.println("The circumference of a circle with a radius of " + radius + " is " + circumference);
	}
}