import java.util.Scanner;
import java.io.*;
import java.lang.Math;
import java.text.DecimalFormat;

public class Lab12C
{
	public static void main(String[] args) {
		Lab12C lab = new Lab12C();
		double[] inputs = new double[2];
		double area;
		inputs = lab.input();
		area = lab.area(inputs);
		lab.output(inputs, area);
	}

	public double[] input() {
		double[] input_vals = new double[2];
		double input_val_1, input_val_2;
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter the distance from A to B: ");
		input_val_1 = sc.nextDouble();

		System.out.print("Enter the distance from A to C: ");
		input_val_2 = sc.nextDouble();
		System.out.println();

		input_vals[0] = input_val_1;
		input_vals[1] = input_val_2;
		return input_vals;
	}

	public static double area(double[] inputs) {
		//A = (AB) * (sqrt(AC^2 - AB^2))
		return inputs[0] * (Math.sqrt(Math.pow(inputs[1],2)-Math.pow(inputs[0],2)));
	}

	public void output(double[] inputs, double area) {
		DecimalFormat df = new DecimalFormat("#.#");
		System.out.println("The area of the rectangle where AB is " + inputs[0] + " and AC is " + inputs[1] + " is " + df.format(area));
	}
}