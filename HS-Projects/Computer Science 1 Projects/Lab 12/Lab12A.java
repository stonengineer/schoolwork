import java.util.Scanner;
import java.io.*;

public class Lab12A
{
	private static int kilometers;

	public static void main(String[] args)
	{
		kilometers = 0;

		input();
		output();
	}

	public static void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the speed limit in kilometers per hour: ");
		kilometers = sc.nextInt();
	}

	public static double kilometersToMiles(int kilometers)
	{
		return kilometers * 0.6214;
	}

	public static void output()
	{
		System.out.println("" + kilometers + " kilometers per hour = " + kilometersToMiles(kilometers) + " miles per hour.");
	}
}