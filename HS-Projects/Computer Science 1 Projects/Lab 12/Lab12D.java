//I don't have access to the instructor's source code,
//just his old PDF files, so I can't work this problem

import java.io.*;

public class Lab12D
{
	public static void main(String[] args) {
		System.out.println("I don't have access to the instructor's source code,");
		System.out.println("just his old PDF files, so I can't work this problem");
	}
}