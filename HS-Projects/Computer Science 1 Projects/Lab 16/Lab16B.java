import java.util.Scanner;

public class Lab16B
{
	private static int height;

	public static void main(String[] args) {
		Lab16B lab = new Lab16B();
		lab.input();
		lab.output();
	}

	private static void input() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the height of the triangle: ");
		height = sc.nextInt();
	}

	private static void output() {
		for(int y = 1; y < height+1; y++) {
			for(int x = 0; x < y; x++) {
				System.out.print(x+1);
			}
			System.out.println();
		}
	}
}