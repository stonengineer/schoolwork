import java.util.Scanner;

public class Lab16D
{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the height of the triangle: ");
		int num = sc.nextInt();
		for(int y = 0; y < num; y++) {
			for(int x = num; x > y; x--) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}