import java.util.Scanner;

public class Lab16C
{
	private static int num;

	public static void main(String[] args) {
		Lab16C lab = new Lab16C();
		lab.input();
		lab.output();
	}

	private static void input() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a number: ");
		num = sc.nextInt();
	}

	private static void output() {
		for(int y = num; y > 5; y--) {
			for(int x = y; x > 3; x--) {
				System.out.print(x + " ");
			}
			System.out.println();
		}
	}
}