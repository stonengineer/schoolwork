import java.util.Scanner;

public class Lab16A
{
	private static int width;
	private static int height;

	public static void main(String[] args) {
		Lab16A lab = new Lab16A();
		lab.input();
		lab.output();
	}

	private static void input() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the width and height of the rectangle: ");
		width = sc.nextInt();
		height = sc.nextInt();
	}

	private static void output() {
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}
}