import java.util.Scanner;

public class Lab03D
{
	private final int year = 2011;
	private String name;
	private int age;
	private int birth_year;

	public static void main(String[] args)
	{
		Lab03D lab = new Lab03D();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your name: ");
		name = sc.nextLine();
		System.out.print("Enter your age: ");
		age = sc.nextInt();
	}

	public void process()
	{
		birth_year = year - age;
	}

	public void output()
	{
		System.out.println("\nHello, " + name + ". You were born in " + birth_year + ".");
	}
}