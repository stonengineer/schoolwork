import java.util.Scanner;

public class Lab03E
{
	private String first_name, last_name, math_class;
	private int age;

	public static void main(String[] args)
	{
		Lab03E lab = new Lab03E();
		lab.input();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter student\'s first name: ");
		first_name = sc.nextLine();
		System.out.print("Enter student\'s last name: ");
		last_name = sc.nextLine();
		System.out.print("Enter student\'s age: ");
		age = sc.nextInt(); sc.nextLine();
		System.out.print("Enter student\'s math class: ");
		math_class = sc.nextLine();
	}

	public void output()
	{
		System.out.println("My name is " + first_name + " " + last_name + ".");
		System.out.println("I am " + age + " years old.");
		System.out.println("I am currently enrolled in " + math_class + ".");
	}
}