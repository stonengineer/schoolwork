import java.util.Scanner;

public class Lab03B
{
	private String first_name;
	private String last_name;
	private int char_num;

	public static void main(String[] args)
	{
		Lab03B lab = new Lab03B();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter your first name: ");
		first_name = sc.nextLine();
		System.out.print("Enter your last name: ");
		last_name = sc.nextLine();
	}

	public void process()
	{
		char_num = 0;

		for(int i = 0; i < first_name.length(); i++)
		{
			if(first_name.charAt(i) != ' ')
				char_num = char_num + 1;
		}

		for(int i = 0; i < last_name.length(); i++)
		{
			if(last_name.charAt(i) != ' ')
				char_num = char_num + 1;
		}
	}

	public void output()
	{
		System.out.println("\nMy name is " + first_name + " " + last_name + ".");
		System.out.println("My name has " + char_num + " characters.");
	}
}