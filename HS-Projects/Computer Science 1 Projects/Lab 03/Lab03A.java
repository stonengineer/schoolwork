import java.util.Scanner;

public class Lab03A
{
	private String first_word;
	private String second_word;
	private String third_word;

	public static void main(String[] args)
	{
		Lab03A lab = new Lab03A();
		lab.input();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter three words: ");
		first_word = sc.next();
		second_word = sc.next();
		third_word = sc.next();
	}

	public void output()
	{
		System.out.println("\n" + third_word + " " + second_word + " " + first_word);
	}
}