import java.util.Scanner;

public class Lab03C
{
	private String sentence;
	private int whitespace;
	private int ascii_chars;

	public static void main(String[] args)
	{
		Lab03C lab = new Lab03C();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a sentence: ");
		sentence = sc.nextLine();
	}

	public void process()
	{
		for(int i = 0; i < sentence.length(); i++)
		{
			if(sentence.charAt(i) == ' ')
				whitespace = whitespace + 1;
			else
				ascii_chars = ascii_chars + 1;
		}
	}

	public void output()
	{
		System.out.println("\n" + sentence + " contains " + sentence.length() + " characters; " + whitespace + " of which are whitespace, and the other " + ascii_chars + " are letters or numbers.");
		System.out.println("The first character is \'" + sentence.charAt(0) + "\' and the last character is \'" + sentence.charAt(sentence.length()-1) + "\'.");
	}
}