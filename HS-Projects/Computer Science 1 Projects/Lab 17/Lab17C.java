import java.util.Scanner;

public class Lab17C {
	static int decimal;
	static char format;

	public static void main(String[] args) {
		Lab17C lab = new Lab17C();
		lab.input();
		lab.output();
	}

	void input() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Number Systems");
		System.out.println("B - Decimal to Binary");
		System.out.println("O - Decimal to Octal");
		System.out.println("H - Decimal to Hexadecimal");
		System.out.print("Conversion Format: ");
		format = sc.nextLine().charAt(0);
		System.out.print("Enter the number to convert: ");
		decimal = sc.nextInt();
	}

	void output() {
		String formatted, base;
		switch(format) {
			case 'b':
			case 'B':
				formatted = Integer.toBinaryString(decimal);
				base = "2";
				break;
			case 'o':
			case 'O':
				formatted = Integer.toOctalString(decimal);
				base = "8";
				break;
			case 'h':
			case 'H':
				formatted = Integer.toHexString(decimal);
				base = "16";
				break;
			default:
				formatted = "ERROR";
				base = "";
				break;
		}
		System.out.println(decimal + " (Base 10) = " + formatted + " (Base " + base + ")");
	}
}