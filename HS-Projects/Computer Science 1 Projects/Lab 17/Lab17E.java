import java.util.Scanner;
import java.io.File;

public class Lab17E {
	public static void main(String[] args) {
		try {
			Scanner sc = new Scanner(new File("lab17e.dat"));
			String input = sc.nextLine();
			String result = "";
			for(int i = 0; i < input.length(); i++) {
				char curr = input.charAt(i);
				if(Character.isLetter(curr)) {
					if(Character.isLowerCase(curr)) {
						if(curr == 'z') {
							curr = 'a';
						}
						else {
							curr += 1;
						}
					}
					else if(Character.isUpperCase(curr)) {
						if(curr == 'A') {
							curr = 'Z';
						}
						else {
							curr -= 1;
						}
					}
					curr = Character.toUpperCase(curr);
				}
				result += curr;
			}
			System.out.println(result);
		}
		catch(Exception e) {
			System.out.println("Could not find file.");
		}
	}
}