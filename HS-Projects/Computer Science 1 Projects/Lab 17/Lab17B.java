import java.util.Scanner;
import java.io.File;

public class Lab17B {
	static int[] decimal;
	static int count;

	public static void main(String[] args) {
		Lab17B lab = new Lab17B();
		decimal = new int[6];
		count = 0;
		lab.input();
		lab.output();
	}

	void input() {
		try {
			Scanner sc = new Scanner(new File("lab17b.dat"));
			String line = sc.nextLine();
			String[] tmp = line.split("/", 6);
			for(int i = 0; i < 6; i++) {
				decimal[i] = Integer.parseInt(tmp[i]);
				++count;
			}
		}
		catch(Exception e) {
			System.out.println("Cannot find file.");
		}
	}

	void output() {
		for(int i = 0; i < count; i++) {
			System.out.println(decimal[i] + " (Base 10) = " + Integer.toBinaryString(decimal[i]) + " (Base 2)");
		}
	}
}