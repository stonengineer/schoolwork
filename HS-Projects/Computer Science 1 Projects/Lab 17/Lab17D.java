import java.util.Scanner;

public class Lab17D {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a sentence: ");
		String sentence = sc.nextLine();
		for(int i = 0; i < sentence.length(); i++) {
			if(Character.isDigit(sentence.charAt(i))) {
				System.out.println("\'" + sentence.charAt(i) + "\' is a number.");
			}
			else if(Character.isLetter(sentence.charAt(i))) {
				System.out.println("\'" + sentence.charAt(i) + "\' is a letter.");
			}
			else if(sentence.charAt(i) == ' ') {
				System.out.println("\'" + sentence.charAt(i) + "\' is white space.");
			}
			else {
				System.out.println("\'" + sentence.charAt(i) + "\' is a symbol.");
			}
		}
	}
}