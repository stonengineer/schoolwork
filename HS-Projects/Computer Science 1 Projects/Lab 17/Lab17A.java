import java.util.Scanner;
import java.io.File;

public class Lab17A {
	static String[] binary;
	static int count;

	public static void main(String[] args) {
		Lab17A lab = new Lab17A();
		binary = new String[3];
		count = 0;
		lab.input();
		lab.output();
	}

	void input() {
		try {
			Scanner sc = new Scanner(new File("lab17a.dat"));
			for(int i = 0; i < 3; i++) {
				binary[i] = sc.nextLine();
				++count;
			}
		}
		catch(Exception e) {
			System.out.println("Cannot find file.");
		}
	}

	String IPAddress(String binaryAddr) {
		String result = "";
		int index = 0;
		for(int i = 0; i < 4; i++) {
			String num = Integer.toString(Integer.parseInt(binaryAddr.substring(index, index+8), 2));
			result = result + num + ".";
			index += 8;
		}
		result = result.substring(0, result.length()-1);
		return result;
	}

	void output() {
		for(int i = 0; i < 3; i++) {
			System.out.println(binary[i] + " = " + IPAddress(binary[i]));
		}
	}
}