import java.util.Scanner;
import java.util.Arrays;

public class Lab15B
{
	private static String[] list;
	private static int count;

	public static void main(String[] args) {
		Lab15B lab = new Lab15B();
		list = new String[100];
		count = 0;
		lab.input();
		lab.process();
		lab.output();
	}

	private static void input() {
		Scanner sc = new Scanner(System.in);
		boolean run = true;
		while(run) {
			System.out.print("Enter a name: ");
			String temp = sc.nextLine();
			if(temp.equalsIgnoreCase("done")) {
				run = false;
			}
			else {
				list[count] = temp;
				count++;
			}
		}
	}

	private static void process() {
		Arrays.sort(list, 0, count);
	}

	private static void output() {
		for(int i = 0; i < count; i++) {
			System.out.println(list[i]);
		}
		System.out.println("" + count + " names were entered.");
	}
}