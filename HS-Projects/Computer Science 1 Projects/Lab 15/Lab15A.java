import java.util.Scanner;

public class Lab15A
{
	private static int[] list;
	private static int count;
	private static int sum;
	private static double average;

	public static void main(String[] args)
	{
		Lab15A lab = new Lab15A();
		list = new int[8];
		count = sum = 0;
		average = 0;
		lab.input();
		lab.process();
		lab.output();
	}

	private static void input() {
		Scanner kybd = new Scanner(System.in);
		boolean run = true;
		while(run) {
			System.out.print("Enter an integer (0 to quit): ");
			int temp = kybd.nextInt();
			if(temp == 0) {
				run = false;
			}
			else {
				list[count] = temp;
				count++;
			}
			if(count == 8) {
				run = false;
			}
		}
	}

	private static void process() {
		for(int i = 0; i < count; i++) {
			sum += list[i];
		}
		average = ((double)sum) / ((double)count);
	}

	private static void output() {
		System.out.println("Sum of the integers: "+sum);
		System.out.println("Average of the integers: "+average);
	}
}