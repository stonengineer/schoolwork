import java.util.Scanner;
import java.text.DecimalFormat;

public class Lab15D
{
	private static String[] items;
	private static double[] prices;
	private static int count;

	public static void main(String[] args) {
		Lab15D lab = new Lab15D();
		items = new String[100];
		prices = new double[100];
		lab.input();
		lab.output();
	}

	private static void input() {
		Scanner sc = new Scanner(System.in);
		boolean run = true;
		while(run) {
			System.out.print("Enter the name of an item: ");
			String temp = sc.nextLine();
			if(temp.equalsIgnoreCase("quit") || count == 100) {
				run = false;
			}
			else {
				items[count] = temp;
				System.out.print("Enter the price for "+temp+": ");
				double tempPrice = sc.nextDouble();
				prices[count] = tempPrice;
				count++;
				sc.nextLine();
			}
		}
	}

	private static void output() {
		System.out.println("At the market you bought:");
		DecimalFormat df = new DecimalFormat("#.##");
		for(int i = 0; i < count; i++) {
			System.out.println("  "+items[i]+" for $"+df.format(prices[i]));
		}
		System.out.println("The total cost was $"+df.format(totalCost(prices, count)));
	}

	private static double totalCost(double[] nums, int size) {
		double result = 0;
		for(int i = 0; i < size; i++) {
			result += nums[i];
		}
		return result;
	}
}