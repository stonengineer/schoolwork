import java.util.Scanner;

public class Lab15C
{
	private static int[] list;
	private static int count;

	public static void main(String[] args) {
		Lab15C lab = new Lab15C();
		list = new int[100];
		count = 0;
		lab.input();
		lab.output();
	}

	private static void input() {
		Scanner sc = new Scanner(System.in);
		boolean run = true;
		while(run) {
			System.out.print("Enter a number: ");
			int temp = sc.nextInt();
			if(temp == -99 || count == 100) {
				run = false;
			}
			else {
				list[count] = temp;
				count++;
			}
		}
	}

	private static void output() {
		System.out.println("The sum of the "+count+" numbers is "+sum());
		System.out.println("The average of the "+count+" numbers is "+average());
	}

	private static int sum() {
		int result = 0;
		for(int i = 0; i < count; i++) {
			result += list[i];
		}
		return result;
	}

	private static double average() {
		return ((double)sum()) / ((double)count);
	}
}