import java.util.Scanner;
import java.io.*;

public class Lab04A
{
	private final int current_year = 2011;
	private String name;
	private int age;
	private int year;

	public static void main(String[] args)
	{
		Lab04A lab = new Lab04A();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		try
		{
			Scanner sc = new Scanner(new File("Data/lab04a.dat")).useDelimiter("/");
			name = sc.next();
			age = sc.nextInt();
		}
		catch(IOException e)
		{
			System.out.println("File not found");
			System.exit(0);
		}
	}

	public void process()
	{
		year = current_year - age;
	}

	public void output()
	{
		System.out.println(name + ", you were born in " + year + ".");
	}
}