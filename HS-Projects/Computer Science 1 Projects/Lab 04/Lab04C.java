import java.util.Scanner;
import java.io.*;

public class Lab04C
{
	private String l1;
	private String l2;
	private String l3;
	private String l4;

	public static void main(String[] args)
	{
		Lab04C lab = new Lab04C();
		lab.input();
		lab.output();
	}

	public void input()
	{
		try
		{
			Scanner sc = new Scanner(new File("Data/lab04c.dat"));
			l1 = sc.nextLine();
			l2 = sc.nextLine();
			l3 = sc.nextLine();
			l4 = sc.nextLine();
		}
		catch(IOException e)
		{
			System.out.println("File not found.");
			System.exit(0);
		}
	}

	public void output()
	{
		String endl = "\n";
		System.out.println(l4 + endl + l3 + endl + l2 + endl + l1);
	}
}