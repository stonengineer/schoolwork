import java.util.Scanner;
import java.io.*;

public class Lab04B
{
	private String first;
	private String second;
	private String third;

	public static void main(String[] args)
	{
		Lab04B lab = new Lab04B();
		lab.input();
		lab.output();
	}

	public void input()
	{
		try
		{
			Scanner sc = new Scanner(new File("Data/lab04b.dat")).useDelimiter(",");
			first = sc.next();
			second = sc.next();
			third = sc.next();
		}
		catch(IOException e)
		{
			System.out.println("File not found.");
			System.exit(0);
		}
	}

	public void output()
	{
		String tab = "\t";
		System.out.println(first + tab + second + tab + third);
		System.out.println(first + tab + third + tab + second);
		System.out.println(second + tab + first + tab + third);
		System.out.println(second + tab + third + tab + first);
		System.out.println(third + tab + first + tab + second);
		System.out.println(third + tab + second + tab + first);
	}
}