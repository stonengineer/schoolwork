import java.util.Scanner;
import java.io.*;

public class Lab04E
{
	public static void main(String[] args)
	{
		try
		{
			Scanner sc = new Scanner(new File("Data/lab04e.dat"));
			sc.useDelimiter("\\.");
			int whole = sc.nextInt();
			int fract = sc.nextInt();

			System.out.println("The whole part is " + whole + ".\nThe fractional part is ." + fract + ".");
		}
		catch(IOException e)
		{
			System.out.println("File not found.");
			System.exit(0);
		}
	}
}