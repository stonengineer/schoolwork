import java.util.Scanner;
import java.io.*;
import java.text.DecimalFormat;

public class Lab04D
{
	public static void main(String[] args)
	{
		try
		{
			Scanner sc = new Scanner(new File("Data/lab04d.dat")).useDelimiter(",|\n");
			while(sc.hasNext())
			{
				int a = sc.nextInt();
				int b = sc.nextInt();

				int add = a + b;
				int sub = a - b;
				int mul = a * b;
				double div = ((double)a) / ((double)b);

				DecimalFormat df = new DecimalFormat("#.#");
				System.out.println(a + " + " + b + " = " + add);
				System.out.println(a + " - " + b + " = " + sub);
				System.out.println(a + " * " + b + " = " + mul);
				System.out.println(a + " / " + b + " = " + df.format(div));
				System.out.println();
			}
		}
		catch(IOException e)
		{
			System.out.println("File not found.");
			System.exit(0);
		}
	}
}