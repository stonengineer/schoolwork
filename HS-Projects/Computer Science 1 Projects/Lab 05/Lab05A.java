import java.util.Scanner;

public class Lab05A
{
	private int dividend;
	private int divisor;
	private int quotient;
	private int remainder;

	public static void main(String[] args)
	{
		Lab05A lab = new Lab05A();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter two integers: ");
		dividend = sc.nextInt();
		divisor = sc.nextInt();
	}

	public void process()
	{
		quotient = dividend / divisor;
		remainder = dividend % divisor;
	}

	public void output()
	{
		System.out.println("\n" + dividend + " divided by " + divisor + " equals " + quotient + " with a remainder of " + remainder + ".");
	}
}