import java.util.Scanner;

public class Lab05B
{
	private int a, b, c, tens, ones;

	public static void main(String[] args)
	{
		Lab05B lab = new Lab05B();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter three integers: ");
		a = sc.nextInt();
		b = sc.nextInt();
		c = sc.nextInt();
	}

	public void process()
	{
		tens = (a/10) + (b/10) + (c/10);
		ones = (a%10) + (b%10) + (c%10);
	}

	public void output()
	{
		System.out.println("\nTens = " + tens);
		System.out.println("Ones = " + ones);
	}
}