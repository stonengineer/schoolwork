import java.util.Scanner;

public class Lab05C
{
	private int time, hours, minutes, seconds;

	public static void main(String[] args)
	{
		Lab05C lab = new Lab05C();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Number of seconds to convert: ");
		time = sc.nextInt();
	}

	public void process()
	{
		hours = time / 3600;
		minutes = (time / 60) % 60;
		seconds = (time % 3600) % 60;
	}

	public void output()
	{
		System.out.println("\n" + time + " seconds = " + hours + " hours " + minutes + " minutes and " + seconds + " seconds.");
	}
}