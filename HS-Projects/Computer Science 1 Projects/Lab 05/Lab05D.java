import java.util.Scanner;

public class Lab05D
{
	private int number, thousands, hundreds, tens, ones;

	public static void main(String[] args)
	{
		Lab05D lab = new Lab05D();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a four digit number: ");
		number = sc.nextInt();
	}

	public void process()
	{
		thousands = (number / 1000) % 10;
		hundreds = (number / 100) % 10;
		tens = (number / 10) % 10;
		ones = number % 10;
	}

	public void output()
	{
		System.out.println("\n" + number + " equals\n" + thousands + " thousands\n" + hundreds + " hundreds\n" + tens + " tens\n" + ones + " ones");
	}
}