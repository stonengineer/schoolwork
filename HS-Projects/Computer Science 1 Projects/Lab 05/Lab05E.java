import java.util.Scanner;

public class Lab05E
{
	private double amount;
	private int cents;
	private int twenties, tens, fives, ones, quarters, dimes, nickels, pennies;

	public static void main(String[] args)
	{
		Lab05E lab = new Lab05E();
		lab.input();
		lab.process();
		lab.output();
	}

	public void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a monetary amount: ");
		amount = sc.nextDouble();
	}

	public void process()
	{
		cents = (int)(amount * 100);

		twenties = cents / 2000;
		tens = (cents / 1000) % 2;
		fives = (cents / 500) % 2;
		ones = (cents / 100) % 5;
		quarters = (cents / 25) % 4;
		dimes = (cents / 10) % 2;
		nickels = (cents / 5) % 3;
		pennies = cents % 10;
	}

	public void output()
	{
		System.out.println("\n" + amount + " = " + cents + " cents\n" + twenties + " = twenties\n" + tens + " = tens\n" + fives + " = fives\n" + ones + " = ones\n" + quarters + " = quarters\n" + dimes + " = dimes\n" + nickels + " = nickels\n" + pennies + " = pennies");
	}
}