import java.util.Scanner;
import java.io.*;

public class Lab11A
{
	private static int age;

	public static void main(String[] args)
	{
		while(true)
		{
			age = 0;
			System.out.print("Enter your age: ");

			Scanner sc = new Scanner(System.in);
			age = sc.nextInt();

			switch(age)
			{
				case 13:
					System.out.println("Teenager!");
					break;
				case 16:
					System.out.println("Keys, please?");
					break;
				case 18:
					System.out.println("Rise to vote, sir!");
					break;
				case 21:
					System.out.println("Full adult privileges");
					break;
				default:
					System.out.println("Too young to do anything.");
			}
		}
	}
}