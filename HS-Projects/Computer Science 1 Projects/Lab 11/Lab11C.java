import java.util.Scanner;
import java.io.*;

public class Lab11C
{
	private static char code;
	private static double units;

	public static void main(String[] args)
	{
		code = '\0';
		units = 0;

		input();
		output();
	}

	public static void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("<P - Pounds>\n<F - Feet>\n<M - Miles>\n");
		System.out.print("Make a selection: ");
		code = sc.next().charAt(0);
		System.out.print("Enter a real number: ");
		units = sc.nextDouble();
		System.out.println();
	}

	public static void output()
	{
		switch(code)
		{
			case 'P':
				System.out.println("" + units + " pounds = " + (units*4.9) + " newtons");
				break;
			case 'F':
				System.out.println("" + units + " feet = " + (units*0.30488) + " meters");
				break;
			case 'M':
				System.out.println("" + units + " miles = " + (units*1.61) + " kilometers");
				break;
		}
	}
}