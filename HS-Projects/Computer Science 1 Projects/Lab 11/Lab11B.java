import java.util.Scanner;
import java.io.*;

public class Lab11B
{
	private static int code, length;
	private static double cost;

	public static void main(String[] args)
	{
		code = length = 0;
		cost = 0;

		input();
		process();
		output();
	}

	private static void input()
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter a code number: ");
		code = sc.nextInt();
		System.out.print("Enter the length of the call: ");
		length = sc.nextInt();
	}

	private static void process()
	{
		switch(code)
		{
			case 1:
				cost = length * 0.22;
				break;
			case 2:
				cost = length * 0.14;
				break;
			case 3:
				cost = length * 0.73;
				break;
			case 4:
				cost = length * 1.12;
				break;
			case 5:
				cost = length * 2.38;
				break;
		}
	}

	private static void output()
	{
		System.out.println("\nYour code " + code + " call for " + length + " minutes costs $" + cost);
	}
}