#pragma once

#include "TemplateDoublyLinkedList.h"
#include <cstddef>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> indexes; //stores the indexes of the old sets

// Disjoint Set
template <typename T>
class DisjointSet {
private:
	vector<DListNode<T>*> nodeLocator;
public:
	~DisjointSet();
	DisjointSet(int n);
	vector<DListNode<T>*> getNodeLocator() const { return nodeLocator; }
	DListNode<T>* MakeSet(int key, T node);
	DListNode<T>* Union(DListNode<T>* nodeI, DListNode<T>* nodeJ);
	DListNode<T>* FindSet(DListNode<T>* node);
	DListNode<T>* FindSet(int nodeKey);
};

template <typename T>
DisjointSet<T>::DisjointSet(int n)
{
	nodeLocator.reserve(n);
}

template <typename T>
DisjointSet<T>::~DisjointSet()
{
	for(int i = 0; i < nodeLocator.size(); i++)
	{
		delete nodeLocator[i];
	}
	vector<DListNode<T>*> nodeLocator;
}

template <typename T>
DListNode<T>* DisjointSet<T>::MakeSet(int key, T node)
{
	if(key > nodeLocator.size()) {nodeLocator.resize(key*2);}
	nodeLocator[key-1] = new DListNode<T>(key, node);
	nodeLocator[key-1]->setRepresentative(nodeLocator[key-1]);
	nodeLocator[key-1]->setTrailer(nodeLocator[key-1]);
	return nodeLocator[key-1];
}

template <typename T>
DListNode<T>* DisjointSet<T>::Union(DListNode<T>* nodeI, DListNode<T>* nodeJ)
{
	int indexJ = -1;
	for(int i = 0; i < nodeLocator.size()-1; i++)
	{
		if(nodeI->getKey() == nodeLocator[i]->getKey())
			indexes.push_back(i);
		if(nodeJ->getKey() == nodeLocator[i]->getKey())
			indexJ = i;
	}
	while(nodeI != NULL)
	{
		nodeJ->getTrailer()->insert_after(nodeI->getElem());
		nodeI = nodeI->getNext();
	}
	nodeLocator[indexJ] = nodeJ->getRepresentative();
	return nodeJ->getRepresentative();
}

template <typename T>
DListNode<T>* DisjointSet<T>::FindSet(DListNode<T>* node)
{
	return node->getRepresentative();
}

template <typename T>
DListNode<T>* DisjointSet<T>::FindSet(int nodeKey)
{
	for(int i = 0; i < nodeLocator.size(); i++)
	{
		DListNode<T>* temp = nodeLocator[i];
		while(temp != NULL)
		{
			if(temp->getKey() == nodeKey)
				return temp->getRepresentative();
			temp = temp->getNext();
		}
	}
	return NULL;
}

template <typename T>
ostream& operator<<(ostream& out, const DisjointSet<T>& ds) {
	int count = 0;
	for(int i = 0; i < ds.getNodeLocator().size()-1; i++)
	{
		if(!(find(indexes.begin(), indexes.end(), i) != indexes.end()))
		{
			DListNode<T>* temp = ds.getNodeLocator()[i];
			out<<"Set "<<++count<<": ";
			if(temp != NULL)
			{
				while(temp != NULL)
				{
					cout<<temp->getElemt()<<" ";
					temp = temp->getNext();
				}
			}
			out<<endl;
		}
	}
	out<<"";
}