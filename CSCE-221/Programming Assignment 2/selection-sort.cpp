//============================================================================
// Name        : selection-sort.cpp
// Author      : 
// Date        :
// Copyright   : 
// Description : Implementation of selection sort in C++
//============================================================================

#include "sort.h"
#include <iostream>

void
SelectionSort::sort(int A[], int size)				// main entry point
{
   /*Complete this function with the implementation of selection sort algorithm 
     Record number of comparisons in variable num_cmps of class Sort*/

   for(int i = 0; i < size; i++)
   {
     int lowest = i;
     for(int j = i; j < size; j++)
     {
       if(A[lowest] > A[j]) 
       {
         lowest = j;
         ++num_cmps;
       }
     }
     int temp = A[i];
     A[i] = A[lowest];
     A[lowest] = temp;
   }
}
