//============================================================================
// Name        : bubble-sort.cpp
// Author      : 
// Date        :
// Copyright   : 
// Description : Implementation of bubble sort in C++
//============================================================================

#include "sort.h"

void 
BubbleSort::sort(int A[], int size)			// main entry point
{
   /*Complete this function with the implementation of bubble sort algorithm 
     Record number of comparisons in variable num_cmps of class Sort */
   int comps = 0;

   bool unsorted = true;
   while(unsorted)
   {
     comps = 0;
     for(int i = 1; i < size; i++)
     {
       if(A[i-1] > A[i])
       {
         int temp = A[i-1];
         A[i-1] = A[i];
         A[i] = temp;
         ++num_cmps;
         ++comps;
       }
     }
     if(comps == 0) {unsorted = false;}
   }
}
