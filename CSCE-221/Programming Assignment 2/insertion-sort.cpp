//============================================================================
// Name        : insertion-sort.cpp
// Author      : 
// Date        :
// Copyright   : 
// Description : Implementation of insertion sort in C++
//============================================================================

#include "sort.h"

void
InsertionSort::sort(int A[], int size)				// main entry point
{
   /*Complete this function with the implementation of insertion sort algorithm 
     Record number of comparisons in variable num_cmps of class Sort*/

   for(int i = 0; i < size; i++)
   {
     int j = i;
     while(A[j-1] > A[j] && j > 0)
     {
       int temp = A[j-1];
       A[j-1] = A[j];
       A[j] = temp;
       --j;
       ++num_cmps;
     }
   }
}
