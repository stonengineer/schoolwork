#include "std_lib_facilities_4.h"

int main()
{
   int sets[5] = {10,100,1000,10000,100000};
   cout<<"NUMERIC INTEGER SET GENERATOR\n\nPROCEED (Y/N) : ";
   char c;
   cin>>c;
   if(c == 'Y' || c == 'y')
   {
     cout<<endl<<"GENERATING RANDOM SETS...\n";
     srand(time(NULL));
     for(int i = 0; i < 5; i++)
     {
       ostringstream os;
       os<<"RAND-"<<sets[i]<<".txt";
       ofstream result(os.str());
       if(result.is_open()) {result<<sets[i]<<endl;}
       for(int j = 0; j < sets[i]; j++)
       {
         int temp = std::rand()%sets[i] + 1;
         if(result.is_open()) {result<<temp<<endl;}
       }
     }
     cout<<"RANDOMS GENERATED\nGENERATING INCREASING SETS...\n";
     for(int i = 0; i < 5; i++)
     {
       ostringstream os;
       os<<"INCR-"<<sets[i]<<".txt";
       ofstream result(os.str());
       if(result.is_open()) {result<<sets[i]<<endl;}
       for(int j = 0; j < sets[i]; j++) {result<<j<<endl;}
     }
     cout<<"INCREASING GENERATED\nGENERATING DECREASING SETS...\n";
     for(int i = 0; i < 5; i++)
     {
       ostringstream os;
       os<<"DECR-"<<sets[i]<<".txt";
       ofstream result(os.str());
       if(result.is_open()) {result<<sets[i]<<endl;}
       for(int j = sets[i]; j > 0; j--) {result<<j<<endl;}
     }
     cout<<"ALL SETS GENERATED SUCCESSFULLY. TERMINATING.\n";
   }
   else{return 0;}
}


























