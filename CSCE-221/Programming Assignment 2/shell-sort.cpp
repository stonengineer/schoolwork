//============================================================================
// Name        : shell-sort.cpp
// Author      : 
// Date        :
// Copyright   : 
// Description : Implementation of shell sort in C++
//============================================================================

#include "sort.h"
#include <iostream>
#include <cmath>

void
ShellSort::sort(int A[], int size)
{
   /*Complete this function with the implementation of shell sort algorithm 
   Record number of comparisons in variable num_cmps of class Sort*/

   int* gaps;
   int k = 0;
   if(size > 0) 
   {
     k = log2(size+1);
     gaps = new int[k+1];
     int count = 0;
     for(int i = k; i > 0; --i) {gaps[count++] = pow(2,i) - 1;}
   }
   else
   {
     std::cerr<<"Invalid size.\n";
     return;
   }

   for(int i = 0; i < k; i++)
   {
     for(int j = gaps[i]; j < size; j++)
     {
       int x = j;
       while(A[x-1] > A[x] && x > 0)
       {
         int temp = A[x-1];
         A[x-1] = A[x];
         A[x] = temp;
         --x;
         ++num_cmps;
       }
     }
   }
























}
