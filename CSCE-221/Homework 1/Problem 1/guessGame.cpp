#include "guessGame.h"

int guessGame::compGuess(int max)
{
   //computer guess algorithm
   
   //calculates number of guesses
   int n = 0;
   while(pow(2,n) < max) {++n;}
   ++n;

   int count = 0;

   if(max == 1)
   {
     cout<<"Your number is 1,\n";
     return count;
   }

   int min = 0;
   int oldguess = 0;
   cout<<"I will guess your number within "<<n<<" guesses.\n";
   for(int i = 0; i < n; i++)
   {
     int guess = (max + min)/2;
     if(guess == oldguess) {++guess;}
     cout<<"Is your number "<<guess<<"? (Y/N) : ";
     oldguess = guess;
     ++count;
     char c;
     cin>>c;
     if(c == 'Y' || c == 'y') {return count;}
     else if(c == 'N' || c == 'n')
     {
       cout<<"Was my guess higher or lower than your number? (H/L) : ";
       char b;
       cin>>b;
       if(b == 'H' || b == 'h') {max = guess;}
       else if(b == 'L' || b == 'l') {min = guess;}
       else{exit(0);}
     }
     else {exit(0);}
   }
   cout<<"You are either lying, or you have proven an essential mathematical theorem incorrect. If it is the latter, please mention Joseph William Stone (author of this program) in your Nobel Prize speech. In the more likely case that you were lying, please understand the fact that you sacrificed your moral character in order to beat a computer game. Let that sink in for a second, then reevaluate some things.....\n";
   exit(0);
}

bool guessGame::humanGuess(int max)
{
   //human guess algorithm

   //calculates number of guesses
   int n = 0;
   while(pow(2,n) < max) {++n;}
   ++n;

   //generates random number and gives human 1 extra guess (ez-mode)
   n+=1;
   cout<<"Thinking..."<<endl;
   srand(time(NULL));
   int number = rand()%max + 1;
   cout<<"Got it! You have "<<n<<" guesses, good luck."<<endl;

   //algorithm runs through human's guesses
   while(n > 0)
   {
     cout<<"Your guess: ";
     int guess;
     cin>>guess;
     if(guess > number)
       cout<<"Too high!\n";
     else if(guess < number)
       cout<<"Too low!\n";
     else
       return true;
     cout<<--n<<" guesses remaining.\n";
   }

   //if human doesn't know binary searches
   cout<<"Sorry, you failed to guess my number, "<<number<<". Better luck next time!"<<endl;
   return false;
}

int guessGame::testGuess(int max)
{
   //calculates number of required guesses
   int n = 0;
   while(pow(2,n) < max) {++n;}
   ++n;

   int min = 0;
   int guess = max/2;
   int oldguess = 0;
   for(int i = 0; i < n; i++)
   {
     guess = (max+min)/2;
     if(oldguess == guess) {++guess;}
     oldguess = guess;
     if(guess < max) {min = guess;}
     else if(guess > max) {max = guess;}
     else {return i;}
   }
}
