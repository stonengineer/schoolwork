#include "guessGame.h"

int main()
{
   //get max value from user
   int userMax;
   cout<<"Enter the maximum number: ";
   cin>>userMax;
   if(cin.fail())
   {
     cout<<"Fatal error.\n";
     return 1;
   }

   //enter play mode
   cout<<"Enter mode: (C)omputer Guesses or (P)layer Guesses : ";
   char m;
   cin>>m;

   
   if(cin.fail())
   {
     cout<<"Fatal error.\n";
     return 2;
   }
   guessGame g;   
   if(m == 'C' || m == 'c')
   {
     cout<<"Computer mode! I will begin guessing."<<endl;
     int guess = g.compGuess(userMax);
     cout<<"Computer wins! I guessed the correct number in "<<guess<<" guesses!\n";
   }
   else if(m == 'P' || m == 'p')
   {
     cout<<"You think you can guess my number? Good luck..."<<endl;
     if(g.humanGuess(userMax))
     {
       cout<<"Congratulations! You guessed my number!"<<endl;
       return 0;
     }
   }
   else {main();}

   return 0;
}
