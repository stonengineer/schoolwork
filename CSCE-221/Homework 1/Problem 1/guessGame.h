#include "std_lib_facilities_4.h"

class guessGame
{
   public:
     int compGuess(int max);
     bool humanGuess(int game);
     int testGuess(int max); 
};
