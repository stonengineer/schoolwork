#include "guessGame.h"

class contain {
   string range;
   int n;
   int guesses;
   int formula;

   public:
     contain(string s, int i1, int i2, int i3)
     {
       range = s;
       n = i1;
       guesses = i2;
       formula = i3;
     }
     string toString()
     {
       ostringstream os;
       os<<range<<" | n ="<<n<<" | Algorithm guesses:"<<guesses<<" | Formula prediction:"<<formula<<endl;
       return os.str();
     }
};

int main()
{
   cout<<"**********************************\n";
   cout<<"****TESTING GUESS GAME RUNTIME****\n";
   cout<<"**********************************\n\n";
   
   remove("result.txt");

   //Tests the number of guesses between a specified range, and compares this
   //number to the mathematical estimation of the worst-case scenario
   
   guessGame g;
   vector<contain> v;
   ofstream test("result.txt");

   for(int i = 0; i < 12; i++)
   {
     int p = pow(2,i);
     ostringstream os;
     os<<"[1,"<<p<<"]";
     contain temp(os.str(),p,g.testGuess(p),log2(p));
     v.push_back(temp);
   }

   for(int i = 1; i < 12; i++)
   {
     int p = pow(2,i)-1;
     ostringstream os;
     os<<"[1,"<<p<<"]";
     contain temp(os.str(),p,g.testGuess(p),log2(p));
     v.push_back(temp);
   }

   if(test.is_open()){for(int i = 0; i < v.size(); i++) {test<<v[i].toString();}}
   else {cerr<<"Unable to open file.\n";}

   cout<<"Written to file.\n";
}
