#include "std_lib_facilities_4.h"

int main()
{
   vector<string> v;
   try
   {
     ifstream myfile;
     myfile.open("problem2.dat");
     while(!myfile.eof()) {
       string temp;
       myfile>>temp;
       v.push_back(temp);
     }
   }
   catch(exception) {cerr<<"No file in directory.\n";}

   cout<<"Problem 2\n";
   for(int i = 0; i < v.size(); i++)
   {
     if(v[i].length() != 0)
     {
       cout<<"Current String: "<<v[i]<<endl;
       bool temp = true;
       int max = v[i].length()-1;
       for(int x = 0; x < v[i].length()/2; x++)
       {
         if(v[i][x] != v[i][max]) {temp = false;}
         --max;
       }
       if(temp) {cout<<v[i]<<" is a palindrome!\n";}
       else {cout<<v[i]<<" is not a palindrome!\n";}
       cout<<endl;
     }
     else {cout<<"Current String: \"\" \n\"\" is not a palindrome!";}
     cout<<endl;
   }
}
