lim (n->0) of (nlog(n))/(n^2)
= lim (n->0) of log(n)/n
= lim (n->0) of (1/n)/1 //L'Hopitals
= lim (n->0) of 1/n
= 0

This proves that, until n = 100, the algorithm with 
O(n^2) complexity will run faster than the one with
O(nlogn) complexity. However, as n goes to infinity,
the O(nlogn) function becomes astronomically faster
than the O(n^2) algorithm.