#include "gVec.h"

template <typename T>
gVec::gVec(const gVec& vec)
{
	size = vec.get_size();
	capacity = vec.get_capacity();
	ptr = new T[capacity];
	for(int i = 0; i < size; i++)
		ptr[i] = vec[i];
}

template <typename T>
gVec& gVec::operator=(const gVec& vec)
{
	size = vec.get_size();
	capacity = vec.get_capacity();
	ptr = new T[capacity];
	for(int i = 0; i < size; i++)
		ptr[i] = vec[i];
}

template <typename T>
T gVec::operator[](int i) const
{
	if(i < 0 || i > size)
		throw exception();
	return ptr[i];
}

template <typename T>
T gVec::operator[](int i)
{
	if(i < 0 || i > size)
		throw exception();
	return ptr[i];
}

template <typename T>
bool gVec::empty() const
{
	return size == 0;
}

template <typename T>
T gVec::get(int i) const
{
	if(i < 0 || i > size)
		throw exception();
	return ptr[i];
}

template <typename T>
void gVec::insert(int i, const T data)
{
	if(i < 0 || i > size)
		throw exception();
	++size;
	if(size == capacity)
		capacity *= 2;
	T *temp = ptr;
	ptr = new T[capacity];
	if(i == 0)
	{
		ptr[0] = data;
		for(int x = 0; x < size; x++)
			ptr[x+1] = temp[x];
	}
	else
	{
		for(int x = 0; x < i; x++)
			ptr[x] = temp[x];
		ptr[i] = data;
		for(int x = i+1; x < size; x++)
			ptr[x] = temp[x-1];
	}
}

template <typename T>
void gVec::replace(int i, const T data)
{
	if(i < 0 || i > size)
		throw exception();
	ptr[i] = data;
}

template <typename T>
void gVec::remove(int i)
{
	if(i < 0 || i > size)
		throw exception();
	T *temp = ptr;
	ptr = new T[capacity];
	int index = 0;
	for(int x = 0; x < size; x++)
		if(x != i)
			ptr[++index] = temp[x];
}

template <typename T>
ostream& gVec::operator<<(ostream& out, const gVec& vec)
{
	for(int i = 0; i < vec.get_size(); i++)
		out<<vec[i]<<" ";
	out<<endl;
}

template <typename T>
int gVec::findMaxIndex(const gVec& vec, int size)
{
	if(size > vec.get_size())
		throw exception();
	int largest = vec[0];
	int index = 0;
	for(int i = 1; i < size; i++)
	{
		if(vec[i] > largest)
		{
			largest = vec[i];
			index = i;
		}
	}
	return index;
}

template <typename T>
void gVec::sort_max(gVec& vec)
{
	gVec temp();
	temp = vec;
	for(int i = 0; i < vec.get_size(); i++)
	{
		int index = findMaxIndex(temp, temp.get_size());
		vec.replace(index, temp[index]);
		temp.remove(index);
	}
}