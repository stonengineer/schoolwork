/*gVec.h
Joseph Stone
Header file for generic vector
*/

#include <ostream>
#include <iostream>
#include <exception>

using namespace std;

template <typename T>
class gVec 
{
	//member variables
	int size, capacity;
	T *ptr;
	public:
	//member functions
	gVec() 
	{
		size = 0;
		capacity = 1;
		ptr = new T[capacity];
	}
	gVec(const gVec& vec);
	gVec& operator=(const gVec& vec);
	int getSize() const { return size; }
	int getCap() const { return capacity; }
	T operator[](int i) const;
	T operator[](int i);
	bool empty() const;
	T get(int i) const;
	void insert(int i, const T data);
	void replace(int i, const T data);
	void remove(int i);
};

ostream& operator<<(ostream& out, const gVec& vec);
int findMaxIndex(const gVec& vec, int size);
void sort_max(gVec& vec);
