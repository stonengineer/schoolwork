#include "My_vec.h"
#include <iostream>

My_vec::My_vec()
{
   size = 0;
   capacity = 1;
   ptr = new char[capacity];
}

My_vec::~My_vec()
{
   size = capacity = 0;
   ptr = new char[0];
}

My_vec::My_vec(const My_vec& vec)
{
   size = vec.size;
   capacity = vec.capacity;
   ptr = vec.ptr;
}

My_vec& My_vec::operator=(const My_vec& vec) 
{
   size = vec.size;
   capacity = vec.capacity;
   ptr = vec.ptr;
}

int My_vec::get_size() const {return size;}

int My_vec::get_capacity() const {return capacity;}

char& My_vec::operator[](int i) const {return ptr[i];}

char& My_vec::operator[](int i) {return ptr[i];}

bool My_vec::is_empty() const {return size==0;}

char& My_vec::elem_at_rank(int r) const {return ptr[r];}

void My_vec::insert_at_rank(int r, const char& elem)
{
   char *temp = ptr;
   ++size;
   if(r > size) 
   {
     cerr<<"Rank too large. Element inserted at position "<<size-1<<" instead.\n";
     r = size-1;
   }
   ++capacity;
   ptr = new char[capacity];
   if(r == 0)
   {
     ptr[0] = elem;
     for(int i = 0; i < size; i++) 
     {
       ptr[i+1] = temp[i];
     }
   }
   else if(r < size && r > 0)
   {
      for(int i = 0; i < r; i++) {ptr[i] = temp[i];}
      ptr[r] = elem;
      for(int i = r; i < size; i++) {ptr[i+1] = temp[i];}
   }
   else if(r == size)
   {
     for(int i = 0; i < size; i++) {ptr[i] = temp[i];}
     ptr[size] = elem;
   }
   else {cerr<<"Invalid rank.\n";}
}

void My_vec::replace_at_rank(int r, const char& elem) 
{
   if(r > size || r < 0) {cerr<<"Invalid rank.\n";}
   ptr[r] = elem;
}

void My_vec::remove_at_rank(int r)
{
   if(r > size || r < 0) {cerr<<"Invalid rank.\n";}
   char *temp = ptr;
   --size;
   --capacity;
   ptr = new char[capacity];
   for(int i = 0; i < r; i++) {ptr[i] = temp[i];}
   for(int i = r; i < capacity; i++) {ptr[i] = temp[i+1];}
}

ostream& operator<<(ostream& out, const My_vec& vec) 
{
   for(int i = 0; i < vec.get_size(); i++) 
   {
      cout<<vec[i]<<endl;
   }
   cout<<"";
}

int find_max_index(const My_vec& v, int size)
{
   int index = 0;
   char c = v[0];
   for(int i = 1; i < size; i++)
   {
     if(c < v[i]) 
     {
       c = v[i];
       index = i;
     }
   }
   return index;
}

void sort_max(My_vec& vec)
{
   My_vec temp;
   int count = vec.get_size();
   int i = 0;
   while(count > 0)
   {
     int index = find_max_index(vec, count);
     temp.insert_at_rank(i, vec.elem_at_rank(index));
     ++i;
     --count;
   }
   vec = temp;
}
















