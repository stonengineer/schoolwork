#include "Graph.h"
#include "TemplateDoublyLinkedList.h"
#include <iostream>

using namespace std;

void Graph::buildGraph() {
  if((n<0) || (e<0)){
    throw GraphException("either row or column is negative!");
  }
  EdgeList.resize(e);
  
  for(int f = 0; f < n; f++)
  {
	  Vertex temp(0);
	  AdjacencyList.push_back(new DListNode<Vertex>(f, temp));
	  for(int y = 0; y < e; y++)
	  {
		  AdjacencyList[f]->insert_after(temp);
	  }
  }
}

void Graph::insertEdge(int i, int j, double w) 
{
	if(curEdges >= e)
	{
		throw GraphException("edge number is not correct!");
	}

	Edge* edge = new Edge(i, j, w);
	EdgeList[curEdges] = edge;
	Vertex vert(w);
	vert.edge = edge;
	DListNode<Vertex>* node = new DListNode<Vertex>(j, vert);
	AdjacencyList[i]->insert_before(node->getElem());
		
	curEdges++;
}

double Graph::getWeight(int i, int j) 
{
	//implement your code here
	for(int f = 0; f < curEdges; f++)
	{
		if((EdgeList[f]->vertex_i == i && EdgeList[f]->vertex_j == j) || (EdgeList[f]->vertex_i == j && EdgeList[f]->vertex_j == i))
		{
			return EdgeList[f]->weight;
		}
	}
	return 0;
}