#include <string>
#include <sstream>
#include <fstream>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include "BinarySearchTree.h"

using namespace std;

bool first = true;

void start()
{
  cout<<"*************************************************\n";
  cout<<"*****************WELCOME TO THE******************\n";
  cout<<"***************BINARY SEARCH TREE****************\n";
  cout<<"****************TESTING FUNCTION*****************\n";
  cout<<"*************************************************\n\n";
  cout<<"(c) Joseph Stone, 2015\n\n";
  cout<<"This program allows you to select a\n";
  cout<<"file you would like to use and test\n";
  cout<<"my binary search tree data structure.\n\n";
  cout<<"Commands are as follows:\n";
  cout<<"   run [filename] specifies the testing file.\n";
  cout<<"   [U] allows user to perform tests.\n";
  cout<<"   [G] prints given file as GUI BST to file.\n";
  cout<<"   [D] displays the available test files.\n";
  cout<<"   [X] exits the program.\n";
  cout<<"Sample input:\n";
  cout<<"run 9r U - This runs a user-defined test using\n";
  cout<<"           the data from the file 9r.\n";
  cout<<"A full list of filenames may be found\n";
  cout<<"in the test-files folder.\n\n";
  first = false;
}

void fail(string reason);
void userTests(BinarySearchTree bst);
void generateFile(BinarySearchTree bst, string file);
void displayDir();

int main() {
  if(first) {start();}
  cout<<"--------------------------------------------------\\\n";
  cout<<"% ";
  string command;
  cin>>command;
  if(command == "X" || command == "x") {exit(0);}
  if(command == "D" || command == "d") {displayDir();}
  if(command == "run") {cin>>command;}
  else {fail("Invalid command syntax.");}
  string file = command;
  BinarySearchTree bst;
  try {
    ostringstream os;
    os<<"test-files/"<<command;
    ifstream in(os.str());
    while(!in.eof())
    {
      int temp = 0;
      int prev = temp;
      in>>temp;
      if(temp != prev) {bst.add(temp);}
    }
  }
  catch(const exception& e) {fail("File not found.");}
  cin>>command;
  if(command.length() != 1) {fail("Invalid command syntax.");}
  char commandConv = command[0];
  switch(commandConv)
  {
    case 'U':
    case 'u':
    	userTests(bst);
    	break;
    case 'G':
    case 'g':
    	generateFile(bst,file);
    	break;
    default:
    	fail("Invalid command syntax.");
    	break;
  }
  return 0;
}

void fail(string reason)
{
  cout<<reason<<endl;
  main();
}

void userTests(BinarySearchTree bst)
{
  cout<<"Instructions:\n";
  cout<<"[A] _ adds an element to the Tree\n";
  cout<<"[R] _ removes an element from the Tree\n";
  cout<<"[S] _ finds search cost of given element\n";
  cout<<"[P] prints the entire tree\n";
  cout<<"[V] prints the average search cost\n";
  cout<<"[N] prints total number of nodes\n";
  cout<<"[X] return to menu\n";
  cout<<"Example: A 4 - adds 4 to the Tree\n\n";

  char in;
  int num;
  while(in != 'x' || in != 'X') 
  {
    cout<<"% ";
    cin>>in;
    switch(in)
    {
      case 'A':
      case 'a':
          cin>>num;
          if(cin) {bst.add(num);}
          break;
      case 'R':
      case 'r':
          cin>>num;
          if(cin) {bst.remove(num);}
          break;
      case 'S':
      case 's':
          cin>>num;
          if(cin) {bst.getSearchCost(num);}
          break;
      case 'P':
      case 'p':
          cout<<"Print:\n[L] lists all data\n[G] prints tree\n%";
		  char choice;
		  if(cin>>choice)
		  {
			  if(choice == 'l' || choice == 'L')
				  bst.print();
			  else if(choice == 'g' || choice == 'G')
				  bst.guiPrint();
			  else
			  {
				  cerr<<"Invalid command option.\n";
				  break;
			  }
		  }
		  else {cerr<<"Invalid command option.\n";}
          break;
      case 'V':
      case 'v':
          cout<<"Average Search Cost: "<<bst.getAverage()<<endl;
          break;
      case 'N':
      case 'n':
          cout<<"Total Number of Nodes: "<<bst.size()<<endl;
          break;
      case 'X':
      case 'x':
          main();
          break;
      default:
          cout<<"Invalid selection.\n";
          break;
    }
    cout<<endl;
  }
}

void generateFile(BinarySearchTree bst, string file)
{
  ofstream out;
  stringstream ss;
  ss<<"1-4_Generated_BST/"<<file<<".txt";
  out.open(ss.str());
  bst.printer(out);
  cout<<"File generated.\n";
  main();
}

void displayDir()
{
  DIR *dp;
  struct dirent *dirp;
  if((dp = opendir("test-files")) == NULL)
  {
    cerr<<"Cannot open directory.\n";
    return;
  }
  while((dirp = readdir(dp)) != NULL) {cout<<string(dirp->d_name)<<endl;}
  closedir(dp);
  main();
}
