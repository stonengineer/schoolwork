#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <queue>

using namespace std;

struct node {
  int data;
  int searchCost;
  node* left;
  node* right;
};

class BinarySearchTree {
  private:
    node* head;
    int totalNodes = 0;
    int sumCost = 0;
	int lowestLevel = 0;
    void insert(int elem, node* leaf)
    {
      int data = leaf->searchCost;
      if(elem < leaf->data)
      {
        if(leaf->left != NULL) {insert(elem, leaf->left);}
        else
        {
          leaf->left = new node;
          leaf->left->data = elem;
          leaf->left->searchCost = data+1;
          sumCost += data+1;
		  if(data+1 > lowestLevel) {lowestLevel = data+1;}
          leaf->left->left = leaf->left->right = NULL;
        }
      }
      else if(elem >= leaf->data)
      {
        if(leaf->right != NULL) {insert(elem, leaf->right);}
        else
        {
          leaf->right = new node;
          leaf->right->data = elem;
          leaf->right->searchCost = data+1;
          sumCost += data+1;
		  if(data+1 > lowestLevel) {lowestLevel = data+1;} 
          leaf->right->left = leaf->right->right = NULL;
        }
      }
    }
    node* findLeftmost(node* leaf)
    {
      if(leaf->left != NULL) {return findLeftmost(leaf->left);}
      return leaf;
    }
	node* findRightmost(node* leaf)
	{
		if(leaf->right != NULL) {return findRightmost(leaf->right);}
		return leaf;
	}
    node* parent(node* leaf)
    {
      while(leaf->right != NULL)
        leaf = leaf->right;
      return leaf;
    }
    node* kill(int elem, node* leaf)
    {
      if(leaf != NULL)
      {
        if(elem == leaf->data)
        {
          node* save = NULL;
          if(leaf->left == NULL)
          {
            save = leaf->right;
            sumCost -= leaf->searchCost;
            delete leaf;
            return save;
          }
          else if(leaf->right == NULL)
          {
            save = leaf->left;
            sumCost -= leaf->searchCost;
            delete leaf;
            return save;
          }
          else
          {
            node* super = parent(leaf->left);
            leaf->data = super->data;
            leaf->searchCost = super->searchCost;
            leaf->left = kill(super->data, leaf->left);
          }
        }
        else if(elem < leaf->data) {leaf->left = kill(elem, leaf->left);}
        else {leaf->right = kill(elem, leaf->right);}
      }
      else 
      {
        cerr<<"Cannot find node.\n";
        return NULL;
      }
      return leaf;
    }
    void destroy(node* leaf)
    {
      if(leaf != NULL)
      {
        destroy(leaf->left);
        destroy(leaf->right);
        delete(leaf);
      }
    }
    void printNode(node* leaf)
    {
      if(leaf == NULL) {return;}
      printNode(leaf->left);
      cout<<"Key: "<<leaf->data<<" | Search Cost: "<<leaf->searchCost<<endl;
      printNode(leaf->right);
    }
    void printToFile(node* leaf, ostream& write)
    {
      if(leaf == NULL) {return;}
      printToFile(leaf->left, write);
      write<<"Key: "<<leaf->data<<" | Search Cost: "<<leaf->searchCost<<endl;
      printToFile(leaf->right, write);
    }
    bool deleteHead()
    {
      if(totalNodes == 1)
      {
        delete(head);
        --totalNodes;
        return true;
      }
      else {
        if(head->right != NULL)
        {
          node* temp = findLeftmost(head->right);
          node* parent = head->right;
          while(parent->left != temp) {parent = parent->left;}
          parent->left = NULL;
          temp->right = head->right;
          temp->left = head->left;
          sumCost -= head->searchCost;
          delete head;
          --totalNodes;
          temp->searchCost = 1;
          head = temp;
          return true;
        }
        else
        {
          node* temp = head->left;
          sumCost -= head->searchCost;
          delete head;
          --totalNodes;
          temp->searchCost = 1;
          head = temp;
          return true;
        }
      }
      return false;
    }
    node* search(int elem, node* n)
    {
      if(n == NULL) {return NULL;}
      if(n->data == elem) {return n;}
      else if(elem > n->data) {return search(elem, n->right);}
      else {return search(elem, n->left);}
    }
  public:
    BinarySearchTree() {head = NULL;}
    ~BinarySearchTree() {destroy(head);}
    void add(int elem)
    {
      if(head == NULL)
      {
        head = new node;
        head->data = elem;
        head->searchCost = 0;
        ++sumCost;
        head->left = head->right = NULL;
        ++totalNodes;
      }
      else
      {
        insert(elem, head);
        ++totalNodes;
      }
    }
    void remove(int elem) 
    {
      if(elem == head->data) {deleteHead();}
      else 
      {
        node* n = kill(elem, head);
        if(n != NULL) {--totalNodes;}
      }
    }
    void print()
    {
      if(totalNodes < 16) {printNode(head);}
      else 
      {
        ofstream write;
        cout<<"The size of the tree is\ngreater than 16; therefore\nI must create an output file.\nFile name: ";
        string in;
        cin>>in;
        stringstream os;
        os<<"outputs/"<<in<<".txt";
        write.open(os.str());
        printToFile(head, write);
      }
    }
	void guiPrint()
	{
		if(totalNodes < 16)
		{
			printer(cout);
		}
		else
		{
			ofstream out;
			cout<<"The size of the tree is\ngreater than 16; therefore\nI must create an output file.\nFile name: ";
			string in;
			cin>>in;
			stringstream os;
			os<<"outputs/"<<in<<".txt";
			out.open(os.str());
			printer(out);
		}
	}
	void printer(ostream& out)
	{
		if(head == NULL) {return;}
		queue<node*> current, next;
		current.push(head);
		int count = 0;
		while(!current.empty() && count <= lowestLevel)
		{
			node* temp = current.front();
			current.pop();
			if(temp)
			{
				out<<temp->data<<" ";
				next.push(temp->left);
				next.push(temp->right);
			}
			else
			{
				out<<"X ";
				next.push(NULL);
			}
			if(current.empty())
			{
				out<<endl;
				queue<node*> hold = current;
				current = next;
				next = hold;
				++count;
			}
		}
	}
    int size() {return totalNodes;}
    node* getHead() {return head;}
    int getSearchCost(int elem)
    {
      node* found = search(elem, head);
      if(found != NULL) 
      {
        cout<<"Search cost for ["<<elem<<"]: "<<found->searchCost<<endl;
        return found->searchCost;
      }
      else
      {
        cerr<<"Cannot find node.\n";
        return -1;
      }
    }
    double getAverage() {return sumCost * 1.0 / totalNodes;}
};
