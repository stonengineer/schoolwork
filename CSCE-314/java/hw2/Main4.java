import java.lang.*;
import java.lang.reflect.*;

public class Main4
{
	public static void main(String[] args)
	{
		try {
			String className = args[0];
			Object obj = Class.forName(className).newInstance();
			Class testerClass = obj.getClass();

			Method[] definedMethods = testerClass.getDeclaredMethods();
			for(Method method : definedMethods)
			{
				Class[] allParameters = method.getParameterTypes();
				String returnType = method.getReturnType().getName();
				boolean startsWithTest = false;
				if(method.getName().length() >= 4)
				{
					String start = "";
					for(int i = 0; i < 4; i++) { start += method.getName().charAt(i); }
					if(start.equals("test")) { startsWithTest = true; }
				}
				if(Modifier.isStatic(method.getModifiers()) && Modifier.isPublic(method.getModifiers()) && allParameters.length == 0 && returnType.equals("boolean") && startsWithTest)
				{
					Object result = method.invoke(obj);
					if((boolean)result)
						System.out.println("OK: " + method.getName() + " succeeded.");
					else
						System.out.println("FAILED: " + method.getName() + " failed.");
				}
			}
		}
		catch(Exception e) { System.out.println(e.toString()); }
	}
}