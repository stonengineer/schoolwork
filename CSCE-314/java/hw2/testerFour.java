public class testerFour
{
	public testerFour () 
	{
		System.out.println("Output should be: ");
		System.out.println("OK: testA succeeded.");
		System.out.println("FAILED: testB failed.");
		System.out.println("OK: testG succeeded.\n\nBegin test output:");
	}

	public static boolean testA() { return true; }

	public static boolean testB() { return false; }

	public static boolean testC(boolean a) { return a; }

	private static boolean testD() { return true; }

	public static boolean eTest() { return true; }

	public static int testF () { return 0; }

	public static boolean testG() { return true; }

	public boolean testH() { return false; }
}