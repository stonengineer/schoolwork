import java.util.*;

public class Main2
{
	private static final String message7 = "\n7 second message.";
	private static final String message15 = "\n15 second message.";

	private static class Counter implements Runnable
	{
		private int count = 0;
		private boolean stop = false;

		public Counter() { new Thread(this).start(); }

		public int getCount() { return count; }

		public void stop() { this.stop = true; }

		public void run()
		{
			try {
				while(!stop)
				{
					++count;
					Thread.sleep(1000);
				}
			}
			catch(Exception e) {}
		}
	}

	private static class Message7 implements Runnable
	{
		private Object lock = new Object();
		private boolean stop = false;
		private Counter keepCount;

		public Message7(Counter start)
		{
			keepCount = start;
			new Thread(this).start();
		}

		public void stop() { this.stop = true; }

		public void run()
		{
			try {
				while(!stop)
				{
					synchronized(lock)
					{
						if(keepCount.count % 7 == 0)
						{
							System.out.println(message7);
						}
						Thread.sleep(1000);
					}
				}
			}
			catch(Exception e) {}
		}
	}

	private static class Message15 implements Runnable
	{
		private Object lock = new Object();
		private boolean stop = false;
		private Counter keepCount;

		public Message15(Counter start)
		{
			keepCount = start;
			new Thread(this).start();
		}

		public void stop() { this.stop = true; }

		public void run()
		{
			try {
				while(!stop)
				{
					synchronized(lock)
					{
						if(keepCount.count % 15 == 0)
						{
							System.out.println(message15);
						}
						Thread.sleep(1000);
					}
				}
			}
			catch(Exception e) {}
		}
	}

	public static void main(String[] args)
	{
		Counter c = new Counter();
		Message7 seven = new Message7(c);
		Message15 fifteen = new Message15(c);
		long start = System.currentTimeMillis();
		System.out.print(c.getCount() + " ");
		while(true)
		{
			if(System.currentTimeMillis() - start > 1000)
			{
				System.out.print(c.getCount());
				start = System.currentTimeMillis();
				System.out.print(" ");
			}
		}
	}
}