import java.lang.reflect.*;
import java.lang.*;

public class Main3
{
	private static class A {
		void foo(int a, boolean b) {}
		int bar(char a, String b, double c) { return 1; }
		static double doo() { return 1.0; }
	}

	static void displayMethodInfo(Object obj)
	{
		Class define = obj.getClass();
		Method[] allMethods = define.getDeclaredMethods();
		for(int i = 0; i < allMethods.length; i++)
		{
			System.out.print(allMethods[i].getName() + " (");
			Class[] allParameters = allMethods[i].getParameterTypes();
			if(!Modifier.isStatic(allMethods[i].getModifiers()))
			{
				System.out.print(define.getName());
				if(allParameters.length != 0)
					System.out.print(", ");
			}
			for(int x = 0; x < allParameters.length; x++)
			{
				System.out.print(allParameters[x].getName());
				if(x != (allParameters.length - 1))
					System.out.print(", ");
			}
			System.out.println(") -> " + allMethods[i].getReturnType().getName());
			allParameters = new Class[0];
		}
	}

	public static void main(String args[])
	{
		try {
			A test = new A();
			displayMethodInfo(test);
		}
		catch(Exception e) {}
	}
}