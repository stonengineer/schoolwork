import java.util.*;
 
class PostBox implements Runnable {
    static final int MAX_SIZE = 10;
    private Object lock = new Object(); //object lock is passed around by future
 										//methods for synchronization.
    class Message {
        String sender;
        String recipient;
        String msg;
        Message(String sender, String recipient, String msg) {
            this.sender = sender;
            this.recipient = recipient;
            this.msg = msg;
        }
    }
 
    private final LinkedList<Message> messages;
    private LinkedList<Message> myMessages;
    private String myId;
    private boolean stop = false;
 
    public PostBox(String myId) {
        messages = new LinkedList<Message>();
        this.myId = myId;
        this.myMessages = new LinkedList<Message>();
        new Thread(this).start();
    }
 
    public PostBox(String myId, PostBox p) {
        this.myId = myId;
        this.messages = p.messages;
        this.myMessages = new LinkedList<Message>();
        new Thread(this).start();
    }
 
    public String getId() { return myId; }
 
    public void stop() {
        // make it so that this Runnable will stop
        this.stop = true;
    }
 
    public void send(String recipient, String msg) {
        // add a message to the shared message queue
	    Message newMessage = new Message(myId, recipient, msg);
	    synchronized(lock) //lock in order to safely modify messages
	    {
	    	this.messages.addFirst(newMessage);
	    }
    }
 
    public List<String> retrieve() {
        // return the contents of myMessages
        // and empty myMessages
        List<String> result = new LinkedList<String>();
        synchronized(lock) //lock in order to safely read from myMessages
        {
	        for(int i = 0; i < this.myMessages.size(); i++)
	        {
	        	Message temp = this.myMessages.get(i);
	        	result.add("From " + temp.sender + " to " + temp.recipient + ": " + temp.msg);
	        	this.myMessages.removeLast();
	        }
	        return result;
	    }
    }
 
    public void run() {
        // loop forever
        //   1. approximately once every second move all messages
        //      addressed to this post box from the shared message
        //      queue to the private myMessages queue
        //   2. also approximately once every second, if the message
        //      queue has more than MAX_SIZE messages, delete oldest messages
        //      so that size is at most MAX_SIZE
	    try {
	    	while(!stop)
	    	{
	    		synchronized(lock) //lock in order to handle messages and MyMessages
	    		{
	    			ArrayList<Integer> toDelete = new ArrayList<Integer>();
	    			for(int i = 0; i < messages.size(); ++i)
	    			{
	    				if(messages.get(i).recipient.equals(this.getId()))
	    				{
	    					if(this.myMessages.size() == MAX_SIZE)
	    					{
	    						this.myMessages.removeFirst();
	    						this.myMessages.add(messages.get(i));
	    						toDelete.add(i);
	    					}
	    					else
	    					{
	    						this.myMessages.add(messages.get(i));
	    						toDelete.add(i);
	    					}
	    				}
	    			}
	    			for(int i = 0; i < toDelete.size(); ++i)
	    			{
	    				int index = toDelete.get(toDelete.size() - (2*i) - 1);
	    				this.messages.remove(index);
	    			}
	    		}
	    		Thread.sleep(1000);
	    	}
	    }
	    catch(Exception e) {}
    }
}