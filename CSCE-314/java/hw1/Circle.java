/* Joseph Stone's implementation *
 * of the Circle class.          */

import java.lang.Math;

public class Circle extends Shape
{
	Point center;
	double radius;

	public Circle(Point c, double r)
	{
		center = c;
		radius = r;
	}

	public Circle(double x, double y, double r)
	{
		center = new Point(x, y);
		radius = r;
	}

	public Point position() {return center;}

	public double area()
	{
		return Math.PI * Math.pow(radius, 2);
	}

	public boolean equals(Object o)
	{
		if(o instanceof Circle)
		{
			Circle test = (Circle)o;
			return (center.equals(test.center)) && (radius == test.radius);
		}
		return false;
	}

	public int hashCode()
	{
		int hash = 19;
		hash = 31 * hash + (int)(Double.doubleToLongBits(radius)^(Double.doubleToLongBits(radius) >>> 32));
		hash = 31 * hash + center.hashCode();
		return hash;
	}

	public int compareTo(Object o)
	{
		Shape s = (Shape)o;
		return (int)(this.area() - s.area());
	}

	public String toString()
	{
		return "Circle (" + center.x + ", " + center.y + "), radius=" + radius;
	}
}