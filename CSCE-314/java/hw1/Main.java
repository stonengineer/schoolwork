/* Joseph Stone's implementation *
 * of the Main class.            */

import java.util.*;

public class Main
{
	public static void main(String[] args)
	{
		Shape shapes[] = new Shape[0];
		Point points[] = new Point[0];
		for(int i = 0; i < args.length; i++)
		{
			Scanner reader = new Scanner(args[i]);
			char type = args[i].charAt(0);
			if(isDigit(type))
			{
				Point temp[] = new Point[points.length+1];
				for(int x = 0; x < points.length; x++)
					temp[x] = points[x];
				double x = reader.nextDouble();
				double y = reader.nextDouble();
				Point p = new Point(x, y);
				temp[points.length] = p;
				points = temp;
			}
			else
			{
				Shape temp[] = new Shape[shapes.length+1];
				for(int x = 0; x < shapes.length; x++)
					temp[x] = shapes[x];
				reader.next();
				switch(type)
				{
					case 'c':
						double x = reader.nextDouble();
						double y = reader.nextDouble();
						double r = reader.nextDouble();
						Circle c = new Circle(new Point(x, y), r);
						temp[shapes.length] = c;
						break;
					case 't':
						double xt1 = reader.nextDouble();
						double yt1 = reader.nextDouble();
						double xt2 = reader.nextDouble();
						double yt2 = reader.nextDouble();
						double xt3 = reader.nextDouble();
						double yt3 = reader.nextDouble();
						Triangle t = new Triangle(new Point(xt1, yt1), new Point(xt2, yt2), new Point(xt3, yt3));
						temp[shapes.length] = t;
						break;
					case 'r':
						double x1 = reader.nextDouble();
						double y1 = reader.nextDouble();
						double x2 = reader.nextDouble();
						double y2 = reader.nextDouble();
						Rectangle rec = new Rectangle(new Point(x1, y1), new Point(x2, y2));
						temp[shapes.length] = rec;
						break;
					default:
						break;
				}
				shapes = temp;
			}
		}

		System.out.println("\n********************************************");
		System.out.println("** Testing the Shapes and Generic methods **");
		System.out.println("********************************************\n");

		//Testing the AreaCalculator class
		System.out.printf("The total area for the %d objects is %1.2f units squared.\n", shapes.length, AreaCalculator.calculate(shapes));
		
		System.out.println();

		//Testing the points
		for(int i = 0; i < points.length; i++)
		{
			if(i == 0) { System.out.println("The user entered the following points:"); }
			System.out.println("(" + points[i].x + "," + points[i].y + ")");
		}

		System.out.println();

		//Comparing shapes for equality
		Point p1 = new Point(-1.0, 2.0);
		Point p2 = new Point(-1.0, 2.0);
		Point p3 = new Point(2.0, 4.0);
		Point p4 = new Point(3.5, 2.7);
		Point p5 = new Point(-10.0, 9.6);
		Triangle t1 = new Triangle(p1, p3, p4);
		Triangle t2 = new Triangle(p2, p3, p4);
		Triangle t3 = new Triangle(p3, p4, p5);
		Rectangle r1 = new Rectangle(p1, p3);
		Rectangle r2 = new Rectangle(p2, p3);
		Rectangle r3 = new Rectangle(p4, p5);
		Circle c1 = new Circle(p1, 10.0);
		Circle c2 = new Circle(p2, 10);
		Circle c3 = new Circle(p4, 5.0);
		System.out.println("Testing derived shapes equals and hash methods...");
		System.out.println("T1 should equal T2. T1 hashCode = " + t1.hashCode() + ", T2 hashCode = " + t2.hashCode() + ". So, T1 = T2 is " + t1.equals(t2));
		System.out.println("T2 should not equal T3. T2 hashCode = " + t2.hashCode() + ", T3 hashCode = " + t3.hashCode() + ". So, T2 = T3 is " + t2.equals(t3));
		System.out.println("R1 should equal R2. R1 hashCode = " + r1.hashCode() + ", R2 hashCode = " + r2.hashCode() + ". So, R1 = R2 is " + r1.equals(r2));
		System.out.println("R2 should not equal R3. R2 hashCode = " + r2.hashCode() + ", R3 hashCode = " + r3.hashCode() + ". So, R2 = R3 is " + r2.equals(r3));
		System.out.println("C1 should equal C2. C1 hashCode = " + c1.hashCode() + ", C2 hashCode = " + c2.hashCode() + ". So, C1 = C2 is " + c1.equals(c2));
		System.out.println("C2 should not equal C3. C2 hashCode = " + c2.hashCode() + ", C3 hashCode = " + c3.hashCode() + ". So, C2 = C3 is " + c2.equals(c3));

		System.out.println();

		//Outputting sorted shapes
		System.out.println("Sorting inputted shapes by area...");
		Arrays.sort(shapes);
		int count = 0;
		for(Shape s : shapes)
		{
			String statement = "" + ++count + ") " + s;
			System.out.printf("%-60s area=", statement);
			System.out.print(s.area() + "\n");
		}

		System.out.println();

		//Testing Node class with max area
		Node<Shape> headA;
		if(shapes.length > 2)
		{
			Node<Shape> next = new Node<Shape>(shapes[1], null);
			headA = new Node<Shape>(shapes[0], next);
			for(int i = 2; i < shapes.length; i++)
			{
				Node<Shape> temp = new Node<Shape>(shapes[i], null);
				next.next = temp;
				next = temp;
			}
		}
		else
		{
			if(shapes.length > 1)
			{
				headA = new Node<Shape>(shapes[0], new Node<Shape>(shapes[1], null));
			}
			else
				headA = new Node<Shape>(shapes[0], null);
		}
		System.out.println("Maximum area: " + maxArea(headA));

		System.out.println();

		//Testing Node class with bounding rectangle
		Rectangle r11 = new Rectangle(new Point(-3,5), new Point(-1,2));
		Rectangle r12 = new Rectangle(new Point(4,4), new Point(7,2));
		Rectangle r13 = new Rectangle(new Point(1,-1), new Point(3,-3));
		Node<Rectangle> last = new Node<Rectangle>(r13, null);
		Node<Rectangle> mid = new Node<Rectangle>(r12, last);
		Node<Rectangle> headB = new Node<Rectangle>(r11, mid);
		Rectangle bound = boundingRect(headB);
		System.out.println("Bounding Rectangle test with the following rectangles:");
		for(Rectangle e : headB)
			System.out.println(e);
		System.out.println("Bounding rectangle: " + bound);

		//Testing ShapeList
		System.out.println("This is the ShapeList class testing method.");
		Circle c11 = new Circle(1,1,1);
        Circle d1 = new Circle(2,2,2);
        Circle e1 = new Circle(3,3,3);
        ShapeList<Shape> emptyShapes = new ShapeList<Shape>();
        ShapeList<Circle> someCircles = new ShapeList<Circle>(Arrays.asList(c11, d1, e1));
        System.out.println("emptyShapes = " + emptyShapes);
        System.out.println("reversed emptyShapes = " + emptyShapes.reverse());
        System.out.println("someCircles = " + someCircles);
        System.out.println("reversed someCircles = " + someCircles.reverse());
        double sumOfXs = 0.0;
        double sumOfYs = 0.0;
        for (Circle c : someCircles) 
        {
            sumOfXs += c.position().x;
            sumOfYs += c.position().y;
        }
        System.out.println("Sum of Xs = " + sumOfXs);
        System.out.println("Sum of Ys = " + sumOfYs);
	}

	private static boolean isDigit(char c)
	{
		return (c >= '0') && (c <= '9');
	}

	private static Shape maxArea(Node<Shape> list)
	{
		double largest = list.v.area();
		Shape winner = list.v;
		for(Shape e : list)
		{
			double temp = e.area();
			if(temp > largest) 
			{
				winner = e;
				largest = temp;
			}
		}
		return winner;
	}

	private static Rectangle boundingRect(Node<Rectangle> list)
	{
		double leftMost = list.v.t_left.x;
		double rightMost = list.v.b_right.x;
		double topMost = list.v.t_left.y;
		double bottomMost = list.v.b_right.y;

		for(Rectangle e : list)
		{
			double l = e.t_left.x;
			double r = e.b_right.x;
			double t = e.t_left.y;
			double b = e.b_right.y;

			if(leftMost > l) { leftMost = l; }
			if(rightMost < r) { rightMost = r; }
			if(topMost < t) { topMost = t; }
			if(bottomMost > b) { bottomMost = b; }
		}

		return new Rectangle(new Point(leftMost, topMost), new Point(rightMost, bottomMost));
	}
}