/* Joseph Stone's implementation *
 * of the Rectangle class.       */

public class Rectangle extends Shape
{
	Point t_left, b_right;

	public Rectangle(Point top_left, Point bottom_right)
	{
		t_left = top_left;
		b_right = bottom_right;
	}

	public Point position()
	{
		double x = (t_left.x + b_right.x)/2;
		double y = (t_left.y + b_right.y)/2;
		return new Point(x, y);
	}

	public double area()
	{
		double l = b_right.x - t_left.x;
		double w = b_right.y - t_left.y;
		return Math.abs(l * w);
	}

	public boolean equals(Object o)
	{
		if(o instanceof Rectangle)
		{
			Rectangle test = (Rectangle)o;
			return (t_left.equals(test.t_left)) && (b_right.equals(test.b_right));
		}
		return false;
	}

	public int hashCode()
	{
		int hash = 17;
		hash = 31 * hash + t_left.hashCode();
		hash = 31 * hash + b_right.hashCode();
		return hash;
	}

	public int compareTo(Object o)
	{
		Shape s = (Shape)o;
		return (int)(this.area() - s.area());
	}

	public String toString()
	{
		return "Rectangle (" + t_left.x + ", " + t_left.y + ") <-> (" + b_right.x + ", " + b_right.y + ")";
	}
}