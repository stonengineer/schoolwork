/* Joseph Stone's implementation *
 * of the Shape List class       */

import java.util.*;

public class ShapeList<T extends Shape> implements Iterable<T>
{
	private Node<T> head;

	public ShapeList()
	{
		head = null;
	}

	public ShapeList(Iterable<T> iterable)
	{
		Node<T> next = null;
		for(T e : iterable)
		{
			if(head == null)
				head = new Node<T>(e, null);
			else
			{
				if(head.next == null)
				{
					next = new Node<T>(e, null);
					head.next = next;
				}
				else
				{
					next.next = new Node<T>(e, null);
					next = next.next;
				}
			}
		}
	}

	public ShapeList<T> reverse()
	{
		if(head == null) { return new ShapeList<T>(); }
		Node<T> curr = head;
		Node<T> rHead = null;
		while(curr != null)
		{
			Node<T> next = curr.next;
			curr.next = rHead;
			rHead = curr;
			curr = next;
		}
		head = rHead;
		return new ShapeList<T>(rHead);
	}

	public String toString()
	{
		if(head == null) { return "[]"; }
		String s = "[";
		for(T e : head)
		{
			s = s + "{" + e + "}";
		}
		s = s + "]";
		return s;
	}

	public Iterator<T> iterator()
	{
		return head.iterator();
	}
}