/* Joseph Stone's implementation *
 * of the Triangle class.        */

import java.lang.Math;

public class Triangle extends Shape
{
	Point top, left, right;

	public Triangle(Point a, Point b, Point c)
	{
		top = a;
		left = b;
		right = c;
	}

	public Point position() 
	{
		double x = (top.x + left.x + right.x)/3;
		double y = (top.y + left.y + right.y)/3;
		return new Point(x, y);
	}

	public double area()
	{
		double a = Math.sqrt(Math.pow((top.x - left.x), 2) + Math.pow((top.y - left.y), 2));
		double b = Math.sqrt(Math.pow((top.x - right.x), 2) + Math.pow((top.y - right.y), 2));
		double c = Math.sqrt(Math.pow((left.x - right.x), 2) + Math.pow((left.y - right.y), 2));
		double s = (a + b + c) / 2;
		return Math.sqrt(s*(s-a)*(s-b)*(s-c));
	}

	public boolean equals(Object o)
	{
		if(o instanceof Triangle)
		{
			Triangle test = (Triangle)o;
			return (top.equals(test.top)) && (left.equals(test.left)) && (right.equals(test.right));
		}
		return false;
	}

	public int hashCode()
	{
		int hash = 13;
		hash = 31 * hash + top.hashCode();
		hash = 31 * hash + left.hashCode();
		hash = 31 * hash + right.hashCode();
		return hash;
	}

	public int compareTo(Object o)
	{
		Shape s = (Shape)o;
		return (int)(this.area() - s.area());
	}

	public String toString()
	{
		return "Triangle (" + top.x + ", " + top.y + ") <-> (" + left.x + ", " + left.y + ") <-> (" + right.x + ", " + right.y + ")";
	}
}