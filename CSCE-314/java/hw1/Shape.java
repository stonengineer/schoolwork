/* Joseph Stone's implementation *
 * of the Shape class.           */

public abstract class Shape implements Comparable
{
	public abstract Point position();
	public abstract double area();
	public abstract boolean equals(Object o);
	public abstract int hashCode();
	public abstract int compareTo(Object o);
	public abstract String toString();
}