/* Joseph Stone's implementation *
 * of the Point class.           */

public class Point
{
	double x;
	double y;
	Point(double x_coord, double y_coord) {x = x_coord; y = y_coord;}
	public void setPosition(double x_coord, double y_coord) {x = x_coord; y = y_coord;}
	public boolean equals(Point p) {return x == p.x && y == p.y;}
	public int hashCode()
	{
		int hash = 7;
		hash = 31 * hash + (int)(Double.doubleToLongBits(this.x)^(Double.doubleToLongBits(this.x) >>> 32));
		hash = 31 * hash + (int)(Double.doubleToLongBits(this.y)^(Double.doubleToLongBits(this.y) >>> 32));
		return hash;
	}
}