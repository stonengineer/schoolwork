/* Joseph Stone's implementation *
 * of the Area Calculator class. */

public class AreaCalculator
{
	public static double calculate(Shape[] shapes)
	{
		double total = 0;
		for(int i = 0; i < shapes.length; i++)
		{
			total += shapes[i].area();
		}
		return total;
	}
}