/* Joseph Stone's implementation *
 * of the Node and Node Iterator */

import java.util.*;

public final class Node<T extends Shape> implements Iterable<T>
{
	private Node<T> head;
	public final T v;
	public Node<T> next;
	public Node (T val, Node<T> link) { v = val; next = link; }
	
	public Iterator<T> iterator()
	{
		return new NodeIterator(this);
	}

	private class NodeIterator implements Iterator<T>
	{
		Node<T> currNode;

		public NodeIterator(Node<T> n)
		{
			currNode = n;
		}

		public T next() 
		{
			if(!hasNext()) { throw new NoSuchElementException(); }
			T data = currNode.v;
			currNode = currNode.next;
			return data;
		}

		public boolean hasNext() 
		{
			return currNode != null;
		}

		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}
}