-- |=======================================\
-- | This is Joseph Stone's homework 3 code
-- | UIN: 422008041, Section: 500
-- | Date: March 1, 2016
-- |=======================================/

-- |=======================================\
-- | Tree Class
-- |-----------------|
data Tree a b = Branch b (Tree a b) (Tree a b)
	| Leaf a
-- |-----------------|
-- | Display the tree
-- |-----------------|
displayTree (Leaf a) b = (replicate b ' ') ++ (show a)
displayTree (Branch b t l) a = (replicate a ' ') ++ show b ++ "\n" ++ displayTree t (a+4) 
	++ "\n" ++ displayTree l (a+4)
instance (Show a, Show b) => Show (Tree a b) where
	show (Branch b t l) = displayTree (Branch b t l) (0)
-- |-----------------|
-- | Traversing the tree
-- |-----------------|
preorder :: (a -> c) -> (b -> c) -> Tree a b -> [c]
preorder x y (Leaf a) = [x a]
preorder x y (Branch b t l) = [y b] ++ preorder x y t ++ preorder x y l
-- |-----------------|
postorder :: (a -> c) -> (b -> c) -> Tree a b -> [c]
postorder x y (Leaf a) = [x a]
postorder x y (Branch b t l) = postorder x y t ++ postorder x y l ++ [y b]
-- |-----------------|
inorder ::(a -> c) -> (b -> c) -> Tree a b -> [c]
inorder x y (Leaf a) = [x a]
inorder x y (Branch b t l) = inorder x y t ++ [y b] ++ inorder x y l
-- |-----------------|
-- |=======================================/

-- |=======================================\
-- | A Tiny Language
-- |-----------------|
data E = IntLit Int
	| BoolLit Bool 
	| Plus E E
	| Minus E E
	| Multiplies E E
	| Exponentiate E E 
	| Equals E E 
	  deriving (Eq, Show)
-- |-----------------|
-- | Function Definitions
-- |-----------------|
(!+!) :: E -> E -> E
(!+!) (IntLit x) (IntLit y) = (IntLit (x+y))
(!-!) :: E -> E -> E
(!-!) (IntLit x) (IntLit y) = (IntLit (x-y))
(!*!) :: E -> E -> E
(!*!) (IntLit x) (IntLit y) = (IntLit (x*y))
(!/!) :: E -> E -> E 
(!/!) (IntLit x) (IntLit y) = (IntLit (x `div` y))
(!^!) :: E -> E -> E
(!^!) (IntLit x) (IntLit y) = (IntLit (x^y))
(!==!) :: E -> E -> E
(!==!) (BoolLit x) (BoolLit y) = (BoolLit (x == y))
(!==!) (IntLit x) (IntLit y) = (BoolLit (x == y))
-- |-----------------|
-- | Evaluations
-- |-----------------|
eval :: E -> E
eval (IntLit x) = IntLit x
eval (BoolLit x) = BoolLit x
eval (Plus x y) = ((!+!) (eval x) (eval y))
eval (Minus x y) = ((!-!) (eval x) (eval y))
eval (Multiplies x y) = ((!*!) (eval x) (eval y))
eval (Exponentiate x y) = ((!^!) (eval x) (eval y))
eval (Equals x y) = ((!==!) (eval x) (eval y))
-- |-----------------|
-- |Transforming Expressions
-- |-----------------|
log2 :: E -> E 
log2 (IntLit 1) = (IntLit 0)
log2 (IntLit 2) = (IntLit 1)
log2 (IntLit x) = ((!+!) (IntLit 1) (log2 ((!/!) (IntLit x) (IntLit 2))))
-- |-----------------|
logGen :: E -> E 
logGen (IntLit x) = 
	if((IntLit x) == ((!^!) (IntLit 2) (log2 $ IntLit x)))
	then (log2 $ IntLit x)
	else (error "Non-exhaustive patterns in function log2Sim")
-- |-----------------|
log2Sim :: E -> E 
log2Sim (IntLit x) = logGen $ (IntLit x)
log2Sim (BoolLit x) = BoolLit x
log2Sim (Plus x y) = ((!+!) (log2Sim x) (log2Sim y))
log2Sim (Minus x y) = ((!-!) (log2Sim x) (log2Sim y))
log2Sim (Multiplies x y) = (Plus (log2Sim x) (log2Sim y))
log2Sim (Exponentiate x y) = (Multiplies x (log2Sim y))
log2Sim (Equals x y) = (Equals (log2Sim x) (log2Sim y))
-- |-----------------|
-- |=======================================/