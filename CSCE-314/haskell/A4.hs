-- Joseph Stone's Homework 4
-- CSCE 314, sec. 500

------------------------- \
-- Import Statements
--------------------
import Parsing
import Data.Char
import System.IO
------------------------- /

--HTML Problem:
----------------------------------------------------------- \
-- test cases
doc0 = "<head></head><body></body>" -- valid
doc1 = " <head> Test1 </head> <body> Testing <p> Test <b> bold </b> text. <ol> <li> test item <li> another one </ol> </body>" -- valid
doc2 = "<head> Test2 <body> Testing </body>" -- invalid
doc3 = "<head> Test3 </head> <body> <a href=\"test.doc\">hyperlink <i> text </a> </i> </body>" -- invalid
doc4 = "<b> <i> <b> blah</b> </i> </b>" -- invalid
doc5 = " <head> ahelaswad </head> <body> babab <b> <i> ada</i> </b> c </body>" -- valid
doc6 = " <head> ahelaswad </head> <body> babab <b> </i> ada</i> </b> c </body>" -- invalid
doc7 = " <head> ahelaswad </head> <body> babab <b> <b> ada</b> </b> c </body>" -- invalid
doc8 = " <head> ahelaswad </head> <body> babab <a href=\"\"> </a> </body>" -- valid
doc9 = " <head> ahelaswad </head> <body> babab <a href=\"hello.txt\"> </a> </body>" -- valid
doc10 = " <head> ahelaswad </head> <body> babab <a href=\"hello.txt\"> <b> bold </b> </a> </body>" -- valid
doc11 = " <head> ahelaswad </head> <body> babab <b> <a href=\"hello.txt\"> <b> bold </b> </a> </b> </body>" -- invalid
doc12 = " <head> ahelaswad </head> <body> babab <i> <a href=\"hello.txt\"> <b> bold </b> </a> </i> </body>" -- valid
doc13 = " <head> ahelaswad </head> <body> babab <li> </body>" -- invalid
doc14 = " <head> ahelaswad </head> <body> <b> hel </b> <a href=\"hello\"> hell </a> </body>" -- valid
doc15 = " <head> ahelaswad </head> <body> babab <ul><li> </ul> </body>" -- valid
doc16 = " <head> ahelaswad </head> <body> babab <ul><li> </ol> </body>" -- invalid
doc17 = " <head> ahelaswad </head> <body> babab <ul><li> <li> </ul> </body>" -- valid
doc18 = " <head> ahelaswad </head> <body> babab <ul><li> <li> </ul> <li> </body>" -- invalid
doc19 = " <head> ahelaswad </head> <body> babab <ul><li> <li> </ul> <ol><li></ol> </body>" -- valid

html = do
    head <- head_statement
    end <- end_of_time
    if end
    then do return head
    else do 
         if head
         then body_statement
         else do return head

text = many (sat (/= '<')) >> return []
tend = many (sat (/= '>')) >> return []
tend_ret = many (sat (/= '>')) >>= \input -> return input
space1 = many1 (sat (== ' ')) >> return []

open s = symbol $ "<" ++ s ++ ">"
close s = symbol $ "</" ++ s ++ ">"

end_of_time = do
    space1
    return True
    +++ return False

head_statement = do
    open "head"
    text
    close "head"
    return True
    +++ return False

body_statement = do
    space
    open "body"
    text
    verify_tag
    +++ return False

verify_tag = do
    space
    tag <- tend_ret
    char '>'
    case tag of
        "</body" -> return True
        "<p" -> reset_stream
        "<br" -> reset_stream
        "<b" -> close_tag "b"
        "<i" -> close_tag "i"
        "<ol" -> list_tag "ol"
        "<ul" -> list_tag "ul"
        _ -> unhandled_tag tag

reset_stream = do
    text
    verify_tag
    +++ return False

close_tag xs = do
    text
    close xs
    verify_tag
    +++ return False

list_tag xs = do
    text
    tag <- tend_ret
    char '>'
    case tag of
        "<li" -> reset_tag xs
        "</ol" -> verify_list xs "ol"
        "</ul" -> verify_list xs "ul"
        _ -> return False

reset_tag xs = list_tag xs

verify_list xs ys
    | (xs == ys) = verify_tag
    | otherwise = return True

unhandled_tag tag = do
    let xs = (take 2 tag)
    case xs of
        "<a" -> close_tag "a"
        _ -> return False
----------------------------------------------------------- /

--IO Problem:
----------------------------------------------------------- \
-- Main Method
--------------
evalpoly :: IO ()
evalpoly = do
    putStr "What is the degree of the polynomial: "
    d <- getInt
    trash <- getLine
    evaluator (coefQuery (digitToInt d))
    putStr ""
--------------------------------------- /

-------------------------- \
-- Returns number as char
-------------------------
getInt :: IO Char
getInt = do
    x <- getChar
    if isDigit x
    then return x
    else return '0'
-------------------------- /

---------------------------------- \
-- Generates list of coefficients
---------------------------------
coefQuery :: Num a => Int -> IO [Double]
coefQuery d = grab [] d

grab :: [Double] -> Int -> IO [Double]
grab coefficients d = do
    if d >= 0
    then do
        putStr "What is the x^"
        putStr (show d)
        putStr " coefficient: "
        hFlush stdout
        num <- getDouble
        grab (coefficients ++ ((read num :: Double) : [])) (d-1)
    else return coefficients

getDouble :: IO String
getDouble = do
    xs <- getLine
    if (isDouble (testStr xs))
    then return xs
    else error("Invalid input.")

isDouble :: [Bool] -> Bool
isDouble [] = False
isDouble xs = ((length xs > 1) && not (elem False xs))

testStr :: String -> [Bool]
testStr "" = True : []
testStr (x:xs) = (isDigit x || x == '.' || x == '-' || x == ' ') : testStr xs
---------------------------------- /

---------------------------------- \
-- Evaluates function
---------------------
evaluator :: IO [Double] -> IO ()
evaluator ds = do
    xs <- ds
    putStr "What value do you want to evaluate at: "
    v <- getDouble
    putStr "The value of "
    eq <- displayFunction "" ((length xs)-1) xs
    putStr eq
    putStr " evaluated at "
    putStr (show v)
    putStr " is "
    putStr (show (calculate xs (generateX (read v :: Double) ((length xs)-1))))
    putStr "\n"

generateX :: Double -> Int -> [Double]
generateX _ 0 = 1 : []
generateX v n = (power v n) : generateX v (n-1)

power :: Num a => a -> Int -> a
power _ 0 = 1
power a n = a * power a (n-1)

calculate :: [Double] -> [Double] -> Double
calculate xs ys = sum (products xs ys)

products :: [Double] -> [Double] -> [Double]
products [] _ = []
products _ [] = []
products (x:xs) (y:ys) = (x*y) : products xs ys
---------------------------------- /

--------------------------------------- \
-- Displays function aestetically
---------------------------------
displayFunction :: Num a => String -> Int -> [Double] -> IO String
displayFunction equation d [] = return equation
displayFunction equation d (x:xs) = do
    if (d > 0 && x /= 0)
    then do
        if x == 0
        then do displayFunction equation (d-1) xs
        else do
            if (x > 0 && ((length equation) == 0)) || x < 0
            then do displayFunction (equation ++ (spacing x) ++ " x^" ++ (show d)) (d-1) xs
            else do displayFunction (equation ++ " + " ++ (spacing x) ++ " x^" ++ (show d)) (d-1) xs
    else do
        if (d == 0 || x == 0)
        then do 
            if x == 0
            then do displayFunction equation (d-1) xs
            else do
                if (x > 0 && ((length equation) == 0)) || x < 0
                then do displayFunction (equation ++ (spacing x)) (d-1) xs
                else do displayFunction (equation ++ " + " ++ (spacing x)) (d-1) xs
        else return equation

spacing :: Num a => Double -> String
spacing x = do
    if x < 0
    then (" - " ++ (show (x * (-1.0))))
    else (show x)
--------------------------------------- /
----------------------------------------------------------- /