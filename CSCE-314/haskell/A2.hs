--This is Joseph Stone's A2 Code
--UIN: 422008041
--Section: 500

{------------------------------------------------------------------------}
--Chinese Remainder Theorem
maxNum :: [(Integer, Integer)] -> Integer
maxNum [] = 1
maxNum (x:xs) = snd(x) * maxNum xs
denominatorList :: Integer -> Integer -> Integer -> Integer -> [Integer]
denominatorList a n x l
     | (x >= l) = []
     | (a == (x `mod` n)) = x : denominatorList a n (x+1) l
     | otherwise = denominatorList a n (x+1) l 
bigList ::[(Integer, Integer)] -> Integer -> [Integer]
bigList [] _ = []
bigList (x:xs) l = (denominatorList (fst(x)) (snd(x)) 0 l) ++ bigList xs l
occurs :: [Integer] -> Integer -> Int
occurs [] _ = 0
occurs (x:xs) a 
     | (x == a) = 1 + occurs xs a 
     | otherwise = occurs xs a 
reduceList :: [Integer] -> Int -> [Integer]
reduceList [] _ = []
reduceList (x:xs) l 
     | ((occurs xs x) == l) = x : reduceList xs l
     | otherwise = reduceList xs l
crt :: [(Integer, Integer)] -> (Integer, Integer)
crt xs = ((reduceList (bigList xs (maxNum xs)) ((length xs)-1))!!0, (maxNum xs))
{-----------------------------------}
factors :: Int -> Int -> [Int]
factors x n
     | (n == 0) = 0 : factors x (n+1)
     | (x == n) = n : []
     | (x `mod` n == 0) = n : factors x (n+1)
     | otherwise = factors x (n+1)
generate :: Fractional a => Int -> Int -> [Int]
generate x n =
     if(length(factors n 1) == (x*2)) 
     then n : (generate x (n+1))
     else generate x (n+1)
kcomposite :: Integral t => Int -> [Int]
kcomposite x = generate x 1
{------------------------------------}
makeStr2Comp :: String -> String
makeStr2Comp [] = []
makeStr2Comp xs
     | ((length xs) `elem` (take (length xs) $ kcomposite 2)) = xs ++ []
     | otherwise = makeStr2Comp (xs ++ "X")
block :: String -> Int -> Int -> String
block [] a b = []
block xs a b
     | (a > b) = []
     | otherwise = (xs !! a) : block xs (a+1) b
blocker :: String -> Int -> Int -> [String]
blocker xs a b
     | (a >= (length xs)) = []
     | otherwise = block xs a ((((factors (length xs) 1)!!2)*b)-1) : blocker xs (((factors (length xs) 1)!!2)*b) (b+1)
customZip :: [[Char]] -> [Char]
customZip xs
     | (xs == [[]]) = []
     | otherwise = map head xs ++ customZip(map tail xs)
anagramEncode :: [Char] -> [Char]
anagramEncode xs = customZip (blocker(makeStr2Comp xs) 0 1)
{------------------------------------}
reverseBlock :: [Char] -> Int -> Int -> [Char]
reverseBlock xs a b
     | (a >= (length xs)) = []
     | otherwise = xs!!a : reverseBlock xs (a+b) b
reverseBlocker :: [Char] -> Int -> [Char]
reverseBlocker xs a
     | (a >= ((factors (length xs) 1)!!1)) = []
     | otherwise = (reverseBlock xs a ((factors (length xs) 1)!!1)) ++ (reverseBlocker xs (a+1))
xRemover :: [Char] -> [Char]
xRemover (x:xs)
     | (x == 'X') = xRemover(xs)
     | otherwise = xs
anagramDecode :: [Char] -> [Char]
anagramDecode xs = reverse (xRemover (reverse ((reverseBlocker xs 0))))
{-----------------------------------}
type Set a = [a]
{-----------------------------------}
mkSet :: Eq a => [a] -> Set a
mkSet [] = []
mkSet (x:xs)
     | (x `elem` xs) = mkSet xs
     | otherwise = x : (mkSet xs)
{-----------------------------------}
numMatches :: Eq a => Set a -> Set a -> Int
numMatches _ [] = 0
numMatches [] _ = 0
numMatches (x:xs) bs
     | (x `elem` bs) = 1 + (numMatches xs bs)
     | otherwise = 0 + (numMatches xs bs)
subset :: Eq a => Set a -> Set a -> Bool
subset xs bs = ((length xs) == (numMatches xs bs))
{-----------------------------------}
setEqual :: Eq a => Set a -> Set a -> Bool
setEqual xs bs = ((subset xs bs) && (subset bs xs))
{-----------------------------------}
prod :: t -> [t1] -> [(t, t1)]
prod _ [] = []
prod x (b:bs) = (x, b) : (prod x bs)
setProd :: (Eq t, Eq t1) => [t] -> [t1] -> Set (t, t1)
setProd _ [] = []
setProd [] _ = []
setProd (x:xs) bs = prod x bs ++ setProd xs bs
{-----------------------------------}
subsequences xs =  [] : potentialSubs xs
potentialSubs :: [a] -> [[a]]
potentialSubs [] =  []
potentialSubs (x:xs) =  [x] : foldr f [] (potentialSubs xs)
     where f ys r = ys : (x : ys) : r
connectSets [] [] = [[]]
connectSets _ [] = [[]]
connectSets [] _ = [[]]
connectSets (x:xs) (y:ys) 
     | (length x) < (length y) = [[x,y]] ++ (connectSets xs ys)
     | otherwise = [[y,x]] ++ (connectSets xs ys)
mapper xs [] = [[]]
mapper xs (y:ys) = [complement xs y] ++ (mapper xs ys)
complement :: Eq a => Set a -> Set a -> Set a
complement [] ys = []
complement (x:xs) (ys)     
     | elem x ys = (complement xs ys)
     | otherwise = (x : complement xs ys)
splitter [] = [[]]
splitter (x:xs) = [[x]] ++ (splitter xs)
halfer [] = []
halfer (x:_:xs) = x : (halfer xs)
halfer (x:xs) = []
partitionBuilder (xs) = halfer (connectSets (subsequences xs) (mapper (xs) (subsequences xs)))
partitionSet :: Eq t => Set t -> Set( Set (Set t))
partitionSet xs = ((take ((length (partitionBuilder xs))) (partitionBuilder xs))++[(splitter xs)])
{-----------------------------------}
bellNum :: Int -> Int
bellNum 0 = 1
bellNum 1 = 1
bellNum x = length (partitionSet [1..x])
{-----------------------------------}
{------------------------------------------------------------------------}