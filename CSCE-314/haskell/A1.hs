--This is Joseph Stone's A1 program.
--Section: 500
--UIN: 422008041

--Basic Drills------------------------------------------------------------------------------------\
{-------------------------------------------------------------------------------------------------}
{-Imports-------------------------}
import Data.Char
{---------------------------------}

--Problem 1:
{---------------------------------}
increaseTen :: Num a => a -> a
increaseTen x = x + 10
{---------------------------------}

--Problem 2:
{---------------------------------}
circleArea :: Fractional a => a -> a
circleArea x = 3.14 * (x * x)
{---------------------------------}

--Problem 3:
{---------------------------------}
midList :: Eq a => [a] -> [a]
midList xs = tail(reverse(tail(reverse(xs))))
{---------------------------------}

--Problem 4:
{---------------------------------}
countdownList :: Num a => Integer -> Integer -> [Integer]
countdownList a b = reverse([x | x <- [a..b]])
{---------------------------------}

--Problem 5:
{---------------------------------}
isRight :: Num a => Float -> Float -> Float -> Bool
isRight a b c = ((c*c) == ((a*a) + (b*b)))
{---------------------------------}

--Problem 6:
{---------------------------------}
multComplex :: Fractional a => ((Float, Float), (Float, Float)) -> (Float, Float)
multComplex ((r1, i1), (r2, i2)) = ((r1*r2), (i1*i2))
{---------------------------------}

--Problem 7:
{---------------------------------}
countChar :: Num a => Char -> String -> Integer
countChar c (x:xs)
    | (xs == []) = 0
    | (c == x) = 1 + countChar c xs
    | (c == (toLower(x))) = 1 + countChar c xs
    | otherwise = 0 + countChar c xs
{---------------------------------}

--Problem 8:
{---------------------------------}
getFirsts :: Num a => [(f,s)] -> [f]
getFirsts [] = []
getFirsts (x:xs) = fst(x) : getFirsts(xs)
{---------------------------------}

--Problem 9:
{---------------------------------}
halfList :: Eq a => [a] -> [a]
halfList [] = []
halfList (x:xs)
    | (((length(xs)) `mod` 2) /= 0) = x : halfList(xs)
    | otherwise = halfList(xs)
{---------------------------------}

--Problem 10:
{---------------------------------}
uppercaseList :: Num a => [Char] -> [(Bool, Bool, Bool)]
uppercaseList [] = []
uppercaseList (x:xs)
    | isUpper(x) = (True, False, False) : uppercaseList(xs)
    | isLower(x) = (False, True, False) : uppercaseList(xs)
    | isDigit(x) = (False, False, True) : uppercaseList(xs)
    | otherwise = (False, False, False) : uppercaseList(xs)
{---------------------------------}
{-------------------------------------------------------------------------------------------------}
--End of Basic Drills-----------------------------------------------------------------------------/

--Haskell Problems--------------------------------------------------------------------------------\
{-------------------------------------------------------------------------------------------------}
--Problem 1:
{---------------------------------}
altSeries :: Num a => [a] -> a
altSeries xs = case (xs) of
    [] -> 0
    xs -> head(xs) + altSeries(tail(xs))
{---------------------------------}

--Problem 2:
{---------------------------------}
worker :: [Char] -> [Char] -> [Char]
worker [] duplicates = []
worker (x:xs) duplicates
    | x `elem` duplicates = '_' : worker xs duplicates
    | otherwise = x : (worker xs (x : duplicates))
markDups :: [Char] -> [Char]
markDups [] = []
markDups xs = worker xs []
{---------------------------------}

--Problem 3:
{---------------------------------}
back :: String -> String
back [] = []
back (x:xs)
    | (x == 'v') = 'm' : back(xs)
    | otherwise = 'v' : back(xs)
fold' :: Int -> String
fold' x 
    | (x == 1) = 'v' : []
    | otherwise = fold'((x-1)) ++ "v" ++ back(reverse(fold'((x-1))))
fold :: Int -> String
fold x = reverse(fold'(x))
{---------------------------------}

--Problem 4:
{---------------------------------}
leftSide' :: (Fractional a) => Int -> [Int] --takes first integer, then returns the left hand list of numbers
leftSide' a
    | (a == 0) = []
    | otherwise = a : leftSide'(a `div` 2)
rightSide' :: (Num a) => ([Int], Int) -> [Int] --using the values of the left side, calculates a list of doubled elements, discounting the evens on the left side
rightSide' ((x:xs), a)
    | (xs == []) = a : []
    | (x `mod` 2 /= 0) = a : rightSide'((xs, a*2))
    | otherwise = rightSide'((xs, a*2))
myMult :: (Integral a) => Int -> (Int -> Int) --calculates the product of two numbers
myMult l r = sum(rightSide'(leftSide'(l),r))
{---------------------------------}
{-------------------------------------------------------------------------------------------------}
--End Of Haskell Problems-------------------------------------------------------------------------/