/*  
 *  OpenAquarium sensor platform for Arduino from Cooking-hacks.
 *  
 *  Copyright (C) Libelium Comunicaciones Distribuidas S.L. 
 *  http://www.libelium.com 
 *  
 *  This program is free software: you can redistribute it and/or modify 
 *  it under the terms of the GNU General Public License as published by 
 *  the Free Software Foundation, either version 3 of the License, or 
 *  (at your option) any later version. 
 *  a
 *  This program is distributed in the hope that it will be useful, 
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License 
 *  along with this program.  If not, see http://www.gnu.org/licenses/. 
 *  
 *  Version:           1.0
 *  Design:            David Gascón 
 *  Implementation:    Marcos Martinez, Luis Martin & Jorge Casanova
 */
#include "OpenAquarium.h"
#include "Wire.h"
float temperature;
int pHArray[ArrayLenth];   //Store the average value of the sensor feedback
int pHArrayIndex=0;    
int status=0;
DateTime now; 

void setup() {
  OpenAquarium.init();   //Initialize
  pinMode(LED,OUTPUT); 
  Serial.begin(115200);
  OpenAquarium.initRTC(); 
  OpenAquarium.setTime(); 
}

void loop() {
  now = OpenAquarium.getTime(); //Get date and time
  Serial.print("Date: ");
  OpenAquarium.printTime(now); 
  
  Serial.print("Water Level: ");
  OpenAquarium.waterlevel1(); 
  
  temperature = OpenAquarium.readtemperature(); //Read the sensor
  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.println("'C");
  
  bool i = true;
  while(i == true){
    static unsigned long samplingTime = millis();
    static unsigned long printTime = millis();
    static float pHValue,ph_voltage;
    if(millis()-samplingTime > samplingInterval)
    {
        pHArray[pHArrayIndex++]=analogRead(SensorPin);
        if(pHArrayIndex==ArrayLenth)pHArrayIndex=0;
        ph_voltage = avergearray(pHArray, ArrayLenth)*5.0/1024;
        pHValue = 3.5*ph_voltage+Offset;
        samplingTime=millis();
    }
    if(millis() - printTime > printInterval)   //Every 800 milliseconds, print a numerical, convert the state of the LED indicator
    {
        Serial.print("Voltage:");
        Serial.print(ph_voltage,2);
        Serial.print("    pH value: ");
        Serial.println(pHValue,2);
        digitalWrite(LED,digitalRead(LED)^1);
        printTime=millis();
        i = false;
    }
    
  }
  //Serial.println(""); 
  delay(720000); 
}  
double avergearray(int* arr, int number){
  int i;
  int max,min;
  double avg;
  long amount=0;
  if(number<=0){
    Serial.println("Error number for the array to avraging!/n");
    return 0;
  }
  if(number<5){   //less than 5, calculated directly statistics
    for(i=0;i<number;i++){
      amount+=arr[i];
    }
    avg = amount/number;
    return avg;
  }else{
    if(arr[0]<arr[1]){
      min = arr[0];max=arr[1];
    }
    else{
      min=arr[1];max=arr[0];
    }
    for(i=2;i<number;i++){
      if(arr[i]<min){
        amount+=min;        //arr<min
        min=arr[i];
      }else {
        if(arr[i]>max){
          amount+=max;    //arr>max
          max=arr[i];
        }else{
          amount+=arr[i]; //min<=arr<=max
        }
      }//if
    }//for
    avg = (double)amount/(number-2);
  }//if
  return avg;
}
