import time
import serial
serialport = serial.Serial("/dev/ttyACM0",115200)
c = 0
while True:
    filename = 'Documents/data/%d.txt' % c
    file = open(filename, "w")
    command = serialport.readline()
    file.write(command)
    command = serialport.readline()
    file.write(command)
    command = serialport.readline()
    file.write(command)
    command = serialport.readline()
    file.write(command)
    file.close()
    c = c + 1
    # should be slower than sensor time
    # otherwise print blank lines 
    time.sleep(900)

