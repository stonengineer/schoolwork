import picamera # camera library code
from time import sleep # sleep function
import os # handle scp command
import sys # format console output

host = "pi@controller.local"
path = "Documents/pictures/"

camera = picamera.PiCamera() # init camera

cam_ctrl = 'cam-three' # modify for each camera
cam_pic_index = 0 # always start at zero

while True: #loop forever
    cam_pic_index = cam_pic_index + 1 # increment pic number
    
    # build image name. chose 8 digits, will give enough unique names to
    # take an image every 5 minutes for approx 950 years
    image_name = cam_ctrl + '_' + (str(cam_pic_index).zfill(8)) + '.jpg'
    
    camera.capture(image_name) # take the picture
    
    #upload the picture back to rpi3 controller
    upload = 'scp "%s" "%s:%s"' % (image_name, host, path)
    # print 'Running: %s' % upload
    os.system(upload)
    delete = 'rm %s' % image_name
    # print 'Running: %s' % delete
    os.system(delete)
    
    count = 0
    # print '\r>> %d seconds have passed.' % count,
    sys.stdout.flush()
    while count < 7200:
        count = count + 1
        sleep(1)
        # print '\r>> %d seconds have passed.' % count,
        sys.stdout.flush()
    # print '\r\n'
        
