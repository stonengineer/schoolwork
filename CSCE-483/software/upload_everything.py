from subprocess import call
import glob
import os
import time

while True:
	all_pictures = glob.glob("/home/pi/Documents/pictures/*.jpg")
	last_pic_num = "0"
	for line in all_pictures:
            last_pic_num = line
	    name = os.path.basename(line)
	    command = ("/home/pi/Dropbox-Uploader/dropbox_uploader.sh upload %s /pictures/%s" % (line,name))
	    call ([command], shell=True)
	all_documents = glob.glob("/home/pi/Documents/data/*.txt")
	last_doc_num = "0"
	for line in all_documents:
            last_doc_num = line
	    name = os.path.basename(line)
	    command = ("/home/pi/Dropbox-Uploader/dropbox_uploader.sh upload %s /data/%s" % (line,name))
	    call ([command], shell=True)
	command = ("echo \"%s %s\" > meta.txt" % (last_pic_num, last_doc_num))
	call ([command], shell=True)
	upload_meta = ("/home/pi/Dropbox-Uploader/dropbox_uploader.sh upload /home/pi/meta.txt /meta.txt")
	call ([upload_meta], shell=True)
	print "done uploading."
	remove_meta = ("rm -f /home/pi/meta.txt")
	call ([remove_meta], shell=True)
	remove_pics = ("rm -f /home/pi/Documents/pictures/*.jpg")
	call ([remove_pics], shell=True)
	remove_docs = ("rm -f /home/pi/Documents/data/*.txt")
	call ([remove_docs], shell=True)
	print "done removing"
	time.sleep(14400) # sleep for 4 hours


