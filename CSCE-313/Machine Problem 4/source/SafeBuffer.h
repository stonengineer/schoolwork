//
//  SafeBuffer.h
//  
//
//  Created by Joshua Higginbotham on 11/4/15.
//
//

#ifndef SafeBuffer_h
#define SafeBuffer_h

#include <stdio.h>
#include <queue>
#include <string>
#include <pthread.h>
#include <mutex>
#include <condition_variable>

class semaphore {
private:
    std::mutex mutex;
    std::condition_variable condition;
    unsigned long count = 0;

public:
    void V() {
        std::unique_lock<decltype(mutex)> lock(mutex);
        ++count;
        condition.notify_one();
    }

    void P() {
        std::unique_lock<decltype(mutex)> lock(mutex);
        while(!count)
            condition.wait(lock);
        --count;
    }
	
	void set_count(unsigned long num)
	{
		count = num;
	}
};

class SafeBuffer {
	/*
		Only two data members are really necessary for this class:
		a mutex, and a data structure to hold elements. Recall
		that SafeBuffer is supposed to be FIFO, so something
		like std::vector will adversely affect performance if
		used here. We recommend something like std::list
		or std::queue, because std::vector is very inefficient when
		being modified from the front.
	*/
private:
	std::queue<std::string> buffer;
	semaphore full_slots;
	semaphore mutex;
	
public:
    SafeBuffer();
	~SafeBuffer();
	int size();
    void push_back(std::string str);
    std::string retrieve_front();
};

#endif /* SafeBuffer_ */
