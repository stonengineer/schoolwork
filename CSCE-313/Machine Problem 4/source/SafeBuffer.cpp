//
//  SafeBuffer.cpp
//
//
//  Created by Joshua Higginbotham on 11/4/15.
//
//

#include "SafeBuffer.h"
#include <string>
#include <queue>

SafeBuffer::SafeBuffer() {
	full_slots.set_count(0);
	mutex.set_count(1);
}

SafeBuffer::~SafeBuffer() {
	//delete &buffer;
	//delete &full_slots;
	//delete &mutex;
}

int SafeBuffer::size() {
    return buffer.size();
}

void SafeBuffer::push_back(std::string str) {
	mutex.P();
	buffer.push(str);
	mutex.V();
	full_slots.V();
}

std::string SafeBuffer::retrieve_front() {
	full_slots.P();
	mutex.P();
	std::string item = buffer.front();
	buffer.pop();
	mutex.V();
	return item;
}
