#!/bin/sh
#  test.sh
#  Created by Joshua Higginbotham on 11/8/15.

# This is only provided for your convenience.
# The tests used in this file may or may not
# match up with what is called for for the report,
# so be sure to read that part of the handout.
# But you're free to modify this script however
# you need to, it's not graded.

echo "Cleaning and compiling..."
make
echo "Running tests..."
./client -w 5
./client -w 6
./client -w 7
./client -w 8
./client -w 9
./client -w 10
./client -w 11
./client -w 12
./client -w 13
./client -w 14
./client -w 15
./client -w 16
./client -w 17
./client -w 18
./client -w 19
./client -w 20
./client -w 21
./client -w 22
./client -w 23
./client -w 24
./client -w 25
./client -w 26
./client -w 27
./client -w 28
./client -w 29
./client -w 30
./client -w 31
./client -w 32