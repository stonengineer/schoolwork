/*
	File: network_request_channel.h

	Author: J. Stone
			Texas A&M University
	Date  : 2017/4/27
*/

#ifndef _network_request_channel_H_
#define _network_request_channel_H_

#include <string>
#include <iostream>
#include <stdexcept>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <cassert>
#include <cstring>
#include <sstream>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

class NetworkRequestChannel {
private:
	int fd, p;

public:
	NetworkRequestChannel(const std::string _server_host_name, const unsigned short _port_no);
	NetworkRequestChannel(const unsigned short _port_no, void (*connection_handler) (int), int backlog);
	~NetworkRequestChannel();
	
	std::string send_request(std::string _request);
	
	std::string cread();
	int cwrite(std::string _msg);
	
	int read_fd() { return fd; }
	int get_port() { return p; }
};

#endif