/*
    File: client.cpp

    Author: J. Higginbotham
    Department of Computer Science
    Texas A&M University
    Date  : 2016/05/21

    Based on original code by: Dr. R. Bettati, PhD
    Department of Computer Science
    Texas A&M University
    Date  : 2013/01/31
 */

/*--------------------------------------------------------------------------*/
/* DEFINES */
/*--------------------------------------------------------------------------*/

    /* -- (none) -- */
    /* -- This might be a good place to put the size of
        of the patient response buffers -- */

/*--------------------------------------------------------------------------*/
/* INCLUDES */
/*
    No additional includes are required
    to complete the assignment, but you're welcome to use
    any that you think would help.
*/
/*--------------------------------------------------------------------------*/

#include <cassert>
#include <cstring>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <string>
#include <sstream>
#include <time.h>
#include <assert.h>
#include <fstream>
#include <numeric>
#include <vector>
#include <chrono>
#include <algorithm>
#include <signal.h>
#include <iomanip>
#include "network_request_channel.h"

/*
    This next file will need to be written from scratch, along with
    semaphore.h and (if you choose) their corresponding .cpp files.
 */

#include "bounded_buffer.h"

/*--------------------------------------------------------------------------*/
/* DATA STRUCTURES */
/*--------------------------------------------------------------------------*/

/*
    All *_params structs are optional,
    but they might help.
 */
struct PARAMS {
    int w, n, p, num_requests;
    bounded_buffer requests, john_data, jane_data, jack_data;
	std::vector<int> john_freq, jane_freq, jack_freq;
    NetworkRequestChannel *chan;
	semaphore m;
	std::string h;
};

typedef std::chrono::high_resolution_clock Clock;

/*
    This class can be used to write to standard output
    in a multithreaded environment. It's primary purpose
    is printing debug messages while multiple threads
    are in execution.
 */
class atomic_standard_output {
    pthread_mutex_t console_lock;
public:
    atomic_standard_output() { pthread_mutex_init(&console_lock, NULL); }
    ~atomic_standard_output() { pthread_mutex_destroy(&console_lock); }
    void print(std::string s){
        pthread_mutex_lock(&console_lock);
        std::cout << s << std::endl;
        pthread_mutex_unlock(&console_lock);
    }
};

atomic_standard_output threadsafe_standard_output;

/*--------------------------------------------------------------------------*/
/* CONSTANTS */
/*--------------------------------------------------------------------------*/

    /* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* HELPER FUNCTIONS */
/*--------------------------------------------------------------------------*/

std::string make_histogram(std::string name, std::vector<int> *data) {
    std::string results = "Frequency count for " + name + ":\n";
    for(int i = 0; i < data->size(); ++i) {
        results += std::to_string(i * 10) + "-" + std::to_string((i * 10) + 9) + ": " + std::to_string(data->at(i)) + "\n";
    }
	results += "\n";
    return results;
}

/*
    You'll need to fill these in. 
*/
void* request_thread_function(void* arg) {
	/* Populate Request Buffer */
	PARAMS *worker = static_cast<PARAMS*>(arg);
	int n = worker->n;
	int w = worker->w;
	fflush(NULL);
	while(worker->num_requests < n) {
		worker->requests.push_back("data John Smith");
		worker->requests.push_back("data Jane Smith");
		worker->requests.push_back("data Jack Smith");
		worker->m.P();
		++worker->num_requests;
		worker->m.V();
	}

	/* Add Quit Requests to the end of Buffer */
	fflush(NULL);
	worker->requests.push_back("quit");
	
	pthread_exit(NULL);
}

void* worker_thread_function(void* arg) {
	PARAMS *worker = static_cast<PARAMS*>(arg);

    NetworkRequestChannel* channels[worker->w];
	std::string buffer_requests[worker->w];
	fd_set read;
	int max, read_count, write_count = 0;
	int select_result;
	struct timeval t = {0,10};
	
	for(int i = 0; i < worker->w; i++) {
		channels[i] = new NetworkRequestChannel(worker->h, worker->p);
		std::string request = worker->requests.retrieve_front();
		buffer_requests[i] = request;
		channels[i]->cwrite(request);
		++write_count;
	}
	
	while(true) { 
		FD_ZERO(&read);
		for(int i = 0; i < worker->w; i++) {
			if(channels[i]->read_fd() > max)
				max = channels[i]->read_fd();
			FD_SET(channels[i]->read_fd(), &read);
		}
		select_result = select(max+1, &read, NULL, NULL, &t);
		if(select_result)
		{
			for(int i = 0; i < worker->w; i++) {
				if(FD_ISSET(channels[i]->read_fd(), &read)) {
					std::string return_value = channels[i]->cread();
					++read_count;
					std::string buffer_value = buffer_requests[i];
					if(buffer_value == "data John Smith")
						worker->john_data.push_back(return_value);
					else if(buffer_value == "data Jane Smith")
						worker->jane_data.push_back(return_value);
					else if(buffer_value == "data Jack Smith")
						worker->jack_data.push_back(return_value);
					else if(buffer_value == "quit") {
						worker->john_data.push_back(return_value);
						worker->jane_data.push_back(return_value);
						worker->jack_data.push_back(return_value);
						break;
					}
					
					if(write_count < (3*worker->n)) {
						std::string request = worker->requests.retrieve_front();
						buffer_requests[i] = request;
						channels[i]->cwrite(request);
						++write_count;
					}
				}
			}
		}
		if(read_count == (3*worker->n)) { break; }
	}
	
	for(int i = 0; i < worker->w; i++)
		channels[i]->send_request("quit");
	
	pthread_exit(NULL);
}

void* john_stat_function(void* arg) {
	PARAMS *worker = static_cast<PARAMS*>(arg);
	std::string front_data = worker->john_data.retrieve_front();
	int index = 0;
	while(front_data != "bye" && !front_data.empty())
	{
		int inc = stoi(front_data) / 10;
		worker->m.P();
		worker->john_freq.at(inc) += 1;
		worker->m.V();
		if(++index == worker->n) { break; }
		front_data = worker->john_data.retrieve_front();
	}
	pthread_exit(NULL);
}

void* jane_stat_function(void* arg) {
	PARAMS *worker = static_cast<PARAMS*>(arg);
	std::string front_data = worker->jane_data.retrieve_front();
	int index = 0;
	while(front_data != "bye" && !front_data.empty())
	{
		int inc = stoi(front_data) / 10;
		worker->m.P();
		worker->jane_freq.at(inc) += 1;
		worker->m.V();
		if(++index == worker->n) { break; }
		front_data = worker->jane_data.retrieve_front();
	}
	pthread_exit(NULL);
}

void* jack_stat_function(void* arg) {
	PARAMS *worker = static_cast<PARAMS*>(arg);
	std::string front_data = worker->jack_data.retrieve_front();
	int index = 0;
	while(front_data != "bye" && !front_data.empty())
	{
		int inc = stoi(front_data) / 10;
		worker->m.P();
		worker->jack_freq.at(inc) += 1;
		worker->m.V();
		if(++index == worker->n) { break; }
		front_data = worker->jack_data.retrieve_front();
	}
	pthread_exit(NULL);
}

std::string make_histogram_table(std::string name1, std::string name2,
        std::string name3, std::vector<int> *data1, std::vector<int> *data2,
        std::vector<int> *data3) {
	std::stringstream tablebuilder;
	tablebuilder << std::setw(25) << std::right << name1;
	tablebuilder << std::setw(15) << std::right << name2;
	tablebuilder << std::setw(15) << std::right << name3 << std::endl;
	for (int i = 0; i < data1->size(); ++i) {
		tablebuilder << std::setw(10) << std::left
		        << std::string(
		                std::to_string(i * 10) + "-"
		                        + std::to_string((i * 10) + 9));
		tablebuilder << std::setw(15) << std::right
		        << std::to_string(data1->at(i));
		tablebuilder << std::setw(15) << std::right
		        << std::to_string(data2->at(i));
		tablebuilder << std::setw(15) << std::right
		        << std::to_string(data3->at(i)) << std::endl;
	}
	tablebuilder << std::setw(10) << std::left << "Total";
	tablebuilder << std::setw(15) << std::right
	        << accumulate(data1->begin(), data1->end(), 0);
	tablebuilder << std::setw(15) << std::right
	        << accumulate(data2->begin(), data2->end(), 0);
	tablebuilder << std::setw(15) << std::right
	        << accumulate(data3->begin(), data3->end(), 0) << std::endl;

	return tablebuilder.str();
}

/*--------------------------------------------------------------------------*/
/* MAIN FUNCTION */
/*--------------------------------------------------------------------------*/

int main(int argc, char * argv[]) {
    int n = 10; //default number of requests per "patient"
    int b = 50; //default size of request_buffer
    int w = 10; //default number of worker threads
	std::string h = "build.tamu.edu";
	int p = 12050;
    bool USE_ALTERNATE_FILE_OUTPUT = false;
    int opt = 0;
    while ((opt = getopt(argc, argv, "n:b:w:m:h")) != -1) {
        switch (opt) {
            case 'n':
                n = atoi(optarg);
                break;
            case 'b':
                b = atoi(optarg);
                break;
            case 'w':
                w = atoi(optarg);
                break;
            case 'm':
                if(atoi(optarg) == 2) USE_ALTERNATE_FILE_OUTPUT = true;
                break;
            case 'h':
				h = opt;
				break;
			case 'p':
				p = atoi(optarg);
				if(p <= 1024) { std::cerr << "Ports less than 1025 are reserved." << std::endl; p = 1025; }
				break;
            default:
                std::cout << "This program can be invoked with the following flags:" << std::endl;
                std::cout << "-n [int]: number of requests per patient" << std::endl;
                std::cout << "-b [int]: size of request buffer" << std::endl;
                std::cout << "-w [int]: number of worker threads" << std::endl;
                std::cout << "-m 2: use output2.txt instead of output.txt for all file output" << std::endl;
                std::cout << "-h: print this message and quit" << std::endl;
                std::cout << "Example: ./client_solution -n 10000 -b 50 -w 120 -m 2" << std::endl;
                std::cout << "If a given flag is not used, a default value will be given" << std::endl;
                std::cout << "to its corresponding variable. If an illegal option is detected," << std::endl;
                std::cout << "behavior is the same as using the -h flag." << std::endl;
                exit(0);
        }
    }

    int pid = fork();
	if (pid > 0) {
        struct timeval start_time;
        struct timeval finish_time;
        int64_t start_usecs;
        int64_t finish_usecs;
        std::ofstream ofs;
        if(USE_ALTERNATE_FILE_OUTPUT) ofs.open("output2.txt", std::ios::out | std::ios::app);
        else ofs.open("output.txt", std::ios::out | std::ios::app);

        std::cout << "n == " << n << std::endl;
        std::cout << "b == " << b << std::endl;
        std::cout << "w == " << w << std::endl;
		std::cout << "h == " << h << std::endl;
		std::cout << "p == " << p << std::endl;

        std::cout << "CLIENT STARTED:" << std::endl;
        std::cout << "Establishing control channel... " << std::flush;
        NetworkRequestChannel *chan = new NetworkRequestChannel(h, p);
        std::cout << "done." << std::endl;

        /*
            This time you're up a creek.
            What goes in this section of the code?
            Hint: it looks a bit like what went here
            in MP7, but only a *little* bit.
        */

        /* Buffers to hold requests */
        bounded_buffer request_buffer(b); //Holds all requests
		bounded_buffer john_data(n); //Holds John's responses
        bounded_buffer jane_data(n); //Holds Jane's responses
        bounded_buffer jack_data(n); //Holds Jack's responses
		std::vector<int> john_freq(10, 0); //Holds John's frequencies
		std::vector<int> jane_freq(10, 0); //Holds Jane's frequencies
		std::vector<int> jack_freq(10, 0); //Holds Jack's frequencies
		
		/* Struct to pass data to Threads */
		PARAMS *worker;
		
		/* Load Data to pass to Worker Function */
		worker = new PARAMS;
		worker->w = w;
		worker->n = n;
		worker->p = p;
		worker->h = h;
		worker->num_requests = 0;
		worker->requests.assign_function(request_buffer);
		worker->john_data.assign_function(john_data);
		worker->jane_data.assign_function(jane_data);
		worker->jack_data.assign_function(jack_data);
		worker->john_freq = john_freq;
		worker->jane_freq = jane_freq;
		worker->jack_freq = jack_freq;
		worker->chan = chan;
		
		/* Starting Timer Here */
		auto start = Clock::now();

		/* Call Thread Functions */
		pthread_t threader[5];
		
		std::cout << "Creating request thread...";
		pthread_create(&threader[0], NULL, request_thread_function, worker);
		std::cout << "done." << std::endl;
		
		std::cout << "Creating event handler...";
		pthread_create(&threader[1], NULL, worker_thread_function, worker);
		std::cout << "done." << std::endl;
		
		std::cout << "Creating statistic threads...";
		pthread_create(&threader[2], NULL, john_stat_function, worker);
		pthread_create(&threader[3], NULL, jane_stat_function, worker);
		pthread_create(&threader[4], NULL, jack_stat_function, worker);
		std::cout << "done." << std::endl;
		
		std::cout << "Joining threads...";
		for(int i = 0; i < 5; i++)
			pthread_join(threader[i], NULL);
		std::cout << "done." << std::endl;
		
		std::cout << "Finished! " << std::endl;
		
		/* Ending Timer Here */
		auto end = Clock::now();
		double runtime = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
		
		std::string histogram = make_histogram_table("John", "Jane", "Jack", &worker->john_freq, &worker->jane_freq, &worker->jack_freq);
		std::cout << histogram << std::endl;
		
		std::cout << "Results for n == " << n << ", w == " << w << ", b == " << b << std::endl;
		std::cout << "Total time: " << runtime << " ms." << std::endl;

        ofs.close();
        std::cout << "Sleeping..." << std::endl;
        usleep(10000);
        std::string finale = chan->send_request("quit");
        std::cout << "Finale: " << finale << std::endl;
    }
	else if (pid == 0)
		execl("dataserver", NULL);
}
