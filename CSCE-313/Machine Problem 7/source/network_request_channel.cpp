#include "network_request_channel.h"

NetworkRequestChannel::NetworkRequestChannel(const std::string _server_host_name, const unsigned short _port_no) {
	/* Creates a CLIENT-SIDE local copy of the channel. The channel is 
	connected to the given port number at the given server host. 
	THIS CONSTRUCTOR IS CALLED BY THE CLIENT. */

	try {
		struct addrinfo hints, *serv = NULL;
		int temp_fd;

		//gather information
		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		
		int status = getaddrinfo(_server_host_name.c_str(), std::to_string(_port_no).c_str(), &hints, &serv);
		if(status != 0) { throw std::runtime_error("Error gathering information (hp)"); }

		//create socket
		temp_fd = socket(serv->ai_family, serv->ai_socktype, serv->ai_protocol);
		if(temp_fd == -1) { throw std::runtime_error("Error creating socket."); }

		//connect socket
		int connection = connect(temp_fd, serv->ai_addr, serv->ai_addrlen);
		if(connection == -1) { std::cout << strerror(errno) << std::endl; throw std::runtime_error("Error connecting socket."); }
		
		freeaddrinfo(serv);
		//close(fd);
		this->fd = temp_fd;
	}
	catch(std::exception &e) {
		std::cerr << e.what() << std::endl;
		exit(1);
	}
	catch(...) {
		std::cerr << "Error in constructor." << std::endl;
	}
}

NetworkRequestChannel::NetworkRequestChannel(const unsigned short _port_no, void (*connection_handler) (int), int backlog) {
	/* Creates a SERVER-SIDE local copy of the channel that is accepting 
	connections at the given port number. NOTE that multiple clients can 
	be connected to the same server-side end of the request channel. 
	Whenever a new connection comes in, it is accepted by the server, and 
	the given connection handler is invoked. The parameter to the 
	connection handler is the file descriptor of the slave socket returned 
	by the accept call. NOTE that the connection handler does not want to 
	deal with closing the socket. You will have to close the socket once 
	the connection handler is done. */
	
	try {
		struct addrinfo hints, *serv = NULL;
		struct sockaddr_storage their_addr;
		socklen_t sin_size;
		int temp_fd;
		
		//gather information
		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_flags = AI_PASSIVE;
		
		int status = getaddrinfo(NULL, std::to_string(_port_no).c_str(), &hints, &serv);
		if(status != 0) { throw std::runtime_error("Error gathering information (pcb)"); }
		
		//create socket
		temp_fd = socket(serv->ai_family, serv->ai_socktype, serv->ai_protocol);
		if(temp_fd == -1) { throw std::runtime_error("Error creating socket."); }
		
		//bind socket to addservs
		int test = bind(temp_fd, serv->ai_addr, serv->ai_addrlen);
		if(test == -1) { throw std::runtime_error("Error binding to port"); }
		freeaddrinfo(serv);
		
		if(listen(temp_fd, backlog) == -1) { throw std::runtime_error("Error listening"); }
		
		//listen
		while(true) {
			sin_size = sizeof(their_addr);
			int connect = accept(temp_fd, (struct sockaddr *)&their_addr, &sin_size);
			if(connect == -1) {
				throw std::runtime_error("Error accepting instruction");
			}
			connection_handler(connect);
		}
		
		//close(fd);
		this->fd = temp_fd;
	}
	catch(std::exception &e) {
		std::cerr << e.what() << std::endl;
		exit(1);
	}
	catch(...) {
		std::cerr << "Error in constructor." << std::endl;
	}	
}

NetworkRequestChannel::~NetworkRequestChannel() {
	/* Destructor of the local copy of the channel. */
	
	close(fd);
}

std::string NetworkRequestChannel::send_request(std::string _request) {
	/* Send a string over the channel and wait for a reply. */
	
	cwrite(_request);
	std::string s = cread();
	return s;
}

std::string NetworkRequestChannel::cread() {
	/* Blocking read of data from the channel. Returns a string of 
	characters read from the channel. Returns NULL if read failed. */
	
	char buff[255]; //maximum message size
	
	if(recv(fd, buff, 255, 0) < 0) { std::cerr << "Error reading message" << std::endl; }
	
	std::string s = buff;
	return s;
}

int NetworkRequestChannel::cwrite(std::string _msg) {
	/* Write the data to the channel. The function returns the number of 
	characters written to the channel. */
	
	const char * s = _msg.c_str();
	
	if(send(fd, s, strlen(s)+1, 0) < 0) { std::cerr << "Error writing message" << std::endl; }
	
	return _msg.length();
}