/* 
    File: dataserver.C

    Author: R. Bettati
            Department of Computer Science
            Texas A&M University
    Date  : 2012/07/16

    Dataserver main program for MP3 in CSCE 313
*/

/*--------------------------------------------------------------------------*/
/* DEFINES */
/*--------------------------------------------------------------------------*/

    /* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* INCLUDES */
/*--------------------------------------------------------------------------*/

#include <cassert>
#include <cstring>
#include <sstream>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>

#include <pthread.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#include "network_request_channel.h"

/*--------------------------------------------------------------------------*/
/* DATA STRUCTURES */ 
/*--------------------------------------------------------------------------*/

    /* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* CONSTANTS */
/*--------------------------------------------------------------------------*/

    /* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* VARIABLES */
/*--------------------------------------------------------------------------*/

static int nthreads = 0;

/*--------------------------------------------------------------------------*/
/* FORWARDS */
/*--------------------------------------------------------------------------*/

void handle_process_loop(NetworkRequestChannel & _channel);

/*--------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS -- SUPPORT FUNCTIONS */
/*--------------------------------------------------------------------------*/

	/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS -- THREAD FUNCTIONS */
/*--------------------------------------------------------------------------*/

void * handle_data_requests(void * args) {

	NetworkRequestChannel * data_channel =  (NetworkRequestChannel*)args;

	// -- Handle client requests on this channel. 

	handle_process_loop(*data_channel);

	// -- Client has quit. We remove channel.

	delete data_channel;
	
	return nullptr;
}

/*--------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS -- INDIVIDUAL REQUESTS */
/*--------------------------------------------------------------------------*/

void process_hello(NetworkRequestChannel & _channel, const std::string & _request) {
	_channel.cwrite("hello to you too");
}

void process_data(NetworkRequestChannel & _channel, const std::string &  _request) {
	usleep(1000 + (rand() % 5000));
	//_channel.cwrite("here comes data about " + _request.substr(4) + ": " + std::to_string(random() % 100));
	_channel.cwrite(std::to_string(rand() % 100));
}

void connect_proc(int fd) {
	int error;
	nthreads ++;
	
	int *sfd = new int;
	*sfd = fd;
	
	pthread_t thread_id;
	
	if((error = pthread_create(&thread_id, NULL, handle_data_requests, sfd))) {
		perror(std::string("pthread_create failure").c_str());
	}
}

/*--------------------------------------------------------------------------*/
/* LOCAL FUNCTIONS -- THE PROCESS REQUEST LOOP */
/*--------------------------------------------------------------------------*/

void process_request(NetworkRequestChannel & _channel, const std::string & _request) {

	if (_request.compare(0, 5, "hello") == 0) {
		process_hello(_channel, _request);
	}
	else if (_request.compare(0, 4, "data") == 0) {
		process_data(_channel, _request);
	}
	else {
		_channel.cwrite("unknown request");
	}
}

void handle_process_loop(NetworkRequestChannel & _channel) {
	
	for(;;) {
		//std::cout << "Reading next request from channel (" << _channel.name() << ") ..." << std::flush;
		//std::cout << std::flush;
		std::string request = _channel.cread();
		//std::cout << " done (" << _channel.name() << ")." << std::endl;
		//std::cout << "New request is " << request << std::endl;

		if (request.compare("quit") == 0) {
			_channel.cwrite("bye");
			usleep(10000);          // give the other end a bit of time.
			break;                  // break out of the loop;
		}

		process_request(_channel, request);
	}
}

/*--------------------------------------------------------------------------*/
/* MAIN FUNCTION */
/*--------------------------------------------------------------------------*/


int main(int argc, char * argv[]) {
	std::cout << "Establishing dataserver channel... " << std::flush;
	
	int p = 12050;
	int b = 20;
	
	int opt;
	
	while((opt = getopt(argc, argv, "p:b:")) != -1)
	{
		switch(opt) {
			case 'p':
				p = atoi(optarg);
				break;
			case 'b':
				b = atoi(optarg);
				break;
		}
	}
	
	NetworkRequestChannel control_channel(p, &connect_proc, b);
	std::cout << "done.\n" << std::flush;
}

