/* --------------------------------------------------------------------------- */
/* Developer: Andrew Kirfman, Margaret Baxter                                  */
/* Project: CSCE-313 Machine Problem #1                                        */
/*                                                                             */
/* File: ./MP1/Part_2/main.cpp                                                 */
/* --------------------------------------------------------------------------- */

/* --------------------------------------------------------------------------- */
/* Standard Library Includes                                                   */
/* --------------------------------------------------------------------------- */

#include <stdlib.h>
#include <string.h>
#include <climits>

/* --------------------------------------------------------------------------- */
/* User Defined Includes                                                       */
/* --------------------------------------------------------------------------- */

#include "linked_list2.h"

int main(int argc, char ** argv) 
{
	//std::cout << INT_MAX;
	int b = 128;
	int M = b * 16;  	// so we have space for 16 items in the whole list
	int t = 4;			// 4 tiers and 4 items per tier
	
	char buf [1024];
	memset (buf, 97, 1024);		// set each byte to 1
	
	char * msg = "a sample message";
	
	// Instantiate the tiered list class
	linked_list2 *test_list = new linked_list2;
	
	test_list->Destroy(); //tests destroying unitialized list
	
	test_list->Init(M, b, t); // initialize
	// test operations
	test_list->PrintList(); //tests printing an empty list
	test_list->Delete(5); //tests deleting nonexistent item
	test_list->Lookup(5); //tests looking up nonexistent item
	
	//There are multiple cases to test so just do one at a time and comment out the others
	//int testnums [] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 1<<29 , (1<<29) + 5 , 50, (1<<30) + 5, (1<<30) - 500}; //tests case if more than 16 items
	//int testnums [] = {-45}; //tests negative nums
	//int testnums [] = {1<<35}; //test nums that are too high
	//int testnums [] = {-45, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 1<<29 , (1<<29) + 5 , 50, (1<<30) + 5, (1<<30) - 500, 1<<35}; //tests all together
	int testnums [] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}; //use this one to try and insert a 17th item

	// some sample insertions
	for (int i=0; i< sizeof(testnums); i++) //need to change i for different cases of testnums above
	{
		test_list->Insert (testnums [i], buf, 50);   // insert 50 bytes from the buffer as value for each of the insertions
	}
	test_list->Insert (17, buf, 50); //tests inserting a item that will make list above size declared
	test_list->PrintList();
	
		
	// end test operations	
	test_list->Destroy();
}
