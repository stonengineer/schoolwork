/* --------------------------------------------------------------------------- */
/* Developer: Andrew Kirfman, Margaret Baxter                                  */
/* Project: CSCE-313 Machine Problem #1                                        */
/*                                                                             */
/* File: ./MP1/linked_list2.cpp                                                */
/* --------------------------------------------------------------------------- */

/* --------------------------------------------------------------------------- */
/* Standard Library Includes                                                   */
/* --------------------------------------------------------------------------- */

#include <iostream>
#include <string.h>
#include <climits>

/* --------------------------------------------------------------------------- */
/* User Defined Includes                                                       */
/* --------------------------------------------------------------------------- */

#include "linked_list2.h"

using namespace std;

linked_list2::linked_list2()
{
	head_pointer = NULL;
	front_pointer = NULL;
	free_pointer = NULL;
	free_data_pointer = NULL;
	
	block_size = mem_size = max_data_size = num_tiers = -1;
	initialized = false;
}

void linked_list2::Init(int M, int b, int t)
{
	if(initialized)
	{
		cerr << "Error: previously initialized." << endl;
		return;
	}
	if(M <= 0 || b <= 0 || t <= 0)
	{
		cerr << "Error: Invalid Input: One or more of the size parameters is leq 0." << endl;
		return;
	}
	if(b > M)
	{
		cerr << "Error: Invalid Input: Block size should not be larger than memory size." << endl;
		return;
	}
	if(t > M)
	{
		cerr << "Error: Invalid Input: Number of layers should be smaller than blocksize." << endl;
	}

	block_size = b;
	mem_size = M;
	max_data_size = (mem_size / block_size);
	num_tiers = t;
	nodes_per_tier = (max_data_size / num_tiers);

	head_pointer = new char*[num_tiers];
	front_pointer = new node*[num_tiers];
	free_pointer = new node*[num_tiers];
	free_data_pointer = new node*[num_tiers];
	num_nodes_layer = new int[num_tiers];

	for(int i = 0; i < num_tiers; i++)
	{
		head_pointer[i] = (char*) malloc(nodes_per_tier * block_size);
		num_nodes_layer[i] = 0;
		for(int n = 0; n < nodes_per_tier; n++)
		{
			node* curr = new node();
			curr->key = -1;
			curr->value_len = -1;
			curr->next = NULL;
			if(n == 0)
			{
				front_pointer[i] = curr;
				free_pointer[i] = curr;
			}
			else if(n == 1)
			{
				front_pointer[i]->next = curr;
				free_pointer[i] = curr;
			}
			else
			{
				free_pointer[i]->next = curr;
				free_pointer[i] = curr;
			}
		}
		free_pointer[i] = front_pointer[i];
	}

	initialized = true;
}


void linked_list2::Destroy()
{
	if(!initialized)
	{
		cerr << "Error: List not initialized; nothing to destroy." << endl;
		return;
	}
	block_size = mem_size = max_data_size = num_tiers = -1;
	initialized = false;
	for(int i = 0; i < num_tiers; i++)
	{
		delete(head_pointer[i]);
		while(front_pointer[i]->next != NULL)
		{
			node* temp = front_pointer[i]->next;
			delete(front_pointer[i]);
			front_pointer[i] = temp;
		}
		delete(free_pointer[i]);
	}
}

void linked_list2::Insert(int k,char * data_ptr, int data_len)
{
	if(!initialized)
	{
		cerr << "Error: List not initalized; cannot insert." << endl;
		return;
	}
	if(k < 0)
	{
		cerr << "Error: Key must be greater than zero." << endl;
		return;
	}
	if(data_len < 0)
	{
		cerr << "Error: Length of data to insert must be greater than zero." << endl;
		return;
	}

	int curr_tier = Find_tier(k);

	if(num_nodes_layer[curr_tier] >= nodes_per_tier)
	{
		cerr << "Error: Cannot insert node on full layer." << endl;
		return;
	}

	++num_nodes_layer[curr_tier];
	node* ins = new node();
	ins->key = k;
	ins->value_len = data_len;
	ins->next = NULL;

	int node_num = 0;
	if(front_pointer[curr_tier]->key == -1)
	{
		front_pointer[curr_tier] = free_pointer[curr_tier] = ins;
		node_num = 0;
	}
	else
	{
		free_pointer[curr_tier]->next = ins;
		free_pointer[curr_tier] = free_pointer[curr_tier]->next;

		node* n = front_pointer[curr_tier];
		while(n != free_pointer[curr_tier])
		{
			++node_num;
			n = n->next;
		}
	}

	for(int i = 0; i < block_size; i++)
	{
		if(i < data_len)
			head_pointer[curr_tier][i+(node_num*block_size)] = data_ptr[i];
		else
			head_pointer[curr_tier][i+(node_num*block_size)] = '\0';
	}
}

int linked_list2::Delete(int delete_key)
{
	if(!initialized)
	{
		cerr << "Error: List not initialized; cannot delete." << endl;
		return -1;
	}
	if(delete_key < 0)
	{
		cerr << "Error: Key cannot be below 0." << endl;
		return -1;
	}

	int curr_tier = Find_tier(delete_key);

	node* curr = front_pointer[curr_tier];
	node* prev = front_pointer[curr_tier];
	for(int i = 0; i < (max_data_size / nodes_per_tier); i++)
	{
		if(curr == NULL)
		{
			return -1;
		}
		if(curr->key == delete_key)
		{
			if(curr->next == NULL)
				front_pointer[curr_tier] = prev;
			else
				front_pointer[curr_tier] = curr->next;
			prev->next = curr->next;
			curr->next = NULL;
			curr->key = -1;
			curr->value_len = -1;

			node* n = front_pointer[curr_tier];
			int heap_loc = 0;
			while(n->key != delete_key)
			{
				++heap_loc;
				n = n->next;

				if(heap_loc > (max_data_size / num_tiers))
				{
					cerr << "Joe wrote a bad loop." << endl;
					return -1;
				}
			}
			heap_loc *= block_size;

			for(int i = 0; i < block_size; i++)
			{
				head_pointer[curr_tier][i+heap_loc] = '\0';
			}
		}
	}
	return -1;
}

node* linked_list2::Lookup(int lookup_key)
{
	if(!initialized)
	{
		cerr << "Error: List not initialized; cannot lookup." << endl;
		return NULL;
	}
	if(lookup_key < 0)
	{
		cerr << "Error: Key cannot be below 0." << endl;
		return NULL;
	}

	int curr_tier = Find_tier(lookup_key);

	node* curr = front_pointer[curr_tier];
	node* prev = front_pointer[curr_tier];
	for(int i = 0; i < (max_data_size / nodes_per_tier); i++)
	{
		if(curr->key == lookup_key)
			return curr;
		curr = curr->next;
	}
	return NULL;
}

void linked_list2::PrintList()
{
	
	/* IMPORTANT NOTE!
	 * 
	 * In order for the script that will grade your assignment to work 
	 * (i.e. so you get a grade higher than a 0),
	 * you need to print out each member of the list using the format below.  
	 * Your print list function should be written as a while loop that prints 
	 * the tier of the list and then each node underneath it.  
	 * the following four lines exactly for each node and nothing else.  If
	 * you have any difficulties, talk to your TA and he will explain it further. 
	 * 
	 * The output lines that you should use are provided so that you will know
	 * exactly what you should output.  
	 */ 
	// Do this for every tier
	// std::cout << "Tier " << <TIER NUMBER GOESHERE> << std::endl;
	
	// Do this for every node 
	//std::cout << "Node: " << std::endl;
	//std::cout << " - Key: " << <KEY GOES HERE!> << std::endl;
	//std::cout << " - Data: " << <KEY GOES HERE!> << std::endl;
	
	/* Short example:
	 *   - Assume a list with two tiers each with two elements in each tier.  
	 *     the print out would appear as follows
	 * 
	 * Tier 0
	 * Node: 
	 *  - Key: 1
	 *  - Data: Hello
	 * Node:
	 *  - Key: 2
	 *  - Data: World!
	 * Tier 1
	 * Node:
	 *  - Key: 3
	 *  - Data: Hello
	 * Node:
	 *  - Key: 4
	 *  - Data: World!
	 * 
	 * ^^ Your output needs to exactly match this model to be counted as correct.  
	 * (With the exception that the values for key and data will be different 
	 * depending on what insertions you perform into your list.  The values provided
	 * here are for pedagogical purposes only)
	 */

	if(!initialized)
	{
		cerr << "Error: List not initialized; cannot print." << endl;
		return;
	}

	for(int i = 0; i < num_tiers; i++)
	{
		node* n = front_pointer[i];
		if(n->key != -1)
		{
			cout << "Tier " << i << endl;
			int step = 0;
			while(n != NULL)
			{
				cout << "Node:" << endl;
				cout << " - Key: " << n->key << endl;
				cout << " - Data: ";
				for(int d = (step*block_size); d < ((step+1)*block_size); d++)
				{
					cout << head_pointer[i][d];
				}
				cout << endl;
				++step;
				n = n->next;
			}
		}
	}
}

int linked_list2::Find_tier(int key)
{
	if(!initialized)
	{
		cerr << "Error: List not initialized; cannot find tier." << endl;
		return -1;
	}
	if(key < 0)
	{
		cerr << "Error: Key cannot be below 0." << endl;
		return -1;
	}

	int curr_tier = -1;
	for(int i = 1; i <= num_tiers; i++)
	{
		if(key <= (i * (INT_MAX / num_tiers)) && key >= 0)
		{
			curr_tier = i-1;
			break;
		}
	}
	return curr_tier;
}

/* Getter Functions */
char** linked_list2::getHeadPointer()
{
	return head_pointer;
}

node** linked_list2::getFrontPointer()
{
	return front_pointer;
}

node** linked_list2::getFreePointer()
{
	return free_pointer;
}

node** linked_list2::getFreeDataPointer()
{
	return free_data_pointer;
}

int linked_list2::getBlockSize()
{
	return block_size;
}

int linked_list2::getMemSize()
{
	return mem_size;
}

int linked_list2::getMaxDataSize()
{
	return max_data_size;
}

int linked_list2::getNumTiers()
{
	return num_tiers;
}

int linked_list2::getInitialized()
{
	return initialized;
}

/* Setter Functions */
void linked_list2::setHeadPointer(char** new_head_pointer)
{
	head_pointer = new_head_pointer;
}

void linked_list2::setFrontPointer(node** new_front_pointer)
{
	front_pointer = new_front_pointer;
}

void linked_list2::setFreePointer(node** new_free_pointer)
{
	free_pointer = new_free_pointer;
}

void linked_list2::setFreeDataPointer(node** new_free_data_pointer)
{
	free_data_pointer = new_free_data_pointer;
}

void linked_list2::setBlockSize(int new_block_size)
{
	block_size = new_block_size;
}

void linked_list2::setMemSize(int new_mem_size)
{
	mem_size = new_mem_size;
}

void linked_list2::setMaxDataSize(int new_max_data_size)
{
	max_data_size = new_max_data_size;
}

void linked_list2::setNumTiers(int new_num_tiers)
{
	num_tiers = new_num_tiers;
}

void linked_list2::setInitialized(bool new_initialized)
{
	initialized = new_initialized;
}
