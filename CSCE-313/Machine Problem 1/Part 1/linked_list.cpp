/* --------------------------------------------------------------------------- */
/* Developer: Andrew Kirfman, Margaret Baxter                                  */
/* Project: CSCE-313 Machine Problem #1                                        */
/*                                                                             */
/* File: ./MP1/linked_list.cpp                                                 */
/* --------------------------------------------------------------------------- */

/* --------------------------------------------------------------------------- */
/* User Defined Includes                                                       */
/* --------------------------------------------------------------------------- */

#include "linked_list.h"

using namespace std;

/* Constructor */
linked_list::linked_list()
{
	head_pointer = NULL;
	front_pointer = free_pointer = free_data_pointer = NULL;
	block_size = mem_size = max_data_size = -1;
	initialized = false;
}


void linked_list::Init(int M, int b)
{
	if(initialized)
	{
		cerr << "Error: already initialized. Initialization failed." << endl;
		return;
	}

	mem_size = M;
	block_size = b;
	if(b == 0)
	{
		cerr << "Error: Block size is zero. Initialization failed." << endl;
		return;
	}
	max_data_size = mem_size / block_size;

	head_pointer = (char*) malloc(mem_size);
	for(int i = 0; i < mem_size; i++)
		head_pointer[i] = '\0';
	node* start = new node();
	front_pointer = free_pointer = start;
	for(int i = 0; i < max_data_size; i++)
	{
		node* next = new node();
		free_pointer->next = next;
		free_pointer = next;
	}

	count = 0;
	free_pointer = front_pointer;
	initialized = true;
}

void linked_list::Destroy()
{
	delete(head_pointer);
	block_size = mem_size = max_data_size = -1;
	initialized = false;
	while(front_pointer->next != NULL)
	{
		node* temp = front_pointer->next;
		delete(front_pointer);
		front_pointer = temp;
	}
	delete(free_pointer);
} 

/* Insert an element into the list with a given key, given data element, and with a given length*/
void linked_list::Insert (int k, char * data_ptr, int data_len)
{
	if(count > max_data_size)
	{
		cerr << "Error: No more memory to insert to. Segmentation fault." << endl;
		return;
	}

	if(data_len > block_size)
	{
		cerr << "Error: Data to insert is greater than given block size." << endl;
		return;
	}

	node* ins = new node();
	ins->key = k;
	ins->value_len = data_len;
	if(count == 0)
	{
		free_pointer = front_pointer = ins;
	}
	else
	{	
		free_pointer->next = ins;
		free_pointer = ins;
	}
	for(int i = 0; i < block_size; i++)
	{
		if(i < data_len)
			head_pointer[(count*block_size)+i] = data_ptr[i];
		else
			head_pointer[(count*block_size)+i] = '\0';
	}
	++count;
}


int linked_list::Delete(int delete_key)
{
	node* del = front_pointer;
	node* prev = front_pointer;
	int iterations = -1;
	int del_count = 0;
	bool is_last = false;;
	while(del != NULL)
	{
		if(del->key == delete_key)
		{
			if(del == front_pointer)
			{
				del = front_pointer->next;
				delete(front_pointer);
				front_pointer = del;
				iterations = del_count;
			}
			if(del == free_pointer)
			{
				prev->next = NULL;
				free_pointer = prev;
				delete(del);
				iterations = del_count;
				is_last = true;
			}
			prev->next = del->next;
			delete(del);
			iterations = del_count;
		}
		prev = del;
		del = del->next;
		++del_count;
	}

	if(iterations == -1)
	{
		cerr << "Error: Key not found." << endl;
		return iterations;
	}

	if(is_last)
	{
		for(int i = (iterations*block_size); i < ((iterations+1)*block_size); i++)
		{
			head_pointer[i] = '\0';
		}
	}
	else
	{
		for(int i = ((iterations+1)*block_size); i < mem_size; i++)
		{
			head_pointer[i] = head_pointer[i-block_size];
		}
	}

	--count;
	return delete_key;
}

/* Iterate through the list, if a given key exists, return the pointer to it's node */
/* otherwise, return NULL                                                           */
struct node* linked_list::Lookup(int lookup_key)
{
	node* find = front_pointer;
	while(find != NULL)
	{
		if(find->key == lookup_key)
			return find;
		find = find->next;
	}
	cerr << "Error: Key not found." << endl;
	return find;
}

/* Prints the list by printing the key and the data of each node */
void linked_list::PrintList()
{
	
	/* IMPORTANT NOTE!
	 * 
	 * In order for the script that will grade your assignment to work 
	 * (i.e. so you get a grade higher than a 0),
	 * you need to print out each member of the list using the format below.  
	 * Your print list function should be written as a while loop that prints
	 * the following three lines exactly for each node and nothing else.  If
	 * you have any difficulties, talk to your TA and he will explain it further.  
	 * 
	 * The output lines that you should use are provided so that you will know
	 * exactly what you should output.  
	 */ 
	//std::cout << "Node: " << std::endl;
	//std::cout << " - Key: " << <KEY GOES HERE!> << std::endl;
	//std::cout << " - Data: " << <KEY GOES HERE!> << std::endl;

	int iterations = 0;
	node* print = front_pointer;
	while(print != NULL && iterations <= max_data_size)
	{
		cout << "Node: " << endl; cout.flush();
		cout << " - Key: " << print->key << endl; cout.flush();
		cout << " - Data: "; cout.flush();
		for(int i = (iterations*block_size); i < ((iterations+1)*block_size); i++)
		{
			cout << head_pointer[i];
		}
		cout << endl;
		++iterations;
		print = print->next;
	}
	
	/* Short example:
	 *   - Assume that you have a list with 4 elements.  
	 *     Your output should appear as follows
	 * 
	 * Node:
	 *  - Key: 1
	 *  - Data: Hello
	 * Node:
	 *  - Key: 2
	 *  - Data: World!
	 * Node:
	 *  - Key: 3
	 *  - Data: Hello
	 * Node:
	 *  - Key: 4
	 *  - Data: World!
	 * 
	 * ^^ Your output needs to exactly match this model to be counted as correct.  
	 * (With the exception that the values for key and data will be different 
	 * depending on what insertions you perform into your list.  The values provided
	 * here are for pedagogical purposes only)
	 */
}

/* Getter Functions */
char* linked_list::getHeadPointer()
{
	return head_pointer;
}

node* linked_list::getFrontPointer()
{
	return front_pointer;
}

node* linked_list::getFreePointer()
{
	return free_pointer;
}

node* linked_list::getFreeDataPointer()
{
	return free_data_pointer;
}

int linked_list::getBlockSize()
{
	return block_size;
}

int linked_list::getMemSize()
{
	return mem_size;
}

int linked_list::getMaxDataSize()
{
	return max_data_size;
}

bool linked_list::getInitialized()
{
	return initialized;
}

/* Setter Functions */
void linked_list::setHeadPointer(char *new_pointer)
{
	head_pointer = new_pointer;
}

void linked_list::setFrontPointer(node* new_pointer)
{
	front_pointer = new_pointer;
}

void linked_list::setFreePointer(node* new_pointer)
{
	free_pointer = new_pointer;
}

void linked_list::setFreeDataPointer(node* new_pointer)
{
	free_data_pointer = new_pointer;
}

void linked_list::setBlockSize(int new_block_size)
{
	block_size = new_block_size;
}

void linked_list::setMemSize(int new_mem_size)
{
	mem_size = new_mem_size;
}

void linked_list::setMaxDataSize(int new_max_data_size)
{
	max_data_size = new_max_data_size;
}

void linked_list::setInitialized(bool new_initialized)
{
	initialized = new_initialized;
}
