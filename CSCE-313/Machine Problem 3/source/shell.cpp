#include <unistd.h>     // getpid(), getcwd()
#include <sstream>      // ostringstream support
#include <sys/types.h>  // type definitions, e.g., pid_t
#include <sys/wait.h>   // wait()
#include <signal.h>     // signal name constants and kill()
#include <iostream>     // io for prompt and debugging
#include <vector>       // to hold chopped commands
#include <cstring>      // strings
#include <string.h>     // string commands
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

using namespace std;

char* run_command(char* prog, vector<char*> args, char* command, bool pipe_curr)
{
	int link[2];
	char* result;

	char** argv = new char*[args.size()+1];
	for(int k = 0; k < args.size(); k++)
		argv[k] = args[k];

	argv[args.size()] = NULL;

	if(!strcmp(command, "exit"))
	{
		exit(0);
	}
	else
	{
		if(!strcmp(prog, "cd"))
		{
			if (argv[1] == NULL)
				chdir ("/"); 
			else 
			{
				if(!chdir (argv[1])) { /* success! */ }
				else { perror(command); }
			}
		}
		else if(pipe_curr)
		{
			if(pipe(link))
			{
				perror("Internal error: pipe broke.");
				return result;
			}
			pid_t child_pid = fork();
			if(child_pid < 0)
			{
				perror("Internal error: cannot fork.");
				return result;
			}
			else if(child_pid == 0)
			{
				//child
				dup2(link[1], STDOUT_FILENO);
				close(link[0]);
				close(link[1]);
				execvp(prog, argv);
				perror(command); //error
			}
			else
			{
				//parent
				close(link[1]);
				int nbytes = read(link[0], result, sizeof(result));
				if (waitpid(child_pid, 0, 0) < 0) //wait
				{
					perror("Internal error: cannot wait for child.");
					return result;
				}
			}
		}
		else
		{
			pid_t child_pid = fork();
			if(child_pid < 0)
			{
				perror("Internal error: cannot fork.");
				return result;
			}
			else if(child_pid == 0)
			{
				//child
				execvp(prog, argv);
				perror(command); //error
			}
			else
			{
				//parent
				if (waitpid(child_pid, 0, 0) < 0) //wait
				{
					perror("Internal error: cannot wait for child.");
					return result;
				}
			}
		}
	}
	return result;
}

int main()
{
	while(1)
	{
		char* prompt = getcwd(NULL, 0);
		if(strlen(prompt) > 30)
		{
			ostringstream os;
			os << "...:";
			for(int i = strlen(prompt)-26; i < strlen(prompt); i++)
				os << prompt[i];
			prompt = new char[30];
			for(int i = 0; i < 30; i++)
				prompt[i] = os.str().c_str()[i];
		}
		cout << prompt << ">> " << flush;

		char command[1024];
		cin.getline(command, 1024);

		vector<vector<char*>> args;
		args.resize(1);
		vector<char*> prog;
		prog.push_back(strtok(command, " "));
		char* temp = prog[0];
		bool next_is_prog = false;
		int num_progs = 0;
		while(temp != NULL)
		{
			if(!strcmp(temp, "|"))
				next_is_prog = true;
			else if(next_is_prog)
			{
				prog.push_back(temp);
				++num_progs;
				next_is_prog = false;
				args.resize(num_progs+1);
			}
			else if(!strcmp(temp, "<") && strcmp(prog[prog.size()-1], "cat"))
			{
				char cat[] = {'c','a','t'};
				prog.push_back(cat);
				++num_progs;
				args.resize(num_progs+1);
			}
			else
				args[num_progs].push_back(temp);
			temp = strtok(NULL, " ");	
		}

		for(int i = 0; i < prog.size(); i++)
		{
			char* add_to_args = run_command(prog[i], args[i], command, (i == (prog.size()-1) && prog.size() != 1));
			args[i].push_back(strtok(add_to_args, " "));
		}
	}
	return 0;
}