//
//  bounded_buffer.cpp
//  
//
//  Created by Joshua Higginbotham on 11/4/15.
//
//

#include "bounded_buffer.h"

bounded_buffer::bounded_buffer() {
	//empty constructor
}

bounded_buffer::bounded_buffer(int _capacity) {
	size = _capacity;
	lock = new semaphore(1);
	full = new semaphore(0);
	empty = new semaphore(_capacity);
}

bounded_buffer::~bounded_buffer() {
	delete lock;
	delete full;
	delete empty;
}

void bounded_buffer::assign_function(bounded_buffer &cp) {
	buffer = cp.get_buffer();
	lock = cp.get_lock();
	full = cp.get_full();
	empty = cp.get_empty();
}

void bounded_buffer::push_back(std::string req) {
	empty->P();
	lock->P();
	buffer.push(req);
	lock->V();
	full->V();
}

std::string bounded_buffer::retrieve_front() {
	full->P();
	lock->P();
	std::string ret = buffer.front();
	buffer.pop();
	lock->V();
	empty->V();
	return ret;
}