/* 
	File: semaphore.H

	Author: R. Bettati
			Department of Computer Science
			Texas A&M University
	Date  : 08/02/11

*/

#ifndef _semaphore_H_                   // include file only once
#define _semaphore_H_

/*--------------------------------------------------------------------------*/
/* DEFINES */
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* INCLUDES */
/*--------------------------------------------------------------------------*/

#include <pthread.h>

/*--------------------------------------------------------------------------*/
/* DATA STRUCTURES */ 
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* FORWARDS */ 
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* CLASS   s e m a p h o r e  */
/*--------------------------------------------------------------------------*/

class semaphore {
private:
	/* -- INTERNAL DATA STRUCTURES */
	int count;
	pthread_mutex_t m;
	pthread_cond_t c;

public:

	/* -- CONSTRUCTOR/DESTRUCTOR */

	semaphore() {
		pthread_mutex_init(&m, NULL);
		pthread_cond_init(&c, NULL);
		count = 1;
	}
	
	semaphore(int _val) {
		pthread_mutex_init(&m, NULL);
		pthread_cond_init(&c, NULL);
		count = _val;
	}

	~semaphore(){
		pthread_mutex_destroy(&m);
		pthread_cond_destroy(&c);
	}

	/* -- SEMAPHORE OPERATIONS */
	
	void P() {
		pthread_mutex_lock(&m);
		while(count <= 0)
			if(pthread_cond_wait(&c, &m) != 0)
				return;
		--count;
		pthread_mutex_unlock(&m);
	}

	void V() {
		pthread_mutex_lock(&m);
		++count;
		pthread_cond_broadcast(&c);
		pthread_mutex_unlock(&m);
	}
};

#endif