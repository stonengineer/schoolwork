/* 
	File: my_allocator.c

	Author: <your name>
			Department of Computer Science
			Texas A&M University
	Date  : <date>

	Modified: 

	This file contains the implementation of the module "MY_ALLOCATOR".

*/

/*--------------------------------------------------------------------------*/
/* DEFINES */
/*--------------------------------------------------------------------------*/

	/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* INCLUDES */
/*--------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "my_allocator.h"

/*--------------------------------------------------------------------------*/
/* DATA STRUCTURES */ 
/*--------------------------------------------------------------------------*/

	/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* CONSTANTS */
/*--------------------------------------------------------------------------*/

const unsigned int header_size = 13;

/*--------------------------------------------------------------------------*/
/* FORWARDS */
/*--------------------------------------------------------------------------*/

	/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* FUNCTIONS FOR MODULE MY_ALLOCATOR */
/*--------------------------------------------------------------------------*/

unsigned int is_power_two(unsigned int n)
{
	double l = log2(n);
	if(l == ((int)l * 1.0))
		return 1;
	return 0;
}

extern unsigned int init_allocator(unsigned int _basic_block_size, unsigned int _length)
{
	if(_basic_block_size >= _length)
	{
		printf("Error: block size cannot exceed memory.\n");
		return 0;
	}

	basic_block_size = _basic_block_size;
	if(is_power_two(basic_block_size) == 0)
	{
		int n = (int) log2(basic_block_size) + 1;
		basic_block_size = pow(2, n);
		if(basic_block_size > _length)
		{
			printf("Error: block size cannot exceed memory.\n");
			basic_block_size = 0;
			return 0;
		}
	}
	block_index = (int)log2(basic_block_size);

	memory_size = _length;

	memory_pool = (char*) calloc(memory_size, 1);
	free_list = (struct block**) calloc((int)log2(memory_size) - block_index + 1, sizeof(struct block*));
	struct block *top = (struct block*) memory_pool;
	top->next = NULL;
	top->size = memory_size;
	top->is_free = '0';
	free_list[(int)log2(memory_size) - block_index] = top;

	initialized = 1;

	return 0;
}

extern int release_allocator()
{
	free(free_list);
	free(memory_pool);
	free_list = NULL;
	memory_pool = NULL;
	initialized = 0;
	return 0;
}

extern Addr my_malloc(unsigned int _length) 
{
	if(initialized != 1)
	{
		printf("Error: memory manager has not been initialized.\n");
	}
	if(memory_pool == NULL)
	{
		printf("Error: memory has already been released.\n");
		return NULL;
	}

	int req_size = basic_block_size;
	while(req_size <= (_length + header_size))
		req_size *= 2;

	int avail_size = req_size;
	while(free_list[(int)log2(avail_size) - block_index] == NULL)
	{
		if(avail_size > memory_size)
		{
			printf("Error: not enough remaining space to hold %d.\n", avail_size);
			return NULL;
		}
		avail_size *= 2;
	}

	while(1)
	{
		if(avail_size == req_size)
		{
			struct block* curr = free_list[(int)log2((double)avail_size) - block_index];
			free_list[(int)log2((double)avail_size) - block_index] = curr->next;
			curr->is_free = '1';
			curr->next = NULL;
			return (char*) curr+13;
		}
		else
		{
			struct block* curr = free_list[(int)log2((double)avail_size) - block_index];
			char* temp = (char*) curr;
			struct block* buddy = (struct block*) (temp + avail_size/2);
			buddy->next = curr->next;
			curr->next = buddy;
			curr->size = avail_size/2;
			buddy->size = avail_size/2;
			curr->is_free = '0';
			buddy->is_free = '0';

			free_list[(int)log2((double)avail_size) - block_index] = buddy->next;
			
			avail_size /= 2;
			free_list[(int)log2((double)avail_size) - block_index] = curr;
			buddy->next = NULL;
		}
	}
	return (void *)0;
}

int merge_block()
{
	int final_index = (int)log2(memory_size) - block_index;
	int index = 0;
	while(index+1 < final_index)
	{
		struct block* curr = free_list[index];
		for(;;)
		{
			if(curr == NULL || curr->next == NULL)
				break;
			else if(curr->next == (struct block*)((((char*)curr - memory_pool) ^ curr->size)+memory_pool))
			{
				if(curr > curr->next)
				{
					struct block* buddy = curr->next;
					struct block* buddy_next = curr->next->next;
					struct block* hold = curr;
					curr = buddy;
					curr->next = hold;
					curr->next->next = buddy_next;
				}
				curr->next = curr->next->next;
				curr->size = (curr->size) * 2.0;
				free_list[index] = curr->next;

				struct block* iter = free_list[index+1];
				for(;;)
				{
					if(iter == NULL)
					{
						free_list[index+1] = curr;
						break;
					}
					else if(iter->next == NULL)
					{
						iter->next = curr;
						break;
					}
					else if(iter == (struct block*)((((char*)curr - memory_pool) ^ curr->size)+memory_pool))
					{
						curr->next = iter->next;
						iter->next = curr;
						break;
					}
					iter = iter->next;
				}
			}
			curr = curr->next;
		}
		++index;
	}
	return 0;
}

struct block* at_back(int size)
{
	struct block* curr = free_list[(int)log2(size) - block_index];
	while(curr->next != NULL)
		curr = curr->next;
	return curr;
}

extern int my_free(Addr _a) 
{
	struct block* remove = (struct block*) ((char*) _a -13);
	remove->is_free = '\0';
	if(free_list[(int)log2(remove->size) - block_index] == NULL)
		free_list[(int)log2(remove->size) - block_index] = remove;
	else
		at_back(remove->size)->next = remove;
	merge_block();
	if(free_list[(int)log2(memory_size) - block_index] != NULL)
	{
		int size = sizeof(memory_pool);
		memset(memory_pool, '\0', size);
		struct block* top = (struct block*)memory_pool;
		top->is_free = '0';
		top->size = memory_size;
		top->next = NULL;
		free_list[(int)log2(memory_size) - block_index] = top;
	}
	return 0;
}