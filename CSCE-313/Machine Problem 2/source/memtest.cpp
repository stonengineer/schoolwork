/* ------------------------------------------------------------------------ */
/* Developer: Preston Percival, Joe Stone                                   */
/* Project: CSCE-313 Machine Problem 2                                      */
/*                                                                          */
/* File: ./memtest.cpp                                                      */
/* ------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------ */
/* User Defined Includes                                                    */
/* ------------------------------------------------------------------------ */

#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <unistd.h>
#include "ackerman.h"
#include "my_allocator.h"


using namespace std;


int main(int argc, char ** argv) 
{
	// Use getopt to grab command line arguments
	/*int opt = 0;
	int basic_block_size = 0;
	int mem_size = 0;
	while((opt = getopt( argc, argv, "b:s:")) != -1)
	{
		switch(opt)
		{
			case 'b':
				basic_block_size = atoi(optarg);
				break;
			case 's':
				mem_size = atoi(optarg);
				break;
			default:
				abort();
		}
		if (basic_block_size == 0) basic_block_size = 128;
		if (mem_size == 0) mem_size = 524288;
	}*/

	int basic_block_size = 128;
	int mem_size = 524288;
	
	std::cout << "basic_block_size: " << basic_block_size << endl; //testing getopt
	std::cout << "mem_size: " << mem_size << endl; //testing getopt
	
	// from argv.  Use these arguments as inputs to your program
	
	init_allocator(basic_block_size, mem_size);

    //std::cout << "Hello World!" << std::endl;
	
	ackerman_main(); 
	
	// release_allocator()

	return 0;
}
