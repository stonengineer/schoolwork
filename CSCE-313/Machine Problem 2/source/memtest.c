#include "ackerman.h"
#include "my_allocator.h"
#include <unistd.h>

int main(int argc, char ** argv) {

	// input parameters (basic block size, memory length)
  
	int opt = 0;
	int basic_block_size = 0;
	int mem_size = 0;
	while((opt = getopt( argc, argv, "b:s:")) != -1)
	{
		switch(opt)
		{
			case 'b':
				basic_block_size = atoi(optarg);
				break;
			case 's':
				mem_size = atoi(optarg);
				break;
			default:
				abort();
		}
		if (basic_block_size == 0) basic_block_size = 128;
		if (mem_size == 0) mem_size = 524288;
	}
	
	printf("basic_block_size: %d", basic_block_size);
	printf("\nmem_size: %d", mem_size);
	
	init_allocator(basic_block_size, mem_size);

	ackerman_main();

	release_allocator();
}
