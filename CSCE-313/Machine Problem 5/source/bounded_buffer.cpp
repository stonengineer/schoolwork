//
//  bounded_buffer.cpp
//  
//
//  Created by Joshua Higginbotham on 11/4/15.
//
//

#include "bounded_buffer.h"
#include <iostream>
#include <string>
#include <queue>

bounded_buffer::bounded_buffer() {
	//This is why C++ sucks.
}

bounded_buffer::bounded_buffer(int _capacity) {
	empty = new semaphore(_capacity);
	full = new semaphore(0);
}

bounded_buffer::~bounded_buffer() {
	//delete empty;
	//delete full;
	//delete &m;
}

void bounded_buffer::assign_function(bounded_buffer &cp) {
	buffer = cp.get_buffer();
	empty = cp.get_empty();
	full = cp.get_full();
}

void bounded_buffer::push_back(std::string req) {
	empty->P();
	m.lock();
	buffer.push(req);
	m.unlock();
	full->V();
}

std::string bounded_buffer::retrieve_front() {
	full->P();
	m.lock();
	std::string ret = buffer.front();
	buffer.pop();
	m.unlock();
	empty->V();
	return ret;
}

int bounded_buffer::size() {
	return buffer.size();
}