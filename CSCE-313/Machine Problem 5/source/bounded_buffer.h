//
//  bounded_buffer.hpp
//  
//
//  Created by Joshua Higginbotham on 11/4/15.
//
//

#ifndef bounded_buffer_h
#define bounded_buffer_h

#include <stdio.h>
#include <queue>
#include <string>
#include <pthread.h>
#include "semaphore.h"

class bounded_buffer {
private:
	/* Internal data here */
	std::queue<std::string> buffer;
	semaphore *full, *empty;
	std::mutex m;

public:
	bounded_buffer();
    bounded_buffer(int _capacity);
    ~bounded_buffer();
	void assign_function(bounded_buffer &cp);
    void push_back(std::string str);
    std::string retrieve_front();
    int size();
	std::queue<std::string> get_buffer() { return buffer; }
	semaphore * get_empty() { return empty; }
	semaphore * get_full() { return full; }
};

#endif /* bounded_buffer_h */
