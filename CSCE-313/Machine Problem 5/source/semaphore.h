/* 
	File: semaphore.H

	Author: R. Bettati
			Department of Computer Science
			Texas A&M University
	Date  : 08/02/11

*/

#ifndef _semaphore_H_                   // include file only once
#define _semaphore_H_

/*--------------------------------------------------------------------------*/
/* DEFINES */
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* INCLUDES */
/*--------------------------------------------------------------------------*/

#include <pthread.h>
#include <mutex>
#include <condition_variable>

/*--------------------------------------------------------------------------*/
/* DATA STRUCTURES */ 
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* FORWARDS */ 
/*--------------------------------------------------------------------------*/

/* -- (none) -- */

/*--------------------------------------------------------------------------*/
/* CLASS   s e m a p h o r e  */
/*--------------------------------------------------------------------------*/

class semaphore {
private:
	/* -- INTERNAL DATA STRUCTURES */
	std::mutex mutex;
	std::condition_variable condition;
	unsigned long count = 0;

public:

	/* -- CONSTRUCTOR/DESTRUCTOR */

	semaphore(int _val) {
		count = _val;
	}

	~semaphore(){
		count = -1;
		delete &mutex;
		delete &condition;
	}

	/* -- SEMAPHORE OPERATIONS */
	
	void P() {
		std::unique_lock<decltype(mutex)> lock(mutex);
		while(count == 0)
			condition.wait(lock);
		--count;
	}

	void V() {
		std::unique_lock<decltype(mutex)> lock(mutex);
		++count;
		condition.notify_one();
	}
};

#endif