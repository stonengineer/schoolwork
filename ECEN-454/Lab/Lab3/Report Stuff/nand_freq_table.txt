f(MHz) If(uA)  //C = If/(2*pi*f) (put column of C's after If's)
10.4M  185n
50M    893n
100M   1.79u
151M   2.69u
301M   5.38u
449M   8.02u
600M   10.7u
751M   13.4u
900M   16.1u
1G     17.8u
